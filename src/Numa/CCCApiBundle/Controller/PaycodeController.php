<?php

namespace Numa\CCCApiBundle\Controller;

use Numa\CCCAdminBundle\Entity\Cheque;
use Numa\CCCAdminBundle\Entity\Drivers;
use Numa\CCCAdminBundle\Entity\PayCode;
use Numa\CCCAdminBundle\Entity\PayPeriod;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PaycodeController extends Controller
{
    /**
     * @Rest\View
     */
    public function getAllAction()
    {
        $entities = $this->getDoctrine()->getRepository(PayCode::class)->findAll();
        if ($entities === null) {
            return new View("Paycode not found", Response::HTTP_NOT_FOUND);
        }
        return $entities;
    }


}
