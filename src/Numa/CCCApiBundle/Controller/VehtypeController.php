<?php

namespace Numa\CCCApiBundle\Controller;

use Numa\CCCAdminBundle\Entity\Rates;
use Numa\CCCAdminBundle\Entity\Vehtypes;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;

class VehtypeController extends Controller
{
    /**
     * @Rest\View
     */
    public function getVehtypeAction($id)
    {
        $vt = $this->getDoctrine()->getRepository(Vehtypes::class)->find($id);
        if ($vt === null) {
            return new View("Vehicle type not found", Response::HTTP_NOT_FOUND);
        }
        return $vt;
    }
    /**
     * @Rest\View
     */
    public function getVehtypesAction()
    {
        $vts = $this->getDoctrine()->getRepository(Vehtypes::class)->findAll();
        if ($vts === null) {
            return new View("Vehicles types not found", Response::HTTP_NOT_FOUND);
        }
        return $vts;
    }
}
