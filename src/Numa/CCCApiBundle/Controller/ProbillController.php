<?php

namespace Numa\CCCApiBundle\Controller;

use FOS\RestBundle\View\View;
use Numa\CCCAdminBundle\Entity\Batch;
use Numa\CCCAdminBundle\Entity\Probills;
use Numa\CCCAdminBundle\Entity\User;
use Numa\CCCAdminBundle\Entity\BillingPeriod;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ProbillController extends Controller
{
    /**
     * @Rest\View
     */
    public function getProbillAction($id)
    {
        $probill = $this->getDoctrine()->getRepository(Probills::class)->find($id);
        if ($probill === null) {
            return new View("Probill not found", Response::HTTP_NOT_FOUND);
        }

        return $probill;
    }

    public function getProbillsByBatchAction($id)
    {

        $probill = $this->getDoctrine()->getRepository(Probills::class)->findAllByBatchBP();
        if ($probill === null) {
            return new View("Probills not found", Response::HTTP_NOT_FOUND);
        }

        return $probill;
    }

    /**
     * @Rest\View
     */
    public function getProbillsByBillingPeriodAction($id)
    {

        $probill = $this->getDoctrine()->getRepository(Probills::class)->findByBillingPeriods($id);
        if ($probill === null) {
            return new View("Probill not found", Response::HTTP_NOT_FOUND);
        }

        return $probill;
    }

    public function updateProbillAction(Request $request)
    {
        $data = $request->request->all();
        $this->get("numa.probill")->updateProbillFromArray($data);

    }

    /**
     * @Rest\View(serializerGroups={"dataentry"})
     */
    public function getBatchProbillAction(Request $request, $id)
    {

        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        $finalized = $request->attributes->get('finalized');
        $correction = $request->attributes->get('correction');
        $billType = $request->attributes->get('billType');
        $em=$this->getDoctrine()->getManager();

        $sort = $request->query->get('sort');
        $order = $request->query->get('order');

        if($finalized){
            $billingPeriod = $em->getRepository(BillingPeriod::class)->find($id);
            $probills = $this->getDoctrine()->getRepository(Probills::class)->findFinalizedByBillingPeriod($billingPeriod,$sort,$order);
        }else{
            $batch = $em->getRepository(Batch::class)->find($id);

            $probills = $this->getDoctrine()->getRepository(Probills::class)->findByBatchBP($batch, $billType,$user,$finalized,$sort,$order,$correction);
        }

        if ($probills === null) {
            return new View("Probills not found", Response::HTTP_NOT_FOUND);
        }
        return $probills;
    }

    /**
     * @Rest\View
     */
    public function getAllProbillAction()
    {

        $probills = $this->getDoctrine()->getRepository(Probills::class)->findBy(array(),array("id"=>"desc"),100);
        if ($probills === null) {
            return new View("Probills not found", Response::HTTP_NOT_FOUND);
        }

        return $probills;
    }
}
