<?php

namespace Numa\CCCApiBundle\Controller;

use Numa\CCCAdminBundle\Entity\Cheque;
use Numa\CCCAdminBundle\Entity\Drivers;
use Numa\CCCAdminBundle\Entity\PayPeriod;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ChequeController extends Controller
{
    /**
     * @Rest\View
     */
    public function getChequePayPeriodAction(PayPeriod $pay_period)
    {
        $cheques = $this->getDoctrine()->getRepository(Cheque::class)->findAllInPayperiod($pay_period->getId());

        if ($cheques === null) {
            return new View("Billing period not found", Response::HTTP_NOT_FOUND);
        }
        return $cheques;
    }

    public function updateAction(Request $request)
    {
        $data = $request->request->all();

        $em = $this->getDoctrine()->getManager();

        $id = 0;
        if (!empty($data['id'])) {
            $id = intval($data['id']);
        }

        if ($id > 0) {
            $cheque = $this->get("numa.cheque")->updateChequeFromArray($data);
        }


        $serializer = $this->container->get('jms_serializer');
//        $jsonProbill = $serializer->serialize($probill, 'dump();die();json');

        $ret = array(
            "data" => $serializer->toArray($cheque),
            "id" => $id
        );

        return new JsonResponse($ret,201);
    }

    public function createAction(Request $request)
    {
        $data = $request->request->all();
        $cheque = $this->get('numa.cheque')->insertNewChequeFromArray($data);

        $serializer = $this->container->get('jms_serializer');
        return new JsonResponse($serializer->toArray($cheque));
    }

    public function deleteAjaxAction(Request $request)
    {
//        $data = $request->request->all();
//
//        $ret = array();
//        $em = $this->getDoctrine()->getManager();
//        $probill = $em->getRepository(Probills::class)->find(intval($data['id']));
//        if(!$probill instanceof Probills){
//            $ret['error'] = "Probill does not exsists";
//            return new JsonResponse($ret);
//        }
//        if(!$this->container->get("numa.probill")->isDeleteGranted($probill)){
//            $ret['error'] = "This user cannot delete the probill";
//        }
//        $serializer = $this->container->get('jms_serializer');
//        $ret['data'] = $serializer->toArray($probill);
//        //dump($ret);die();
//        $em->remove($probill);
//        $em->flush();
//
//
//        return new JsonResponse($ret);
    }


}
