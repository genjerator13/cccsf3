<?php

namespace Numa\CCCApiBundle\Controller;

use Numa\CCCAdminBundle\Entity\Customers;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
class CustomerController extends Controller
{
    /**
     * @Rest\View
     */
    public function getCustomerAction($id)
    {
        $customer = $this->getDoctrine()->getRepository(Customers::class)->find($id);
        if ($customer === null) {
            return new View("Customer not found", Response::HTTP_NOT_FOUND);
        }
        //dump($customer);die();
        return $customer;
    }

    public function getAllCustomerAction()
    {
        $customers = $this->getDoctrine()->getRepository(Customers::class)->findOnlyActive();
        if ($customers === null) {
            return new View("Customers not found", Response::HTTP_NOT_FOUND);
        }

        return $customers;
    }
}
