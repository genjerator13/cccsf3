<?php

namespace Numa\CCCApiBundle\Controller;

use Numa\CCCAdminBundle\Entity\Customers;
use Numa\CCCAdminBundle\Entity\Dispatchcard;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DispatchcardController extends Controller
{
    /**
     * @Rest\View
     */
    public function getDispatchcardAction($id)
    {
        $vt = $this->getDoctrine()->getRepository(Dispatchcard::class)->find($id);
        if ($vt === null) {
            return new View("Dispatchcard not found", Response::HTTP_NOT_FOUND);
        }
        return $vt;
    }

    public function getDispatchcardsAction()
    {
        $user = $this->container->get('security.token_storage')->getToken()->getUser();

        if($user instanceof Customers){
            $vts = $this->getDoctrine()->getRepository(Dispatchcard::class)->findBY(array("Customer"=>$user), array("id"=>"desc"), 200);
        }else {
            $vts = $this->getDoctrine()->getRepository(Dispatchcard::class)->findBY(array(), array("id"=>"desc"), 200);
        }
        if ($vts === null) {
            return new View("Dispatchcard not found", Response::HTTP_NOT_FOUND);
        }
        return $vts;
    }
    /**
     * @Rest\View(serializerGroups={"dataentry"})
     */
    public function getDispatchcardsdeAction()
    {
        $vts = $this->getDoctrine()->getRepository(Dispatchcard::class)->findLastn(3000);
        if ($vts === null) {
            return new View("Dispatchcard not found", Response::HTTP_NOT_FOUND);
        }
        return $vts;
    }
}
