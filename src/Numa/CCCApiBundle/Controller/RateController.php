<?php

namespace Numa\CCCApiBundle\Controller;

use Numa\CCCAdminBundle\Entity\Rates;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;

class RateController extends Controller
{
    /**
     * @Rest\View
     */
    public function getRateAction($id)
    {
        $rate = $this->getDoctrine()->getRepository(Rates::class)->find($id);
        if ($rate === null) {
            return new View("user not found", Response::HTTP_NOT_FOUND);
        }

        return $rate;
    }

    public function getRatesAction()
    {
        $rates = $this->getDoctrine()->getRepository(Rates::class)->findAll();
        if ($rates === null) {
            return new View("user not found", Response::HTTP_NOT_FOUND);
        }

        return $rates;
    }
}
