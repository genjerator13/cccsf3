<?php

namespace Numa\CCCApiBundle\Controller;

use Numa\CCCAdminBundle\Entity\Drivers;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;

class DriverController extends Controller
{
    public function indexAction()
    {
        return $this->render('NumaCCCApiBundle:Default:index.html.twig');
    }

    /**
     * @Rest\View
     */
    public function getDriverAction($id)
    {
        $driver = $this->getDoctrine()->getRepository(Drivers::class)->findOneBy(array('id'=>$id,'active'=>true));
        if ($driver === null) {
            return new View("Driver not found", Response::HTTP_NOT_FOUND);
        }
        return $driver;
    }
    /**
     * @Rest\View
     */
    public function getAllDriversAction()
    {
        $drivers = $this->getDoctrine()->getRepository(Drivers::class)->findBy(array(),array("drivernum"=>"ASC"));
        if ($drivers === null) {
            return new View("Drivers not found", Response::HTTP_NOT_FOUND);
        }
        return $drivers;
    }

    /**
     * @Rest\View
     */
    public function getAllDriversActiveAction()
    {
        $drivers = $this->getDoctrine()->getRepository(Drivers::class)->findBy(array("active"=>true),array("drivernum"=>"ASC"));
        if ($drivers === null) {
            return new View("Drivers not found", Response::HTTP_NOT_FOUND);
        }
        return $drivers;
    }
}
