function calculateTime(phr, pmin, dhr, dmin) {

    phr = parseInt(phr) || 0;
    pmin = parseInt(pmin) || 0;
    dhr = parseInt(dhr) || 0;
    dmin = parseInt(dmin) || 0;

    thr = 0;
    tmin = 0;

    if (phr > dhr) {
        if (pmin > dmin) {
            thr = (24 - phr + dhr - 1);
            tmin = (60 - pmin + dmin);
        }
        else if (pmin == dmin) {
            thr = (24 - phr + dhr);
            tmin = (dmin - pmin);
        }
        else {
            thr = (24 - phr + dhr);
            tmin = (dmin - pmin);
        }
    }
    else if (phr == dhr) {
        if (pmin > dmin) {
            thr = (24 - phr + dhr - 1);
            tmin = (60 - pmin + dmin);
        }
        else if (pmin == dmin) {
            thr = (dhr - phr);
            tmin = (dmin - pmin);
        }
        else {
            thr = (dhr - phr);
            tmin = (dmin - pmin);
        }
    }
    else {
        if (pmin > dmin) {
            thr = (dhr - phr - 1);
            tmin = (60 - pmin + dmin);
        }
        else if (pmin == dmin) {
            thr = (dhr - phr);
            tmin = (dmin - pmin);
        }
        else {
            thr = (dhr - phr);
            tmin = (dmin - pmin);
        }
    }

    time = [2];
    time['hr'] = thr;
    time['min'] = tmin;
    return time;
}

function calculatePreTax(basicRateAmt, extra, rushAmt, waitchrg, trailerchg, misc) {
    basicRateAmt = basicRateAmt || 0;
    extra = extra || 0;
    rushAmt = rushAmt || 0;
    waitchrg = waitchrg || 0;
    trailerchg = trailerchg || 0;
    misc = misc || 0;
    totalPreTax = parseFloat(basicRateAmt) + parseFloat(extra) + parseFloat(rushAmt) + parseFloat(waitchrg) + parseFloat(trailerchg) + parseFloat(misc);
    return totalPreTax.toFixed(2);
}

function calculateExtra(extraPiece, extraPceRate) {
    extraPiece = extraPiece || 0;
    extraPceRate = extraPceRate || 0;
    totalExtra = parseFloat(extraPiece) * parseFloat(extraPceRate);
    return totalExtra.toFixed(2);
    //calculatePreTax();
}

function calculateFuelRate(cussurchargerate, total) {
    //console.log(cussurchargerate);
    //console.log(total);
    cussurchargerate = cussurchargerate || 0;
    total = total || 0;
    totalcustsurchargeamt = parseFloat(cussurchargerate) * parseFloat(total);
    return totalcustsurchargeamt.toFixed(2);

}

function calculateSubTotal(total, custsurchargeamt) {
    total = total || 0;
    custsurchargeamt = custsurchargeamt || 0;
    subtotal = parseFloat(total) + parseFloat(custsurchargeamt);
    return subtotal.toFixed(2);
    //calculateGrandTotal()
}

function calculateDirect(basicRateAmt, extra) {
    basicRateAmt = basicRateAmt || 0;
    extra = extra || 0;
    direct = (parseFloat(basicRateAmt) + parseFloat(extra)) / 2;
    return direct.toFixed(2);
    //calculateSubTotal();
}

function calculateGrandTotal(subtotal, taxcode) {
    subtotal = subtotal || 0;
    taxcode = taxcode || "";

    v = 0;
    if (taxcode.length > 0) {
        if (taxcode.substr(0, 1).toLowerCase() == 'g') {
            v = 0.05;
        }
    }
    grandTotal = (parseFloat(subtotal) * v) + parseFloat(subtotal);

    ret = [2];
    ret['gstAmt'] = (parseFloat(subtotal) * v).toFixed(2);
    ret['grandtotal'] = grandTotal.toFixed(2);
    //console.log("calculateGrandTotal");
    //console.log(ret);
    return ret;
}


function calculateDriverTotals(custsurchargeamt,basicRateAmt,extra,rush_amt,waitchrg, subtotal, driverRate, drvSurchargeReturnRate) {
    custsurchargeamt = parseFloat(custsurchargeamt) || 0;
    basicRateAmt = basicRateAmt || 0;
    extra = extra || 0;
    rush_amt = rush_amt || 0;
    waitchrg = waitchrg || 0;
    subtotal = subtotal || 0;
    driverRate = driverRate || 0;
    drvSurchargeReturnRate = drvSurchargeReturnRate || 0;


    console.log("custsurchargeamt="+custsurchargeamt+" , basicRateAmt="+basicRateAmt+" , extra="+extra+" , rush_amt="+rush_amt+" , waitchrg="+waitchrg+" , subtotal="+subtotal+" , driverRate="+driverRate+" ,drvSurchargeReturnRate="+drvSurchargeReturnRate);





    direct = parseFloat(rush_amt);
    console.log("direct="+direct);
    waitload = parseFloat(waitchrg);
    console.log("waitload="+waitload);
    total = parseFloat(basicRateAmt)+parseFloat(extra)+parseFloat(direct)+parseFloat(waitload);
    console.log("total="+total);
    drvRate=parseFloat(driverRate);
    console.log("drvRate="+drvRate);
    fuelSurcharge = parseFloat(custsurchargeamt)
    console.log("fuelSurcharge="+fuelSurcharge);
    driverSurchargeAmount = parseFloat(parseFloat(total)*parseFloat(fuelSurcharge)*(parseFloat(drvSurchargeReturnRate)/100));
    console.log("driverSurchargeAmount="+driverSurchargeAmount);
    temp = parseFloat(parseFloat(total)*parseFloat(drvRate));
    console.log("temp="+temp);
    driverTotalFS = parseFloat(parseFloat(temp) + parseFloat(driverSurchargeAmount));
    console.log("driverTotalFS="+driverTotalFS);
    gross = parseFloat(parseFloat(subtotal) - parseFloat(driverTotalFS));
    console.log("gross="+gross);
    DrvRateAmount = (parseFloat(basicRateAmt) + parseFloat(extra) + parseFloat(direct) + parseFloat(waitload)) * parseFloat(drvRate);
    console.log("DrvRateAmount="+DrvRateAmount);


    //console.log("custsurchargeamt="+custsurchargeamt);
    //console.log("drvSurchargeReturnRate="+drvSurchargeReturnRate);
    //console.log("direct="+direct);
    //
    //
    //console.log("drvRate="+drvRate);
    //console.log("fuelSurcharge="+fuelSurcharge);
    //console.log("temp="+temp);
    //console.log("driverTotalFS="+driverTotalFS);





   //
   // ///fuelSurcharge = fuelSurcharge || 0;
   //
   // //console.log("custsurchargeamt"+custsurchargeamt, "subtotal"+subtotal, "basicRateAmt"+basicRateAmt,  "extra"+extra,  "waitchrg"+waitchrg,  "rush_amt"+rush_amt, "driverRate"+driverRate, "drvSurchargeReturnRate"+drvSurchargeReturnRate);
   // driverSurchargeAmount = (parseFloat(custsurchargeamt)* (parseFloat(drvSurchargeReturnRate) / 100));
   // driverRateAmount = (parseFloat(basicRateAmt)+ parseFloat(extra) + parseFloat(rush_amt)+ parseFloat(waitchrg))*parseFloat(driverRate);
   // driverTotalFS = driverRateAmount + driverSurchargeAmount;
   //// drvgross = total * driverRate;
   //
   //
   // gross = subtotal - driverTotalFS;
    ret = [4];
    ret['driversurcharge'] = driverSurchargeAmount.toFixed(2);
    ret['drvTotal'] = driverTotalFS.toFixed(2);
    ret['drvgross'] = DrvRateAmount.toFixed(2);
    ret['gross'] = gross.toFixed(2);
    console.log(ret);
    return ret;
}


