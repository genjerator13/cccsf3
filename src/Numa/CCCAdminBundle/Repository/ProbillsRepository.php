<?php

namespace Numa\CCCAdminBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Numa\CCCAdminBundle\Entity\BillingPeriod;
use Numa\CCCAdminBundle\Entity\Probills;
use Numa\CCCAdminBundle\Entity\User;

class ProbillsRepository extends EntityRepository
{
    /**
     * @param $customer
     * @param int $batchid
     * @param string $searchText
     * @return \Doctrine\ORM\Query
     * NOT IN USE
     */
//    public function findAllByCustomer($customer, $batchid = 0, $searchText = '')
//    {
//        $cid = $customer;
//        if ($customer instanceof \Numa\CCCAdminBundle\Entity\Customers) {
//            $cid = $customer->getId();
//        }
//        $qb = $this->createQueryBuilder('p')
//            ->where('p.customersId = :custcode')
//            ->setParameter('custcode', $cid)
//            ->orderBy('p.pDate', 'ASC');
//        if (!empty($batchid)) {
//            $qb->andWhere('p.batchX= :batchX');
//            $qb->setParameter('batchX', $batchid);
//        }
//        if (!empty($searchText)) {
//            $qb->andWhere('p.waybill like :waybill');
//            $qb->setParameter('waybill', "%" . $searchText . "%");
//        }
//        $query = $qb->getQuery();
//        return $query;
//    }

    public function findAllByCustomer($customer, $billingPeriodId = 0, $searchText = '')
    {
        $cid = $customer;
        if ($customer instanceof \Numa\CCCAdminBundle\Entity\Customers) {
            $cid = $customer->getId();
        }
        $qb = $this->createQueryBuilder('p')
            ->where('p.customersId = :custcode')
            ->setParameter('custcode', $cid)
            ->orderBy('p.pDate', 'ASC');
        if (!empty($batchid)) {
            $qb->leftJoin('p.batchPB','b');
            $qb->andWhere('b.billing_period_id= :bpid');
            $qb->setParameter('bpid', $billingPeriodId);
        }
        if (!empty($searchText)) {
            $qb->andWhere('p.waybill like :waybill');
            $qb->setParameter('waybill', "%" . $searchText . "%");
        }
        $query = $qb->getQuery();
        return $query;
    }

    public function findAllByCustomerInBillingPeriod($customer, $billingPeriod,$order="customer")
    {
        $cid = $customer;
        if ($customer instanceof \Numa\CCCAdminBundle\Entity\Customers) {
            $cid = $customer->getId();
        }
        $qb = $this->createQueryBuilder('p')
            ->where('p.customersId = :cust')
            ->setParameter('cust', $cid)
            ->orderBy('p.pDate', 'ASC')
            ->leftJoin('p.batchPB','b')
            ;
       if($order=="customer"){
           $qb->orderBy('p.pDate', 'ASC');
       }elseif($order=="invoice"){
           $qb->orderBy('p.invoice', 'ASC');
       }
        if ($billingPeriod instanceof BillingPeriod) {
            $qb->andWhere('b.billing_period_id=:billingPeriod');
            $qb->setParameter('billingPeriod', $billingPeriod->getId());
        }

        $query = $qb->getQuery();

        return $query->getResult();
    }

    public function anyProbillsForCustomerBillingPeriod($customer, $billingPeriod)
    {
        $res = $this->findAllByCustomerInBillingPeriod($customer,$billingPeriod);
        return count($res)>0;
    }

    /**
     * Returns all probill for a batch
     * @param int $batch
     * @return type Array of Probills
     * NOT IN USE
     */
    public function findAllByBatch($batchid, $searchText = '')
    {

        $qb = $this->createQueryBuilder('p')
            ->orderBy('p.pDate', 'ASC');
        if ($batchid > 0) {
            $qb->andWhere('p.batchX= :batchX');
            $qb->setParameter('batchX', $batchid);
        }
        if (!empty($searchText)) {
            $qb->andWhere('p.waybill like :waybill');
            $qb->setParameter('waybill', "%" . $searchText . "%");
        }
        return $qb->getQuery();
    }

    /**
     * Returns all probill for a batch
     * @param int $batch
     * @return type Array of Probills
     */
    public function findAllByBillingPeriod($billingPeriodId, $searchText = '',$customerid=null)
    {

        $qb = $this->createQueryBuilder('p')
            ->orderBy('p.pDate', 'ASC');
        if ($billingPeriodId > 0) {
            $qb->leftJoin('p.batchPB','b');
            $qb->andWhere('b.billing_period_id= :bpid');
            $qb->setParameter('bpid', $billingPeriodId);

        }
        if (!empty($searchText)) {
            $qb->andWhere('p.waybill like :waybill');
            $qb->setParameter('waybill', "%" . $searchText . "%");
        }
        if (!empty($customerid)) {
            $qb->andWhere('p.customersId = :cid');
            $qb->setParameter('cid', $customerid);
        }
        return $qb->getQuery();
    }

    /**
     * Returns all probill for a batchBP
     * @param int $batch
     * @return type Array of Probills
     */
    public function findAllByBatchPB(int $batch)
    {
        $qb = $this->getEntityManager()
            ->createQueryBuilder();
        $qb->select('p')
            ->from(Probills::class, 'p')
            ->andWhere('p.batch_id=:batch')
            ->setParameter('batch', $batch);

        $query = $qb->getQuery();
        return $query->getResult();

    }

    /**
     *
     * @param int $batchid
     * @return type array of customers found in requested batch
     */
    public function findAllCustomersInBatch($batchid)
    {
        $query = $this->getEntityManager()
            ->createQuery(
                'SELECT c FROM  NumaCCCAdminBundle:Customers c '
                . ' JOIN c.probills p'
                . ' WHERE p.batchX= :batchX'
                . ' GROUP BY c.custcode'
                . ' ORDER BY c.custcode'
            )->setParameter('batchX', $batchid);


        return $query->getResult();
    }

    /**
     * @param int $batchid
     * @param int $customer
     * @return array
     *
     * NOT USED
     */
    public function getProbils($batchid = 0, $customer = 0)
    {

        $qb = $this->createQueryBuilder('p')
            ->orderBy('p.Customer', 'ASC')
            ->orderBy('p.dept', 'ASC')
            ->orderBy('p.pDate', 'ASC');
        if (!empty($customer)) {
            $qb->where('p.customersId = :custcode')
                ->setParameter('custcode', $customer);
        }
        if ($batchid > 0) {
            $qb->andWhere('p.batchX= :batchX');
            $qb->setParameter('batchX', $batchid);
        }
        $query = $qb->getQuery();
        return $query->getResult();
    }

    public function findNonBilled($waybill,$billingPeriodId=null,$customer = null,$status=null)
    {

        $qb = $this->createQueryBuilder('p')
            //->andWhere("p.status like 'PENDING'")
            ->orderBy('p.Customer', 'ASC')
            ->orderBy('p.dept', 'ASC')
            ->orderBy('p.pDate', 'ASC');
        if (!empty($waybill)) {
            $qb->andWhere('p.waybill like :waybill');
            $qb->setParameter('waybill', "%" . $waybill . "%");
        }
        if (!empty($customer)) {
            $qb->andWhere('p.customersId = :custcode')
                ->setParameter('custcode', $customer);
        }
        $qb->leftJoin('p.batchPB','b');
        if (!empty($billingPeriodId)) {
            $qb->andWhere('b.billing_period_id= :bpid');
            $qb->setParameter('bpid', $billingPeriodId);
        }
        if(!empty($status)){

            $qb->andWhere('b.status= :status');
            $qb->setParameter('status', $status);
        }
        $query = $qb->getQuery();

        return $query;
    }

    public function getTotals(array $results, $discount = 0)
    {
        $total = 0;
        $custSurchargeAMT = 0;
        $subtotal = 0;
        $gstAMT = 0;
        $balance = 0;
        $invoice = "";
        $totalDiscount = 0;
        $balanceDiscount = 0;
        $subtotalDiscount = 0;


        foreach ($results as $key => $res) {
            $total = $total + $res->getTotal();
            $custSurchargeAMT = $custSurchargeAMT + $res->getCustSurchargeAMT();
            $subtotal = $subtotal + $res->getSubtotal();
            $gstAMT = $gstAMT + $res->getGstAMT();
            $invoice = $res->getInvoice();
        }

        $balance = $total + $custSurchargeAMT + $gstAMT;
        if ($discount > 0) {
            $totalDiscount = $total;
            $subtotalDiscount = $subtotal * $discount;
            $balanceDiscount = ($subtotal - $subtotalDiscount) + $gstAMT;
            $subtotalDiscount = $subtotal * $discount;
        }

        $res = array('total' => $total,
            'custSurchargeAMT' => $custSurchargeAMT,
            'subtotal' => $subtotal,
            'gstAMT' => $gstAMT,
            'balance' => $balance,
            'invoice' => $invoice,
            'totalDiscount' => $totalDiscount,
            'balanceDiscount' => $balanceDiscount,
            'subtotalDiscount' => $subtotalDiscount,
        );


        return $res;;
    }

    public function findByBillingPeriods($billingPeriodId,$order="",$limit=false)
    {
        $qb = $this->createQueryBuilder('p')
            ->join("p.batchPB", "b")
            ->join("b.BillingPeriod", 'bp')
            ->andWhere("bp.id=:billing_period_id")
            ->setParameter("billing_period_id", $billingPeriodId);
        if($order=="customer") {
            $qb->join("p.customers","c");
            $qb->orderBy("c.custcode");
        }

        $query = $qb->getQuery();
//        if(!empty($limit)){
//            $query->setMaxResults($limit);
//
//        }

        return $query->getResult();
    }

    public function getLastBillingPeriodId(){
        $sql = "SELECT id FROM BillingPeriod WHERE old_batch_id is null ORDER BY ID DESC LIMIT 1";

        $em = $this->getEntityManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        //dump($stmt->fetchAll()[0]['id']);die();
        return $stmt->fetchAll()[0]['id'];
    }

    public function getLastBatchFromBillingPeriod($billingPeriodId){
        $sql = "SELECT id FROM Batch WHERE billing_period_id=".$billingPeriodId." and status=2 ORDER BY ID DESC LIMIT 1";

        $em = $this->getEntityManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        //dump($stmt->fetchAll()[0]['id']);die();
        return $stmt->fetchAll()[0]['id'];
    }

    public function findFinalizedByBillingPeriod($billingPeriodId, $sort=null,$order="ASC"){
        $billingPeriodId = $this->getLastBillingPeriodId();
        $qb = $this->createQueryBuilder('p')
            ->leftJoin("p.batchPB", "b")
            ->leftJoin("b.BillingPeriod", "bp")
            ->leftJoin("p.customers", "c")
            ->leftJoin("p.drivers", "d")
            ->leftJoin("p.vehtypes", "vt")
            ->leftJoin("p.rates", "r")

            ->andWhere("bp.id=:bid")
            ->andWhere("b.status=2")
            ->setParameter("bid", $billingPeriodId)
        ;
        if($sort=='customer'){
            $qb->orderBy("c.custcode",$order);
        }
        if($sort=='driver'){
            $qb->orderBy("d.drivernum",$order);
        }
        if($sort=='date'){
            $qb->orderBy("p.pDate",$order);
        }
        if($sort=='shipper'){
            $qb->orderBy("p.shipper",$order);
        }
        if($sort=='receiver'){
            $qb->orderBy("p.receiver",$order);
        }
        if($sort=='status'){
            $qb->orderBy("p.status",$order);
        }
        if($sort=='waybill'){
            $qb->orderBy("p.waybill",$order);
        }
        if($sort=='billtype'){
            $qb->orderBy("p.billtype",$order);
        }

        if($sort=='vehicle'){
            $qb->orderBy("vt.vehcode",$order);
        }

        $query = $qb->getQuery();
//dump($query->getSQL());
        return $query->getResult();
    }
    public function findByBatchBP($batchId, $billType = "ALL",$user=null,$finalized=null,$sort=null,$order="ASC",$correction=false)
    {

        $qb = $this->createQueryBuilder('p')
            ->leftJoin("p.batchPB", "b")
            ->leftJoin("p.customers", "c")
            ->leftJoin("p.drivers", "d")
            ->leftJoin("p.vehtypes", "vt")
            ->leftJoin("p.rates", "r")

            ->andWhere("b.id=:batch_id")
            ->setParameter("batch_id", $batchId);
        if($user instanceof User && $user->isCsrUser() && !$correction){
            $qb->join("p.User", "u");
            $qb->andWhere("u.id=:user_id");
            $qb->setParameter("user_id", $user->getId());
        }
        if($sort=='customer'){
            $qb->orderBy("c.custcode",$order);
        }
        if($sort=='driver'){
            $qb->orderBy("d.drivernum",$order);
        }
        if($sort=='date'){
            $qb->orderBy("p.pDate",$order);
        }
        if($sort=='shipper'){
            $qb->orderBy("p.shipper",$order);
        }
        if($sort=='receiver'){
            $qb->orderBy("p.receiver",$order);
        }
        if($sort=='status'){
            $qb->orderBy("p.status",$order);
        }
        if($sort=='waybill'){
            $qb->orderBy("p.waybill",$order);
        }
        if($sort=='billtype'){
            $qb->orderBy("p.billtype",$order);
        }
        if($sort=='ratelevel'){
            $qb->orderBy("p.ratelevel",$order);
        }
        if($sort=='vehicle'){
            $qb->orderBy("vt.vehcode",$order);
        }
//        if(isset($finalized)){
//            if($finalized) {
//                $qb->andWhere("p.status='FINAL'");
//            }else{
//                $qb->andWhere("p.status='PENDING'");
//            }
//        }
        if ($billType == "CTY" || $billType == "HWY") {
            $qb->andWhere("p.billtype=:billtype")
                ->setParameter("billtype", $billType);
        }
        $query = $qb->getQuery();

        return $query->getResult();
    }

    public function findByBatchBPRaw($batchId, $billType = "ALL",$user=null)
    {
        $fields = "id, customer_id as customer_pid, rates_id as rates_pid,driver";
        $fields = "*";
        $sql = "SELECT ".$fields." FROM Probills p WHERE 1=1 ";
        if($user instanceof User && $user->isCsrUser()){
            $sql.=" AND user_id=".$user->getId();
        }
        if ($billType == "CTY" || $billType == "HWY") {
            $sql.=" AND BillType='".$billType."'";
        }

        $em = $this->getEntityManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }






    public function trailerData($billingPeriod, $entity = "trailer")
    {
//        $queryBuilder->andWhere('r.winner IN (:ids)')
//            ->setParameter('ids', $ids);

        $qb = $this->createQueryBuilder('p')
            ->select("p")
            ->join('p.batchPB', 'b')
            ->join('b.BillingPeriod', 'bp')
            ->andWhere("bp.id IN (:ids)");
        if ($entity == "trailer") {
            $qb->andWhere("p.trailerchg>0 and p.trailerchg is not null");
        } else {
            $qb->andWhere("p.miscAmt>0 and p.miscAmt is not null");
        }

        $qb->groupBy("p")
            ->setParameter("ids", $billingPeriod);

        $query = $qb->getQuery();

        return $query->getResult();
    }

    public function findNullBatch()
    {
        $qb = $this->createQueryBuilder('p')
            ->select("p")
            ->andWhere("p.batch_id IS NULL");
        $query = $qb->getQuery();

        return $query->getResult();
    }

    public function updateDates($ids,$date){
        $datet = strtotime($date);
        $newformat = date('Y-m-d H:i:s',$datet);

        if (!empty($ids)) {
            $qb = $this->getEntityManager()
                ->createQueryBuilder()
                ->update(Probills::class, 'p')
                ->set('p.pDate',':date')
                ->where('p.id IN (:ids)')
                ->setParameter('ids', array_values($ids))
                ->setParameter('date',$newformat);
            $qb->getQuery()->execute();
        }
    }
}
