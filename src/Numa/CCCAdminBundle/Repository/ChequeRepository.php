<?php

namespace Numa\CCCAdminBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Numa\CCCAdminBundle\Entity\Rates;
use Numa\CCCAdminBundle\Entity\Vehtypes;

class ChequeRepository extends EntityRepository
{


    public function findAllInPayperiod($payperiod_id)
    {
        $qb = $this->createQueryBuilder('c')
            ->leftJoin("c.Driver","d")
            ->orderBy('d.drivernum', 'ASC')
            ->andWhere('c.pay_period_id=:payperiod_id')
            ->setParameter("payperiod_id",$payperiod_id);

        return $qb->getQuery()->getResult();
    }

    public function findByDriver($driver_id)
    {
        $qb = $this->createQueryBuilder('c')
            ->leftJoin("c.Driver","d")
            ->orderBy('d.drivernum', 'ASC')
            ->andWhere('c.pay_period_id=:driver_id')
            ->setParameter("driver_id",$driver_id);

        return $qb->getQuery()->getResult();
    }
}
