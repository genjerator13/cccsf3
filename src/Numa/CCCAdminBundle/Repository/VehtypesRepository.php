<?php

namespace Numa\CCCAdminBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Numa\CCCAdminBundle\Entity\Rates;
use Numa\CCCAdminBundle\Entity\Vehtypes;

class VehtypesRepository extends EntityRepository
{
    public function activate($ids, $active = true)
    {
        if (!empty($ids)) {
            $qb = $this->getEntityManager()
                ->createQueryBuilder()
                ->update(Vehtypes::class, 'vt')
                ->set('vt.active', $active ? 1 : 0)
                ->where('vt.id IN (:ids)')
                ->setParameter('ids', array_values($ids));
            $qb->getQuery()->execute();
        }
    }

    public function findAllActive()
    {
//        $qb = $this->getEntityManager()
//            ->createQueryBuilder('v')
//            ->andWhere('v.active is NULL or v.active=1')
//            ->orderBy('v.id');
//        $qb->getQuery()->getResult();
        $qb = $this->createQueryBuilder('v')
            ->orderBy('v.vehcode', 'ASC')
            ->andWhere('v.active is NULL or v.active=1');

        return $qb->getQuery()->getResult();
    }

}
