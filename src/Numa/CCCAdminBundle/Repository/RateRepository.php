<?php

namespace Numa\CCCAdminBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Numa\CCCAdminBundle\Entity\Rates;

class RateRepository extends EntityRepository
{
    public function activate($ids, $active = true)
    {
        if (!empty($ids)) {
            $qb = $this->getEntityManager()
                ->createQueryBuilder()
                ->update(Rates::class, 'r')
                ->set('r.active', $active ? 1 : 0)
                ->where('r.id IN (:ids)')
                ->setParameter('ids', $ids);
            $qb->getQuery()->execute();
        }
        //dump($qb->getQuery()->getSQL());
    }


    public function findAllActive()
    {

        $qb = $this->createQueryBuilder('r')
            ->orderBy('r.rateDescription', 'ASC')
            ->andWhere('r.active is NULL or r.active=1');

        return $qb->getQuery()->getResult();
    }

}
