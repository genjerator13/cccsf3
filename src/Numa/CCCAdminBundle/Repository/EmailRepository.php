<?php

namespace Numa\CCCAdminBundle\Repository;

use Doctrine\ORM\EntityRepository;

class EmailRepository extends EntityRepository {

    public function deleteByBatch($batch_id) {

        return $this->getEntityManager()
                        ->createQuery(
                                'DELETE FROM NumaCCCAdminBundle:Email e WHERE e.batch_id= :batchid'
                        )->setParameter('batchid', $batch_id)
                        ->execute();
    }
    
    public function  getNotSendEmails($limit=10,$attachment=false,$batchid=0) {
        $qb = $this->getEntityManager()->createQueryBuilder('e');
        $qb->add('select', 'e')
                ->add('from', 'NumaCCCAdminBundle:Email e')                
                ->where('e.ended_at is null');
        if(!$attachment){
            $qb->andWhere('e.attachment is null OR e.attachment = \'\'');
        }else{
            $qb->andWhere('e.attachment is not null AND NOT e.attachment = \'\'');
        }
        if(!empty($batchid)){
            $qb->andWhere('e.batch_id='.$batchid);
        }
        $query = $qb->getQuery();
        $res = $query->getResult();
        return $res;
    }
    
    public function countNotSendEmails($attachment=false,$billing_period_id=0) {
        $qb = $this->getEntityManager()->createQueryBuilder('e');
        $qb->add('select', 'count(e)')
                ->add('from', 'NumaCCCAdminBundle:Email e')                
                ->where('e.ended_at is null');
        if(!empty($billing_period_id)) {
            $qb->andWhere('e.billing_period_id='.$billing_period_id);
        }
        if(!$attachment){
            $qb->andWhere('e.attachment is null OR e.attachment = \'\'');
        }else{
            $qb->andWhere('e.attachment is NOT null AND length(e.attachment) >0 ');
        }
        $query = $qb->getQuery();

        $res = $query->getSingleScalarResult();

        return $res;
    }
    
    public function findEmailsByBillingPeriod($billingPeriodId) {
        $qb = $this
                ->createQueryBuilder("e");
        $qb
                ->join('e.CustomerReportEmail','cre')
                ->join('cre.CustomerReport','cr')
                ->andWhere('cr.billing_period_id=:bpid')
                ->andWhere('e.status=0')
                ->setParameter('bpid', $billingPeriodId)
        ;

        $query = $qb->getQuery();
        return $query->getResult();
    }

}
