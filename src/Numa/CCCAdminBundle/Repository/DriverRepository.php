<?php

namespace Numa\CCCAdminBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Numa\CCCAdminBundle\Entity\Batch;
use Numa\CCCAdminBundle\Entity\BillingPeriod;
use Numa\CCCAdminBundle\Entity\Drivers;
use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class DriverRepository extends EntityRepository  implements UserProviderInterface, UserLoaderInterface
{
    public function loadUserByUsername($username)
    {

        $driver = $this->findOneByUsernameOrEmail($username);

        if (!$driver) {
            throw new UsernameNotFoundException('No driver found for username ' . $username);
        }

        return $driver;
    }

    public function findOneByUsernameOrEmail($username)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.username = :username')
            ->andWhere('d.active IS NULL OR d.active=1')
            ->setParameter('username', $username)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function refreshUser(UserInterface $user)
    {
        $class = get_class($user);
        if (!$this->supportsClass($class)) {
            throw new UnsupportedUserException(sprintf(
                'Instances of "%s" are not supported.',
                $class
            ));
        }

        if (!$refreshedUser = $this->find($user->getId())) {
            throw new UsernameNotFoundException(sprintf('User with id %s not found', json_encode($user->getId())));
        }

        return $refreshedUser;
    }

    public function supportsClass($class)
    {
        return $this->getEntityName() === $class
        || is_subclass_of($class, $this->getEntityName());
    }

    public function driverDetailsData($billingPeriod){
        if($billingPeriod instanceof BillingPeriod) {
            $qb = $this->getEntityManager()
                ->createQueryBuilder();
            $qb->select('b.id,
                     p.pDate,
                     d.drivernam,
                     d.drivernum,
                     p.id as probill_id,
                     p.total,
                     p.waybill,
                     p.subitem,
                     p.drvRate as drivrate,
                     p.drvTotal,
                     d.drvsurrate,
                     p.driversurcharge,
                     p.custsurchargeamt,
                     p.drvgross,
                     p.gross,
                     p.rateAmt,
                     p.extraAmt,
                     p.rushAmt,
                     p.waitchrg,
                     b.id as batch,
                     p.servType,
                     v.vehcode,
                     p.user_id,
                     c.custcode,
                     p.pce,
                     p.wgt,
                     p.shipper,
                     p.receiver,
                     p.details,
                     u.initials
                     ')
                ->from('NumaCCCAdminBundle:Probills', 'p')
                ->where('bp.id=:bp_id')
                ->leftJoin('p.drivers', 'd')
                ->leftJoin('p.batchPB', 'b')
                ->leftJoin('p.vehtypes', 'v')
                ->leftJoin('p.customers', 'c')
                ->leftJoin('b.BillingPeriod', 'bp')
                ->leftJoin('p.User', 'u')
                ->groupBy("d.drivernum,b.batch_date ,p.id")
                ->orderBy("d.drivernum")
                ->addOrderBy("p.pDate")
                ->addOrderBy("c.custcode")
                ->setParameter("bp_id", $billingPeriod->getId());
        }elseif($billingPeriod instanceof Batch){
            $qb = $this->getEntityManager()
                ->createQueryBuilder();
            $qb->select('b.id,
                     p.pDate,
                     d.drivernam,
                     d.drivernum,
                     d.drvsurrate,
                     p.id as probill_id,
                     p.total,
                     p.waybill,
                     p.subitem,
                     p.drvRate as drivrate,
                     p.drvTotal,
                     p.driversurcharge,
                     p.custsurchargeamt,
                     p.drvgross,
                     p.gross,
                     p.rateAmt,
                     p.extraAmt,
                     p.rushAmt,
                     p.waitchrg,
                     b.id as batch,
                     p.servType,
                     v.vehcode,
                     p.user_id,
                     c.custcode,
                     p.pce,
                     p.wgt,
                     p.shipper,
                     p.receiver,
                     p.details,
                     u.initials
                     ')
                ->from('NumaCCCAdminBundle:Probills', 'p')
                ->where('p.batch_id=:bp_id')
                ->leftJoin('p.drivers', 'd')
                ->leftJoin('p.batchPB', 'b')
                ->leftJoin('p.vehtypes', 'v')
                ->leftJoin('p.customers', 'c')
                ->leftJoin('p.User', 'u')
                ->groupBy("d.drivernum,b.batch_date ,p.id")
                ->orderBy("d.drivernum")
                ->addOrderBy("p.pDate")
                ->addOrderBy("c.custcode")
                ->setParameter("bp_id", $billingPeriod->getId());
        }
        $query = $qb->getQuery();

        $res = $query->getResult();
        $data[]=array();;

        foreach($res as $row){
            $batchDate = $row['pDate'];

            if($batchDate instanceof \DateTime){
                $batchDate = $batchDate->format("m/d/y");
            }
            $drvname = $row['drivernam']. "(".$row['drivernum'].")";
            $data[ $drvname ][$batchDate][$row['probill_id']]=$row;
            if(empty($data[ $drvname ][$batchDate]['total']['total'])){
                $data[ $drvname ][$batchDate]['total']['total']=0;
            }
            $total = $data[ $drvname ][$batchDate]['total']['total'];
            if(empty($data[ $drvname ][$batchDate]['total']['driver_total'])){
                $data[ $drvname ][$batchDate]['total']['driver_total']=0;
            }

            if(empty($data[ $drvname ][$batchDate]['total']['custsurchargeamt'])){
                $data[ $drvname ][$batchDate]['total']['custsurchargeamt']=0;
            }
            $custsurchargeamt = $data[ $drvname ][$batchDate]['total']['custsurchargeamt'];

            $data[ $drvname ][$batchDate]['total']['total']=$row['total']+$data[ $drvname ][$batchDate]['total']['total'];
            $data[ $drvname ][$batchDate]['total']['custsurchargeamt']=$row['custsurchargeamt']+$data[ $drvname ][$batchDate]['total']['custsurchargeamt'];
            $data[ $drvname ][$batchDate]['total']['driver_total']=$data[ $drvname ][$batchDate]['total']['total']*$row['drivrate'];

        }

        return  $data;
    }

    public function activate($ids, $active = true)
    {
        if (!empty($ids)) {
            $qb = $this->getEntityManager()
                ->createQueryBuilder()
                ->update(Drivers::class, 'vt')
                ->set('vt.active', $active ? 1 : 0)
                ->where('vt.id IN (:ids)')
                ->setParameter('ids', array_values($ids));
            $qb->getQuery()->execute();
        }
    }

    public function getLumpSumAmount($driverId,$billingPeriodId){
        $qb = $this->getEntityManager()
            ->createQueryBuilder();
        $qb->select('sum(p.drvTotal) as drv_total')
            ->from('NumaCCCAdminBundle:Probills', 'p')
            ->where('bp.id=:bp_id')
            ->andWhere('d.id=:driver_id')
            ->leftJoin('p.drivers', 'd')
            ->leftJoin('p.batchPB', 'b')
            ->leftJoin('b.BillingPeriod', 'bp')
            ->setParameter("driver_id", $driverId)
            ->setParameter("bp_id", $billingPeriodId)
        ;
        $query = $qb->getQuery();

        $res = $query->getResult();
        return $res[0]['drv_total'];
    }

}
