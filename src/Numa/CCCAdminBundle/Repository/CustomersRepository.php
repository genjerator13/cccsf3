<?php

namespace Numa\CCCAdminBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Numa\CCCAdminBundle\Entity\BillingPeriod;
use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class CustomersRepository extends EntityRepository implements UserProviderInterface, UserLoaderInterface
{

    public function findOneByCustCode($custcode)
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT c FROM CCCAdminBundle:Customers c WHERE c.cust_code= :custcode'
            )->setParameter('custcode', $custcode)
            ->getOneOrNullResult();
    }

    public function findSendYesEmailCustomers()
    {
        $qb = $this->getEntityManager()
            ->createQueryBuilder();
        $qb->select('c,ce')
            ->from('NumaCCCAdminBundle:Customers', 'c')
            ->innerJoin('c.CustomerEmails', 'ce')
            ->andWhere('c.sendmail not like :sendmail')
            ->setParameter('sendmail', "'%y%'");

        $query = $qb->getQuery();

        return $query->getResult();
    }

    public function findCustomersByIds($ids)
    {
        $qb = $this->getEntityManager()
            ->createQueryBuilder();
        $qb->select('c')
            ->from('NumaCCCAdminBundle:Customers', 'c')
            ->andWhere('c.id IN (:ids)')
            ->setParameter('ids', $ids);;

        $query = $qb->getQuery();

        return $query->getArrayResult();
    }

    public function getAllCustomerNamesArray($field = "name", $onlyActive = false)
    {
        $qb = $this->getEntityManager()
            ->createQueryBuilder();
        $qb->select('c.id,c.custcode,c.name,c.custsurchargerate,c.ratelevel')
            ->from('NumaCCCAdminBundle:Customers', 'c');
        if ($onlyActive) {
            $qb->andWhere("c.activate<>0 or c.activate is null");
        }
        $query = $qb->getQuery();

        $res = $query->getArrayResult();

        $res = array_map(function ($a) use ($field) {
            if ($field == "name") {
                return "{id:" . $a['id'] . ", label:\"" . htmlspecialchars($a[$field]) . " (" . $a['custcode'] . ")" . "\",custcode:\"" . $a['custcode'] . "\",custsurchargerate:\"" . $a['custsurchargerate'] . "\",ratelevel:\"" . $a['ratelevel'] . "\"}";
            }
            return "{id:" . $a['id'] . ", label:\"" . htmlspecialchars($a[$field]) . "\",custcode:\"" . $a['custcode'] . "\",name:\"" . htmlspecialchars($a['name']) . " (" . $a['custcode'] . ")" . "\"}";
        }, $res);
        $res = implode(",", $res);
        return $res;
    }

    public function findSendYesEmailCustomersInProbills()
    {
        $qb = $this->getEntityManager()
            ->createQueryBuilder();
        $qb->select('c,ce')
            ->from('NumaCCCAdminBundle:Customers', 'c')
            ->join('c.probills', 'p')
            ->leftJoin('c.CustomerEmails', 'ce')
            ->andWhere('c.sendmail not like :sendmail')
            ->setParameter('sendmail', "'%n%'");

        $query = $qb->getQuery();
        return $query->getResult();
    }

    public function loadUserByUsername($username)
    {

        $user = $this->findOneByUsernameOrEmail($username);

        if (!$user) {
            throw new UsernameNotFoundException('No user found for username ' . $username);
        }

        return $user;
    }

    public function findOneByUsernameOrEmail($username)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.username = :username')
            ->andWhere('c.activate IS NULL OR c.activate=1')
            ->setParameter('username', $username)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function getAllCustomersAsArray()
    {
        $qb = $this->createQueryBuilder('c')
            ->andWhere('c.activate IS NULL OR c.activate=1');
        $q = $qb->getQuery();
        $res = $q->getResult();

        return $res;
    }

    public function refreshUser(UserInterface $user)
    {
        $class = get_class($user);
        if (!$this->supportsClass($class)) {
            throw new UnsupportedUserException(sprintf(
                'Instances of "%s" are not supported.',
                $class
            ));
        }

        if (!$refreshedUser = $this->find($user->getId())) {
            throw new UsernameNotFoundException(sprintf('User with id %s not found', json_encode($user->getId())));
        }

        return $refreshedUser;
    }

    public function supportsClass($class)
    {
        return $this->getEntityName() === $class
        || is_subclass_of($class, $this->getEntityName());
    }

    public function customerSummaryData($billingPeriod, $entity = "customer")
    {
        $qb = $this->getEntityManager()
            ->createQueryBuilder();
        $qb->select('c as ' . $entity . ',SUM(p.total) as total,
                       SUM(p.custsurchargeamt) as custsurchargeamt,
                       SUM(p.driversurcharge) as driversurcharge,
                       SUM(p.drvTotal) as drvTotal,
                       SUM(p.subtotal) as subtotal,
                       SUM(p.grandtotal) as grandtotal,
                       COUNT(p.id) as count_probills,
                       SUM(p.pce) as pce,
                       SUM(p.wgt) as wgt,
                       SUM(p.gstAmt) as gstAmt,
                       SUM(p.rateAmt) as rateAmt,
                       SUM(p.extraAmt) as extraAmt,
                       SUM(p.miscAmt) as miscAmt,
                       SUM(p.rushAmt) as rushAmt,
                       SUM(p.waitime) as waitime,
                       SUM(p.waitchrg) as waitchrg,
                       SUM(p.trailerchg) as trailerchg,
                       SUM(p.miscAmt) as miscamt,
                       SUM(case when p.billtype=\'CTY\'  then p.total else 0 end) as ctytotal,
                       SUM(case when p.billtype=\'HWY\'  then p.total else 0 end) as hwytotal,
                       SUM(case when p.billtype=\'CTY\'  then p.subtotal else 0 end) as ctysubtotal,
                       SUM(case when p.billtype=\'HWY\'  then p.subtotal else 0 end) as hwysubtotal

                   ');
        if ($entity == 'customer') {

            $qb->from('NumaCCCAdminBundle:Customers', 'c');
            $qb->orderBy("c.custcode");
        } elseif ($entity == 'drivers') {
            $qb->from('NumaCCCAdminBundle:Drivers', 'c');
            //$qb->orderBy("c.drivernum");
            $qb->orderBy("LENGTH(TRIM(LEADING '0' FROM c.drivernum)), TRIM(LEADING '0' FROM c.drivernum)");
        } elseif ($entity == 'vehicles') {
            $qb->from('NumaCCCAdminBundle:Vehtypes', 'c');
            $qb->orderBy("c.vehdesc");
        }


        $qb->where('bp.id=:bp_id')
            ->leftJoin('c.probills', 'p')
            ->leftJoin('p.batchPB', 'b')
            ->leftJoin('b.BillingPeriod', 'bp')
            ->groupBy("c")
            ->setParameter("bp_id", $billingPeriod->getId());

        $query = $qb->getQuery();
        $total = array();
        $total['total'] = 0;
        $total['custsurchargeamt'] = 0;
        $total['driversurcharge'] = 0;
        $total['drvTotal'] = 0;
        $total['subtotal'] = 0;
        $total['grandtotal'] = 0;
        $total['count_probills'] = 0;
        $total['pce'] = 0;
        $total['wgt'] = 0;
        $total['gstAmt'] = 0;
        $total['rateAmt'] = 0;
        $total['extraAmt'] = 0;
        $total['rushAmt'] = 0;
        $total['waitime'] = 0;
        $total['waitchrg'] = 0;
        $total['trailerchg'] = 0;
        $total['miscAmt'] = 0;
        $total['ctytotal'] = 0;
        $total['hwytotal'] = 0;
        $total['ctysubtotal'] = 0;
        $total['hwysubtotal'] = 0;
        foreach ($query->getResult() as $customer) {
            $total['total'] += $customer['total'];
            $total['custsurchargeamt'] += $customer['custsurchargeamt'];
            $total['driversurcharge'] += $customer['driversurcharge'];
            $total['drvTotal'] += $customer['drvTotal'];
            $total['subtotal'] += $customer['subtotal'];
            $total['grandtotal'] += $customer['grandtotal'];
            $total['count_probills'] += $customer['count_probills'];
            $total['pce'] += $customer['pce'];
            $total['wgt'] += $customer['wgt'];
            $total['gstAmt'] += $customer['gstAmt'];
            $total['rateAmt'] += $customer['rateAmt'];
            $total['extraAmt'] += $customer['extraAmt'];
            $total['miscAmt'] += $customer['miscAmt'];
            $total['rushAmt'] += $customer['rushAmt'];
            $total['waitime'] += $customer['waitime'];
            $total['waitchrg'] += $customer['waitchrg'];
            $total['trailerchg'] += $customer['trailerchg'];

            $total['ctytotal'] += $customer['ctytotal'];
            $total['ctysubtotal'] += $customer['ctysubtotal'];
            $total['hwytotal'] += $customer['hwytotal'];
            $total['hwysubtotal'] += $customer['hwysubtotal'];
            $total['count_probills'] += $customer['count_probills'];
        }

        $res['total'] = $total;
        $res['data'] = $query->getResult();
//        dump($query->getResult());
//        dump($billingPeriod);

// dump($res);die();
        return $res;
    }

    public function findOnlyActive($billinPeriod = null)
    {
        $qb = $this->createQueryBuilder('c')
            ->andWhere('c.activate IS NULL OR c.activate=1')
            ->andWhere('c.custcode IS NOT NULL')
            ->orderBy("c.custcode");
        if ($billinPeriod instanceof BillingPeriod) {
            $qb->join("c.probills", 'p');
            $qb->join("p.batchPB", 'b');
            $qb->andWhere('b.billing_period_id=:bpid');
            $qb->setParameter('bpid', $billinPeriod);
        }
        return $qb->getQuery()->getResult();

    }

    public function getCustomersEmails()
    {
        $sql = "SELECT c.custcode, c.id,ce.* FROM Customers c JOIN customer_emails ce ON ce.customer_id=c.id ORDER BY c.id ASC";
        $em = $this->getEntityManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();

    }

    public function deleteAllCustomersEmails()
    {
        $sql = "DELETE FROM customer_emails";
        $em = $this->getEntityManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();

    }

}
