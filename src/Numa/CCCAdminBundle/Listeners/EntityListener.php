<?php

namespace Numa\CCCAdminBundle\Listeners;

use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreFlushEventArgs;
use Numa\CCCAdminBundle\Entity\Batch;
use Numa\CCCAdminBundle\Entity\BillingPeriod;
use Numa\CCCAdminBundle\Entity\Cheque;
use Numa\CCCAdminBundle\Entity\CustomRate;
use Numa\CCCAdminBundle\Entity\Dispatchcard;
use Numa\CCCAdminBundle\Entity\Probills;
use Numa\CCCAdminBundle\Entity\Trip;
use Numa\CCCAdminBundle\Entity\User;
use Proxies\__CG__\Numa\CCCAdminBundle\Entity\Dispatch;

class EntityListener
{

    protected $container;

    public function __construct($container = null)
    {
        $this->container = $container;
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof Dispatchcard) {
            //get user
            //if CSR user, add user name to dispatch card name
            $user = $this->container->get('security.token_storage')->getToken()->getUser();
            if ($user instanceof User) {
                $entity->setCsrName($user->getInitials());
            }
        }elseif ($entity instanceof Trip) {
            //get user
            //if CSR user, add user name to dispatch card name
            $user = $this->container->get('security.token_storage')->getToken()->getUser();
            if ($user instanceof User) {
                $entity->setUser($user);
            }
        }elseif($entity instanceof Batch) {
            $name = $this->container->get("numa.batch")->generateNameFromDate($entity->getBatchDate());
            //$entity->setName($name);
            if(strtoupper($entity->getStatus())!="FINAL" || strtoupper($entity->getStatus())!="PENDING"){
                $entity->setStatus("PENDING");
            }
        }elseif($entity instanceof BillingPeriod) {
            $name = $this->container->get("numa.batch")->generateNameFromDate($entity->getClosed());
            //$entity->setName($name);
            $scanFolder = $this->container->getParameter('scans_path').$name;

            if(!file($scanFolder) && !empty($name)) {
                mkdir($scanFolder, 0777, true);
            }
        }
        if ($entity instanceof Probills){
            $servType = $entity->getServType();
            if(!empty($servType[0]) ){
                if(strtoupper($servType[0])=="D"){
                    $entity->setServType("DIR");
                }else{
                    $entity->setServType("REG");
                }
            }
        }
        if ($entity instanceof Probills || $entity instanceof Batch || $entity instanceof BillingPeriod || $entity instanceof Cheque) {
            //get user
            //if CSR user, add user name to dispatch card name
            $token = $this->container->get('security.token_storage')->getToken();

            if (!empty($token) && $token->getUser() instanceof User) {
                $entity->setUser($token->getUser());
            }

        }
    }

    public function preUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();
        if ($entity instanceof Probills) {
            $this->container->get("numa.probill")->calculateDriver($entity);
            $servType = $entity->getServType();

            if(!empty($servType[0]) ){
                if(strtoupper($servType[0])=="D"){
                    $entity->setServType("DIR");
                }else{
                    $entity->setServType("REG");
                }
            }
        }
    }
    public function postPersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        if ($entity instanceof CustomRate) {
            $this->container->get("numa.customer")->createCustomRatesValues($entity);
        }
        if ($entity instanceof Probills) {
            $this->container->get("numa.probill")->calculateDriver($entity);
        }
        if ($entity instanceof Dispatchcard) {
            $this->container->get("numa.trip")->createTripFromDispatchCard($entity->getId());
        }
    }


    public function postUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();
        if ($entity instanceof CustomRate) {
            $this->container->get("numa.customer")->createCustomRatesValues($entity);
        }
        if ($entity instanceof Probills) {
            $this->container->get("numa.probill")->calculateDriver($entity);
        }

    }

}
