<?php

namespace Numa\CCCAdminBundle\Form;



use Numa\CCCAdminBundle\Form\Type\UploadMedia;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BillingPeriodType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('started',DateType::class)
            ->add('closed',DateType::class)
            ->add('_newsletter',FileType::class,array('mapped' => false,'required' => false)
            )
//            ->add('_newsletter',  FileType::class,array('mapped' => false,'required' => false,
//                'constraints' => array(
//                    new \Symfony\Component\Validator\Constraints\File())
//            ))
            ->add('working_days')
            ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Numa\CCCAdminBundle\Entity\BillingPeriod'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'numa_cccadminbundle_billingperiod';
    }


}
