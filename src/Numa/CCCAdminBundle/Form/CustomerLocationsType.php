<?php

namespace Numa\CCCAdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CustomerLocationsType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('loc1Address1',null,array('label'=>"Address 1"))
            ->add('loc1Address2',null,array('label'=>"Address 2"))
            ->add('loc1City',null,array('label'=>"City"))
            ->add('loc1Prov',null,array('label'=>"Province"))
            ->add('loc1Postal',null,array('label'=>"Postal"))
            ->add('loc1Contact',null,array('label'=>"Contact"))
            ->add('loc2Address1',null,array('label'=>"Address 1"))
            ->add('loc2Address2',null,array('label'=>"Address 2"))
            ->add('loc2City',null,array('label'=>"City"))
            ->add('loc2Prov',null,array('label'=>"Province"))
            ->add('loc2Postal',null,array('label'=>"Postal"))
            ->add('loc2Contact',null,array('label'=>"Contact"))
            ->add('loc3Address1',null,array('label'=>"Address 1"))
            ->add('loc3Address2',null,array('label'=>"Address 2"))
            ->add('loc3City',null,array('label'=>"City"))
            ->add('loc3Prov',null,array('label'=>"Province"))
            ->add('loc3Postal',null,array('label'=>"Postal"))
            ->add('loc3Contact',null,array('label'=>"Contact"))
            ->add('loc4Address1',null,array('label'=>"Address 1"))
            ->add('loc4Address2',null,array('label'=>"Address 2"))
            ->add('loc4City',null,array('label'=>"City"))
            ->add('loc4Prov',null,array('label'=>"Province"))
            ->add('loc4Postal',null,array('label'=>"Postal"))
            ->add('loc4Contact',null,array('label'=>"Contact"))
            ->add('loc5Address1',null,array('label'=>"Address 1"))
            ->add('loc5Address2',null,array('label'=>"Address 2"))
            ->add('loc5City',null,array('label'=>"City"))
            ->add('loc5Prov',null,array('label'=>"Province"))
            ->add('loc5Postal',null,array('label'=>"Postal"))
            ->add('loc5Contact',null,array('label'=>"Contact"))
            ->add('loc',ChoiceType::class,array('data'=>1,'choices'=>array("Use location 1"=>1,"Use Location2"=>2,"Use location 3"=>3,"Use location 4"=>4),'expanded'=>true,'multiple'=>false,'mapped'=>false,'label'=>"Use this location"))
        ;

        $container = $options['container'];

    }


    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Numa\CCCAdminBundle\Entity\Customers'
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'numa_cccadminbundle_customers';
    }

    public function getParent()
    {
        return ContainerAwareType::class;
    }
}
