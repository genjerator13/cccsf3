<?php

namespace Numa\CCCAdminBundle\Form;


use Doctrine\DBAL\Types\StringType;
use Numa\CCCAdminBundle\Entity\Drivers;
use Numa\CCCAdminBundle\Events\ChequeSubscriber;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class ChequeType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public $frame;
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('cheque_date',DateType::class)
            ->add('ref_num')
            ->add('lump_sum_pay_amt')
            ->add('hours')
            ->add('hrate')
            ->add('htotal')
            ->add('mile_rate')
            ->add('miles')
            ->add('mile_total')
            ->add('misc_pay_desc')
            ->add('misc_pay_amt')
            ->add('gross_pay')
            ->add('cpp')
            ->add('social_fund')
            ->add('advance')
            ->add('advance_fee')
            ->add('contract')
            ->add('fuel_expences')
            ->add('fuel_card')
            ->add('fuel_card_fee')
            ->add('purchases')
            ->add('purchases_ref')
            ->add('purchases_fee')
            ->add('amount_in_word')
            ->add('check_num')
            ->add('net_amount')
            ->add('net_pay')
            ->add('reason')
            ->add('deduc_total')
            ->add('wcb')
            ->add('advance_ref')
            ->add('pay_wbc')
            ->add('wcb_code')
            ->add('wcb_rate')
            ->add('wcb_base')
            ->add('cargo_insurance')
            ->add('fuel_credits')
            ->add('city_prov_postal')
            ->add('print_status')
            ->add('misc1')
            ->add('misc2')
            ->add('misc3')
            ->add('misc4')
            ->add('misc1_description',TextType::class,array("required"=>false))
            ->add('misc2_description',TextType::class,array("required"=>false))
            ->add('misc3_description',TextType::class,array("required"=>false))
            ->add('misc4_description',TextType::class,array("required"=>false))
            ->add('misc1_GST')->add('misc2_GST')->add('misc3_GST')->add('misc4_GST')->add('misc1_total')->add('misc2_total')->add('misc3_total')->add('misc4_total')->add('fuel_gst')->add('fuel_total')
            //->add('Driver')->
            ->add('Driver', EntityType::class,
                array('choice_label' => 'displayName',
                    'class' => Drivers::class,
                    'placeholder' => 'Choose a Driver...',
                    'attr'=>array(
                        'ng-model' => 'cheque.Driver',
                        //'ng-value'=>"id",
                        //'ng-options'=>"d.name for d in drivers track by d.id",
                        ),
                    'query_builder' => function ($lr)  {
                        // echo get_class($lr);
                        return $lr->createQueryBuilder('d')
                            ->orderBy('d.drivernum', 'ASC');
                    },
                )
            )
            ->add('PayCode')->add('BillingPeriod');

        $builder->addEventSubscriber(new ChequeSubscriber($options['container'],$options['frame']));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Numa\CCCAdminBundle\Entity\Cheque',
            'frame' =>2
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'cheque';
    }

    public function setFrame($frame){
        $this->frame=$frame;
    }

    public function getParent()
    {
        return ContainerAwareType::class;
    }


}
