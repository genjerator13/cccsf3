<?php

namespace Numa\CCCAdminBundle\Form;

use Numa\CCCAdminBundle\Entity\Dispatch;
use Numa\CCCAdminBundle\Entity\Drivers;
use Numa\CCCAdminBundle\Entity\Trip;
use Numa\CCCAdminBundle\Entity\Vehtypes;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class TripType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $container = $options['container'];
        $builder->add('pickup',null,array('attr'=>array('class'=>'update')));
        $builder->add('delivery',null,array('attr'=>array('class'=>'update')));
        $builder->add('comments',TextType::class,array('attr'=>array('class'=>'update')));
        $builder->add('Driver', EntityType::class, array(
                'class' => Drivers::class,
                'multiple' => false,
                'expanded' => false,
                'attr'=>array('class'=>'update'),
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('d')
                        ->where('d.active=1')
                        ->addOrderBy('d.drivernum', 'ASC');
                }
            ));

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Trip::class
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'trip';
    }

    public function getParent()
    {
        return ContainerAwareType::class;
    }

}
