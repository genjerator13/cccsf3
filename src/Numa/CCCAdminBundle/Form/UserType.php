<?php

namespace Numa\CCCAdminBundle\Form;

use Doctrine\ORM\EntityRepository;
use Numa\CCCAdminBundle\Entity\User;
use Numa\CCCAdminBundle\Entity\UserGroup;
use Numa\CCCAdminBundle\Events\UserSubscriber;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $container = $options['container'];
        $em = $container->get("doctrine.orm.entity_manager");
        $builder
            //->add('name')

            //->add('signature')
            ->add('username')
            ->add('password',PasswordType::class,array("required"=>false))
            ->add('initials')
            ->add('registration_date', DateType::class)
            ->add('UserGroup')
            ->add('chanel',ChoiceType::class,array('label'=>'Chanel','choices'=>array("No Chanel"=>0,"Chanel 1"=>1,"Chanel 2"=>2,"Chanel 3"=>3,"Chanel 4"=>4),'attr'=>array('class'=>'hidden'),'label_attr'=>array('class'=>'hidden')))
        ;
        $builder
            ->add('UserGroup', EntityType::class,array(
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('ug')
                        ->where('ug.id<:id or ug>5')
                        ->setParameter('id',3)
                        ;
                },
                'class' => UserGroup::class,
                'required'  => false,
                //'empty_value' => 'Choose User Group',
                'label' => "User Group"
            ));
        $builder->addEventSubscriber(new UserSubscriber($options['container']));
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Numa\CCCAdminBundle\Entity\User'
        ));
    }
    /**
     * @return string
     */
    public function getName()
    {
        return 'numa_cccadminbundle_user';
    }

    public function getParent()
    {
        return ContainerAwareType::class;
    }
    
}
