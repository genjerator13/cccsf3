<?php

namespace Numa\CCCAdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DriversType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('drivernum')
            ->add('drivernam')
            ->add('address')
            ->add('city')
            ->add('postal')
            ->add('tel')
            ->add('cell')
            ->add('username')
            ->add('password',PasswordType::class,array('required'=>false))
            ->add('hired')
            ->add('dob',BirthdayType::class)
            ->add('sin')
            ->add('married')
            ->add('pic')
            ->add('picExpiry')
            ->add('vehExpiry')
            ->add('vehPlat')
            ->add('vehYear')
            ->add('vehType')
            ->add('vehColor')
            ->add('vehSerial')
            ->add('vehClass')
            ->add('drivRate')
            ->add('insAgent')
            ->add('insPolicy')
            ->add('insAmt')
            ->add('insExpiry')
            ->add('comments')
            ->add('dgc')
            ->add('dgcExpiry')
            ->add('drvsurrate')
            ->add('frame',ChoiceType::class,array('label'=>'Payroll Schedule','choices'=>array("No Commission"=>0,"2 Weeks"=>2,"4 weeks"=>4)))
            ->add('chanel',ChoiceType::class,array('label'=>'Chanel','choices'=>array("No Chanel"=>0,"Chanel 1"=>1,"Chanel 2"=>2,"Chanel 3"=>3,"Chanel 4"=>4)))
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Numa\CCCAdminBundle\Entity\Drivers'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'numa_cccadminbundle_drivers';
    }


}
