<?php

namespace Numa\CCCAdminBundle\Form;

use Doctrine\DBAL\Types\BooleanType;
use Numa\CCCAdminBundle\Entity\Customers;
use Numa\CCCAdminBundle\Entity\Dispatchcard;
use Numa\CCCAdminBundle\Entity\Drivers;
use Numa\CCCAdminBundle\Entity\Rates;
use Numa\CCCAdminBundle\Entity\Vehtypes;
use Numa\CCCAdminBundle\Form\Type\CustomDateType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProbillsType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('isinvoiced', null, array('attr' => array('ng-model' => 'probill.isinvoiced')))
            ->add('shipmentmode', null, array('attr' => array('ng-model' => 'probill.shipmentmode')))
            ->add('pDate',CustomDateType::class, array( 'format' => 'yyyy-M-d','attr' => array('ng-model' => 'probill.p_date')))
            ->add('drivers', EntityType::class,
                array('choice_label' => 'displayName',
                    'class' => Drivers::class,
                    'placeholder' => 'Choose a Driver...',
                    'attr'=>array(
                        'ng-model' => 'probill.driver_pid',
                        //'ng-value'=>"id",
                        //'ng-options'=>"d.name for d in drivers track by d.id",
                        "tabindex"=>4),
                    'query_builder' => function ($lr)  {
                        // echo get_class($lr);
                        return $lr->createQueryBuilder('d')
                            ->andWhere('d.active=1')
                            ->orderBy('d.drivernum', 'ASC');
                    },
                )
            )
            ->add('drvRate', null, array('attr' => array('ng-model' => 'probill.drv_rate',"tabindex"=>5)))
            ->add('drv_rate_override', CheckboxType::class, array('required'=>false,'label'=>'Override','attr' => array('ng-model' => 'probill.drv_rate_override',"tabindex"=>-1)))

            ->add('po',null, array('attr' => array('ng-model' => 'probill.po',"tabindex"=>-1)))
            ->add('ref',null, array('attr' => array('ng-model' => 'probill.ref',"tabindex"=>6)))

            ->add('customers', EntityType::class, array('placeholder' => 'Choose a Customer...','choice_label' => 'displayName', 'class' => Customers::class,"attr"=>array('ng-model' => 'probill.customer_pid',"tabindex"=>7)))
            ->add('waybill',null, array('attr' => array('ng-model' => 'probill.waybill',"tabindex"=>8)))
            ->add('subitem', null, array('attr' => array('ng-model' => 'probill.subitem',"tabindex"=>9)))
            ->add('servType',null, array('attr' => array('ng-model' => 'probill.serv_type',"tabindex"=>10)))
            ->add('dept',null, array('attr' => array('ng-model' => 'probill.dept',"tabindex"=>11)))

            ->add('pickupHr', TextType::class, array('required'=>false,'attr' => array('integers-only'=>'integers-only','min'=>0,'max'=>24,'ng-model' => 'probill.pickup_hr',"tabindex"=>12)))
            ->add('pickupMn',  TextType::class, array('required'=>false,'attr' => array('integers-only'=>'integers-only','min'=>0,'max'=>60,'ng-model' => 'probill.pickup_mn',"tabindex"=>13)))
            ->add('deliveryHr',  TextType::class, array('required'=>false,'attr' => array('integers-only'=>'integers-only','min'=>0,'max'=>24,'ng-model' => 'probill.delivery_hr',"tabindex"=>14)))
            ->add('deliveryMn',  TextType::class, array('required'=>false,'attr' => array('integers-only'=>'integers-only','min'=>0,'max'=>60,'ng-model' => 'probill.delivery_mn',"tabindex"=>15)))
            ->add('ttimeH', TextType::class, array('attr' => array('readonly' => true,'ng-model' => 'probill.ttime_h',"tabindex"=>-1)))
            ->add('ttimeM', TextType::class, array('attr' => array('readonly' => true,'ng-model' => 'probill.ttime_m',"tabindex"=>-1)))


            ->add('vehtypes', EntityType::class,
                array('choice_label' => 'displayName',
                    'class' => Vehtypes::class,
                    'placeholder' => 'Choose a Vehicle Type...',
                    'attr'=>array(
                        'ng-model' => 'probill.vehtype_pid',
                        //'ng-value'=>"id",
                        //'ng-options'=>"d.name for d in drivers track by d.id",
                        "tabindex"=>18),
                    'query_builder' => function ($lr)  {
                        // echo get_class($lr);
                        return $lr->createQueryBuilder('v')
                            ->orderBy('v.vehcode', 'ASC')
                            ->where('v.active=1')
                            ;
                    },
                )
            )
            ->add('trailerId', null, array('attr' => array('ng-model' => 'probill.trailer_id',"tabindex"=>19)))
            ->add('shipper',null, array('attr' => array('ng-model' => 'probill.shipper',"tabindex"=>20)))
            ->add('receiver', null, array('attr' => array('ng-model' => 'probill.receiver',"tabindex"=>21)))

            ->add('pce', null, array('attr' => array('min'=>0,'integers-only'=>'integers-only','ng-model' => 'probill.pce',"tabindex"=>22)))
            ->add('wgt', null, array('attr' => array('min'=>0,'ng-model' => 'probill.wgt',"tabindex"=>23)))
            //->add('rates', null, array('placeholder' => 'Choose a Rate...', "attr"=>array('ng-model' => 'probill.rates',"tabindex"=>24)))
            ->add('rates', EntityType::class,
                array('choice_label' => 'displayName',
                    'class' => Rates::class,
                    'placeholder' => 'Choose a Rate...',
                    'attr'=>array(
                        'ng-model' => 'probill.rates_pid',
                        //'ng-value'=>"id",
                        //'ng-options'=>"d.name for d in drivers track by d.id",
                        "tabindex"=>24),
                    'query_builder' => function ($lr)  {
                        // echo get_class($lr);
                        return $lr->createQueryBuilder('r')
                            ->orderBy('r.rateCode', 'ASC')
                            ->where('r.active=1')
                            ;
                    },
                )
            )
            ->add('rateAmt', null, array('attr' => array('ng-model' => 'probill.rate_amt',"tabindex"=>25)))
            ->add('details', null, array('attr' => array('ng-model' => 'probill.details',"tabindex"=>26,"row"=>8)))
            ->add('extraPiece',TextType::class, array('attr' => array('integers-only'=>'integers-only','ng-model' => 'probill.extra_piece',"tabindex"=>27)))
            ->add('extraPceRate', null, array('attr' => array('ng-model' => 'probill.extra_pce_rate','tabindex'=>28)))
            ->add('waitime', TextType::class, array('attr' => array('ng-model' => 'probill.waitime','tabindex'=>29)))
            ->add('waitchrg', null, array('attr' => array('ng-model' => 'probill.waitchrg','tabindex'=>30)))
            ->add('signature', null, array('attr' => array('ng-model' => 'probill.signature','tabindex'=>31)))

            ->add('pickupAddr1', null, array('attr' => array('ng-model' => 'probill.pickupAddr1')))
            ->add('shiptoAddr1', null, array('attr' => array('ng-model' => 'probill.shiptoAddr1')))
            ->add('driver_code', null, array('attr' => array('ng-model' => 'probill.driver_code')))
            ->add('customer_code', null, array('attr' => array('ng-model' => 'probill.customer_code')))

            ->add('customer', null, array('attr' => array('ng-model' => 'probill.customer')))



            ->add('pU', null, array('attr' => array('ng-model' => 'probill.pU')))
            ->add('dE', null, array('attr' => array('ng-model' => 'probill.dE')))


            ->add('rate_code', null, array('attr' => array('ng-model' => 'probill.rate_code')))
            ->add('rateDescription', null, array('attr' => array('ng-model' => 'probill.rateDescription')))
            ->add('paygst', null, array('attr' => array('ng-model' => 'probill.paygst')))



            ->add('extraAmt', null, array('attr' => array('ng-model' => 'probill.extra_amt')))
            ->add('miscAmt', null, array('attr' => array('ng-model' => 'probill.misc_amt')))
            ->add('miscdesc', null, array('attr' => array('ng-model' => 'probill.miscdesc')))
            ->add('gstAmt', null, array('attr' => array('ng-model' => 'probill.gstAmt')))
            ->add('grandtotal', null, array('attr' => array('ng-model' => 'probill.grandtotal')))
            ->add('paid', null, array('attr' => array('ng-model' => 'probill.paid')))
            ->add('balance', null, array('attr' => array('ng-model' => 'probill.balance')))

            ->add('drvTotal', null, array('attr' => array('ng-model' => 'probill.drvTotal','readonly'=>true)))
            ->add('gross', null, array('attr' => array('ng-model' => 'probill.gross','readonly'=>true)))
            ->add('comments', null, array('attr' => array('ng-model' => 'probill.comments',"tabindex"=>"100")))
            ->add('dimensions', null, array('attr' => array('ng-model' => 'probill.dimensions',"tabindex"=>"100")))
            ->add('miscinfo', null, array('attr' => array('ng-model' => 'probill.miscinfo',"tabindex"=>"100")))


            ->add('invoice', null, array('attr' => array('ng-model' => 'probill.invoice')))


            ->add('vehicleId', null, array('attr' => array('ng-model' => 'probill.vehicleId')))

            ->add('batch', null, array('attr' => array('ng-model' => 'probill.batch')))
            ->add('rushAmt', null, array('attr' => array('ng-model' => 'probill.rush_amt','class'=>'blueinput')))
            ->add('called', null, array('attr' => array('ng-model' => 'probill.called')))
            ->add('delivered', null, array('attr' => array('ng-model' => 'probill.delivered')))
            ->add('deliverytime', null, array('attr' => array('ng-model' => 'probill.deliverytime')))
            ->add('status', null, array('attr' => array('ng-model' => 'probill.status')))
            ->add('custsurchargeamt', null, array('attr' => array('ng-model' => 'probill.custsurchargeamt')))
            ->add('drvsurrate', null, array('attr' => array('ng-model' => 'probill.drvsurrate','readonly'=>true)))
            ->add('cussurchargerate', null, array('attr' => array('ng-model' => 'probill.cussurchargerate')))
            ->add('cussurchargerate_override', CheckboxType::class, array('required'=>false,'label'=>'Override','attr' => array('ng-model' => 'probill.cussurchargerate_override',"tabindex"=>-1)))

            ->add('driversurcharge', null, array('attr' => array('ng-model' => 'probill.driversurcharge','readonly'=>true)))
            ->add('total', null, array('attr' => array('ng-model' => 'probill.total','readonly'=>'readonly')))
            ->add('taxcode', null, array('attr' => array('ng-model' => 'probill.taxcode')))
            ->add('drvgross', null, array('attr' => array('ng-model' => 'probill.drvgross','readonly'=>true)))
            ->add('trailerchg', null, array('attr' => array('ng-model' => 'probill.trailerchg')))
            ->add('miles', null, array('attr' => array('ng-model' => 'probill.miles')))
            ->add('pickupShipper', null, array('attr' => array('ng-model' => 'probill.pickupShipper')))

            ->add('pickupAddr2', null, array('attr' => array('ng-model' => 'probill.pickupAddr2')))
            ->add('pickupCity', null, array('attr' => array('ng-model' => 'probill.pickupCity')))
            ->add('pickupProv', null, array('attr' => array('ng-model' => 'probill.pickupProv')))
            ->add('pickupPostal', null, array('attr' => array('ng-model' => 'probill.pickupPostal')))
            ->add('pickupPhone', null, array('attr' => array('ng-model' => 'probill.pickupPhone')))
            ->add('pickupContact', null, array('attr' => array('ng-model' => 'probill.pickupContact')))
            ->add('pickupDetail', null, array('attr' => array('ng-model' => 'probill.pickupDetail')))
            ->add('pickupRef', null, array('attr' => array('ng-model' => 'probill.pickupRef')))
            ->add('shiptoReceiver', null, array('attr' => array('ng-model' => 'probill.shiptoReceiver')))

            ->add('shiptoAddr2', null, array('attr' => array('ng-model' => 'probill.shiptoAddr2')))
            ->add('shiptoCity', null, array('attr' => array('ng-model' => 'probill.shiptoCity')))
            ->add('shiptoProv', null, array('attr' => array('ng-model' => 'probill.shiptoProv')))
            ->add('shiptoPostal', null, array('attr' => array('ng-model' => 'probill.shiptoPostal')))
            ->add('shiptoPhone', null, array('attr' => array('ng-model' => 'probill.shiptoPhone')))
            ->add('shiptoContact', null, array('attr' => array('ng-model' => 'probill.shiptoContact')))
            ->add('shiptoDetail', null, array('attr' => array('ng-model' => 'probill.shiptoDetail')))
            ->add('shiptoRef', null, array('attr' => array('ng-model' => 'probill.shiptoRef')))
            ->add('posted', null, array('attr' => array('ng-model' => 'probill.posted')))
            ->add('printed', null, array('attr' => array('ng-model' => 'probill.printed')))
            ->add('commodity', null, array('attr' => array('ng-model' => 'probill.commodity')))
            ->add('routecode', null, array('attr' => array('ng-model' => 'probill.routecode')))
            ->add('tracking', null, array('attr' => array('ng-model' => 'probill.tracking')))
            ->add('barcode', null, array('attr' => array('ng-model' => 'probill.barcode')))

            ->add('glaccount', null, array('attr' => array('ng-model' => 'probill.glaccount')))
            ->add('paymenttype', null, array('attr' => array('ng-model' => 'probill.paymenttype')))
            ->add('payref', null, array('attr' => array('ng-model' => 'probill.payref')))
            ->add('codAmt', null, array('attr' => array('ng-model' => 'probill.codAmt')))

            ->add('entrydate', null, array('attr' => array('ng-model' => 'probill.entrydate')))
            ->add('shipmentid', null, array('attr' => array('ng-model' => 'probill.shipmentid')))
            ->add('ratelevel', null, array('attr' => array('ng-model' => 'probill.ratelevel')))
            ->add('subtotal', null, array('attr' => array('ng-model' => 'probill.subtotal')))
            ->add('billtype', null, array('attr' => array('ng-model' => 'probill.billtype')))
            ->add('fuelsurchargedifferential', null, array('attr' => array('ng-model' => 'probill.fuelsurchargedifferential')))
            ->add('diffsurchargeamt', null, array('attr' => array('ng-model' => 'probill.diffsurchargeamt')))
            ->add('commodityId', null, array('attr' => array('ng-model' => 'probill.commodityId')))
            ->add('initials', null, array('attr' => array('ng-model' => 'probill.initials')))
            ->add('shiptoId', null, array('attr' => array('ng-model' => 'probill.shiptoId')))
            ->add('pickupfromId', null, array('attr' => array('ng-model' => 'probill.pickupfromId')))
            ->add('createdAt', null, array('attr' => array('ng-model' => 'probill.createdAt')))
            ->add('vehcode', null, array('attr' => array('ng-model' => 'probill.vehcode')));

            $container = $options['container'];
            $em= $container->get("doctrine")->getManager();
            $dcs = $em->getRepository(Dispatchcard::class)->findLastn(100);

            $builder->add('Dispatchcard', ChoiceType::class,
                array(
                    'placeholder' => 'Choose a Dispatch Card...',
                    'choices' =>$dcs,
                    'attr'=>array(
                        'ng-model' => 'probill.dispatchcard',
                        //'ng-value'=>"id",
                        //'ng-options'=>"d.name for d in drivers track by d.id",
                        "tabindex"=>-1)

                )
            )
        ;
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Numa\CCCAdminBundle\Entity\Probills',
            'allow_extra_fields' => true,
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'numa_cccadminbundle_probills';
    }

    public function getParent()
    {
        return ContainerAwareType::class;
    }
}
