<?php

namespace Numa\CCCAdminBundle\Form;

use Numa\CCCAdminBundle\Entity\Dispatch;
use Numa\CCCAdminBundle\Entity\Vehtypes;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class DispatchType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $container = $options['container'];
        $services =array("No Service Delay"=>"No Service Delay","Service Delay Expected"=>"Service Delay Expected");
        $pickups = array("15 min"=>15,"20 min"=>20,"25 min"=>25,"30 min"=>30,"35 min"=>35,"40 min"=>40,"45 min"=>45,"50 min"=>50,"55 min"=>55,"1 hour"=>60,"90 min"=>90,"More than 2 hours"=>120);

        $builder

//            ->add('VehicleType', HiddenType::class,array('property_path' => 'id'))
            ->add('VehicleType', EntityType::class, array(
                'class' => 'NumaCCCAdminBundle:Vehtypes',
                'multiple' => false,
                'expanded' => false,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('v')
                        ->where('v.active=1')
                        ->addOrderBy('v.vehcode', 'ASC');
                },
                'attr' => array('class' => 'hidden')))

            ->add('service_expectation', ChoiceType::class, array('attr'=>array('class'=>'service_expectation'),'choices'=>$services,'label' => 'Service Expectation', 'required' => false))
            ->add('estimate_pickup', ChoiceType::class, array('attr'=>array('class'=>'estimate_pickup'),'choices'=>$pickups,'label' => 'Estimated Pickup', 'required' => false))
            ;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Dispatch::class
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'numa_cccadminbundle_origin';
    }

    public function getParent()
    {
        return ContainerAwareType::class;
    }

}
