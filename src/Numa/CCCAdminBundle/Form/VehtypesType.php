<?php

namespace Numa\CCCAdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class VehtypesType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $typeChoices = array("Small vehicles"=>1,"Large Vehicles"=>2);
        $builder->add('vehdesc')
            ->add('vehclass')
            ->add('ctyfsrate')
            ->add('hwyfsrate')
            ->add('vehcode')
            ->add('type',ChoiceType::class,array("choices"=>$typeChoices))
            ->add('active')
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Numa\CCCAdminBundle\Entity\Vehtypes'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'numa_cccadminbundle_vehtypes';
    }


}
