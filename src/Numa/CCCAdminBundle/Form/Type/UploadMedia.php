<?php
// src/AppBundle/Form/Type/PersonType.php
namespace Numa\CCCAdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;

class UploadMedia extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('_mediathek',  \Symfony\Component\Form\Extension\Core\Type\FileType::class,array('label' => 'mediathek.upload', 'mapped' => false,'required' => false,
                'constraints' => array(
            new \Symfony\Component\Validator\Constraints\NotBlank(['message' => 'mediathek.select']),
            new \Symfony\Component\Validator\Constraints\File())
                ));
        $builder->add('upload',  \Symfony\Component\Form\Extension\Core\Type\SubmitType::class, array('label' => 'mediathek.action'));
        
        
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        
    }

    public function getName()
    {
        return 'UploadMedia';
    }
}