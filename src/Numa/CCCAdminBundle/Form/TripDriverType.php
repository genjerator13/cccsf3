<?php

namespace Numa\CCCAdminBundle\Form;

use Numa\CCCAdminBundle\Entity\Dispatch;
use Numa\CCCAdminBundle\Entity\Drivers;
use Numa\CCCAdminBundle\Entity\Trip;
use Numa\CCCAdminBundle\Entity\Vehtypes;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class TripDriverType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $container = $options['container'];
//        $builder->add('pickup');
//        $builder->add('delivery');
        $builder->add('comments', TextType::class, array('attr' => array('class' => 'update')));
        $builder->add('waybill');
        $builder->add('PCE',null,array('label'=>"PCE"));
        $builder->add('WGT',null,array('label'=>"WGT"));
        $builder->add('signature');
        $builder->add('vehtype_id', EntityType::class,
            array('choice_label' => 'displayName',
                'class' => Vehtypes::class,
                'label' => "Vehicle Type",
                'required'=>false,
                'placeholder' => 'Choose a Vehicle Type...',
                'attr' => array(
                    'ng-model' => 'probill.vehtype_pid',
                    //'ng-value'=>"id",
                    //'ng-options'=>"d.name for d in drivers track by d.id",
                ),
                'query_builder' => function ($lr) {
                    // echo get_class($lr);
                    return $lr->createQueryBuilder('v')
                        ->orderBy('v.vehcode', 'ASC')
                        ->where('v.active=1');
                }
            )
        );
        $builder->add('pickup_hr', TextType::class, array('required'=>false,'attr' => array('min'=>0,'max'=>24)))
        ->add('pickup_mn',  TextType::class, array('required'=>false,'attr' => array('min'=>0,'max'=>60)))
        ->add('delivery_hr',  TextType::class, array('required'=>false,'attr' => array('min'=>0,'max'=>24)))
        ->add('delivery_mn',  TextType::class, array('required'=>false,'attr' => array('min'=>0,'max'=>60)))
        ->add('ttime_hr', TextType::class, array('attr' => array('readonly' => true)))
        ->add('ttime_mn', TextType::class, array('attr' => array('readonly' => true)))
            ;


    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Trip::class
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'trip';
    }

    public function getParent()
    {
        return ContainerAwareType::class;
    }

}
