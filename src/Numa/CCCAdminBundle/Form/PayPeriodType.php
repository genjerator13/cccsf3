<?php

namespace Numa\CCCAdminBundle\Form;



use Numa\CCCAdminBundle\Entity\PayPeriod;
use Numa\CCCAdminBundle\Form\Type\UploadMedia;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PayPeriodType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $types = array("2 Weeks"=>2,"4 Weeks"=>4);
        $builder
            ->add('name')
            ->add('type',ChoiceType::class,array('choices'=>$types))
            ->add('started',DateType::class,array(
                'data' => new \DateTime()))
            ->add('closed',DateType::class,array(
                'data' => new \DateTime()))
            ->add('cheque_date',DateType::class,array(
                'data' => new \DateTime()))
            ->add('BillingPeriod')
            ->add('wcb_rate')
            ->add('wcb_deduction')
            ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => PayPeriod::class
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'payperiod';
    }


}
