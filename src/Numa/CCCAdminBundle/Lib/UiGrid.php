<?php
/**
 * Created by PhpStorm.
 * User: Genjerator
 * Date: 01.03.18.
 * Time: 15.27
 */

namespace Numa\CCCAdminBundle\Lib;

use Symfony\Component\HttpFoundation\Request;


class UiGrid
{
    protected $container;

    /**
     * ListingFormHandler constructor.
     * @param ContainerInterface $container
     */
    public function __construct($container) // this is @service_container
    {
        $this->container = $container;
    }

    public function getSelectedIds(Request $request)
    {
        $data = json_decode($request->get('data'));
        $values = array();
        if (is_array($data)) {

            foreach ($data as $item_id) {
                $values[] = intval($item_id);
            }
        }else{
            $values[]=$data;
        }


        $values = array_filter($values, function ($a) {
            return ($a !== 0);
        });
        //$item_ids = implode(",",$values);
        return $values;
    }
}