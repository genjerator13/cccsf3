<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of services
 *
 * @author genjerator
 */

namespace Numa\CCCAdminBundle\Lib;

use Numa\CCCAdminBundle\Entity\Attachment;
use Numa\CCCAdminBundle\Entity\BillingPeriod;
use Numa\CCCAdminBundle\Entity\CustomerReport;
use Numa\CCCAdminBundle\Entity\CustomerReportEmail;
use Numa\CCCAdminBundle\Entity\Customers;
use Numa\CCCAdminBundle\Entity\Email;
use Numa\CCCAdminBundle\Entity\Media;
use Numa\CCCAdminBundle\Entity\Probills;
use Numa\CCCAdminBundle\Entity\Settings;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class ReportMailingLib
{
    use ContainerAwareTrait;


    protected $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    public function attachEmailToReport(&$em, CustomerReport $customerReport, $attachments,$billingPeriodId)
    {

        $logger = $this->container->get('logger');
        $emailFrom = $this->container->getParameter('email_from');
        $customer = $customerReport->getCustomers();
        $em->getRepository(CustomerReport::class)->clearEmails($customerReport);
        if (!$customer->getCustomerEmails()->isEmpty()) {
            $emails = $customer->getCustomerEmails();
            foreach ($emails as $email) {
                $emailEntity = new \Numa\CCCAdminBundle\Entity\Email();
                dump($customerReport->getCustomers()->getId());
                $emailEntity->setCustomerId($customerReport->getCustomers()->getId());
                $emailEntity->setBillingPeriodId($billingPeriodId);
                $emailEntity->setSubject($customerReport->getBillingPeriod()->getEmailSubject());
                $emailEntity->setBody($customerReport->getBillingPeriod()->getEmailTemplate());
                $emailEntity->setEmailFrom($emailFrom);
                $emailEntity->setEmailTo($email->getEmail());
                $emailEntity->setStatus(0);
                $emailEntity->setCustomers($customerReport->getCustomers());
                if (strtoupper($customer->getSendmail()) == "Y") {
                    //attachment
                    $deliveryReportPdf = $attachments['deliveryPdfMedia'];
                    $deliveryReportXls = $attachments['deliveryXlsMedia'];
                    $invoice = $attachments['invoiceMedia'];
                    $newsletter = $customerReport->getBillingPeriod()->getNewsletter();

                    $attachment1 = new Attachment();
                    $attachment1->setEmail($emailEntity);
                    $deliveryReportPdf->setOrigin("attachment");
                    $attachment1->setMedia($deliveryReportPdf);

                    $em->persist($attachment1);

                    $attachment2 = new Attachment();
                    $attachment2->setEmail($emailEntity);
                    $deliveryReportXls->setOrigin("attachment");
                    $attachment2->setMedia($deliveryReportXls);

                    $em->persist($attachment2);

                    $attachment3 = new Attachment();
                    $attachment3->setEmail($emailEntity);
                    $invoice->setOrigin("attachment");
                    $attachment3->setMedia($invoice);
                    $em->persist($attachment3);

                    if($newsletter instanceof Media) {
                        $attachment4 = new Attachment();
                        $attachment4->setEmail($emailEntity);
                        $attachment4->setMedia($newsletter);
                        $em->persist($attachment4);
                    }
                }
                $customerReport->getCustomerReportEmail();
                $customerReportEmail = new CustomerReportEmail();
                $customerReportEmail->setEmail($emailEntity);
                $customerReportEmail->setCustomerReport($customerReport);
                $customerReport->addCustomerReportEmail($customerReportEmail);

                $progressText = "email for: " . $customer->getCustcode();

                $logger->warning($progressText);
                $em->persist($emailEntity);
            }
        }
    }

}
