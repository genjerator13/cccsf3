<?php

/**
 * Created by PhpStorm.
 * User: genjerator
 * Date: 5.2.18.
 * Time: 13.46
 */
namespace Numa\CCCAdminBundle\Lib\Report;
use Numa\CCCAdminBundle\Entity\BillingPeriod;
use Numa\CCCAdminBundle\Entity\Customers;
use Symfony\Component\HttpFoundation\Response;

class BillingReport extends Report
{

    private $customer;
    private $billingPeriod;





    public function __construct($container,BillingPeriod $billingPeriod)
    {
        $this->billingPeriod=$billingPeriod;
        parent::__construct($container);
        $this->init();

    }

    public function prepareDataForBillingPeriod(){

    }

    public function setTwigFiles(){
        $this->twigLayoutPage   = "NumaCCCAdminBundle:report:" . $this->getName() . "/layout.pdf.twig";
        $this->twigHeaderPage   = "NumaCCCAdminBundle:report:" . $this->getName() . "/header.pdf.twig";
        $this->twigFooterPage   = "NumaCCCAdminBundle:report:" . $this->getName() . "/footer.pdf.twig";
        $this->twigContentPage   = "NumaCCCAdminBundle:report:" . $this->getName() . "/content.html.twig";
    }

    /**
     * @return \Numa\CCCAdminBundle\Entity\BillingPeriod
     */
    public function getBilingPeriod()
    {
        return $this->bilingPeriod;
    }

    /**
     * @param \Numa\CCCAdminBundle\Entity\BillingPeriod $bilingPeriod
     */
    public function setBilingPeriod(\Numa\CCCAdminBundle\Entity\BillingPeriod $bilingPeriod)
    {
        $this->bilingPeriod = $bilingPeriod;
    }



    /**
     * @return BillingPeriod
     */
    public function getBillingPeriod()
    {
        return $this->billingPeriod;
    }

    /**
     * @param BillingPeriod $billingPeriod
     */
    public function setBillingPeriod($billingPeriod)
    {
        $this->billingPeriod = $billingPeriod;
    }

    /**
     * @return mixed
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @param mixed $customer
     */
    public function setCustomer($customer)
    {
        $this->customer = $customer;
    }

    public function getFilename($absolute=true,$format="pdf"){
        $download_path = $this->getContainer()->getParameter('download_excel_path');
        $part1 = "";
        $partCust = "ALL";
        if($this->getBillingPeriod() instanceof BillingPeriod){
            $part1 = $this->getBillingPeriod()->getId();
        }
        if($this->getCustomer() instanceof Customer){
            $partCust = $this->getCustomer()->getCustcode() ;
        }
        $customer = "";
        if($this->getCustomer() instanceof Customers){
            $customer = $this->getCustomer()->getCustcode();
        }
        $filename = $this->getBillingPeriod()->getId()."/".$this->getName() . '_' . $this->getBillingPeriod()->getId() . '_' . $customer . ".".$format;

        if($absolute){
           $filename =  $download_path . $filename;
        }
        return $filename;
    }

}