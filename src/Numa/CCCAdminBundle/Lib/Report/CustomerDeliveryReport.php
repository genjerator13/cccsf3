<?php

/**
 * Created by PhpStorm.
 * User: genjerator
 * Date: 5.2.18.
 * Time: 13.59
 */
namespace Numa\CCCAdminBundle\Lib\Report;

use Numa\CCCAdminBundle\Entity\BillingPeriod;
use Numa\CCCAdminBundle\Entity\Customers;
use Numa\CCCAdminBundle\Entity\Probills;
use Numa\CCCAdminBundle\Entity\Vehtypes;
use Symfony\Component\HttpFoundation\Response;

class CustomerDeliveryReport extends BillingReport
{
    use CustomersAwareTrait;

    public function __construct($container, $billingPeriod)
    {
        parent::__construct($container, $billingPeriod);
        $this->setName("customerDelivery");
        $this->setTitle("Customer Delivery Report");
        $this->init();
        $this->prepareDataForBillingPeriod();
    }

    public function prepareDataForBillingPeriod()
    {
        $em = $this->getContainer()->get("doctrine")->getManager();
        //$customer = $em->getRepository(Customers::class)->findOneBy(array("Customers"=>$this->getCustomer()));
        //$customer=null;
        $probills = $em->getRepository(BillingPeriod::class)->getProbillsByAllCustomers($this->getBillingPeriod());

        $data = $this->prepareData($probills);

        $this->addTwigParams('data', $data['data']);
        $this->addTwigParams('totals', $data['totals']);
        $vehtypes = $em->getRepository('NumaCCCAdminBundle:Vehtypes')->findBy(array('active' => 1));
        $this->addTwigParams('vehTypes', $vehtypes);
    }

    public function singleProbillReport($id)
    {
        $em = $this->getContainer()->get("doctrine")->getManager();
        $mpdf = new \Mpdf\Mpdf(array('format' => 'letter-L', "margin_left" => 5, "margin_right" => 5, "margin_top" => 5, "margin_bottom" => 5));
        $probill = $em->getRepository(Probills::class)->find($id);
        $probills = array($probill);

        $data = $this->prepareData($probills);

        $mpdf->useOnlyCoreFonts = true;    // false is default
        $mpdf->SetTitle($this->getTitle());
        $mpdf->SetAuthor($this->getAuthor());
        $mpdf->SetDisplayMode('fullpage');

        $templating = $this->getContainer()->get('templating');
        $headerFile = "NumaCCCAdminBundle:report:" . $this->getName() . "/header.pdf.twig";
        $customerFile = "NumaCCCAdminBundle:report:" . $this->getName() . "/customer.pdf.twig";
        $footerFile = "NumaCCCAdminBundle:report:" . $this->getName() . "/footer.pdf.twig";
        $vehiclesFile = "NumaCCCAdminBundle:report:/vehTypesTable.html.twig";


        $html = $templating->render($headerFile, $this->getTwigParams());
        $mpdf->WriteHTML($html);
        $this->addTwigParams('data', $data['data']);
        $this->addTwigParams('totals', $data['totals']);
        $vehtypes = $em->getRepository('NumaCCCAdminBundle:Vehtypes')->findBy(array('active' => 1));

        $customer = $probill->getCustomers();

        $key = $customer->getCustcode();
        if (!empty($data['data'][$key])) {
            $html = $templating->render($customerFile, array('key' => $key, 'customer' => $data['data'][$key], 'totals' => $data['totals']));
            $mpdf->WriteHTML($html);
        }

        $html = $templating->render($vehiclesFile, array('vehTypes' => $vehtypes));

        $mpdf->WriteHTML($html);
//dump($this->getFilename(false));die();
        header('Content-Type: application/pdf');
        header('Content-Disposition: attachment;filename="customerDelivery_' . $key . '_.pdf"');
        header('Cache-Control: max-age=0');
        $mpdf->Output();
        exit;

    }

    public function finalExcelWork(\PHPExcel &$objExcel)
    {
        $styleLeft = array(
            'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
            )
        );
        $objExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
        $objExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
        $objExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $objExcel->getActiveSheet()->getColumnDimension('D')->setWidth(60);
        $objExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
        //$objExcel->getActiveSheet()->getStyle('A1:X1' )->getFont()->setBold( true );
        $objExcel->getDefaultStyle()->applyFromArray($styleLeft);
    }

    public function prepareData($probills)
    {
        $data = array();
        $temp = array();
        $temp2 = array();
        $tempCustomer = array();
        foreach ($probills as $probill) {
            $customer = $probill->getCustomers();
            if ($probill instanceof Probills) {}
            if ($customer instanceof Customers) {
                $tempProbill = array();

                $tempProbill['waybill'] = $probill->getWaybill();
                $tempProbill['p_date'] = $probill->getPDate();

                $tempProbill['subitem'] = $probill->getSubItem();
                $tempProbill['ref'] = $probill->getRef();
                $tempProbill['PCE'] = $probill->getPCE();
                $tempProbill['WGT'] = $probill->getWGT();
                $tempProbill['shipper'] = $probill->getShipper();
                $tempProbill['receiver'] = $probill->getReceiver();
                $th = $probill->getTTimeH();
                $tm = $probill->getTTimeM();
                $tempProbill['time'] = (empty($th) ? "0" : $th) . ":" . (empty($tm) ? "0" : $tm);
                $tempProbill['signature'] = $probill->getSignature();

                $tempProbill['driver'] = $probill->getDriverCode();
                $vehcode ="";
                if($probill->getVehtypes() instanceof Vehtypes){
                    $vehcode =$probill->getVehtypes()->getVehcode();
                }
                $tempProbill['vehicle_code'] = $vehcode;
                $tempProbill['FS'] = $probill->getCusSurchargeRate();
                $tempProbill['FSamt'] = $probill->getCustSurchargeAMT();
                $tempProbill['total'] = $probill->getTotal();
                $tempProbill['serv_type'] = $probill->getServType();
                $tempProbill['details'] = $probill->getDetails();
                $tempProbill['billtype'] = $probill->getBilltype();

                $d = $probill->getDept();
                $dept = empty($d) ? "   " : trim($probill->getDept());
                //$tempCustomer[$probill->getCustomerCode()][$probill->getBillType()][$dept]['probills'][] = $tempProbill;
                //$tempCustomer[$probill->getCustomerCode()][$probill->getBillType()][$dept]['probills'][$probill->getPDate()->format('Y-M-d')."-".$probill->getWaybill()."-".$probill->getSubItem()] = $tempProbill;


                $c = $probill->getCustomers();

                //$customer = empty($c) ? "null" : $probill->getCustomers();
                $tempCustomer[$customer->getCustCode()][$probill->getBillType()][$dept]['probills'][$probill->getPDate()->format('Y-M-d') . "-" . $probill->getWaybill() . "-" . $probill->getSubItem() . "-" . $probill->getId()] = $tempProbill;

                $tempCustomer[$customer->getCustCode()]['name'] = $customer->getName();
                $tempCustomer[$customer->getCustCode()]['custcode'] = $customer->getCustCode();
            }
        }
        //$customer = $this->container->get('security.context')->getToken()->getUser();


        $totals = array();
        $temp2 = array();

        foreach ($tempCustomer as $custcode => $customer) {

            $totals[$custcode]['wgt'] = 0;
            $totals[$custcode]['pce'] = 0;
            $totals[$custcode]['total'] = 0;
            $totals[$custcode]['FSamt'] = 0;
            $ctyhwy = array('CTY', 'HWY');
            foreach ($ctyhwy as $billtype) {

                $totals[$custcode][$billtype]['pce'] = 0;
                $totals[$custcode][$billtype]['wgt'] = 0;
                $totals[$custcode][$billtype]['total'] = 0;
                $totals[$custcode][$billtype]['FSamt'] = 0;

                $temp = array();

                if (!empty($customer[$billtype])) {
                    foreach ($customer[$billtype] as $keyDept => $dept) {
                        $keyDept = (string)$keyDept . ":";

                        if (!empty($dept) && is_array($dept)) {
                            ksort($dept);
                            ksort($dept['probills']);
                        }
                        $temp[$keyDept] = $dept;


                        $fsAmt = 0;
                        $total = 0;
                        $pce = 0;
                        $wgt = 0;
                        $totals[$custcode][$billtype][$keyDept]['pce'] = $pce;
                        $totals[$custcode][$billtype][$keyDept]['wgt'] = $wgt;
                        $totals[$custcode][$billtype][$keyDept]['FSamt'] = $fsAmt;
                        $totals[$custcode][$billtype][$keyDept]['total'] = $total;
                        foreach ($dept['probills'] as $probill) {
                            $pce = $pce + $probill['PCE'];
                            $wgt = $wgt + $probill['WGT'];
                            $total = $total + $probill['total'];
                            $fsAmt = $fsAmt + $probill['FSamt'];
                        }
                        //totals by customer billtype and dept
                        $totals[$custcode][$billtype][$keyDept]['pce'] = $pce;
                        $totals[$custcode][$billtype][$keyDept]['wgt'] = $wgt;
                        $totals[$custcode][$billtype][$keyDept]['FSamt'] = $fsAmt;
                        $totals[$custcode][$billtype][$keyDept]['total'] = $total;

                        //totals by customer and billtype (CTY, HWY)
                        $totals[$custcode][$billtype]['pce'] = $totals[$custcode][$billtype]['pce'] + $pce;
                        $totals[$custcode][$billtype]['wgt'] = $totals[$custcode][$billtype]['wgt'] + $wgt;
                        $totals[$custcode][$billtype]['FSamt'] = $totals[$custcode][$billtype]['FSamt'] + $fsAmt;
                        $totals[$custcode][$billtype]['total'] = $totals[$custcode][$billtype]['total'] + $total;
                        //totals by customer
                        $totals[$custcode]['pce'] = $totals[$custcode]['pce'] + $totals[$custcode][$billtype][$keyDept]['pce'];
                        $totals[$custcode]['wgt'] = $totals[$custcode]['wgt'] + $totals[$custcode][$billtype][$keyDept]['wgt'];
                        $totals[$custcode]['total'] = $totals[$custcode]['total'] + $totals[$custcode][$billtype][$keyDept]['total'];
                        $totals[$custcode]['FSamt'] = $totals[$custcode]['FSamt'] + $totals[$custcode][$billtype][$keyDept]['FSamt'];
                    }
                    uksort($temp, "self::cmp");
                    $temp2[$custcode][$billtype] = $temp;
                }

                //die();
                $temp2[$custcode]['name'] = $customer['name'];
                $temp2[$custcode]['custcode'] = $custcode;
            }
        }

        ksort($temp2);
        return array('data' => $temp2, 'totals' => $totals);
    }

    static function cmp($a, $b)
    {
        if ($a == 'OOT' || $b == 'OOT') {
            if ($a == 'OOT') {
                return strcasecmp('ZZZ', $b);
            }
            if ($b == 'OOT') {
                return strcasecmp($a, 'ZZZZ');
            }
        }
        return strcasecmp($a, $b);
    }

    public function rendermPDF($twigParams = array(), $save = true)
    {
        //$this->prepareDataForBillingPeriod();
        $mpdf = new \Mpdf\Mpdf(array('format' => 'letter-L', "margin_left" => 5, "margin_right" => 5, "margin_top" => 5, "margin_bottom" => 5));


        $mpdf->useOnlyCoreFonts = true;    // false is default
        $mpdf->SetTitle($this->getTitle());
        $mpdf->SetAuthor($this->getAuthor());
        $mpdf->SetDisplayMode('fullpage');

        $templating = $this->getContainer()->get('templating');
        $headerFile = "NumaCCCAdminBundle:report:" . $this->getName() . "/header.pdf.twig";
        $customerFile = "NumaCCCAdminBundle:report:" . $this->getName() . "/customer.pdf.twig";
        $footerFile = "NumaCCCAdminBundle:report:" . $this->getName() . "/footer.pdf.twig";
        $vehiclesFile = "NumaCCCAdminBundle:report:vehTypesTable.html.twig";

        $html = $templating->render($headerFile, $this->getTwigParams());
        $mpdf->WriteHTML($html);
        $data = $this->getTwigParam('data');
        $totals = $this->getTwigParam('totals');
        $vehTypes = $this->getTwigParam('vehTypes');

        //foreach($data as $key=>$customer) {

        foreach ($this->getCustomers() as $customer) {

            $key = $customer->getCustcode();
            if (!empty($data[$key])) {
                $html = $templating->render($customerFile, array('key' => $key, 'customer' => $data[$key], 'totals' => $totals));
                $mpdf->WriteHTML($html);
                $html = $templating->render($vehiclesFile, array('vehTypes' => $vehTypes));
                $mpdf->WriteHTML($html);
            }
        }

        //$html = $templating->render($footerFile);
        //dump($html);die();


        //dump($html);die();


        if ($save) {
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $html);
            //$download_path = $this->getContainer()->getParameter('download_excel_path');
            //$filename = $download_path . $this->getName() . '_' . $this->getBillingPeriod()->getId() . '_' . $this->getCustomer()->getCustcode() . ".pdf";

            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            $mpdf->Output($tmpfile, "F");
            $file = fopen($tmpfile, "rd");
            $filecontent = file_get_contents($tmpfile);
            return $filecontent;
        } else {
            header('Content-Type: application/pdf');
            header('Content-Disposition: attachment;filename="' . $this->getFilename(false));
            header('Cache-Control: max-age=0');
            $mpdf->Output();
            exit;
        }
    }

    public function renderXLS($format = null, $content = false)
    {

        $rendererName = \PHPExcel_Settings::PDF_RENDERER_MPDF;
        $rendererLibrary = 'mPDF';
        $rendererLibraryPath = dirname(__FILE__) . '/../../../../libraries/PDF/' . $rendererLibrary;
        $rendererLibraryPath = '/var/ccc2/vendor/phpoffice/phpexcel/Classes/PHPExcel/Writer/PDF/tcPDF.php';
        ///var/ccc2/vendor/phpoffice/phpexcel/Classes/PHPExcel/Writer/PDF/mPDF.php
        //$rendererLibraryPath = '/var/ccc2/vendor/mpdf/mpdf/' . $rendererLibrary;
        $rendererLibraryPath = (dirname(__FILE__) . '/../../../../../vendor/mpdf/mpdf'); //works

        if (!\PHPExcel_Settings::setPdfRenderer(
            $rendererName, $rendererLibraryPath
        )
        ) {
            die(
                'NOTICE: Please set the $rendererName and $rendererLibraryPath values' .
                '<br />' .
                'at the top of this script as appropriate for your directory structure'
            );
        }
        //disable profiler
        if ($this->getContainer()->has('profiler')) {
            $this->getContainer()->get('profiler')->disable();
        }
        $em = $this->getContainer()->get('doctrine');

        //get user
        //$user = $this->container->get('security.context')->getToken()->getUser();
        //$cc = $user->getIsAdmin() ? 0 : $user->getId();

        $download_path = $this->getContainer()->getParameter('download_excel_path');

        $phpExcelObject = $this->getContainer()->get('phpexcel')->createPHPExcelObject($download_path . 'CustomerDetailsReport.xls');

        //dump($phpExcelObject);
        $phpExcelObject->getProperties()->setCreator("Numa")
            ->setLastModifiedBy("Numa web App")
            ->setTitle("Customer Details Report")
            ->setSubject("Customer Details Report")
            ->setDescription("Customer Details Report");
        $phpExcelObject->setActiveSheetIndex(0);
        $activeSheet = $phpExcelObject->getActiveSheet();
        //count rows in excel
        $deptTotalRow = 0;
        /////// LOOP by Customer
        //dump($data['data']);
        //array_intersect_key($data['data'], array("CAME"=>1,"CUST"=>1,"CREDU"=>1)));
        //die();

        //$customers = $em->getRepository('NumaCCCAdminBundle:Customers')->findById($this->getCustomer());
        $customers = $this->getCustomers();

//        $intersectArray = array();
//
//        foreach ($customers as $customer) {
//            dump($customer);
//            die();
//            $intersectArray[$customer->getCustcode()] = 1;
//        }

        $data = $this->getTwigParam('data');

        $totals = $this->getTwigParam('totals');
        $vehTypes = $this->getTwigParam('vehTypes');

        //$dataData = array_intersect_key($data['data'], $intersectArray);
        $countRows = 0;
        $startRow = 7;
        $deptTotalRow = $countRows + $startRow;
        $headerRow = 4;
        //$customers = array_slice($customers, 0, 15);

        foreach ($customers as $customer) {

            $custKey = $customer->getCustcode();
            if (!empty($data[$custKey])) {
//                dump($custKey);
//                dump($totals[$custKey]);
//                dump($data[$custKey]);
//                die();
                $cust = $data[$custKey];

                $activeSheet->setTitle("CUST" . $cust['custcode']);
                //customer info
                $customerRow = $deptTotalRow-6;
                $activeSheet
                    ->setCellValue('B'.($customerRow+2), 'CUSTOMER: ' . $cust['name'])
                    ->setCellValue('B'.($customerRow+1), 'Customer Code: ' . $cust['custcode'])
                    ->setCellValue('W'.$customerRow, date('m/d/Y H:i'));
                //Loop by billtype hwy cty
//                $countRows = 0;
//                $startRow = 7;
//                $deptTotalRow = $countRows + $startRow;
//                $headerRow = 4;
                $copiedRows = $activeSheet->rangeToArray('B' . $headerRow . ':Z' . ($headerRow + 1));

                $style2 = $activeSheet->getStyle('B' . $headerRow);
                $style = $activeSheet->getStyle('C' . ($headerRow + 1));

                $hasCTY = empty($cust['CTY']) ? false : true;
                $hasHWY = empty($cust['HWY']) ? false : true;
                $onlyCty = $hasCTY && !$hasHWY;
                $onlyHWY = $hasHWY && !$hasCTY;
                $bothHWYCTY = $hasCTY && $hasHWY;

                foreach (array('CTY', 'HWY') as $billtype) {
                    //LOOP by dept

                    $headerRow = $deptTotalRow - 3;
                    if ($onlyHWY) {
                        $activeSheet->setCellValue('B' . $headerRow, 'HiWay Details: ');
                    }
                    if (!empty($cust[$billtype])) {

                        if ($bothHWYCTY && $billtype == 'HWY') {
                            $addRows = 4;
                            $activeSheet->insertNewRowBefore($deptTotalRow, $addRows);
                            $deptTotalRow += $addRows;
                            $headerRow += $addRows;
                            //dump($deptTotalRow);die();
                            $activeSheet->fromArray($copiedRows, null, 'B' . ($headerRow));

                            $activeSheet->mergeCells('B' . ($headerRow) . ':C' . ($headerRow));
                            $activeSheet->mergeCells('F' . ($headerRow + 1) . ':H' . ($headerRow + 1));
                            $activeSheet->mergeCells('I' . ($headerRow + 1) . ':J' . ($headerRow + 1));
                            $activeSheet->mergeCells('L' . ($headerRow + 1) . ':M' . ($headerRow + 1));
                            $activeSheet->mergeCells('T' . ($headerRow + 1) . ':U' . ($headerRow + 1));
                            $activeSheet->mergeCells('X' . ($headerRow + 1) . ':Z' . ($headerRow + 1));
                            $activeSheet->setCellValue('B' . ($headerRow), 'HiWay Details: ');
                            $activeSheet->duplicateStyle($style, 'b' . ($headerRow + 1) . ':z' . ($headerRow + 1));
                            $activeSheet->duplicateStyle($style2, 'b' . ($headerRow));
                        }


                        foreach ($cust[$billtype] as $deptKey => $dept) {
                            $deptKeyTrim = trim((string)$deptKey);
                            //check if dept is single and empty
                            if (count($cust[$billtype]) > 1 && !empty($deptKeyTrim)) {
                                $activeSheet->insertNewRowBefore($deptTotalRow);
                                $activeSheet->setCellValue('B' . ($deptTotalRow), 'Department: ' . $deptKey);
                                $activeSheet->mergeCells("B" . ($deptTotalRow) . ":C" . ($deptTotalRow));
                                $deptTotalRow++;
                            }

                            //loop by probills in dept

                            foreach ($dept['probills'] as $probillKey => $probill) {

                                $pdate = $probill['p_date'];
                                //insert row by row
                                $activeSheet
                                    ->insertNewRowBefore($deptTotalRow)
                                    ->setCellValue('B' . $deptTotalRow, $probill['waybill'])
                                    ->setCellValue('C' . $deptTotalRow, $pdate instanceof \DateTime ? $pdate->format('m/d/Y') : "")
                                    ->setCellValue('D' . $deptTotalRow, $probill['ref'])
                                    ->setCellValue('E' . $deptTotalRow, $probill['PCE'])
                                    ->setCellValue('F' . $deptTotalRow, $probill['WGT'])
                                    ->setCellValue('I' . $deptTotalRow, $probill['shipper'])
                                    ->setCellValue('K' . $deptTotalRow, $probill['receiver'])
                                    ->setCellValue('L' . $deptTotalRow, $probill['time'])
                                    ->setCellValue('N' . $deptTotalRow, $probill['signature'])
                                    ->setCellValue('P' . $deptTotalRow, $probill['driver'])
                                    ->setCellValue('Q' . $deptTotalRow, $probill['vehicle_code'])
                                    ->setCellValue('R' . $deptTotalRow, $probill['FS'])
                                    ->setCellValue('S' . $deptTotalRow, $probill['FSamt'])
                                    ->setCellValue('U' . $deptTotalRow, $probill['total'])
                                    ->setCellValue('W' . $deptTotalRow, $probill['serv_type'])
                                    ->setCellValue('X' . $deptTotalRow, $probill['details']);
                                $activeSheet->mergeCells("L" . $deptTotalRow . ":M" . $deptTotalRow);
                                $activeSheet->mergeCells("X" . $deptTotalRow . ":Z" . $deptTotalRow);
                                $deptTotalRow++;
                            }

                            // totals by dept

                            if (count($cust[$billtype]) > 0 && !empty($deptKeyTrim)) {
                                $activeSheet
                                    ->insertNewRowBefore($deptTotalRow)
                                    ->setCellValue('N' . $deptTotalRow, "Department Total:")
                                    ->setCellValue('S' . $deptTotalRow, $totals[$custKey][$billtype][$deptKey]['FSamt'])
                                    ->setCellValue('U' . $deptTotalRow, $totals[$custKey][$billtype][$deptKey]['total']);
                                $activeSheet->mergeCells("N" . ($deptTotalRow) . ":R" . ($deptTotalRow));
                                $deptTotalRow++;
                            }
                            //dump($deptTotalRow);die();
                        }
                    }

                    //total by customer billtype (HWY, CTY)
                    if (!empty($data['totals'][$custKey][$billtype]['total']) && !empty($data['totals'][$custKey][$billtype]['FSamt'])) {
                        $activeSheet->insertNewRowBefore($deptTotalRow);

                        $activeSheet
                            ->setCellValue('S' . $deptTotalRow, $totals[$custKey][$billtype]['FSamt'])
                            ->setCellValue('U' . $deptTotalRow, $totals[$custKey][$billtype]['total'])
                            ->setCellValue('E' . $deptTotalRow, $totals[$custKey][$billtype]['pce'])
                            ->setCellValue('F' . $deptTotalRow, $totals[$custKey][$billtype]['wgt']);
                        //$deptTotalRow++;
                        $activeSheet->mergeCells("B" . $deptTotalRow . ":D" . $deptTotalRow);
                        $activeSheet->mergeCells("K" . $deptTotalRow . ":P" . $deptTotalRow);

                        if ($billtype == 'CTY') {
                            $activeSheet->setCellValue("B" . $deptTotalRow, "City sub totals: ");
                            $activeSheet->setCellValue("K" . $deptTotalRow, "City subtotal before tax: ");
                        } elseif ($billtype == 'HWY') {
                            $activeSheet->setCellValue("B" . $deptTotalRow, "HiWay sub totals: ");
                            $activeSheet->setCellValue("K" . $deptTotalRow, "HiWay subtotal before tax: ");
                        }
                        $deptTotalRow++;
                    }
                }

                $deptTotalRow--;
                $activeSheet->removeRow($deptTotalRow, 2);
                $deptTotalRow++;
                $activeSheet->setCellValue("T" . $deptTotalRow, $totals[$custKey]['total']);
                $deptTotalRow++;
                $activeSheet->setCellValue("T" . $deptTotalRow, $totals[$custKey]['FSamt']);
                $activeSheet->mergeCells('T' . $deptTotalRow . ':U' . $deptTotalRow);

                $deptTotalRow++;
                $activeSheet->setCellValue("T" . $deptTotalRow, ($totals[$custKey]['FSamt'] + $totals[$custKey]['total']));
                //$phpExcelObject->getActiveSheet()->duplicateStyle($style, 'B' . ($headerRow + 3) . ':Z' . ($headerRow + 3));
                //$phpExcelObject->getActiveSheet()->duplicateStyle($style, 'B' . ($headerRow + 3) . ':Z' . ($headerRow + 3));
                $deptTotalRow = $deptTotalRow+10;

                //EXCEL format//


                //$filename = $download_path . $this->getName() . '_' . $this->getBillingPeriod()->getId() . '_' . $this->getCustomer()->getCustcode() . ".xls";

                //$writer->save($this->getFilename(true, "xls"));
// create the response
//                $response = $this->getContainer()->get('phpexcel')->createStreamedResponse($writer);
//                $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
//                $response->headers->set('Content-Disposition', 'attachment;filename='.$this->getFilename(false, "xls"));
//                $response->headers->set('Pragma', 'public');
//                $response->headers->set('Cache-Control', 'maxage=1');
                //$writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel5');
            }
        }
        $writer = $this->getContainer()->get('phpexcel')->createWriter($phpExcelObject, 'Excel5');

        $download_path = $this->getContainer()->getParameter('download_excel_path');
        if ($phpExcelObject instanceof \PHPExcel) {
            $objWriter = $this->getContainer()->get('phpexcel')->createWriter($phpExcelObject, 'Excel5');
//
            if ($content) {

                $tmpfile = tempnam(sys_get_temp_dir(), 'xls');
                $objWriter->save($tmpfile);

                return file_get_contents($tmpfile);
            } else {
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header('Content-Disposition: attachment;filename=' . $this->getFilename(false, "xls"));
                header('Cache-Control: max-age=0');

                //$objWriter = $this->getContainer()->get('phpexcel')->createWriter($phpExcelObject, 'Excel5');
                $objWriter->save('php://output');
            }
        }
        // return $response;


    }


}