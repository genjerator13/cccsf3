<?php

/**
 * Created by PhpStorm.
 * User: genjerator
 * Date: 5.2.18.
 * Time: 13.59
 */
namespace Numa\CCCAdminBundle\Lib\Report;

use Numa\CCCAdminBundle\Entity\BillingPeriod;
use Numa\CCCAdminBundle\Entity\Customers;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\Common\Collections\ArrayCollection;

class InvoiceReport extends BillingReport
{
    use CustomersAwareTrait;

    public function __construct($container, $billingPeriod)
    {
        parent::__construct($container, $billingPeriod);
        $this->setName("invoice");
        $this->setTitle("Customer Invoice");
        $this->init();
    }

    /**
     * @param bool $stream
     * @return Response
     */

    public function renderPDF2($saveTo = "stream")
    {
        $loggor = $this->getContainer()->get('logger');
        $em = $this->getContainer()->get('doctrine')->getManager();
        //get all probills for the customer
        $path = $this->getContainer()->get('kernel')->getRootDir() . "/../web/";

        $templating = $this->getContainer()->get('templating');

        $double = true;


        $html = $templating->render(
            'NumaCCCAdminBundle:reports:headerPdf.pdf.twig', array(
                'path' => $path
            )
        );
//        $this->setSnailCustomers();
//
//        $this->setCustomers($this->getCustomers());

        $customers = new ArrayCollection($this->getCustomers());

        //foreach ($this->getCustomers() as $key => $customer) {
        $customer = $customers->current();
        $i = 0;
        while ($customer instanceof Customers) {

            //$customer = $customers->current();
            $probills = $em->getRepository('NumaCCCAdminBundle:Probills')->findAllByCustomerInBillingPeriod($customer, $this->getBillingPeriod());
            $totals = $em->getRepository('NumaCCCAdminBundle:Probills')->getTotals($probills, $customer->getDiscount());

            if ($totals['total'] != 0) {
                $class = $i%2==0?"odd":"even";
                $html .= $templating->render(
                    $this->getTwigLayoutFile(), array(
                        'probills' => $totals,
                        'customer' => $customer,
                        'billing_period' => $this->getBillingPeriod(),
                        'path' => $path,
                        'i'=>$i,
                        'class' => $class
                    )
                );
                $i++;
            }


//            if($i==10){
//                ;die("AAAAAAA");
//            }
            $customer = $customers->next();
        }

        $loggor->warning("filename:" . $this->getFilename());
        $filelocation = $this->getFilename();
        $outputType = "pdf";
        $loggor->warning("outputType:" . $outputType);
        if ($saveTo=="file") {

            $this->getContainer()->get('knp_snappy.pdf')->generateFromHtml($html, $this->getFilename(true) . '.pdf', array(), true);
        }elseif($saveTo=="content"){
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $html);
            //$download_path = $this->getContainer()->getParameter('download_excel_path');
            //$filename = $download_path . $this->getName() . '_' . $this->getBillingPeriod()->getId() . '_' . $this->getCustomer()->getCustcode() . ".pdf";

            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            $this->getContainer()->get('knp_snappy.pdf')->generateFromHtml($html, $tmpfile , array(), true);

            $file = fopen($tmpfile, "rd");
            $filecontent = file_get_contents($tmpfile);
            return $filecontent;
        }else {

            return new Response(

                $this->getContainer()->get('knp_snappy.pdf')->getOutputFromHtml($html), 200, array(
                    'Content-Type' => 'application/pdf',
                    'Content-Disposition' => 'attachment; filename="' . $this->getFilename(false) . '"'
                )
            );
        }


    }


}