<?php

/**
 * Created by PhpStorm.
 * User: genjerator
 * Date: 5.2.18.
 * Time: 13.59
 */
namespace Numa\CCCAdminBundle\Lib\Report;
use Numa\CCCAdminBundle\Entity\BillingPeriod;
use Numa\CCCAdminBundle\Entity\Probills;

class MiscReport extends BillingReport
{
    public function __construct($container, $billingPeriod)
    {
        parent::__construct($container,$billingPeriod);
        $this->setName("misc");
        $this->setTitle("Misc Report");
        $this->init();
    }

    public function setPeriods($periods){
        $p = array();
        foreach($periods as $period){
            $p[]=$period;
        }
        $this->periods = $p;

    }

    public function prepareDataForBillingPeriod(){
        $em = $this->getContainer()->get("doctrine")->getManager();
        //$data = $em->getRepository(Probills::class)->trailerData($this->getBillingPeriod(),'misc');

        $data = $em->getRepository(Probills::class)->trailerData($this->periods,"misc");
        $this->addTwigParams('probills',$data);
    }



    public function finalExcelWork(\PHPExcel &$objExcel){
        $styleLeft = array(
            'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
            )
        );
        $objExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
        $objExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
        $objExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $objExcel->getActiveSheet()->getColumnDimension('D')->setWidth(60);
        $objExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
        $objExcel->getActiveSheet()->getStyle('A1:X1' )->getFont()->setBold( true );
        $objExcel->getDefaultStyle()->applyFromArray($styleLeft);
    }
}