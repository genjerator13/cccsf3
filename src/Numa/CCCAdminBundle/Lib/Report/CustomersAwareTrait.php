<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Numa\CCCAdminBundle\Lib\Report;
use Numa\CCCAdminBundle\Entity\Customers;

/**
 * ContainerAware trait.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
trait CustomersAwareTrait
{
    public $customers;

    public function setAllCustomers($billingPeriod=null)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $customers = $em->getRepository(Customers::class)->findOnlyActive($billingPeriod);

        $this->setCustomers($customers);
    }

    public function setSnailCustomers()
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $customers = $em->getRepository(Customers::class)->findBy(['sendmail'=>'N'],array('custcode'=>"ASC"));

        $this->setCustomers($customers);

    }

    /**
     * @return mixed
     */
    public function getCustomers()
    {
        return $this->customers;
    }

    /**
     * @param mixed $customers
     */
    public function setCustomers($customers)
    {
        $this->customers = $customers;
    }
}