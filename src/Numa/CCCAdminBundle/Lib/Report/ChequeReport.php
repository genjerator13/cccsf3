<?php

/**
 * Created by PhpStorm.
 * User: genjerator
 * Date: 5.2.18.
 * Time: 13.46
 */
namespace Numa\CCCAdminBundle\Lib\Report;
use Numa\CCCAdminBundle\Entity\BillingPeriod;

use Numa\CCCAdminBundle\Entity\Drivers;
use Numa\CCCAdminBundle\Entity\PayPeriod;
use Symfony\Component\HttpFoundation\Response;

class ChequeReport extends Report
{
    private $drivers;
    private $cheques;
    private $payPeriod;

    public function __construct($container)
    {

        parent::__construct($container);
        $this->init();
    }

    public function prepareDataForBillingPeriod(){

    }

    public function setTwigFiles(){
        $this->twigLayoutPage   = "NumaCCCAdminBundle:report:cheque/" . $this->getName() . "/layout.pdf.twig";
        $this->twigHeaderPage   = "NumaCCCAdminBundle:report:cheque/" . $this->getName() . "/header.pdf.twig";
        $this->twigFooterPage   = "NumaCCCAdminBundle:report:cheque/" . $this->getName() . "/footer.pdf.twig";
        $this->twigContentPage   = "NumaCCCAdminBundle:report:cheque/" . $this->getName() . "/content.html.twig";
    }


    /**
     * @return PayPeriod
     */
    public function getPayPeriod()
    {
        return $this->payPeriod;
    }

    /**
     * @param PayPeriod $payPeriod
     */
    public function setPayPeriod($payPeriod)
    {
        $this->payPeriod = $payPeriod;
    }

    /**
     * @return mixed
     */
    public function getDrivers()
    {
        return $this->drivers;
    }

    /**
     * @param mixed $drivers
     */
    public function setDrivers($drivers)
    {
        $this->drivers = $drivers;
    }

    /**
     * @return mixed
     */
    public function getCheques()
    {
        return $this->cheques;
    }

    /**
     * @param mixed $cheques
     */
    public function setCheques($cheques)
    {
        $this->cheques = $cheques;
    }

    public function getFilename($absolute=true,$format="pdf"){
        $download_path = $this->getContainer()->getParameter('download_excel_path');
        $part1 = "";
        $partDriver = "ALL";
        if($this->getPayPeriod() instanceof PayPeriod){
            $part1 = $this->getPayPeriod()->getId();
        }
        if($this->getDrivers() instanceof Drivers){
            $partDriver = $this->getDrivers()->getDrivnum() ;
        }

        $filename = $part1."/".$this->getName() . '_' . $part1 . '_' . $partDriver . ".".$format;

        if($absolute){
           $filename =  $download_path . $filename;
        }
        return $filename;
    }

}