<?php

/**
 * Created by PhpStorm.
 * User: genjerator
 * Date: 5.2.18.
 * Time: 13.59
 */
namespace Numa\CCCAdminBundle\Lib\Report;
use Numa\CCCAdminBundle\Entity\Batch;
use Numa\CCCAdminBundle\Entity\Drivers;

class DriverDetailsReport extends BillingReport
{
    public $batch;
    public function __construct($container,$billingPeriod)
    {
        parent::__construct($container,$billingPeriod);
        $this->setName("driverDetails");
        $this->setTitle("Driver Details Report");
        $this->init();

    }

    public function setBatch(Batch $batch){
        $this->batch=$batch;
    }

    public function prepareDataForBillingPeriod()
    {
        $em = $this->getContainer()->get("doctrine")->getManager();
        if($this->batch instanceof Batch){
            $data = $em->getRepository(Drivers::class)->driverDetailsData($this->batch, 'drivers');
        }else {
            $data = $em->getRepository(Drivers::class)->driverDetailsData($this->getBillingPeriod(), 'drivers');
        }

        $vehtypes = $em->getRepository('NumaCCCAdminBundle:Vehtypes')->findBy(array('active' => 1));
        $this->addTwigParams('vehTypes',$vehtypes);
        $this->addTwigParams('data',$data);
    }

    public function rendermPDF($twigParams=array(),$content=false)
    {
        $this->prepareDataForBillingPeriod();
        $mpdf = new \Mpdf\Mpdf(array('format' => 'letter-L', "margin_left" => 5, "margin_right" => 5, "margin_top" => 5, "margin_bottom" => 5));


        $mpdf->useOnlyCoreFonts = true;    // false is default
        $mpdf->SetTitle($this->getTitle());
        $mpdf->SetAuthor($this->getAuthor());
        $mpdf->SetDisplayMode('fullpage');

        $templating = $this->getContainer()->get('templating');
        $twigFile = "NumaCCCAdminBundle:report:" . $this->getName() . "/layout.pdf.twig";
        $twigDriver = "NumaCCCAdminBundle:report:" . $this->getName() . "/driverSingle.html.twig";
        $twigHeader = "NumaCCCAdminBundle:report:" . $this->getName() . "/header.html.twig";
        $twigFooter = "NumaCCCAdminBundle:report:" . $this->getName() . "/footer.html.twig";
        $data = $this->getTwigParams()['data'];

        $em = $this->getContainer()->get("doctrine")->getManager();
        $vehtypes = $em->getRepository('NumaCCCAdminBundle:Vehtypes')->findBy(array('active' => 1));
        $this->addTwigParams('vehTypes', $vehtypes);
        $html = $templating->render($twigHeader);
        $mpdf->WriteHTML($html);
        //$data = array_slice($data, 20, 26);

        foreach($data as $key=>$driver){

            if(!empty($driver) && !empty(str_replace(array("(",")"),"",$key))) {
//                dump($key);
//                array_shift($driver);
//                dump($driver);
//                dump("AAAAA");
//                die();
                $html ="<table >";
                $html .= $templating->render($twigDriver, array("driver" => $driver, "key" => $key,"vehTypes"=>$vehtypes));
                $html.="</table>";
                $html.="<div class='newpage'></div>";

//                $html = $templating->render("NumaCCCAdminBundle:report:vehTypesTable.html.twig", array("vehTypes"=>$vehtypes));
                $mpdf->WriteHTML($html);
            }

        }

        $html = $templating->render($twigFooter);
        $mpdf->WriteHTML($html);
        //dump($html);die();
        if($content) {
            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            file_put_contents($tmpfile, $html);
            //$download_path = $this->getContainer()->getParameter('download_excel_path');
            //$filename = $download_path . $this->getName() . '_' . $this->getBillingPeriod()->getId() . '_' . $this->getCustomer()->getCustcode() . ".pdf";

            $tmpfile = tempnam(sys_get_temp_dir(), 'html');
            $mpdf->Output($tmpfile, "F");
            $file = fopen($tmpfile, "rd");
            $filecontent = file_get_contents($tmpfile);
            return $filecontent;
        }else{
            $mpdf->WriteHTML($html);

            header('Content-Type: application/pdf');
            header('Content-Disposition: attachment;filename="' . $this->getFilename());
            header('Cache-Control: max-age=0');
            $mpdf->Output();
            exit;
        }
    }
}