<?php

/**
 * Created by PhpStorm.
 * User: genjerator
 * Date: 5.2.18.
 * Time: 13.59
 */
namespace Numa\CCCAdminBundle\Lib\Report\Cheque;

use Numa\CCCAdminBundle\Entity\BillingPeriod;
use Numa\CCCAdminBundle\Entity\Cheque;
use Numa\CCCAdminBundle\Entity\PayPeriod;
use Numa\CCCAdminBundle\Lib\Report\ChequeReport;

class ChequePrintReport extends ChequeReport
{
    public function __construct($container, PayPeriod $payPeriod)
    {
        parent::__construct($container);
        $this->setPayPeriod($payPeriod);
        $this->setName("Cheque");
        $this->setTitle("WCB Report");
        $this->init();
        $this->orientation = "portrait";
        $this->addTwigParams('payPeriod',$payPeriod);

    }

    public function prepareDataForBillingPeriod(){

        $this->addTwigParams('cheques',$this->getCheques());
    }

    public function finalExcelWork(\PHPExcel &$objExcel){
        $styleLeft = array(
            'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
            )
        );
        $objExcel->getDefaultStyle()->applyFromArray($styleLeft);
        $objExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
        $objExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
        $objExcel->getActiveSheet()->getColumnDimension('C')->setWidth(60);
        $objExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $objExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
        //$objExcel->getActiveSheet()->getStyle('A1:X1' )->getFont()->setBold( true );

    }
    public function rendermPDF($twigParams=array())
    {
        $this->prepareDataForBillingPeriod();
        $mpdf = new \Mpdf\Mpdf(array('orientation' => 'P','format' => 'A4', "margin_left" => 5, "margin_right" => 5, "margin_top" => 5, "margin_bottom" => 5));

        $mpdf->useOnlyCoreFonts = true;    // false is default
        $mpdf->SetTitle($this->getTitle());

        $mpdf->SetAuthor($this->getAuthor());
        $mpdf->SetDisplayMode('fullpage');

        $templating = $this->getContainer()->get('templating');


        $html = $templating->render( $this->getTwigHeaderFile(), $this->getTwigParams() );
        $mpdf->WriteHTML($html);
        $html = $templating->render( $this->twigLayoutPage, $this->getTwigParams() );
        $mpdf->WriteHTML($html);
        $html = $templating->render( $this->getTwigFooterFile(), $this->getTwigParams() );
        $mpdf->WriteHTML($html);
        header('Content-Type: application/pdf');
        header('Content-Disposition: attachment;filename="' .$this->getFilename());
        header('Cache-Control: max-age=0');
        $mpdf->Output();
        exit;
    }

}