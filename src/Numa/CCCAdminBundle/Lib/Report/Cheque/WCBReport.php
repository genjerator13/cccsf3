<?php

/**
 * Created by PhpStorm.
 * User: genjerator
 * Date: 5.2.18.
 * Time: 13.59
 */
namespace Numa\CCCAdminBundle\Lib\Report\Cheque;

use Numa\CCCAdminBundle\Entity\BillingPeriod;
use Numa\CCCAdminBundle\Entity\Cheque;
use Numa\CCCAdminBundle\Entity\PayPeriod;
use Numa\CCCAdminBundle\Lib\Report\ChequeReport;

class WCBReport extends ChequeReport
{
    public function __construct($container, PayPeriod $payPeriod)
    {
        parent::__construct($container);
        $this->setPayPeriod($payPeriod);
        $this->setName("WCB");
        $this->setTitle("WCB Report");
        $this->init();
        $this->orientation = "portrait";
        $this->addTwigParams('payPeriod',$payPeriod);

    }

    public function prepareDataForBillingPeriod(){
        $em = $this->getContainer()->get('doctrine')->getManager();
        $cheques = $em->getRepository(Cheque::class)->findAllInPayperiod($this->getPayPeriod()->getId());
        $this->addTwigParams('cheques',$cheques);
    }

    public function finalExcelWork(\PHPExcel &$objExcel){
        $styleLeft = array(
            'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
            )
        );
        $objExcel->getDefaultStyle()->applyFromArray($styleLeft);
        $objExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
        $objExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
        $objExcel->getActiveSheet()->getColumnDimension('C')->setWidth(60);
        $objExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $objExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
        //$objExcel->getActiveSheet()->getStyle('A1:X1' )->getFont()->setBold( true );

    }


}