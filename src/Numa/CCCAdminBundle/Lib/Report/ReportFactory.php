<?php
/**
 * Created by PhpStorm.
 * User: genjerator
 * Date: 10.2.18.
 * Time: 20.59
 */

namespace Numa\CCCAdminBundle\Lib\Report;


use Numa\CCCAdminBundle\Entity\BillingPeriod;
use Numa\CCCAdminBundle\Entity\Cheque;
use Symfony\Component\BrowserKit\Request;

class ReportFactory
{
    public static function create($container, $report, BillingPeriod $billingPeriod, $args=array())
    {

        if ($report == 'delivery_all' || $report=='delivery_snail') {
            return new CustomerDeliveryReport($container, $billingPeriod);
        } elseif ($report == 'customer_summary') {
            return new CustomerSummaryReport($container, $billingPeriod);
        } elseif ($report == 'driver_summary') {
            return new DriverSummaryReport($container, $billingPeriod);
        } elseif ($report == 'driver_details') {
            return new DriverDetailsReport($container, $billingPeriod);
        } elseif ($report == 'trailer') {
            $trailerReport= new TrailerReport($container, $billingPeriod);
            $trailerReport->setPeriods($args);
            return $trailerReport;
        } elseif ($report == 'misc') {
            $miscReport = new MiscReport($container, $billingPeriod);
            $miscReport->setPeriods($args);
            return $miscReport;
        } elseif ($report == 'vehicles') {
            return new VehiclesSummaryReport($container, $billingPeriod);
        } elseif ($report == 'invoices') {
            return new InvoiceReport($container, $billingPeriod);
        }elseif ($report == 'remittance') {
            return new RemittanceReport($container, $billingPeriod);
        }

    }

    public static function createChequeReport($container,Cheque $cheque){
        return new ChequeReport($container, $cheque);
    }
}