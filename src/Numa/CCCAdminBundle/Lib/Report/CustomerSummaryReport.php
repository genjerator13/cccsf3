<?php

/**
 * Created by PhpStorm.
 * User: genjerator
 * Date: 5.2.18.
 * Time: 13.59
 */
namespace Numa\CCCAdminBundle\Lib\Report;
use Numa\CCCAdminBundle\Entity\Customers;

class CustomerSummaryReport extends BillingReport
{
    public function __construct($container,$billingPeriod)
    {
        parent::__construct($container,$billingPeriod);
        $this->setName("customerSummary");
        $this->setTitle("Customer Summary Report");
        $this->init();
    }

    public function prepareDataForBillingPeriod()
    {
        $em = $this->getContainer()->get("doctrine")->getManager();
        $data = $em->getRepository(Customers::class)->customerSummaryData($this->getBillingPeriod());
        $this->addTwigParams('customers',$data['data']);
        $this->addTwigParams('total',$data['total']);
    }


    public function finalExcelWork(\PHPExcel &$objExcel){
        $styleLeft = array(
            'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
            )
        );
        $objExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
        $objExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
        $objExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $objExcel->getActiveSheet()->getColumnDimension('D')->setWidth(60);
        $objExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
        //$objExcel->getActiveSheet()->getStyle('A1:X1' )->getFont()->setBold( true );
        $objExcel->getDefaultStyle()->applyFromArray($styleLeft);
    }
}