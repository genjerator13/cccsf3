<?php

/**
 * Created by PhpStorm.
 * User: genjerator
 * Date: 5.2.18.
 * Time: 13.59
 */
namespace Numa\CCCAdminBundle\Lib\Report;
use Numa\CCCAdminBundle\Entity\Customers;
use Numa\CCCAdminBundle\Entity\Drivers;

class DriverSummaryReport extends BillingReport
{
    public function __construct($container,$billingPeriod)
    {
        parent::__construct($container,$billingPeriod);
        $this->setName("driverSummary");
        $this->setTitle("Driver Summary Report");
        $this->init();
    }

    public function prepareDataForBillingPeriod()
    {
        $em = $this->getContainer()->get("doctrine")->getManager();
        $data = $em->getRepository(Customers::class)->customerSummaryData($this->getBillingPeriod(),'drivers');

        $this->addTwigParams('customers',$data['data']);
        $this->addTwigParams('total',$data['total']);
    }
}