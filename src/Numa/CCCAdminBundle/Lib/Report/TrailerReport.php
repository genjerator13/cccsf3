<?php

/**
 * Created by PhpStorm.
 * User: genjerator
 * Date: 5.2.18.
 * Time: 13.59
 */
namespace Numa\CCCAdminBundle\Lib\Report;
use Numa\CCCAdminBundle\Entity\Probills;

class TrailerReport extends BillingReport
{
    public $periods=array();
    public function __construct($container,$billingPeriod)
    {
        parent::__construct($container,$billingPeriod);
        $this->setName("trailer");
        $this->setTitle("Trailer Report");
        $this->init();
    }

    public function setPeriods($periods){
        $p = array();
        foreach($periods as $period){
            $p[]=$period;
        }
        $this->periods = $p;

    }

    public function prepareDataForBillingPeriod()
    {
        $em = $this->getContainer()->get("doctrine")->getManager();

        $data = $em->getRepository(Probills::class)->trailerData($this->periods);
        $this->addTwigParams('probills',$data);
    }

    public function finalExcelWork(\PHPExcel &$objExcel){
        $styleLeft = array(
            'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
            )
        );
        $objExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
        $objExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
        $objExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $objExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $objExcel->getActiveSheet()->getColumnDimension('E')->setWidth(60);
        $objExcel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
        $objExcel->getActiveSheet()->getStyle('A1:X1' )->getFont()->setBold( true );
        $objExcel->getDefaultStyle()->applyFromArray($styleLeft);
    }
}