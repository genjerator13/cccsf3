<?php

/**
 * Created by PhpStorm.
 * User: genjerator
 * Date: 5.2.18.
 * Time: 13.46
 */
namespace Numa\CCCAdminBundle\Lib\Report;
use Numa\CCCAdminBundle\Entity\BillingPeriod;
use Numa\CCCAdminBundle\Entity\Customers;
use Symfony\Component\HttpFoundation\Response;

class Report
{
    private $container;
    private $content;
    private $name;
    public $title;
    private $author;
    private $twigParams=array();
    public $topMargin=5;
    public $bottomMargin=5;
    public $leftMargin=5;
    public $rightMargin=5;
    public $orientation="landscape";


    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getContent()
    {
        return $this->content;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $name string
     */
    public function setName($name)
    {
        $this->name = $name;
    }


    public function __construct($container)
    {

        $this->container=$container;
        $this->title = "Report";
        $this->setName("report");
        $this->author = "Custom Courier Co.";

        $this->init();

    }

    public function init(){
        $this->setTwigParams(array(
                "title"=>$this->getTitle(),
                "name"=>$this->getName())
        );
        $this->setTwigFiles();
    }


    public function store()
    {

    }

    public function setTwigFiles(){
        $this->twigLayoutPage   = "NumaCCCAdminBundle:report:" . $this->getName() . "/layout.pdf.twig";
        $this->twigHeaderPage   = "NumaCCCAdminBundle:report:" . $this->getName() . "/header.pdf.twig";
        $this->twigFooterPage   = "NumaCCCAdminBundle:report:" . $this->getName() . "/footer.pdf.twig";
        $this->twigContentPage   = "NumaCCCAdminBundle:report:" . $this->getName() . "/content.html.twig";

    }

    public function getTwigLayoutFile(){
        return $this->twigLayoutPage;
    }
    public function getTwigHeaderFile(){
        return $this->twigHeaderPage;
    }
    public function getTwigFooterFile(){
        return $this->twigFooterPage;
    }

    public function renderPDF($twigParams=array())
    {

        $this->prepareDataForBillingPeriod();


        $templating = $this->container->get('templating');
        $html = $templating->render( $this->twigLayoutPage, $this->getTwigParams() );

        $options = [
            'margin-top'    => $this->topMargin,
            'margin-right'  => $this->rightMargin,
            'margin-bottom' => $this->bottomMargin,
            'margin-left'   => $this->leftMargin,
            'orientation'   => $this->orientation
        ];

        return new Response(
            $this->container->get('knp_snappy.pdf')->getOutputFromHtml($html,$options), 200, array(
                'Content-Type' => 'application/pdf',
            )
        );

    }
    public function getExcelObject(){
        $this->prepareDataForBillingPeriod();


        $templating = $this->container->get('templating');
        $html = $templating->render( $this->twigContentPage, $this->getTwigParams() );

        $options = [
            'margin-top'    => $this->topMargin,
            'margin-right'  => $this->rightMargin,
            'margin-bottom' => $this->bottomMargin,
            'margin-left'   => $this->leftMargin,
            'orientation'   => $this->orientation
        ];

        $objExcel = $this->container->get("numa.report")->htmlToExcelObject($html,$this->getTitle(),$this->getFilename(false,"xls"));

        return $objExcel;
    }
    public function renderXLS($twigParams=array(),$content=false){

        $objExcel = $this->getExcelObject($twigParams);
        $this->finalExcelWork($objExcel);
        $objWriter = $this->getContainer()->get('phpexcel')->createWriter($objExcel, 'Excel2007');
        if($objExcel instanceof \PHPExcel) {
            if ($content) {

                $tmpfile = tempnam(sys_get_temp_dir(), 'xls');
                $objWriter->save($tmpfile);
                return file_get_contents($tmpfile);
            } else {
                $tmpfile = tempnam(sys_get_temp_dir(), 'xls');
                //$objWriter->save("/var/www/cccsf3/web/upload/test.xls");
                //dump($tmpfile);die();
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header('Content-Disposition: attachment;filename=' . $this->getFilename(false, "xls"));
                header('Cache-Control: max-age=0');
                $objWriter->save('php://output');
                die();
            }
        }
    }
    public function finalExcelWork(\PHPExcel &$objExcel){

    }


    public function rendermPDF($twigParams=array())
    {
        $this->prepareDataForBillingPeriod();
        $mpdf = new \Mpdf\Mpdf(array('format' => 'letter-L', "margin_left" => 5, "margin_right" => 5, "margin_top" => 5, "margin_bottom" => 5));

        $mpdf->useOnlyCoreFonts = true;    // false is default
        $mpdf->SetTitle($this->getTitle());
        $mpdf->SetAuthor($this->author);
        $mpdf->SetDisplayMode('fullpage');

        $templating = $this->container->get('templating');


        $html = $templating->render( $this->twigLayoutPage, $this->getTwigParams() );

        $mpdf->WriteHTML($html);

        header('Content-Type: application/pdf');
        header('Content-Disposition: attachment;filename="' .$this->getFilename());
        header('Cache-Control: max-age=0');
        $mpdf->Output();
        exit;
    }

    /**
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param string $author
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }

    /**
     * @return mixed
     */
    public function getTwigParams()
    {
        return $this->twigParams;
    }

    /**
     * @return mixed
     */
    public function getTwigParam($name)
    {
        return $this->twigParams[$name];
    }

    /**
     * @param mixed $twigParams
     */
    public function setTwigParams($twigParams)
    {
        $this->twigParams = $twigParams;
    }

    /**
     * @param mixed $twigParams
     */
    public function addTwigParams($name,$value)
    {
        $this->twigParams[$name]=$value;

    }

    /**
     * @return mixed
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @param mixed $container
     */
    public function setContainer($container)
    {
        $this->container = $container;
    }



}