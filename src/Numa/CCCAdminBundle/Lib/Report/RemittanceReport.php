<?php

/**
 * Created by PhpStorm.
 * User: genjerator
 * Date: 5.2.18.
 * Time: 13.59
 */
namespace Numa\CCCAdminBundle\Lib\Report;

use Numa\CCCAdminBundle\Entity\BillingPeriod;
use Numa\CCCAdminBundle\Entity\Customers;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\Common\Collections\ArrayCollection;

class RemittanceReport extends BillingReport
{
    use CustomersAwareTrait;
    public $customers;

    public function __construct($container, $billingPeriod)
    {
        parent::__construct($container, $billingPeriod);
        $this->setName("remittance");
        $this->setTitle("Remittance Report");
        $this->orientation="landscape";
        $this->init();
    }

    public function finalExcelWork(\PHPExcel &$objExcel){
        $styleLeft = array(
            'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
            )
        );
        $objExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
        $objExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
        $objExcel->getActiveSheet()->getColumnDimension('C')->setWidth(50);
        $objExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
        $objExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
        $objExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
        $objExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
        $objExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
        $objExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
        $objExcel->getActiveSheet()->getStyle('A1:X1' )->getFont()->setBold( true );
        $objExcel->getDefaultStyle()->applyFromArray($styleLeft);
    }

    public function prepareDataForBillingPeriod()
    {
        $this->setAllCustomers($this->getBillingPeriod());
        $em = $this->getContainer()->get('doctrine')->getManager();

        foreach ($this->getCustomers() as $key => $customer) {
            $probills = $em->getRepository('NumaCCCAdminBundle:Probills')->findAllByCustomerInBillingPeriod($customer, $this->getBillingPeriod(),"invoice");

            $totals = $em->getRepository('NumaCCCAdminBundle:Probills')->getTotals($probills, $customer->getDiscount());
            //if($totals['total']!=0) {
                $temp['customer'] = $customer;
                $temp['probills'] = $totals;
                $data[] = $temp;
            //}
        }
        $this->addTwigParams("probills",$data);
        $this->addTwigParams("billing_period",$this->getBillingPeriod());
    }

}