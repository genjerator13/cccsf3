<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of services
 *
 * @author genjerator
 */

namespace Numa\CCCAdminBundle\Lib;

use Numa\CCCAdminBundle\Entity\BillingPeriod;
use Numa\CCCAdminBundle\Entity\BillingReport;
use Numa\CCCAdminBundle\Entity\Customers;
use Numa\CCCAdminBundle\Entity\Media;
use Numa\CCCAdminBundle\Entity\Probills;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class MediaLib
{
    use ContainerAwareTrait;


    protected $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    public function showOverlayAction(Media $media)
    {
        $templating = $this->container->get("templating");
        //dump("AAAA");die();
        return $templating->render('NumaCCCAdminBundle:Media:media.pdf.twig', array(
            'media' => $media,
        ));
    }

    public function downloadFromTmp(Media $media)
    {

        $tmpfile = sys_get_temp_dir().'/'.$media->getName();
        file_put_contents($tmpfile,  base64_decode($media->getContent()));
        //$download_path = $this->getContainer()->getParameter('download_excel_path');
        //$filename = $download_path . $this->getName() . '_' . $this->getBillingPeriod()->getId() . '_' . $this->getCustomer()->getCustcode() . ".pdf";
        header("Cache-Control: public");
        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=".$media->getName());
        header("Content-Type: Content-Type: application/pdf");
        header("Content-Transfer-Encoding: binary");

        // read the file from disk
        readfile($tmpfile);
        die();
    }

    public function getDriverDetailReportForBillingPeriod(BillingPeriod $billingPeriod,$format='pdf'){
        $em = $this->container->get("doctrine")->getManager();

        $br = $em->getRepository(BillingReport::class)->getReportForBillingPeriod($billingPeriod->getId(),"driver_details_".$format);
        if(!empty($br[0]) && $br[0] instanceof BillingReport){
            return $br[0]->getMedia();
        }
        return null;
    }

}
