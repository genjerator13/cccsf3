<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of services
 *
 * @author genjerator
 */

namespace Numa\CCCAdminBundle\Lib;

use Doctrine\Common\Collections\Criteria;
use Numa\CCCAdminBundle\Entity\Batch;
use Numa\CCCAdminBundle\Entity\BillingPeriod;
use Numa\CCCAdminBundle\Entity\Cheque;
use Numa\CCCAdminBundle\Entity\Customers;
use Numa\CCCAdminBundle\Entity\CustomRate;
use Numa\CCCAdminBundle\Entity\CustomRateValue;
use Numa\CCCAdminBundle\Entity\Dispatchcard;
use Numa\CCCAdminBundle\Entity\Drivers;
use Numa\CCCAdminBundle\Entity\PayCode;
use Numa\CCCAdminBundle\Entity\PayPeriod;
use Numa\CCCAdminBundle\Entity\Probills;
use Numa\CCCAdminBundle\Entity\Rates;
use Numa\CCCAdminBundle\Entity\Vehtypes;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\HttpFoundation\JsonResponse;

class ChequeServices
{
    use ContainerAwareTrait;

    protected $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    public function bindChequeFromArray($data, Cheque $cheque)
    {

        $em = $this->container->get('doctrine.orm.entity_manager');
        $driver = $this->container->get('numa.common')->getEntity($this->container->get('numa.common')->getValueFromArray($data,'driver_pid'), Drivers::class, $em);
        $paycode = $this->container->get('numa.common')->getEntity($this->container->get('numa.common')->getValueFromArray($data,'paycode_pid'), PayCode::class, $em);
        $payPeriodId = $this->container->get('numa.common')->getValueFromArray($data,'pay_period_id');
        $payPeriod = $this->container->get('numa.common')->getEntity($payPeriodId, PayPeriod::class, $em);
        $user = $this->container->get('security.token_storage')->getToken()->getUser();

        $cheque->setUser($user);

        $year = $this->container->get('numa.common')->getValueFromArray($data,'year');
        $month = $this->container->get('numa.common')->getValueFromArray($data,'month');
        $day = $this->container->get('numa.common')->getValueFromArray($data,'day');
        $cdate = $this->container->get('numa.common')->getValueFromArray($data,'cheque_date');
        $chequeDate = new \DateTime();

        $chequeDate->setDate($year,$month,$day)->format("Y-m-d");
        $cheque->setChequeDate($chequeDate);
        $cheque->setDriver($driver);
        $cheque->setPayCode($paycode);
        $cheque->setPayPeriod($payPeriod);
        $cheque->setPayPeriodId($this->container->get('numa.common')->getValueFromArray($data,'pay_period_id'));
        $cheque->setRefNum($this->container->get('numa.common')->getValueFromArray($data,'ref_num'));
        $cheque->setLumpSumPayAmt($this->container->get('numa.common')->getValueFromArray($data,'lump_sum_pay_amt'));
        $cheque->setHours($this->container->get('numa.common')->getValueFromArray($data,'hours'));
        $cheque->setHrate($this->container->get('numa.common')->getValueFromArray($data,'hrate'));
        $cheque->setHtotal($this->container->get('numa.common')->getValueFromArray($data,'htotal'));
        $cheque->setMileRate($this->container->get('numa.common')->getValueFromArray($data,'mile_rate'));
        $cheque->setMiles($this->container->get('numa.common')->getValueFromArray($data,'miles'));
        $cheque->setMileTotal($this->container->get('numa.common')->getValueFromArray($data,'mile_total'));
        $cheque->setMiscPayDesc($this->container->get('numa.common')->getValueFromArray($data,'misc_pay_desc'));
        $cheque->setMiscPayAmt($this->container->get('numa.common')->getValueFromArray($data,'misc_pay_amt'));
        $cheque->setGrossPay($this->container->get('numa.common')->getValueFromArray($data,'gross_pay'));
        $cheque->setCpp($this->container->get('numa.common')->getValueFromArray($data,'cpp'));
        $cheque->setSocialFund($this->container->get('numa.common')->getValueFromArray($data,'social_fund'));
        $cheque->setAdvance($this->container->get('numa.common')->getValueFromArray($data,'advance'));
        $cheque->setAdvanceFee($this->container->get('numa.common')->getValueFromArray($data,'advance_fee'));
        $cheque->setFuelExpences($this->container->get('numa.common')->getValueFromArray($data,'fuel_expences'));
        $cheque->setFuelCard($this->container->get('numa.common')->getValueFromArray($data,'fuel_card'));
        $cheque->setFuelCardFee($this->container->get('numa.common')->getValueFromArray($data,'fuel_card_fee'));
        $cheque->setPurchases($this->container->get('numa.common')->getValueFromArray($data,'purchases'));
        $cheque->setPurchasesRef($this->container->get('numa.common')->getValueFromArray($data,'purchases_ref'));
        $cheque->setPurchasesFee($this->container->get('numa.common')->getValueFromArray($data,'purchases_fee'));

        $cheque->setAmountInWord($this->container->get('numa.common')->getValueFromArray($data,'amount_in_word'));
        $cheque->setCheckNum($this->container->get('numa.common')->getValueFromArray($data,'check_num'));
        $cheque->setNetAmount($this->container->get('numa.common')->getValueFromArray($data,'net_amount'));
        $cheque->setNetPay($this->container->get('numa.common')->getValueFromArray($data,'net_pay'));
        $cheque->setDeducTotal($this->container->get('numa.common')->getValueFromArray($data,'deduc_total'));
        $cheque->setWcb($this->container->get('numa.common')->getValueFromArray($data,'wcb'));
        $cheque->setAdvanceRef($this->container->get('numa.common')->getValueFromArray($data,'advance_ref'));
        $cheque->setPayWbc($this->container->get('numa.common')->getValueFromArray($data,'pay_wbc'));
        $cheque->setWcbCode($this->container->get('numa.common')->getValueFromArray($data,'wcb_code'));
        $cheque->setWcbRate($this->container->get('numa.common')->getValueFromArray($data,'wcb_rate'));
        $cheque->setWcbBase($this->container->get('numa.common')->getValueFromArray($data,'wcb_base'));
        $cheque->setCargoInsurance($this->container->get('numa.common')->getValueFromArray($data,'cargo_insurance'));
        $cheque->setFuelCredits($this->container->get('numa.common')->getValueFromArray($data,'fuel_credits'));
        $cheque->setCityProvPostal($this->container->get('numa.common')->getValueFromArray($data,'city_prov_postal'));
        $cheque->setPrintStatus($this->container->get('numa.common')->getValueFromArray($data,'print_status'));
        $cheque->setMisc1($this->container->get('numa.common')->getValueFromArray($data,'misc1'));
        $cheque->setMisc2($this->container->get('numa.common')->getValueFromArray($data,'misc2'));
        $cheque->setMisc3($this->container->get('numa.common')->getValueFromArray($data,'misc3'));
        $cheque->setMisc4($this->container->get('numa.common')->getValueFromArray($data,'misc4'));
        $cheque->setMisc1Description($this->container->get('numa.common')->getValueFromArray($data,'misc1_description'));
        $cheque->setMisc2Description($this->container->get('numa.common')->getValueFromArray($data,'misc2_description'));
        $cheque->setMisc3Description($this->container->get('numa.common')->getValueFromArray($data,'misc3_description'));
        $cheque->setMisc4Description($this->container->get('numa.common')->getValueFromArray($data,'misc4_description'));
        $cheque->setMisc1GST($this->container->get('numa.common')->getValueFromArray($data,'misc1__g_s_t'));
        $cheque->setMisc2GST($this->container->get('numa.common')->getValueFromArray($data,'misc2__g_s_t'));
        $cheque->setMisc3GST($this->container->get('numa.common')->getValueFromArray($data,'misc3__g_s_t'));
        $cheque->setMisc4GST($this->container->get('numa.common')->getValueFromArray($data,'misc4__g_s_t'));
        $cheque->setMisc1Total($this->container->get('numa.common')->getValueFromArray($data,'misc1_total'));
        $cheque->setMisc2Total($this->container->get('numa.common')->getValueFromArray($data,'misc2_total'));
        $cheque->setMisc3Total($this->container->get('numa.common')->getValueFromArray($data,'misc3_total'));
        $cheque->setMisc4Total($this->container->get('numa.common')->getValueFromArray($data,'misc4_total'));
        $cheque->setFuelGst($this->container->get('numa.common')->getValueFromArray($data,'fuel_gst'));
        $cheque->setFuelTotal($this->container->get('numa.common')->getValueFromArray($data,'fuel_total'));

        return $cheque;
    }

    public function updateChequeFromArray($chequeData)
    {
        $em = $this->container->get('doctrine.orm.entity_manager');
        $cheque = $this->container->get('numa.common')->getEntity($chequeData['id'], Cheque::class, $em);
        if($cheque instanceof Cheque) {
            $cheque = $this->bindChequeFromArray($chequeData, $cheque);
            $em->flush($cheque);
        }
        return $cheque;
    }

    public function insertNewChequeFromArray($data)
    {

        $em = $this->container->get('doctrine.orm.entity_manager');
        $cheque = $this->bindChequeFromArray($data, new Cheque());

        $em->persist($cheque);
        $em->flush($cheque);
        return $cheque;
    }

    public function countChequesInPayPeriod($payPeriodId){
        $em = $this->container->get('doctrine.orm.entity_manager');
        $cheques = $em->getRepository(Cheque::class)->findAllInPayperiod($payPeriodId);
        return count($cheques);
    }

}