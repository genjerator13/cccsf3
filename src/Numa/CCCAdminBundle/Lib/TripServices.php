<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of services
 *
 * @author genjerator
 */

namespace Numa\CCCAdminBundle\Lib;

use Numa\CCCAdminBundle\Entity\Batch;
use Numa\CCCAdminBundle\Entity\BillingPeriod;
use Numa\CCCAdminBundle\Entity\Customers;
use Numa\CCCAdminBundle\Entity\Dispatchcard;
use Numa\CCCAdminBundle\Entity\Probills;
use Numa\CCCAdminBundle\Entity\Trip;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class TripServices
{
    use ContainerAwareTrait;


    protected $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    public function createTripFromDispatchCard($dispatchcardid)
    {
        $em = $this->container->get("doctrine")->getManager();
        $dispatchcard = $em->getRepository(Dispatchcard::class)->find($dispatchcardid);
        if (!$dispatchcard instanceof Dispatchcard) {
            return;
        }
        $trip = $em->getRepository(Trip::class)->findOneBy(array("Dispatchcard" => $dispatchcard));
        if (!$trip instanceof Trip) {
            $trip = new Trip();
            $em->persist($trip);
        }
        $trip->setDispatchcard($dispatchcard);
        $trip->setPickup($dispatchcard->origin());
        $trip->setDelivery($dispatchcard->destination());
        $trip->setComments($dispatchcard->comments());

        $em->flush();
    }

}
