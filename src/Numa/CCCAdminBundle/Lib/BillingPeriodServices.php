<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of services
 *
 * @author genjerator
 */

namespace Numa\CCCAdminBundle\Lib;

use Numa\CCCAdminBundle\Entity\Batch;
use Numa\CCCAdminBundle\Entity\BillingPeriod;
use Numa\CCCAdminBundle\Entity\Customers;
use Numa\CCCAdminBundle\Entity\Probills;
use Numa\CCCAdminBundle\Entity\Drivers;
use Numa\CCCAdminBundle\Entity\Rates;
use Numa\CCCAdminBundle\Entity\Vehtypes;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class BillingPeriodServices
{
    use ContainerAwareTrait;


    protected $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    public function importFromXml(BillingPeriod $billingPeriod)
    {

        $em = $this->container->get("doctrine")->getManager();
        $batch = $em->getRepository(Batch::class)->findOneBy(array('billing_period_id'=>$billingPeriod->getId()));

        $xml = new \DOMDocument("1.0");
        $xml->load('web/probillsApril.xml');
        $probills = $xml->getElementsByTagName('Probill');
        $i = 0;

        dump("probills count".count($probills));
        foreach ($probills as $probillXml) {
            $createdAt = $this->getValueByTagName($probillXml, "created_at");

//            $probillDB = $em->getRepository(Probills::class)->findOneBy(['createdAt' => $createdAt]);
//
//            if (!$probillDB instanceof Probills) {
                $probillDB = new Probills();


                $probillDB = new Probills();
                $probillDB->setBatchPB($batch);

                $value = $this->getValueByTagName($probillXml, 'P_Date');

                $date = new \DateTime($value);
                $probillDB->setPDate($date);

                $this->setValueToProbill($probillDB, $probillXml, 'Serv_Type');
                $this->setValueToProbill($probillDB, $probillXml, 'WayBill');
                $this->setValueToProbill($probillDB, $probillXml, 'Shipper');
                $this->setValueToProbill($probillDB, $probillXml, 'Pickup_HR');
                $this->setValueToProbill($probillDB, $probillXml, 'Pickup_MN');
                $this->setValueToProbill($probillDB, $probillXml, 'Delivery_HR');
                $this->setValueToProbill($probillDB, $probillXml, 'Delivery_MN');
                $this->setValueToProbill($probillDB, $probillXml, 'Receiver');
                $this->setValueToProbill($probillDB, $probillXml, 'PCE');
                $this->setValueToProbill($probillDB, $probillXml, 'WGT');
                $this->setValueToProbill($probillDB, $probillXml, 'Rate_AMT');
                $this->setValueToProbill($probillDB, $probillXml, 'Rate_Description');
                $this->setValueToProbill($probillDB, $probillXml, 'Rate_id');
                $this->setValueToProbill($probillDB, $probillXml, 'Extra_Piece');
                $this->setValueToProbill($probillDB, $probillXml, 'Extra_PCE_Rate');
                $this->setValueToProbill($probillDB, $probillXml, 'Extra_AMT');
                $this->setValueToProbill($probillDB, $probillXml, 'Misc_AMT');
                $this->setValueToProbill($probillDB, $probillXml, 'GST_AMT');
                $this->setValueToProbill($probillDB, $probillXml, 'GrandTotal');
                $this->setValueToProbill($probillDB, $probillXml, 'Paid');
                $this->setValueToProbill($probillDB, $probillXml, 'Balance');
                $this->setValueToProbill($probillDB, $probillXml, 'Drv_Rate');
                $this->setValueToProbill($probillDB, $probillXml, 'Drv_Total');
                $this->setValueToProbill($probillDB, $probillXml, 'Gross');
                $this->setValueToProbill($probillDB, $probillXml, 'Signature');
                $this->setValueToProbill($probillDB, $probillXml, 'TTIME_H');
                $this->setValueToProbill($probillDB, $probillXml, 'TTIME_M');
                $this->setValueToProbill($probillDB, $probillXml, 'Waitime');
                $this->setValueToProbill($probillDB, $probillXml, 'WaitChrg');
                $this->setValueToProbill($probillDB, $probillXml, 'Vehicle_id');
                $this->setValueToProbill($probillDB, $probillXml, 'vehcode');
                $this->setValueToProbill($probillDB, $probillXml, 'Rush_AMT');
                //$this->setValueToProbill($probillDB, $probillXml, 'Called');
                //$this->setValueToProbill($probillDB, $probillXml, 'Delivered');
                //$this->setValueToProbill($probillDB, $probillXml, 'DeliveryTime');
                $this->setValueToProbill($probillDB, $probillXml, 'Status');
                $this->setValueToProbill($probillDB, $probillXml, 'Status');
                $this->setValueToProbill($probillDB, $probillXml, 'CustSurchargeAMT');
                $this->setValueToProbill($probillDB, $probillXml, 'DrvSurRate');
                $this->setValueToProbill($probillDB, $probillXml, 'CusSurchargeRate');
                $this->setValueToProbill($probillDB, $probillXml, 'DriverSurcharge');
                $this->setValueToProbill($probillDB, $probillXml, 'Total');
                $this->setValueToProbill($probillDB, $probillXml, 'TaxCode');
                $this->setValueToProbill($probillDB, $probillXml, 'DrvGross');
                $this->setValueToProbill($probillDB, $probillXml, 'TrailerCHG');
                $this->setValueToProbill($probillDB, $probillXml, 'Miles');
                $this->setValueToProbill($probillDB, $probillXml, 'Tracking');
                $this->setValueToProbill($probillDB, $probillXml, 'COD_AMT');
                //$this->setValueToProbill($probillDB, $probillXml, 'EntryDate');
                $this->setValueToProbill($probillDB, $probillXml, 'ShipmentID');
                $this->setValueToProbill($probillDB, $probillXml, 'RateLevel');
                $this->setValueToProbill($probillDB, $probillXml, 'SubTotal');
                $this->setValueToProbill($probillDB, $probillXml, 'BillType');
                $this->setValueToProbill($probillDB, $probillXml, 'FuelSurchargeDifferential');
                $this->setValueToProbill($probillDB, $probillXml, 'DiffSurchargeAMT');
                $this->setValueToProbill($probillDB, $probillXml, 'Commodity_id');
                $this->setValueToProbill($probillDB, $probillXml, 'Initials');
                $this->setValueToProbill($probillDB, $probillXml, 'shipto_id');
                $this->setValueToProbill($probillDB, $probillXml, 'pickupfrom_id');
                $this->setValueToProbill($probillDB, $probillXml, 'created_at');

                //relations
                $custcode = $this->getValueByTagName($probillXml, "Customer_id");
                $rateCode = $this->getValueByTagName($probillXml, "Rate_id");
                $driverCode = $this->getValueByTagName($probillXml, "Driver_id");
                $vehCode = $this->getValueByTagName($probillXml, "vehcode");

                $driver = $em->getRepository(Drivers::class)->findOneBy(['drivernum' => $driverCode]);
                $customer = $em->getRepository(Customers::class)->findOneBy(['custcode' => $custcode]);
                $rate = $em->getRepository(Rates::class)->findOneBy(['rateCode' => $rateCode]);
                $vehType = $em->getRepository(Vehtypes::class)->findOneBy(['vehcode' => $vehCode]);
                $probillDB->setCustomers($customer);
                $probillDB->setDrivers($driver);
                $probillDB->setRates($rate);
                $probillDB->setVehtypes($vehType);

                $em->persist($probillDB);
                //dump($probillDB);die();


//            }else{
//                dump($i);
//                dump($probillXml);
//                $value = $this->getValueByTagName($probillXml, 'created_at');
//                dump($value);
//                $value = $this->getValueByTagName($probillXml, 'WayBill');
//                dump($value);
//
//            }
            $i++;
            if ($i % 500 == 0) {
                dump("FLUSH");
                $em->flush();
            }

        }
        $em->flush();

    }

    private function setValueToProbill(Probills &$probillDB, $probillXml, $tagname)
    {
        $value = $this->getValueByTagName($probillXml, $tagname);
        $fn = 'set' . str_replace('_', '', ucwords($tagname, '_'));
        if (method_exists(Probills::class, $fn)) {
            $probillDB->$fn($value);
        }

    }

    private function getValueByTagName($probill, $tagname)
    {
        $pid = $probill->getElementsByTagName($tagname);

        if (!empty($pid->item(0)) && !empty($pid->item(0)->firstChild)) {
            return $pid->item(0)->firstChild->data;
        }
        return "";
    }

    public function exportToXml(BillingPeriod $billingPeriod)
    {
        $logger = $this->container->get('logger');
        $em = $this->container->get("doctrine")->getManager();
        $probills = $em->getRepository(Probills::class)->findByBillingPeriods($billingPeriod->getId(), "");
        $xml = new \DOMDocument("1.0");
        $xmlArray = $xml->createElement("ArrayOfProbill");
        $xmlArray->setAttribute('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
        $xmlArray->setAttribute('xmlns:xsd', 'http://www.w3.org/2001/XMLSchema');
        $xml->appendChild($xmlArray);
        $xml->formatOutput = true;
        $i=0;
        foreach ($probills as $probill) {
            if ($probill instanceof Probills) {

           }
            $i++;

            $logger->warning($probill->getWaybill()."     :".$i);
            $this->setProbillXml($probill, $xml);
        }
        return $xml;
    }

    private function setProbillXml(Probills $probill, \DOMDocument &$xml)
    {
        $xmlProbill = $xml->createElement("Probill");
        $xmlArray = $xml->getElementsByTagName("ArrayOfProbill")->item(0);

        $driverId = "";
        $drvSurRate = "";
        if ($probill->getDrivers() instanceof Drivers) {
            $driverId = $probill->getDrivers()->getDrivernum();
            $drvSurRate = $probill->getDrivers()->getDrvsurrate();

            $this->setProbillReference($xml, $xmlProbill, "DriverReference", "Drivers", "DriverNum", $probill->getDrivers()->getDrivernum());
        }
        $this->setProbillField($xml, $xmlProbill, "Driver_id", $driverId);
        $customerId = "";
        $customer = "";
        if ($probill->getCustomers() instanceof Customers) {
            $customerId = $probill->getCustomers()->getCustcode();
            $customer = $probill->getCustomers()->getName();
            $this->setProbillReference($xml, $xmlProbill, "Customer1Reference", "Customers", "CustCode", $probill->getCustomers()->getCustcode());
        }
        //probill reference
        $xmlProbillEntityKey = $xml->createElement("EntityKey");
        $xmlProbillEntitySetName = $xml->createElement("EntitySetName", "Probills");
        $xmlProbillEntityContainerName = $xml->createElement("EntityContainerName", "CCC2Entities");
        $xmlProbillEntityKeyValues = $xml->createElement("EntityKeyValues");
        $xmlProbillEntityKeyMember = $xml->createElement("EntityKeyMember");
        $key = $xml->createElement("Key", "ProbillID");
        $value = $xml->createElement("Value", $probill->getId());
        $value->setAttribute("xsi:type", "xsd:long");
        $xmlProbillEntityKeyMember->appendChild($key);
        $xmlProbillEntityKeyMember->appendChild($value);
        $xmlProbillEntityKeyValues->appendChild($xmlProbillEntityKeyMember);
        $xmlProbillEntityKey->appendChild($xmlProbillEntitySetName);
        $xmlProbillEntityKey->appendChild($xmlProbillEntityContainerName);
        $xmlProbillEntityKey->appendChild($xmlProbillEntityKeyValues);
        $xmlProbill->appendChild($xmlProbillEntityKey);
        $xmlArray->appendChild($xmlProbill);
        $this->setProbillField($xml, $xmlProbill, "ProbillID", $probill->getId());
        $this->setProbillField($xml, $xmlProbill, "P_Date", $probill->getPDate()->format("Y-m-d\Th:m:s"));
        $this->setProbillField($xml, $xmlProbill, "Customer_id", $customerId);
        $this->setProbillField($xml, $xmlProbill, "Serv_Type", $probill->getServType());
        $this->setProbillField($xml, $xmlProbill, "Customer", $customer);
        $this->setProbillField($xml, $xmlProbill, "WayBill", $probill->getWaybill());
        $this->setProbillField($xml, $xmlProbill, "Shipper", $probill->getShipper());
        $this->setProbillField($xml, $xmlProbill, "Pickup_HR", $probill->getPickupHr());
        $this->setProbillField($xml, $xmlProbill, "Pickup_MN", $probill->getPickupMn());
        $this->setProbillField($xml, $xmlProbill, "Delivery_HR", $probill->getDeliveryHr());
        $this->setProbillField($xml, $xmlProbill, "Delivery_MN", $probill->getDeliveryMn());
        $this->setProbillField($xml, $xmlProbill, "Receiver", $probill->getReceiver());
        $this->setProbillField($xml, $xmlProbill, "Receiver", $probill->getReceiver());
        $this->setProbillField($xml, $xmlProbill, "PCE", $probill->getPce());
        $this->setProbillField($xml, $xmlProbill, "WGT", $probill->getWgt());
        $rateId = "";
        $rate = "";
        $rateAmt = "";
        if ($probill->getRates() instanceof Rates) {
            $rateId = $probill->getRates()->getRateCode();
            $rate = $probill->getRates()->getRateDescription();
            $rateAmt = $probill->getRates()->getRateAmt();
            $this->setProbillReference($xml, $xmlProbill, "RateReference", "Rates", "Rate_Code", $probill->getRates()->getRateCode());
        }
        $vehtypeId = "";
        $vehtypeId = "";
        if ($probill->getVehtypes() instanceof Vehtypes) {
            $vehtypeId = $probill->getVehtypes()->getVehcode();
            //$this->setProbillReference($xml,$xmlProbill,"RateReference","Rates","Rate_Code",$probill->getRates()->getRateCode());

        }
        $this->setProbillField($xml, $xmlProbill, "Rate_id", $rateId);
        $this->setProbillField($xml, $xmlProbill, "Rate_Description", $rate);
        $this->setProbillField($xml, $xmlProbill, "Rate_AMT", $probill->getRateAmt());
        $this->setProbillField($xml, $xmlProbill, "Ref", $probill->getRef());
        $this->setProbillField($xml, $xmlProbill, "SubItem", $probill->getSubitem());
        $this->setProbillField($xml, $xmlProbill, "Dept", $probill->getDept());
        $this->setProbillField($xml, $xmlProbill, "Extra_Piece", $probill->getExtraPiece());
        $this->setProbillField($xml, $xmlProbill, "Extra_PCE_Rate", $probill->getExtraPceRate());
        $this->setProbillField($xml, $xmlProbill, "Extra_AMT", $probill->getExtraAmt());
        $this->setProbillField($xml, $xmlProbill, "Misc_AMT", $probill->getMiscAmt());
        $this->setProbillField($xml, $xmlProbill, "GST_AMT", $probill->getGstAmt());
        $this->setProbillField($xml, $xmlProbill, "GrandTotal", $probill->getGrandtotal());
        $this->setProbillField($xml, $xmlProbill, "Paid", $probill->getPaid());
        $this->setProbillField($xml, $xmlProbill, "Balance", $probill->getBalance());
        $this->setProbillField($xml, $xmlProbill, "Drv_Rate", $probill->getDrvRate());
        $this->setProbillField($xml, $xmlProbill, "Drv_Total", $probill->getDrvTotal());
        $this->setProbillField($xml, $xmlProbill, "Gross", $probill->getGross());
        $this->setProbillField($xml, $xmlProbill, "Signature", $probill->getSignature());
        $this->setProbillField($xml, $xmlProbill, "TTIME_H", $probill->getTtimeH());
        $this->setProbillField($xml, $xmlProbill, "TTIME_M", $probill->getTtimeM());
        $this->setProbillField($xml, $xmlProbill, "Invoice", $probill->getInvoice());
        $this->setProbillField($xml, $xmlProbill, "Waitime", $probill->getWaitime());
        $this->setProbillField($xml, $xmlProbill, "WaitChrg", $probill->getWaitchrg());
        $this->setProbillField($xml, $xmlProbill, "Vehicle_id", $vehtypeId);
        $this->setProbillField($xml, $xmlProbill, "vehcode", $vehtypeId);
        $this->setProbillField($xml, $xmlProbill, "Batch", $probill->getBatch());
        $this->setProbillField($xml, $xmlProbill, "Rush_AMT", $probill->getRushAmt());
        $this->setProbillField($xml, $xmlProbill, "Called", $probill->getCalled());
        $this->setProbillField($xml, $xmlProbill, "Delivered", $probill->getDelivered());
        $this->setProbillField($xml, $xmlProbill, "DeliveryTime", $probill->getDeliverytime());
        $this->setProbillField($xml, $xmlProbill, "Status", $probill->getStatus());
        $this->setProbillField($xml, $xmlProbill, "CustSurchargeAMT", $probill->getCustsurchargeamt());
        $this->setProbillField($xml, $xmlProbill, "DrvSurRate", $drvSurRate);
        $this->setProbillField($xml, $xmlProbill, "CusSurchargeRate", $probill->getCussurchargerate());
        $this->setProbillField($xml, $xmlProbill, "DriverSurcharge", $probill->getDriversurcharge());
        $this->setProbillField($xml, $xmlProbill, "Total", $probill->getTotal());
        $this->setProbillField($xml, $xmlProbill, "TaxCode", $probill->getTaxcode());
        $this->setProbillField($xml, $xmlProbill, "DrvGross", $probill->getDrvgross());
        $this->setProbillField($xml, $xmlProbill, "TrailerCHG", $probill->getTrailerchg());
        $this->setProbillField($xml, $xmlProbill, "Trailer_id", $probill->getTrailerId());
        $this->setProbillField($xml, $xmlProbill, "Miles", $probill->getMiles());
        $this->setProbillField($xml, $xmlProbill, "Tracking", $probill->getTracking());
        $this->setProbillField($xml, $xmlProbill, "Details", $probill->getDetails());
        $this->setProbillField($xml, $xmlProbill, "EntryDate", $probill->getEntrydate());
        $this->setProbillField($xml, $xmlProbill, "ShipmentID", $probill->getShipmentid());
        $this->setProbillField($xml, $xmlProbill, "RateLevel", $probill->getRatelevel());
        $this->setProbillField($xml, $xmlProbill, "SubTotal", $probill->getSubtotal());
        $this->setProbillField($xml, $xmlProbill, "BillType", $probill->getBilltype());
        $this->setProbillField($xml, $xmlProbill, "FuelSurchargeDifferential", $probill->getFuelsurchargedifferential());
        $this->setProbillField($xml, $xmlProbill, "DiffSurchargeAMT", $probill->getDiffsurchargeamt());
        $this->setProbillField($xml, $xmlProbill, "Commodity_id", $probill->getCommodityId());
        $this->setProbillField($xml, $xmlProbill, "Initials", $probill->getUsersInitials());
        $this->setProbillField($xml, $xmlProbill, "shipto_id", $probill->getShiptoId());
        $this->setProbillField($xml, $xmlProbill, "pickupfrom_id", $probill->getPickupfromId());
        $this->setProbillField($xml, $xmlProbill, "created_at", self::generateCreatedAt());
        $this->setProbillField($xml, $xmlProbill, "COD_AMT", $probill->getCodAmt());

        $xml->appendChild($xmlArray);
        return $xmlProbill;
    }

    private function setProbillField(&$xml, $xmlProbill, $name, $value)
    {
        $tag = $xml->createElement($name, htmlspecialchars($value));

        if (is_null($value) || trim($value) == "") {
            $tag = $xml->createElement($name);
            $tag->setAttribute("xsi:nil", "true");
        }
        $xmlProbill->appendChild($tag);
    }

    private function setProbillReference(&$xml, &$xmlProbill, $tagname, $entity, $key, $value)
    {
        //driver reference
        $DriverReference = $xml->createElement($tagname);

        $xmlProbillEntityKey = $xml->createElement("EntityKey");
        $xmlProbillEntitySetName = $xml->createElement("EntitySetName", $entity);
        $xmlProbillEntityContainerName = $xml->createElement("EntityContainerName", "CCC2Entities");
        $xmlProbillEntityKeyValues = $xml->createElement("EntityKeyValues");
        $xmlProbillEntityKeyMember = $xml->createElement("EntityKeyMember");
        $tagKey = $xml->createElement("Key", $key);
        $tagValue = $xml->createElement("Value", $value);
        $tagValue->setAttribute("xsi:type", "xsd:string");
        $xmlProbillEntityKeyMember->appendChild($tagKey);
        $xmlProbillEntityKeyMember->appendChild($tagValue);
        $xmlProbillEntityKeyValues->appendChild($xmlProbillEntityKeyMember);
        $xmlProbillEntityKey->appendChild($xmlProbillEntitySetName);
        $xmlProbillEntityKey->appendChild($xmlProbillEntityContainerName);
        $xmlProbillEntityKey->appendChild($xmlProbillEntityKeyValues);
        $DriverReference->appendChild($xmlProbillEntityKey);
        $xmlProbill->appendChild($DriverReference);
    }

    static public function generateCreatedAt()
    {

        return time() . "_" . rand(0, 1000);

    }

}
