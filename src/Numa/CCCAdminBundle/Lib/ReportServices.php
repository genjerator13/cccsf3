<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of services
 *
 * @author genjerator
 */

namespace Numa\CCCAdminBundle\Lib;


use Cocur\BackgroundProcess\BackgroundProcess;
use Numa\CCCAdminBundle\Entity\BillingPeriod;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class ReportServices
{
    use ContainerAwareTrait;


    protected $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    public function htmlToXls($html,$title,$filename){
        $objPHPExcel = $this->htmlToExcelObject($html, $title, $filename);
        $objWriter = $this->container->get('phpexcel')->createWriter($objPHPExcel, 'Excel2007');
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename='.$filename);
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');
        exit;
    }

    public function htmlToExcelObject($html,$title,$filename){
        $tmpfile = tempnam(sys_get_temp_dir(), 'html');
        file_put_contents($tmpfile, $html);

// insert $table into $objPHPExcel's Active Sheet through $excelHTMLReader
        $objPHPExcel     = new \PHPExcel();

        $excelHTMLReader = \PHPExcel_IOFactory::createReader('HTML');

        $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
        $objPHPExcel->getActiveSheet()->setTitle($title);


        //$objWriter = $this->container->get('phpexcel')->createWriter($objPHPExcel, 'Excel2007');
        return $objPHPExcel;
    }

    public function generateCustomerDetails(BillingPeriod $billingPeriod,$subtype="All",$format="pdf"){

        ///php bin/console billing_period generateCustomerSnailReport 1
        //$command = 'php ' . $this->container->get('kernel')->getRootDir()."/../bin/console billing_period generateCustomer".$subtype."Report".$format." " . $billingPeriod->getId();
        $command = 'php ' . $this->container->get('kernel')->getRootDir()."/../bin/console billing_period generateCustomerReport ".$billingPeriod->getId()." ".$subtype." ".$format;
        //dump($command);die();
        $process = new BackgroundProcess($command);
        $process->run();
    }

    public function generateDriverDetails(BillingPeriod $billingPeriod,$format="pdf"){

        ///php bin/console billing_period generateCustomerSnailReport 1
        //$command = 'php ' . $this->container->get('kernel')->getRootDir()."/../bin/console billing_period generateCustomer".$subtype."Report".$format." " . $billingPeriod->getId();
        $command = 'php ' . $this->container->get('kernel')->getRootDir()."/../bin/console billing_period generateDriverReport ".$billingPeriod->getId()." ".$format;
        $process = new BackgroundProcess($command);
        $process->run();
    }


    public function executeAsCommand(){

    }
}
