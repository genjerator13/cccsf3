<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of services
 *
 * @author genjerator
 */

namespace Numa\CCCAdminBundle\Lib;

use Doctrine\ORM\ORMException;
use Numa\CCCAdminBundle\Entity\Customers;
use Numa\CCCAdminBundle\Entity\CustomRate;
use Numa\CCCAdminBundle\Entity\CustomRateValue;
use Numa\CCCAdminBundle\Entity\Dispatchcard;
use Numa\CCCAdminBundle\Form\DestinationType;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class DispatchcardServices
{
    use ContainerAwareTrait;


    protected $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    public function findById($dispatchcard_id)
    {
        $dc = $this->container->get("doctrine.orm.entity_manager")->getRepository(Dispatchcard::class)->find($dispatchcard_id);

        if (!$dc instanceof Dispatchcard) {
            return false;
        }

        $dest = $dc->getDestination()->first();

        $dc->setPo($dest->getPo());
        $dc->setServType($dest->getPo());
    }


}
