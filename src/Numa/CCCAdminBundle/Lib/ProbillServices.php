<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of services
 *
 * @author genjerator
 */

namespace Numa\CCCAdminBundle\Lib;

use Doctrine\Common\Collections\Criteria;
use Numa\CCCAdminBundle\Entity\Batch;
use Numa\CCCAdminBundle\Entity\BillingPeriod;
use Numa\CCCAdminBundle\Entity\Customers;
use Numa\CCCAdminBundle\Entity\CustomRate;
use Numa\CCCAdminBundle\Entity\CustomRateValue;
use Numa\CCCAdminBundle\Entity\Dispatchcard;
use Numa\CCCAdminBundle\Entity\Drivers;
use Numa\CCCAdminBundle\Entity\Probills;
use Numa\CCCAdminBundle\Entity\Rates;
use Numa\CCCAdminBundle\Entity\Vehtypes;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class ProbillServices
{
    use ContainerAwareTrait;

    protected $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    public function bindProbillFromArray($data, Probills $probill)
    {

        $em = $this->container->get('doctrine.orm.entity_manager');
        $billingPeriod = null;
        $batch = null;

        $batchId = $probill->getBatchId();
        if (empty($batchId)) {
            $batchId = intval($data['entityDataGridId']);
        }


        if (!empty($data['entityDataGrid']) && $data['entityDataGrid'] == "billing_period" && !empty($data['entityDataGridId'])) {
            $billingPeriod = $em->getRepository(BillingPeriod::class)->find($data['entityDataGridId']);

        }
        if (!empty($data['entityDataGrid']) && $data['entityDataGrid'] == "batch" && !empty($data['entityDataGridId'])) {
            $batch = $em->getRepository(Batch::class)->find($batchId);
            $probill->setBatchPB($batch);
        }

        $rate = $this->getEntity($this->getValueFromArray($data, 'rates_pid'), Rates::class, $em);
        $driver = $this->getEntity($this->getValueFromArray($data, 'driver_pid'), Drivers::class, $em);
        $vehtype = $this->getEntity($this->getValueFromArray($data, 'vehtype_pid'), Vehtypes::class, $em);
        $customer = $this->getEntity($this->getValueFromArray($data, 'customer_pid'), Customers::class, $em);
        $dispatchcard = $this->getEntity($this->getValueFromArray($data, 'dispatchcard_pid'), Dispatchcard::class, $em);


        $probill->setRates($rate);
        $probill->setDrivers($driver);
        $probill->setVehtypes($vehtype);
        $probill->setCustomers($customer);
        $probill->setDispatchcard($dispatchcard);
        $probill->setServType($this->getValueFromArray($data, 'serv_type'));
        $probill->setReceiver($this->getValueFromArray($data, 'receiver'));
        $probill->setShipper($this->getValueFromArray($data, 'shipper'));
        $probill->setPce($this->getValueFromArray($data, 'pce'));
        $probill->setWgt($this->getValueFromArray($data, 'wgt'));
        $probill->setPo($this->getValueFromArray($data, 'po'));
        $probill->setRef($this->getValueFromArray($data, 'ref'));
        $probill->setWaybill($this->getValueFromArray($data, 'waybill'));
        $probill->setTtimeH($this->getValueFromArray($data, 'ttime_h'));
        $probill->setTtimeM($this->getValueFromArray($data, 'ttime_m'));
        $probill->setDeliveryHr($this->getValueFromArray($data, 'delivery_hr'));
        $probill->setDeliveryMn($this->getValueFromArray($data, 'delivery_mn'));
        $probill->setPickupHr($this->getValueFromArray($data, 'pickup_hr'));
        $probill->setPickupMn($this->getValueFromArray($data, 'pickup_mn'));
        $probill->setDept($this->getValueFromArray($data, 'dept'));
        $probill->setSubitem($this->getValueFromArray($data, 'subitem'));
        $probill->setTrailerId($this->getValueFromArray($data, 'trailer_id'));
        $probill->setComments($this->getValueFromArray($data, 'comments'));
        $probill->setDimensions($this->getValueFromArray($data, 'dimensions'));
        $probill->setMiscinfo($this->getValueFromArray($data, 'miscinfo'));
        $probill->setDetails($this->getValueFromArray($data, 'details'));
        $probill->setMiscdesc($this->getValueFromArray($data, 'miscdesc'));
        $probill->setExtraPiece($this->getValueFromArray($data, 'extra_piece'));
        $probill->setExtraPceRate($this->getValueFromArray($data, 'extra_pce_rate'));
        $probill->setExtraAmt($this->getValueFromArray($data, 'extra'));
        $probill->setRateAmt($this->getValueFromArray($data, 'rate_amt'));
        $probill->setTotal($this->getValueFromArray($data, 'total'));
        $probill->setSubtotal($this->getValueFromArray($data, 'subtotal'));
        $probill->setGrandtotal($this->getValueFromArray($data, 'grandtotal'));
        $probill->setRushAmt($this->getValueFromArray($data, 'rush_amt'));
        $probill->setCussurchargerate($this->getValueFromArray($data, 'cussurchargerate'));
        $probill->setCustsurchargeamt($this->getValueFromArray($data, 'custsurchargeamt'));

        $probill->setDrvRateOverride($this->getValueFromArray($data, 'drv_rate_override'));
        $probill->setCussurchargerateOverride($this->getValueFromArray($data, 'cussurchargerate_override'));

        $probill->setWaitime($this->getValueFromArray($data, 'waitime'));
        $probill->setWaitchrg($this->getValueFromArray($data, 'waitchrg'));
        $probill->setTrailerchg($this->getValueFromArray($data, 'trailerchg'));
        $probill->setMiscAmt($this->getValueFromArray($data, 'misc_amt'));
        $probill->setSignature($this->getValueFromArray($data, 'signature'));
        $probill->setTaxcode($this->getValueFromArray($data, 'taxcode'));
        $probill->setGstAmt($this->getValueFromArray($data, 'gstAmt'));
        $probill->setBilltype($this->getValueFromArray($data, 'billtype'));
        $probill->setDrvRate($this->getValueFromArray($data, 'drv_rate'));
        $probill->setDrvTotal($this->getValueFromArray($data, 'drvTotal'));
        $probill->setGross($this->getValueFromArray($data, 'gross'));
        $probill->setDriversurcharge($this->getValueFromArray($data, 'driversurcharge'));
        $probill->setStatus($this->getValueFromArray($data, 'status'));
        $probill->setRatelevel($this->getValueFromArray($data, 'ratelevel'));

        if (!empty($data['pdate_year']) && !empty($data['pdate_month']) && !empty($data['pdate_month'])) {
            $pdate = new \DateTime($data['pdate_year'] . "-" . $data['pdate_month'] . "-" . $data['pdate_day']);
            $probill->setPDate($pdate);
        }
        return $probill;
    }

    public function bindProbillFromOldArray($data, Probills $probill, $emoldccc)
    {
        $em = $this->container->get('doctrine.orm.entity_manager');


        $sql = "SELECT * FROM Customers WHERE id= " . intval($data['customers_id']);
        $stmt = $emoldccc->getConnection()->prepare($sql);
        $stmt->execute();
        $oldCustomer = $stmt->fetchAll();
        if (!empty($oldCustomer[0])) {
            $customer = $this->createNewCustomerIfNotExists($oldCustomer[0]);
            $probill->setCustomers($customer);
        }

        $sql = 'SELECT * FROM Rates WHERE Rate_Description LIKE :rate';
        $stmt = $emoldccc->getConnection()->prepare($sql);
        $stmt->bindValue('rate', trim($data['Rate_Description']));
        $stmt->execute();
        $oldRate = $stmt->fetchAll();

        if (!empty($oldRate[0])) {
            $rate = $this->createNewRateIfNotExists($oldRate[0]);
            $probill->setRates($rate);
        }

        $sql = "SELECT * FROM VehTypes WHERE id= " . intval($data['vehtypes_id']);
        $stmt = $emoldccc->getConnection()->prepare($sql);
        $stmt->execute();
        $oldVehtypes = $stmt->fetchAll();

        if (!empty($oldVehtypes[0])) {
            $vehtype = $this->createNewVehtypeIfNotExists($oldVehtypes[0]);
            $probill->setVehtypes($vehtype);
        }

        $sql = 'SELECT * FROM Drivers WHERE drivernum LIKE :driver';
        $stmt = $emoldccc->getConnection()->prepare($sql);
        $stmt->bindValue('driver', trim($data['Driver_id']));
        $stmt->execute();
        $oldDriver = $stmt->fetchAll();
        if (!empty($oldDriver[0])) {
            $driver = $this->createNewDriverIfNotExists($oldDriver[0]);
            $probill->setDrivers($driver);
        }




        $probill->setServType($this->getValueFromArray($data, 'Serv_Type'));
        $probill->setReceiver($this->getValueFromArray($data, 'Receiver'));
        $probill->setShipper($this->getValueFromArray($data, 'Shipper'));
        $probill->setRef($this->getValueFromArray($data, 'Ref'));
        $probill->setPce($this->getValueFromArray($data, 'PCE'));
        $probill->setWgt($this->getValueFromArray($data, 'WGT'));
        $probill->setWaybill($this->getValueFromArray($data, 'WayBill'));
        $probill->setTtimeH($this->getValueFromArray($data, 'TTIME_H'));
        $probill->setTtimeM($this->getValueFromArray($data, 'TTIME_M'));
        $probill->setDeliveryHr($this->getValueFromArray($data, 'Delivery_HR'));
        $probill->setDeliveryMn($this->getValueFromArray($data, 'Delivery_MN'));
        $probill->setPickupHr($this->getValueFromArray($data, 'Pickup_HR'));
        $probill->setPickupMn($this->getValueFromArray($data, 'Pickup_MN'));
        $probill->setDept($this->getValueFromArray($data, 'Dept'));
        $probill->setSubitem($this->getValueFromArray($data, 'SubItem'));
        $probill->setTrailerId($this->getValueFromArray($data, 'Trailer_id'));
        $probill->setComments($this->getValueFromArray($data, 'Comments'));
        ////$probill->setMiscinfo($this->getValueFromArray($data,'miscinfo'));
        $probill->setDetails($this->getValueFromArray($data, 'Details'));
        $probill->setMiscdesc($this->getValueFromArray($data, 'MiscDesc'));
        $probill->setExtraPiece($this->getValueFromArray($data, 'Extra_Piece'));
        $probill->setExtraPceRate($this->getValueFromArray($data, 'Extra_PCE_Rate'));
        $probill->setExtraAmt($this->getValueFromArray($data, 'Extra_AMT'));
        $probill->setRateAmt($this->getValueFromArray($data, 'Rate_AMT'));
        $probill->setTotal($this->getValueFromArray($data, 'Total'));
        $probill->setSubtotal($this->getValueFromArray($data, 'SubTotal'));
        $probill->setGrandtotal($this->getValueFromArray($data, 'GrandTotal'));
        $probill->setRushAmt($this->getValueFromArray($data, 'Rush_AMT'));
        $probill->setCussurchargerate($this->getValueFromArray($data, 'CusSurchargeRate'));
        $probill->setCustsurchargeamt($this->getValueFromArray($data, 'CustSurchargeAMT'));

        $probill->setDrvRateOverride($this->getValueFromArray($data, 'drv_rate_override'));//
        $probill->setCussurchargerateOverride($this->getValueFromArray($data, 'cussurchargerate_override'));//

        $probill->setWaitime($this->getValueFromArray($data, 'Waitime'));
        $probill->setWaitchrg($this->getValueFromArray($data, 'WaitChrg'));
        $probill->setTrailerchg($this->getValueFromArray($data, 'TrailerCHG'));
        $probill->setMiscAmt($this->getValueFromArray($data, 'Misc_AMT'));
        $probill->setSignature($this->getValueFromArray($data, 'Signature'));
        $probill->setTaxcode($this->getValueFromArray($data, 'TaxCode'));
        $probill->setGstAmt($this->getValueFromArray($data, 'GST_AMT'));
        $probill->setBilltype($this->getValueFromArray($data, 'BillType'));
        $probill->setDrvRate($this->getValueFromArray($data, 'Drv_Rate'));
        $probill->setDrvTotal($this->getValueFromArray($data, 'Drv_Total'));
        $probill->setGross($this->getValueFromArray($data, 'Gross'));
        $probill->setDriversurcharge($this->getValueFromArray($data, 'DriverSurcharge'));
        $probill->setDrvsurrate($this->getValueFromArray($data, 'DrvSurRate'));
        ////$probill->setStatus($this->getValueFromArray($data,'Status'));
        $probill->setRatelevel($this->getValueFromArray($data, 'RateLevel'));
        $probill->setInvoice($this->getValueFromArray($data, 'Invoice'));
        $probill->setInitials($this->getValueFromArray($data, 'Initials'));
        $probill->setPDate(\DateTime::createFromFormat("Y-m-d", $data['P_Date'])->setTime(0, 0, 0));


        return $probill;
    }

    private function getValueFromArray($data, $field)
    {
        if (array_key_exists($field, $data)) {
            if (strtolower($data[$field]) == "true") {
                return 1;
            } elseif (strtolower($data[$field]) == "false") {
                return 0;
            }
            return $data[$field];
        }
        return null;
    }

    public function updateProbillFromArray($probillData)
    {
        $em = $this->container->get('doctrine.orm.entity_manager');
        $probill = $this->getEntity($probillData['id'], Probills::class, $em);
        if ($probill instanceof Probills) {
            $probill = $this->bindProbillFromArray($probillData, $probill);
            $em->flush($probill);
        }
        return $probill;
    }

    public function bindIntVal($field, $data)
    {
        $ret = null;
        if (!empty($data[$field])) {
            $ret = intval($data[$field]);
        }
        return $ret;
    }

    public function getProbillById($id):Probills
    {
        $em = $this->container->get('doctrine.orm.entity_manager');
        return $em->getRepository(Probills::class)->find($id);
    }

    public function getEntity($entityId, $class, $em)
    {
        if (empty($entityId)) {
            return null;
        }
        return $em->getRepository($class)->find($entityId);
    }

    public function insertNewProbillFromArray($data)
    {

        $em = $this->container->get('doctrine.orm.entity_manager');
        $probill = new Probills();
        //$probill->setBatchPB();
        $probill = $this->bindProbillFromArray($data, $probill);
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        $probill->setUser($user);
        $em->persist($probill);
        $em->flush($probill);
        return $probill;
    }

    public function isDeleteGranted(Probills $probill)
    {
        $securityContext = $this->container->get('security.authorization_checker');
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        if ($securityContext->isGranted("ROLE_SUPER_ADMIN") || ($securityContext->isGranted("ROLE_CSR") && $probill->getUser() == $user)) {
            return true;
        }
        return false;
    }

    public function countCTY(Batch $batch)
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq("billtype", "CTY"));
        $cty = $batch->getProbills()->matching($criteria);
        return count($cty);
    }

    public function countHWY(Batch $batch)
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq("billtype", "HWY"));
        $cty = $batch->getProbills()->matching($criteria);
        return count($cty);
    }

    public function calculateDriver(Probills $probill)
    {
        $subtotal = $probill->getSubtotal();
        $basicRateAmt = $probill->getRateAmt();
        $extra = $probill->getExtraAmt();
        $direct = $probill->getRushAmt();
        $waitload = $probill->getWaitchrg();
        $total = $basicRateAmt + $extra + $direct + $waitload;
        $drvRate = 0;
        $drvSurchargeReturnRate = 0;
        $driver = $probill->getDrivers();
        $drvRate = $probill->getDrvRate();
        if ($driver instanceof Drivers) {
            $drvSurchargeReturnRate = $driver->getDrvsurrate();
        }else{
            $drvSurchargeReturnRate = $probill->getDrvsurrate();
        }
        $fuelSurcharge = $probill->getCussurchargerate();

        $driverSurchargeAmount = ($total * $fuelSurcharge * ($drvSurchargeReturnRate / 100));
        $temp = $total * $drvRate;
        $driverTotalFS = $temp + $driverSurchargeAmount;
        $gross = $subtotal - $driverTotalFS;
        $DrvRateAmount = ($basicRateAmt + $extra + $direct + $waitload) * $drvRate;

        $probill->setDriversurcharge($driverSurchargeAmount);
        $probill->setDrvTotal($driverTotalFS);
        $probill->setDrvgross($DrvRateAmount);
        $probill->setGross($gross);

        return $probill;

    }

    public function calculateDriverOld(Probills $probill)
    {
        $driver = $probill->getDrivers();
        $drvSurchargeReturnRate = "0";
        if ($driver instanceof Drivers) {
            $drvSurchargeReturnRate = $driver->getDrvsurrate();
        }
        $driverSurchargeAmount = ($probill->getCustsurchargeamt()) * ($drvSurchargeReturnRate / 100);
        $driverRateAmount = ($probill->getRateAmt() + $probill->getExtraAmt() + $probill->getRushAmt() + $probill->getWaitchrg() + $probill->getTrailerchg()) * $probill->getDrvRate();
        $driverTotalFS = $driverRateAmount + $driverSurchargeAmount;
        $gross = $probill->getSubtotal() - $driverTotalFS;
        $probill->setDriversurcharge($driverSurchargeAmount);
        $probill->setDrvTotal($driverTotalFS);
        $probill->setDrvgross($driverRateAmount);
        $probill->setGross($gross);
        return $probill;

    }

    public function createNewCustomerIfNotExists($customerOld)
    {
        $em = $this->container->get('doctrine.orm.entity_manager');

        $customer = $em->getRepository(Customers::class)->findOneBy(array('custcode' => $customerOld['CustCode']));
        if (!$customer instanceof Customers && !empty($customerOld['CustCode'])) {
            dump("CUSTOME DOESNT EXISTS CREATE NEW ONE:=" . $customerOld['CustCode']);
            $customer = new Customers();
            $em->persist($customer);
            foreach ($customerOld as $fieldname => $cell) {
                $customer->set($fieldname, $cell);
            }
            $customer->setActivate(false);
        }

        return $customer;
    }

    public function createNewVehtypeIfNotExists($Oldvehtype)
    {
        $em = $this->container->get('doctrine.orm.entity_manager');

        $vehtype = $em->getRepository(Vehtypes::class)->findOneBy(array('vehdesc' => $Oldvehtype['VehDesc']));
        if (!$vehtype instanceof Vehtypes && !empty($Oldvehtype['CustCode'])) {
            dump("vehtype DOESNT EXISTS CREATE NEW ONE:=" . $Oldvehtype['CustCode']);
            $vehtype = new Vehtypes();
            $em->persist($vehtype);
            foreach ($Oldvehtype as $fieldname => $cell) {
                $vehtype->set($fieldname, $cell);
            }
            $vehtype->setActive(false);
        }

        return $vehtype;
    }

    public function createNewDriverIfNotExists($oldDriver)
    {
        $em = $this->container->get('doctrine.orm.entity_manager');

        $driver = $em->getRepository(Drivers::class)->findOneBy(array('drivernum' => $oldDriver['DriverNum']));
        if (!$driver instanceof Drivers && !empty($oldDriver['DriverNum'])) {
            dump("DRIVER DOESNT EXISTS CREATE NEW ONE:=" . $oldDriver['DriverNum']);
            $driver = new Drivers();
            $em->persist($driver);
            foreach ($oldDriver as $fieldname => $cell) {
                $driver->set($fieldname, $cell);
            }
            $driver->setActive(false);
        }

        return $driver;
    }

    public function createNewRateIfNotExists($rateOld)
    {
        $em = $this->container->get('doctrine.orm.entity_manager');

        $rate = $em->getRepository(Rates::class)->findOneBy(array('rateDescription' => $rateOld['Rate_Description']));
        if (!$rate instanceof Rates && !empty($rateOld['Rate_Description'])) {
            dump("RATE DOESNT EXISTS CREATE NEW ONE:=" . $rateOld['Rate_Description']);
            $rate = new Rates();
            $em->persist($rate);
            foreach ($rateOld as $fieldname => $cell) {
                $rate->set($fieldname, $cell);
            }
            $rate->setActive(false);
        }

        return $rate;
    }
}