<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of services
 *
 * @author genjerator
 */

namespace Numa\CCCAdminBundle\Lib;

use Numa\CCCAdminBundle\Entity\Batch;
use Numa\CCCAdminBundle\Entity\BillingPeriod;
use Numa\CCCAdminBundle\Entity\Customers;
use Numa\CCCAdminBundle\Entity\Probills;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class BatchServices
{
    use ContainerAwareTrait;


    protected $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    public function generateNameFromDate( $date){
        if($date instanceof \DateTime) {
            $month = strtoupper($date->format("M"));
            $day = $date->format("d");
            $year = $date->format("y");
            return $month . $day . "-" . $year;
        }
        return "";
    }

    public function generateInvoiceNumbers(BillingPeriod $billingPeriod,$invoice,$gst){
        $em = $this->container->get('doctrine')->getManager();
        $probills = $em->getRepository(Probills::class)->findByBillingPeriods($billingPeriod->getId(),"customer");
        $i=($invoice-1);
        $oldCustomer =0;
        foreach($probills as $probill){
            $this->calculateGst($probill,$gst);
            $customer = $probill->getCustomers();
            if($customer instanceof Customers){
                if($oldCustomer!=$customer->getId()){
                    $i++;
                }
                $probill->setInvoice($i);
                $oldCustomer=$customer->getId();

            }

        }

        $billingPeriod->setDateFinished(new \DateTime("NOW"));
        $em->flush();
    }

    public function calculateGst(Probills &$probill,$gst){
        $length = strlen($gst);
        $gstValue = floatval($gst);

        if(substr($gst, -1) === "%"){
            $gstValue=$gstValue/100;
        }
        $customer = $probill->getCustomers();
        $subtotal = $probill->getSubtotal();
        $subtotalMinusDiscount = $subtotal;
        $taxcode  = $probill->getTaxcode();
        if($customer instanceof Customers && strtoupper($taxcode)=="G"){
            $discount = $customer->getDiscount();
            if($discount>0){
                //subtotalDiscounted = (Double)(discount * subtotal);
                //subtotalminusdiscount = subtotalminusdiscount - subtotalDiscounted;
                $subtotalDiscount = $subtotal*$discount;
                $subtotalMinusDiscount = $subtotal-$subtotalDiscount;

            }
            $probill->setGstAmt($subtotalMinusDiscount * $gstValue);
            $probill->setBalance($subtotalMinusDiscount + $probill->getGstAmt());
        }
        if(strtoupper($taxcode)=="E"){
            $probill->setGstAmt(0);
        }
    }

    public function createIfNotExists($em,$date)
    {
        $batch = $em->getRepository(Batch::class)->findOneBy(array("batch_date"=>$date));

        if(!$batch instanceof Batch){
            //dump("NOT FOUND A BATCH");
            $batch = new Batch();
            $batch->setBatchDate($date);
            $em->persist($batch);
            $em->flush();
        }else{
            //dump("FOUND A BATCH");
        }
        //dump($batch->getBatchDate());

        return $batch;

    }

}
