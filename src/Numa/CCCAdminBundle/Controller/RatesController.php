<?php

namespace Numa\CCCAdminBundle\Controller;

use Numa\CCCAdminBundle\Entity\Rates;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Rate controller.
 *
 */
class RatesController extends Controller
{
    /**
     * Lists all rate entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $rates = $em->getRepository('NumaCCCAdminBundle:Rates')->findAll();

        return $this->render('NumaCCCAdminBundle:rates:indexDataGrid.html.twig', array(
            'rates' => $rates,
        ));
    }

    /**
     * Creates a new rate entity.
     *
     */
    public function newAction(Request $request)
    {
        $rate = new Rates();
        $form = $this->createForm('Numa\CCCAdminBundle\Form\RatesType', $rate);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($rate);
            $em->flush();

            return $this->redirectToRoute('rates_show', array('id' => $rate->getId()));
        }

        return $this->render('NumaCCCAdminBundle:rates:new.html.twig', array(
            'rate' => $rate,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a rate entity.
     *
     */
    public function showAction(Rates $rate)
    {
        $deleteForm = $this->createDeleteForm($rate);

        return $this->render('NumaCCCAdminBundle:rates:show.html.twig', array(
            'rate' => $rate,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing rate entity.
     *
     */
    public function editAction(Request $request, Rates $rate)
    {
        $deleteForm = $this->createDeleteForm($rate);
        $editForm = $this->createForm('Numa\CCCAdminBundle\Form\RatesType', $rate);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('rates_edit', array('id' => $rate->getId()));
        }

        return $this->render('NumaCCCAdminBundle:rates:edit.html.twig', array(
            'rate' => $rate,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a rate entity.
     *
     */
    public function deleteAction(Request $request, Rates $rate)
    {
        $form = $this->createDeleteForm($rate);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($rate);
            $em->flush();
        }

        return $this->redirectToRoute('rates_index');
    }

    /**
     * Creates a form to delete a rate entity.
     *
     * @param Rates $rate The rate entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Rates $rate)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('rates_delete', array('id' => $rate->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    public function activateAction(Request $request)
    {
        $activate = $request->attributes->get('activate');

        $ids = $this->get("numa.uigrid")->getSelectedIds($request);
        $em = $this->getDoctrine()->getManager();
        $em->getRepository(Rates::class)->activate($ids, $activate);
        die();
    }

    public function exportAction(){
        $em = $this->getDoctrine()->getManager();
        $rates = $em->getRepository(Rates::class)->findAll();

        $html= $this->render('NumaCCCAdminBundle:rates:index_excel.html.twig', array(
            'rates' => $rates,
        ));

        // save $table inside temporary file that will be deleted later
        return $this->get("numa.report")->htmlToXls($html->getContent(),"Rates",'Rates.xls');
    }
}
