<?php

namespace Numa\CCCAdminBundle\Controller;

use Doctrine\DBAL\Driver\PDOIbm\Driver;
use Numa\CCCAdminBundle\Entity\BillingPeriod;
use Numa\CCCAdminBundle\Entity\Cheque;
use Numa\CCCAdminBundle\Entity\Customers;
use Numa\CCCAdminBundle\Entity\Drivers;
use Numa\CCCAdminBundle\Entity\Media;
use Numa\CCCAdminBundle\Entity\PayPeriod;
use Numa\CCCAdminBundle\Entity\Probills;
use Numa\CCCAdminBundle\Form\PayPeriodType;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Intl\NumberFormatter\NumberFormatter;

/**
 * Payperiod controller.
 *
 */
class PayPeriodController extends Controller
{
    /**
     * Lists all billingPeriod entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $payPeriods = $em->getRepository(PayPeriod::class)->findBy(array(), array("id" => "desc"));
        return $this->render('NumaCCCAdminBundle:payperiod:index.html.twig', array(
            'payPeriods' => $payPeriods,
        ));
    }

    /**
     * Creates a new billingPeriod entity.
     *
     */
    public function newAction(Request $request)
    {
        $payPeriod = new PayPeriod();
        $form = $this->createForm(PayPeriodType::class, $payPeriod);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($payPeriod);
            $em->flush();

            return $this->redirectToRoute('payperiod_index');
        }

        return $this->render('NumaCCCAdminBundle:payperiod:new.html.twig', array(
            'payPeriod' => $payPeriod,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a billingPeriod entity.
     *
     */
    public function showAction(PayPeriod $payPeriod)
    {

        return $this->render('NumaCCCAdminBundle:billingperiod:show.html.twig', array(
            'billingPeriod' => $payPeriod,
        ));
    }

    /**
     * Displays a form to edit an existing PayPeriod entity.
     *
     */
    public function editAction(Request $request, PayPeriod $payPeriod)
    {
        $editForm = $this->createForm(PayPeriodType::class, $payPeriod);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->flush();
            $this->addFlash("success", "PayPeriod updated");
            return $this->redirectToRoute('payperiod_index', array('id' => $payPeriod->getId()));
        }

        return $this->render('NumaCCCAdminBundle:payperiod:edit.html.twig', array(
            'payPeriod' => $payPeriod,
            'edit_form' => $editForm->createView(),
        ));
    }

    public function generateAction(Request $request, PayPeriod $payPeriod)
    {
        $em = $this->getDoctrine()->getManager();
        $billingPeriod = $payPeriod->getBillingPeriod();
        $drivers = $em->getRepository(BillingPeriod::class)->getDriversWithProbills($billingPeriod, $payPeriod->getType());

        foreach ($drivers as $driver) {
            $cheque = $em->getRepository(Cheque::class)->findOneBy(array("PayPeriod" => $payPeriod, "Driver" => $driver));
            $lumpSum = $em->getRepository(Drivers::class)->getLumpSumAmount($driver->getId(), $billingPeriod->getId());
            if (!$cheque instanceof Cheque) {
                $cheque = new Cheque();

                $em->persist($cheque);

            }
            $wcb = $payPeriod->getWcbRate() * $payPeriod->getWcbDeduction() * $lumpSum;
            $cheque->setWcb($wcb);
            $cheque->setDeducTotal($wcb);
            $cheque->setNetAmount($lumpSum - $wcb);
            $cheque->setLumpSumPayAmt($lumpSum);
            $cheque->setChequeDate($payPeriod->getChequeDate());
            $cheque->setPayPeriod($payPeriod);
            $cheque->setDriver($driver);
            $cheque->setWcbRate($payPeriod->getWcbRate());
            $cheque->setWcbDeduction($payPeriod->getWcbDeduction());
            $number = $cheque->getNetAmount();
            $word = $this->get("numa.common")->number_to_word($number);

            $cheque->setAmountInWord($word);
//
        }
        $em->flush();

        $this->addFlash("success", "Checks are generated");
        return $this->redirectToRoute("payperiod_index");
    }

    public function archiveAction(Request $request, PayPeriod $payPeriod)
    {
        $archive = $request->attributes->get('archive');
        $payPeriod->setStatus($archive);
        $em = $this->getDoctrine()->getManager();
        $em->flush();
        if ($archive == 2) {
            $this->addFlash("success", "Pay Period " . $payPeriod->getName() . " is archived");
        } elseif ($archive == 1) {
            $this->addFlash("success", "Pay Period " . $payPeriod->getName() . " is unarchived");
        }
        return $this->redirectToRoute('payperiod_index');
    }
}
