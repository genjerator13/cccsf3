<?php

namespace Numa\CCCAdminBundle\Controller;

use Numa\CCCAdminBundle\Entity\BillingPeriod;
use Numa\CCCAdminBundle\Entity\Cheque;
use Numa\CCCAdminBundle\Entity\Drivers;
use Numa\CCCAdminBundle\Entity\PayPeriod;
use Numa\CCCAdminBundle\Lib\Report\Cheque\ChequePrintReport;
use Numa\CCCAdminBundle\Lib\Report\Cheque\DriverExpenseReport;
use Numa\CCCAdminBundle\Lib\Report\Cheque\WCBReport;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Numa\CCCAdminBundle\Form\ChequeType;

/**
 * Cheque controller.
 *
 */
class ChequeController extends Controller
{
    /**
     * Lists all cheque entities.
     *
     */
    public function indexAction(PayPeriod $pay_period)
    {
        $em = $this->getDoctrine()->getManager();

        $cheques = $em->getRepository('NumaCCCAdminBundle:Cheque')->findBy(array("PayPeriod" => $pay_period));

        return $this->render('NumaCCCAdminBundle:Cheque:index.html.twig', array(
            'cheques' => $cheques,
            'pay_period' => $pay_period
        ));
    }

    /**
     * Creates a new cheque entity.
     *
     */
    public function newAction(BillingPeriod $billing_period, Request $request)
    {

        $chequeDate = $request->query->get('cheque_date');

        $chequeDate = \DateTime::createFromFormat("Y-m-d", $chequeDate);
        $driverWeekly = $request->query->get('driver_weekly');
        $wcbRate = $request->query->get('wcb_rate');
        $wcbDeduction = $request->query->get('wcb_deduction');

        $cheque = new Cheque();

        $cheque->setBillingPeriod($billing_period);
        $cheque->setChequeDate($chequeDate);
        $cheque->setWcbRate($wcbRate);
        $cheque->setWcb($wcbDeduction);

        $form = $this->createForm(ChequeType::class, $cheque, array('frame' => $driverWeekly));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $cheque->setBillingPeriod($billing_period);
            $em = $this->getDoctrine()->getManager();
            $em->persist($cheque);
            $em->flush();

            return $this->redirectToRoute('cheque_index', array('billing_period' => $billing_period->getId()));
        }

        return $this->render('NumaCCCAdminBundle:Cheque:new.html.twig', array(
            'cheque' => $cheque,
            'cheque_date' => $chequeDate,
            'driver_weekly' => $driverWeekly,
            'wcb_rate' => $wcbRate,
            'wcb_deduction' => $wcbDeduction,
            'billing_period' => $billing_period,
            'form' => $form->createView(),
        ));
    }

    public function printChequeAction(Request $request)
    {

        $chequeId = intval($request->query->get('cheque'));
        $em = $this->getDoctrine()->getManager();
        $cheque = $em->getRepository(Cheque::class)->find($chequeId);
        $wcbReport = new ChequePrintReport($this->container, $cheque->getPayPeriod());

        $wcbReport->setCheques(array($cheque));
        $pdf = $wcbReport->rendermPDF();

        die();

    }

    public function printWcbAction(Request $request, PayPeriod $pay_period)
    {
        $wcbReport = new WCBReport($this->container, $pay_period);

        $xls = $wcbReport->renderXLS();
        die();
    }

    public function printAllChequesAction(Request $request, PayPeriod $pay_period)
    {
        $wcbReport = new ChequePrintReport($this->container, $pay_period);
        $em = $this->getDoctrine()->getManager();
        $cheques = $em->getRepository(Cheque::class)->findAllInPayperiod($pay_period->getId());
        $wcbReport->setCheques($cheques);
        $pdf = $wcbReport->rendermPDF();
        die();
    }

    public function printDriverExpenseAction(Request $request,  Drivers $driver )
    {
        $driverExpense = new DriverExpenseReport($this->container, $driver);
        $xls = $driverExpense->renderXLS();
        dump("AAA");
        die();

    }

    public function entryAction(PayPeriod $pay_period, Request $request)
    {
        $chequeDate = $request->query->get('cheque_date');
        if ($chequeDate instanceof \DateTime) {
            $chequeDate = \DateTime::createFromFormat("Y-m-d", $chequeDate);
        }

        $driverWeekly = $pay_period->getType();
        $wcbRate = $request->query->get('wcb_rate');
        $wcbDeduction = $request->query->get('wcb_deduction');

        $cheque = new Cheque();

        $cheque->setPayPeriod($pay_period);
        $cheque->setWcbRate($wcbRate);
        $cheque->setWcb($wcbDeduction);

        $form = $this->createForm(ChequeType::class, $cheque, array('frame' => $driverWeekly));

        return $this->render('NumaCCCAdminBundle:Cheque:entry.html.twig', array(
            'cheque' => $cheque,
            'cheque_date' => $chequeDate,
            'driver_weekly' => $driverWeekly,
            'wcb_rate' => $wcbRate,
            'wcb_deduction' => $wcbDeduction,
            'pay_period' => $pay_period,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a cheque entity.
     *
     */
    public function showAction(Cheque $cheque)
    {
        $deleteForm = $this->createDeleteForm($cheque);

        return $this->render('cheque/show.html.twig', array(
            'cheque' => $cheque,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing cheque entity.
     *
     */
    public function editAction(Request $request, Cheque $cheque)
    {
        $editForm = $this->createForm('Numa\CCCAdminBundle\Form\ChequeType', $cheque);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('cheque_edit', array('id' => $cheque->getId()));
        }

        return $this->render('NumaCCCAdminBundle:Cheque:new.html.twig', array(
            'cheque' => $cheque,
            'billing_period' => $cheque->getBillingPeriod(),
            'form' => $editForm->createView(),
        ));
    }

    /**
     * Deletes a cheque entity.
     *
     */
    public function deleteAction(Request $request, Cheque $cheque)
    {
        $form = $this->createDeleteForm($cheque);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($cheque);
            $em->flush();
        }

        return $this->redirectToRoute('cheque_index');
    }

    public function dispatchAction(Request $request)
    {
        $form = $this->createPayPeriodSelectForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $pay_period = $data['pay_period'];
            return $this->redirectToRoute("cheque_index", array("pay_period" => $pay_period->getId()));
        }
        return $this->render('NumaCCCAdminBundle:Cheque:precheque.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    public function beforeNewAction(Request $request)
    {
        $form = $this->createBeforeNewForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $billing_period = $data['billing_period'];
            $chequeDate = $data['cheque_date'];
            $chequeDate = $chequeDate->format("Y-m-d");
            $driverWeekly = $data['driver_weekly'];
            $wcbRate = $data['wcb_rate'];
            $wcbDeduction = $data['wcb_deduction'];

            return $this->redirectToRoute("cheque_entry", array(
                "billing_period" => $billing_period->getId(),
                "cheque_date" => $chequeDate,
                "driver_weekly" => $driverWeekly,
                "wcb_rate" => $wcbRate,
                "wcb_deduction" => $wcbDeduction,
            ));
        }
        return $this->render('NumaCCCAdminBundle:Cheque:beforeNewCheque.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * Creates a form to delete a cheque entity.
     *
     * @param Cheque $cheque The cheque entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private
    function createDeleteForm(Cheque $cheque)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('cheque_delete', array('id' => $cheque->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    public
    function allDriversAction(BillingPeriod $billing_period)
    {
        $em = $this->getDoctrine()->getManager();
        $drivers = $em->getRepository(Drivers::class)->findBy(array(), array("drivernum" => "asc"));
        return $this->render('NumaCCCAdminBundle:Cheque:allDrivers.html.twig', array(
            'drivers' => $drivers,
            'billingPeriod' => $billing_period,
        ));
    }

    private function createPayPeriodSelectForm():Form
    {
        $em = $this->getDoctrine()->getManager();
        $payPeriods = $em->getRepository(PayPeriod::class)->findAll();
        $form = $this->createFormBuilder();
        $form->add('pay_period', ChoiceType::class, array('choices' => $payPeriods, 'choice_label' => 'name'));
//        $form->add('driver_weekly', ChoiceType::class, array('choices' => array("2 week"=>2,"4 week"=>4)));
//        $form->add('cheque_date', DateType::class,array("required"=>false,'data' => new \DateTime("now")));
//        $form->add('wcb_rate', NumberType::class,array("required"=>false));
//        $form->add('wcb_deduction', NumberType::class,array("required"=>false));

        return $form->getForm();
    }

    private function createBeforeNewForm():Form
    {
        $em = $this->getDoctrine()->getManager();
        $batches = $em->getRepository(BillingPeriod::class)->getLastBillingPeriods();
        $form = $this->createFormBuilder();
        //$form->add('billing_period', ChoiceType::class, array('choices' => $batches));
        $form->add('billing_period', EntityType::class,
            array('choice_name' => 'displayName',
                'class' => BillingPeriod::class,
                //'placeholder' => 'Choose a Billing Period',
                'query_builder' => function ($lr) {
                    // echo get_class($lr);
                    return $lr->createQueryBuilder('b')
                        ->addOrderBy("b.started", "DESC")
                        ->setMaxResults(10);
                },
            )
        );

        $form->add('driver_weekly', ChoiceType::class, array('choices' => array("2 week" => 2, "4 week" => 4)));
        $form->add('cheque_date', DateType::class, array("required" => false, 'data' => new \DateTime("now")));
        $form->add('wcb_rate', NumberType::class, array("required" => false));
        $form->add('wcb_deduction', NumberType::class, array("required" => false));

        return $form->getForm();
    }


}
