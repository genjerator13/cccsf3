<?php

namespace Numa\CCCAdminBundle\Controller;

use Numa\CCCAdminBundle\Entity\Batch;
use Numa\CCCAdminBundle\Entity\BillingPeriod;
use Numa\CCCAdminBundle\Form\BatchType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;

/**
 * Batch controller.
 *
 */
class BatchController extends Controller
{
    /**
     * Lists all batch entities.
     *
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $id = $request->query->get('id');
        $batches = $em->getRepository(Batch::class)->findAll();
        $billingPeriod = $em->getRepository(BillingPeriod::class)->find($id);

        return $this->render('NumaCCCAdminBundle:batch:index.html.twig', array(
            'batches' => $batches,
            'billingPeriod' => $billingPeriod,
        ));
    }

    public function indexBillingPeriodAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $batches = $em->getRepository('NumaCCCAdminBundle:Batch')->findByBillingPeriod($id);
        $billingPeriod = $em->getRepository('NumaCCCAdminBundle:BillingPeriod')->find($id);
        return $this->render('NumaCCCAdminBundle:batch:index.html.twig', array(
            'batches' => $batches,
            'billingPeriod' => $billingPeriod,
        ));
    }

    public function lockAction(Request $request, Batch $batch)
    {
//        if (!$this->get('security.authorization_checker')->isGranted("ROLE_SUPER_ADMIN")) {
//            throw $this->createAccessDeniedException("This user cannot lock or unlock the batch");
//        }
        $em = $this->getDoctrine()->getManager();
        $action = $request->attributes->get('action');
        $batch->unlock();
        if ($action == Batch::LOCK) {
            $batch->lock();
        }
        $em->flush($batch);
        return $this->redirectToRoute('billing_period_batch_index', array('id' => $batch->getBillingPeriod()->getId()));
    }

    /**
     * Creates a new batch entity.
     *
     */
    public function newAction(Request $request, BillingPeriod $billingPeriod)
    {
        $batch = new Batch();
        $form = $this->createForm(BatchType::class, $batch);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $batch->setBillingPeriod($billingPeriod);

            $em->persist($batch);
            $em->flush();

            return $this->redirectToRoute('billing_period_batch_index', array('id' => $billingPeriod->getId()));
        }

        return $this->render('NumaCCCAdminBundle:batch:new.html.twig', array(
            'batch' => $batch,
            'form' => $form->createView(),
            'id' => $billingPeriod->getId(),
        ));
    }

    /**
     * Finds and displays a batch entity.
     *
     */
    public function showAction(Batch $batch)
    {
        $deleteForm = $this->createDeleteForm($batch);

        return $this->render('NumaCCCAdminBundle:batch:show.html.twig', array(
            'batch' => $batch,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing batch entity.
     *
     */
    public function editAction(Request $request, Batch $batch)
    {

        if (!$this->get('security.authorization_checker')->isGranted("ROLE_SUPER_ADMIN") && $batch->isLocked()) {
            throw $this->createAccessDeniedException("This user cannot edit locked  batch");
        }
        $deleteForm = $this->createDeleteForm($batch);
        $editForm = $this->createForm('Numa\CCCAdminBundle\Form\BatchType', $batch);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('batch_edit', array('id' => $batch->getId()));
        }

        return $this->render('NumaCCCAdminBundle:batch:edit.html.twig', array(
            'batch' => $batch,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    public function finalizeAction(Request $request, Batch $batch)
    {

        if (!$this->get('security.authorization_checker')->isGranted("ROLE_SUPER_ADMIN") &&
            !$this->get('security.authorization_checker')->isGranted("ROLE_CORRECTION")
        ) {
            throw $this->createAccessDeniedException("This user cannot edit locked  batch");
        }

        $batch->setStatus(2);
        $this->getDoctrine()->getManager()->flush();
        $request->getSession()->getFlashBag()->add('success', 'The batch# ' . $batch->getName() . " has been finalized!");
        if ($this->get('security.authorization_checker')->isGranted("ROLE_CORRECTION")) {


            return $this->redirectToRoute('numa_ccc_admin_dataentry');
        }
        return $this->redirectToRoute('batch_index', array('id' => $batch->getBillingPeriodId()));
    }

    /**
     * Deletes a batch entity.
     *
     */
    public function deleteAction(Request $request, Batch $batch)
    {
        $form = $this->createDeleteForm($batch);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($batch);
            $em->flush();
        }

        return $this->redirectToRoute('batch_index');
    }

    /**
     * Creates a form to delete a batch entity.
     *
     * @param Batch $batch The batch entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Batch $batch)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('batch_delete', array('id' => $batch->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    public function dataentryAction(Request $request)
    {
        $formType = $request->attributes->get('form');
        $finalized = $request->attributes->get('finalized');
        $securityContext = $this->container->get('security.authorization_checker');
        if ($securityContext->isGranted("ROLE_ALLCORRECTION")) {
            return $this->redirectToRoute("probill_datagrid_allcorrection");
        }
        $form = $this->createDataentryForm($formType, $finalized);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $batchid = 0;
            if (array_key_exists("billingPeriod", $data)) {
                $batchid = $data['billingPeriod'];
            }

            $batch = $em->getRepository(Batch::class)->find($batchid);

            if ($data['formType'] == 'dataentry') {
                if ($securityContext->isGranted("ROLE_ADMIN")) {
                    throw $this->createAccessDeniedException("Access Denied");
                }
                if ($securityContext->isGranted("ROLE_CORRECTION")) {
                    return $this->redirectToRoute("probill_datagrid_batch", array('id' => $batch->getId()));
                }

                return $this->redirectToRoute("probill_datagrid_batch_" . strtolower($data['billType']), array('id' => $batch->getId()));
            }
        }
        return $this->render('NumaCCCAdminBundle:batch:dataentry.html.twig', array(
            'form' => $form->createView(),
            'formType' => $formType,
            'finalized' => $finalized,
        ));
    }

    public function dataentryroAction(Request $request)
    {
        $formType = $request->attributes->get('form');
        $finalized = $request->attributes->get('finalized');
        $form = $this->createDataentryRoForm($formType, $finalized);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $billingPeriod = $em->getRepository(BillingPeriod::class)->find($data['billingPeriod']);


            return $this->redirectToRoute("probill_datagrid_readonly",array('billingPeriod'=>$billingPeriod->getId()));

        }
        return $this->render('NumaCCCAdminBundle:batch:dataentry.html.twig', array(
            'form' => $form->createView(),
            'formType' => $formType,
            'finalized' => $finalized,
        ));
    }

    private function createDataentryForm($formType = 'probillEntry', $finalized = false):Form
    {
        $em = $this->getDoctrine()->getManager();
        $batches = $em->getRepository(BillingPeriod::class)->getLastBillingPeriodsWithBatches(0);

        if ($formType == 'correction' && !$finalized) {
            $batches = $em->getRepository(BillingPeriod::class)->getLastBillingPeriodsWithBatches(1);
        }
        $securityContext = $this->container->get('security.authorization_checker');
        if ($securityContext->isGranted("ROLE_CORRECTION")) {
            $role = "CORRECTION";
            $batches = $em->getRepository(BillingPeriod::class)->getLastBillingPeriodsWithBatches(1);
        }
        if ($securityContext->isGranted("ROLE_ALLCORRECTION")) {
            $role = "ALLCORRECTION";
            $batches = $em->getRepository(BillingPeriod::class)->getLastBillingPeriodsWithBatches(2);

        }

        //array_unshift($batches , 'item1');
        $form = $this->createFormBuilder();
        $form->add('billType', ChoiceType::class, array('choices' => array('CTY' => 'CTY', 'HWY' => "HWY")));
        $form->add('billingPeriod', ChoiceType::class, array('label' => 'Data Entry Batch', 'choices' => $batches, 'required' => false, 'empty_data' => 'Select the Batch'));
        $form->add('formType', HiddenType::class, array('data' => $formType));
        $form->add('finalized', HiddenType::class, array('data' => $finalized));
        if ($securityContext->isGranted("ROLE_CORRECTION")) {
            $form->remove('billType');
        }
        if ($securityContext->isGranted("ROLE_ALLCORRECTION")) {
            $form->remove('billType');
        }
        if ($formType == 'correction' && $finalized) {
            $form->remove('billingPeriod');
        }

        if ($formType == 'correction' && !$finalized) {
            $form->remove('billType');
        }
        return $form->getForm();
    }

    private function createDataentryRoForm():Form
    {
        $em = $this->getDoctrine()->getManager();
        $billingPeriods = $em->getRepository(BillingPeriod::class)->getLastBillingPeriods();
        $bperiod = array();
        foreach ($billingPeriods as $bp) {
            $bperiod[$bp->getName()] = $bp->getId();
        }
        //array_unshift($batches , 'item1');
        $form = $this->createFormBuilder();
        $form->add('billingPeriod', ChoiceType::class, array('label' => 'Data Entry Billing Period', 'choices' => $bperiod, 'required' => false, 'empty_data' => 'Select the Billing Period'));

        return $form->getForm();
    }
}
