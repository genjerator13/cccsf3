<?php

namespace Numa\CCCAdminBundle\Controller;

use Numa\CCCAdminBundle\Entity\Batch;
use Numa\CCCAdminBundle\Entity\BillingPeriod;
use Numa\CCCAdminBundle\Entity\Drops;
use Numa\CCCAdminBundle\Entity\Pickup;
use Numa\CCCAdminBundle\Entity\Probills;
use Numa\CCCAdminBundle\Form\DropsType;
use Numa\CCCAdminBundle\Form\PickupType;
use Numa\CCCAdminBundle\Form\ProbillsType;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Exception\NotValidCurrentPageException;
use Pagerfanta\Pagerfanta;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Batch controller.
 *
 */
class ProbillController extends Controller
{
    /**
     * Lists all batch entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        //find all the probills by the request batch
        $probills = $em->getRepository('NumaCCCAdminBundle:Batch')->findAll();

        return $this->render('NumaCCCAdminBundle:probill:index.html.twig', array(
            'probills' => $probills,
        ));
    }

    /**
     * List all probills by batch id
     *
     */

    public function indexBatchAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $probills = $em->getRepository('NumaCCCAdminBundle:Probills')->findAllByBatchPB($id);
        $batch = $em->getRepository('NumaCCCAdminBundle:Batch')->find($id);
        return $this->render('NumaCCCAdminBundle:probill:index.html.twig', array(
            'probills' => $probills,
            'batch' => $batch,
        ));
    }



    /**
     * Creates a new batch entity.
     *
     */
    public function newAction(Request $request, int $batch_id)
    {
        $probill = new Probills();
        $form = $this->createForm(ProbillsType::class, $probill);
        $form->handleRequest($request);
        $pickupForm = $this->createPickupForm();
        $dropsForm = $this->createDropsForm();

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $batch = $em->getRepository(Batch::class)->find($batch_id);
            $probill->setBatchPB($batch);
            $em->persist($probill);
            $em->flush();

            return $this->redirectToRoute('probill_show', array('id' => $probill->getId()));
        }

        return $this->render('NumaCCCAdminBundle:probill:new.html.twig', array(
            'pickupForm' => $pickupForm->createView(),
            'dropsForm' => $dropsForm->createView(),
            'probill' => $probill,
            'form' => $form->createView(),
            'action' => 'new',
        ));
    }


    /**
     * Finds and displays a batch entity.
     *
     */
    public function showAction(Probills $probill)
    {
        return $this->render('NumaCCCAdminBundle:probill:show.html.twig', array(
            'probill' => $probill,
        ));
    }


    public function updateAjaxAction(Request $request)
    {
        $data = $request->request->all();

        $em = $this->getDoctrine()->getManager();

        $id = 0;
        if (!empty($data['id'])) {
            $id = intval($data['id']);
        }
        if ($id > 0) {
            $probill = $this->get("numa.probill")->updateProbillFromArray($data);
        }

        $serializer = $this->container->get('jms_serializer');

        $ret = array(
            "data" => $serializer->toArray($probill),
            "id" => $id
        );

        return new JsonResponse($ret);
    }

    public function newAjaxAction(Request $request)
    {
        $data = $request->request->all();

        $edgId = 0;
        if(!empty($data['entityDataGridId'])) {
            $edgId = $data['entityDataGridId'];
        }
        $edg = 0;
        if(!empty($data['entityDataGrid'])) {
            $edg = $data['entityDataGrid'];
        }
        $billtype="";
        if(!empty($data['billtype'])) {
            $billtype = $data['billtype'];
        }
        $param = array();

        $param['entityDataGridId'] = $edgId;
        $param['entityDataGrid'] = $edg;
        $param['billtype'] = $billtype;
        $securityContext = $this->container->get('security.authorization_checker');
        if ($securityContext->isGranted("ROLE_ALLCORRECTION") ) {
            $em = $this->getDoctrine()->getManager();
            //if (!empty($data['entityDataGrid']) && $data['entityDataGrid'] == "batch" && !empty($data['entityDataGridId'])) {
            $lastBillingPeriod = $em->getRepository(Probills::class)->getLastBillingPeriodId();
            $batchId= $em->getRepository(Probills::class)->getLastBatchFromBillingPeriod($lastBillingPeriod);
            //$batch  = $em->getRepository(Batch::class)->find($batchId);
            $param['entityDataGrid'] = "batch";
            $param['entityDataGridId'] = $batchId;

        }
        $probill = $this->get("numa.probill")->insertNewProbillFromArray($param);

        $probill->setPDate(null);





        $serializer = $this->container->get('jms_serializer');

        return new JsonResponse($serializer->toArray($probill));
        //
    }



    public function deleteAjaxAction(Request $request)
    {
        $data = $request->request->all();

        $ret = array();
        $em = $this->getDoctrine()->getManager();
        $probill = $em->getRepository(Probills::class)->find(intval($data['id']));
        if (!$probill instanceof Probills) {
            $ret['error'] = "Probill does not exsists";
            return new JsonResponse($ret);
        }
        if (!$this->container->get("numa.probill")->isDeleteGranted($probill)) {
            $ret['error'] = "This user cannot delete the probill";
        }
        $serializer = $this->container->get('jms_serializer');
        $ret['data'] = $serializer->toArray($probill);
        //dump($ret);die();
        $em->remove($probill);
        $em->flush();


        return new JsonResponse($ret);
    }

    public function massDateUpdateAjaxAction(Request $request)
    {
        $data = $request->request->all()['data'];

        $date = $request->request->all()['date'];

        $ids = json_decode($data, true);
        $ids = array_filter($ids, function ($element) { return ($element != null); } );

        $ret = array();
        $em = $this->getDoctrine()->getManager();
        $error = false;
        if (!$this->validateDate($date)) {
            $ret['error'] = "Date not valid!";
            $error = true;
        }
        if (!is_array($ids)) {
            $ret['error'] = "Nothing is selected";
            $error = true;
        }
        if (!$error){
            $em->getRepository(Probills::class)->updateDates($ids, $date);
            $ret['success']=$ids;
        }

        return new JsonResponse($ret);

        //$probills = $em->getRepository(Probills::class)->find(intval($data['id']));
    }

    function validateDate($date, $format = 'Y-m-d')
    {
        $d = \DateTime::createFromFormat($format, $date);
        // The Y ( 4 digits year ) returns TRUE for any integer with any number of digits so changing the comparison from == to === fixes the issue.
        return $d && $d->format($format) === $date;
    }

    /**
     * Displays a form to edit an existing batch entity.
     *
     */
    public function editAction(Request $request, Probills $probill)
    {
        $editForm = $this->createForm(ProbillsType::class, $probill);
        $pickupForm = $this->createPickupForm();
        $dropsForm = $this->createDropsForm();
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {

            $this->getDoctrine()->getManager()->flush();
//            $request->getSession()->getFlashBag()
//                ->add('success', 'Probill '.$probill->getWaybill().' updated ');
//
//            return $this->redirectToRoute('probills_batch_index', array('id' => $probill->getBatchPB()->getId()));
        }

        return $this->render('NumaCCCAdminBundle:probill:new.html.twig', array(
            'dropsForm' => $dropsForm->createView(),
            'pickupForm' => $pickupForm->createView(),
            'id' => $probill->getId(),
            'form' => $editForm->createView(),
            'action' => 'edit',
        ));
    }

    public function getProbillBatchDatagridAction(Request $request, Batch $batch)
    {
        $securityContext = $this->container->get('security.authorization_checker');
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        if ($securityContext->isGranted("ROLE_CSR") && $batch->isLocked()) {
            throw $this->createAccessDeniedException("The batch is locked!");
        }
        $finalized=false;
        if ($securityContext->isGranted("ROLE_ALLCORRECTION")) {
            $finalized=true;
        }
        $pickupForm = $this->createPickupForm();
        $dropsForm = $this->createDropsForm();
        $form = $this->createForm(ProbillsType::class, new Probills());
        $billType = $request->attributes->get('billType');
        $role = $request->attributes->get('role');

        return $this->render('NumaCCCAdminBundle:probill:indexDataGrid.html.twig',
            array(
                'pickupForm' => $pickupForm->createView(),
                'dropsForm' => $dropsForm->createView(),
                'entity' => 'batch',
                'id' => $batch->getId(),
                'locked' => $batch->isLocked(),
                'finalized' => $finalized,
                'form' => $form->createView(),
                'billType' => $billType,
                'action' => "test",
                'user' => $user
            )
        );
    }

    public function readOnlyDatagridAction(Request $request,BillingPeriod $billingPeriod)
    {
        $securityContext = $this->container->get('security.authorization_checker');
        $user = $this->container->get('security.token_storage')->getToken()->getUser();

        $finalized=false;
        if ($securityContext->isGranted("ROLE_ALLCORRECTION")) {
            $finalized=true;
        }
        $pickupForm = $this->createPickupForm();
        $dropsForm = $this->createDropsForm();
        $form = $this->createForm(ProbillsType::class, new Probills());
        $billType = $request->attributes->get('billType');

        return $this->render('NumaCCCAdminBundle:probill:indexReadOnlyDataGrid.html.twig',
            array(
                'pickupForm' => $pickupForm->createView(),
                'dropsForm' => $dropsForm->createView(),
                'entity' => 'billing_period',
                'id' => $billingPeriod->getId(),
                'finalized' => $finalized,
                'form' => $form->createView(),
                'billType' => $billType,
                'action' => "test",
                'user' => $user,
                'readonly'=>true
            )
        );
    }

    public function getProbillBatchDatagridACAction(Request $request)
    {
        $securityContext = $this->container->get('security.authorization_checker');
        $user = $this->container->get('security.token_storage')->getToken()->getUser();

        $finalized=false;
        if ($securityContext->isGranted("ROLE_ALLCORRECTION")) {
            $finalized=true;
        }
        $pickupForm = $this->createPickupForm();
        $dropsForm = $this->createDropsForm();
        $form = $this->createForm(ProbillsType::class, new Probills());
        $billType = $request->attributes->get('billType');
        return $this->render('NumaCCCAdminBundle:probill:indexDataGrid.html.twig',
            array(
                'pickupForm' => $pickupForm->createView(),
                'dropsForm' => $dropsForm->createView(),
                'entity' => 'batch',
                'id'=>0,
                'finalized' => true,
                'form' => $form->createView(),
                'billType' => "ALL",
                'action' => "test",
                'user' => $user
            )
        );
    }

    public function getProbillDatagridAction($id)
    {

        $pickupForm = $this->createPickupForm();
        $dropsForm = $this->createDropsForm();

        $probill = new Probills();
        $form = $this->createForm(ProbillsType::class, $probill);
        return $this->render('NumaCCCAdminBundle:probill:indexDataGrid.html.twig',
            array(
                'pickupForm' => $pickupForm->createView(),
                'dropsForm' => $dropsForm->createView(),
                'entity' => 'all',
                'action' => 'edit',
                'form' => $form->createView()
            )
        );
    }

    /**
     * Deletes a batch entity.
     *
     */
    public function deleteAction(Request $request, Batch $batch)
    {
        $form = $this->createDeleteForm($batch);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($batch);
            $em->flush();
        }

        return $this->redirectToRoute('batch_index');
    }


    /**
     * Creates a form to delete a batch entity.
     *
     * @param Batch $batch The batch entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Batch $batch)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('batch_delete', array('id' => $batch->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }


    /**
     * Creates a popup form to create pickups for the probill
     *
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createPickupForm()
    {
        $form = $this->createFormBuilder();
        for ($i = 1; $i <= 15; $i++) {
            $form->add('address_' . $i, TextType::class, array('label' => "Address"));
            $form->add('pieces_' . $i, TextType::class, array('label' => "Pieces"));
            $form->add('weight_' . $i, TextType::class, array('label' => "Weight"));
        }

        return $form->getForm();
    }

    /**
     * Creates a popup form to create pickups for the probill
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDropsForm()
    {
        $form = $this->createFormBuilder();
        for ($i = 1; $i <= 15; $i++) {
            $form->add('address_' . $i, TextType::class, array('label' => "Address"));
            $form->add('pieces_' . $i, TextType::class, array('label' => "Pieces"));
            $form->add('weight_' . $i, TextType::class, array('label' => "Weight"));
            $form->add('signature_' . $i, TextType::class, array('label' => "Signature"));
        }

        return $form->getForm();

    }

    /**
     * Finds and displays a Probills entity.
     *
     */
    public function scanAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository(Probills::class)->find($id);

        if (!$entity instanceof Probills) {
            throw $this->createNotFoundException('Unable to find Probills entity.');
        }

        $upload_url = $this->container->getParameter('scans_url');
        $billingPeriod = $entity->getBatchPB()->getBillingPeriod();
//        //$batch = $entity->getBatchX();
//        dump($entity->getBatchId());
//        dump($batch->getClosed());die();
        //$dir = strtoupper($batch->getClosed()->format('Md-Y'));
        $image_src = $upload_url . "/" . $billingPeriod->getName() . "/" . $entity->getWaybill() . ".jpg";

        return $this->render('NumaCCCAdminBundle:Probills:scan.html.twig', array(
            'image_src' => $image_src));
    }
}
