<?php

namespace Numa\CCCAdminBundle\Controller;

use Cocur\BackgroundProcess\BackgroundProcess;
use Numa\CCCAdminBundle\Entity\BillingPeriod;
use Numa\CCCAdminBundle\Entity\Customers;
use Numa\CCCAdminBundle\Entity\Media;
use Numa\CCCAdminBundle\Entity\Probills;
use Numa\CCCAdminBundle\Form\BillingPeriodNewsletterType;
use Numa\CCCAdminBundle\Form\BillingPeriodType;
use Numa\CCCAdminBundle\Lib\Report\CustomerDeliveryReport;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

/**
 * Billingperiod controller.
 *
 */
class BillingPeriodController extends Controller
{
    /**
     * Lists all billingPeriod entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        //$billingPeriods = $em->getRepository('NumaCCCAdminBundle:BillingPeriod')->findBy(array("status"=>1),array("started"=>"desc"));

        //if($this->get('security.authorization_checker')->isGranted("ROLE_ADMIN") || $this->get('security.authorization_checker')->isGranted("ROLE_SUPER_ADMIN")){
            $billingPeriods = $em->getRepository('NumaCCCAdminBundle:BillingPeriod')->findBy(array(),array("started"=>"desc"));
       // }
        return $this->render('NumaCCCAdminBundle:billingperiod:index.html.twig', array(
            'billingPeriods' => $billingPeriods,
        ));
    }

    /**
     * Creates a new billingPeriod entity.
     *
     */
    public function newAction(Request $request)
    {
        $billingPeriod = new Billingperiod();
        $form = $this->createForm('Numa\CCCAdminBundle\Form\BillingPeriodType', $billingPeriod);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($billingPeriod);
            $em->flush();

            return $this->redirectToRoute('billingperiod_index');
        }

        return $this->render('NumaCCCAdminBundle:billingperiod:new.html.twig', array(
            'billingPeriod' => $billingPeriod,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a billingPeriod entity.
     *
     */
    public function showAction(BillingPeriod $billingPeriod)
    {
        $deleteForm = $this->createDeleteForm($billingPeriod);

        return $this->render('NumaCCCAdminBundle:billingperiod:show.html.twig', array(
            'billingPeriod' => $billingPeriod,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing billingPeriod entity.
     *
     */
    public function editAction(Request $request, BillingPeriod $billingPeriod)
    {
        $isNewsletter = $request->query->get('newsletter');

        $editForm = $this->createForm(BillingPeriodType::class, $billingPeriod);

        if($isNewsletter){
            $editForm = $this->createForm(BillingPeriodNewsletterType::class, $billingPeriod);
        }
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $file = $editForm['_newsletter']->getData();
            if(!empty($editForm['email_subject'])) {
                $subject = $editForm['email_subject']->getData();
                $template = $editForm['email_template']->getData();
            }

            // create media entity
            $media = new Media();
            if($billingPeriod->getNewsletter() instanceof Media){
                $media = $billingPeriod->getNewsletter();
            }

            if($file instanceof UploadedFile) {


                $media->setMimetype($file->getMimeType());
                $media->setName($file->getClientOriginalName());
                $fd = $file->openFile("rb");
                $filecontent = $fd->fread($file->getSize());
                $media->setContent(base64_encode($filecontent));

                $billingPeriod->setNewsletter($media);
                $em->persist($media);
            }

            if(!empty($subject)){
                $billingPeriod->setEmailSubject($subject);
                $billingPeriod->setEmailTemplate($template);

            }
            $em->flush();
            if($isNewsletter){
                $this->addFlash("success","Newsletter, Email Subject and body updated");
                return $this->redirectToRoute('billingperiod_billings');
            }
            $this->addFlash("success","Billing period updated");
            return $this->redirectToRoute('billingperiod_index', array('id' => $billingPeriod->getId()));
        }

        return $this->render('NumaCCCAdminBundle:billingperiod:edit.html.twig', array(
            'billingPeriod' => $billingPeriod,
            'edit_form' => $editForm->createView(),

        ));
    }

    /**
     * Deletes a billingPeriod entity.
     *
     */
    public function deleteAction(Request $request, BillingPeriod $billingPeriod)
    {
        $form = $this->createDeleteForm($billingPeriod);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($billingPeriod);
            $em->flush();
        }

        return $this->redirectToRoute('billingperiod_index');
    }

    /**
     * Creates a form to delete a billingPeriod entity.
     *
     * @param BillingPeriod $billingPeriod The billingPeriod entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(BillingPeriod $billingPeriod)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('billingperiod_delete', array('id' => $billingPeriod->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
    public function exportAction(BillingPeriod $billingPeriod)
    {
        $xml = $this->get("numa.billing_period")->exportToXml($billingPeriod);
        $xml->save("export/file.xml");
        return $this->render('NumaCCCAdminBundle:billingperiod:export.html.twig', [
            'billing' => $billingPeriod,
        ]);
    }

    public function importAction(BillingPeriod $billingPeriod)
    {
        $xml = $this->get("numa.billing_period")->importFromXml($billingPeriod);
        $form=$this->createImportXMLForm();
        return $this->render('NumaCCCAdminBundle:billingperiod:import.html.twig', [
            'billing' => $billingPeriod,
            'form' => $form->createView(),
        ]);
    }

    private function createImportXMLForm():Form{
        $em = $this->getDoctrine()->getManager();

        $form = $this->createFormBuilder();
        $form->add('xmlFile', FileType::class, array('label' => 'XML file'));
        $form->add('finish', SubmitType::class);

        return $form->getForm();
    }

    public function batchesAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $searchForm = $this->createNonBilledSearchForm();
        $searchForm->handleRequest($request);
        $formSubmit = false;
        $searchText='';
        if ($searchForm->isValid()) {
            $data = $searchForm->getData();
            $searchText = $data['text'];
            $formSubmit = true;
        }

        $user = $this->get('security.token_storage')->getToken()->getUser();
        $customer=null;
        if($user instanceof Customers){
            $customer = $user;
        }

        $lastBillingPeriod = $em->getRepository(Probills::class)->getLastBillingPeriodId();
        $nonBilled = $em->getRepository(Probills::class)->findNonBilled($searchText,$lastBillingPeriod,$customer,2);

        $pagerfanta = new Pagerfanta(new DoctrineORMAdapter($nonBilled));
        $pagerfanta->setMaxPerPage(15);
        $page = $request->get('page');

        if (!$page) {
            $page = 1;
        }
        $pagerfanta->setCurrentPage($page);
        $entities = $em->getRepository(BillingPeriod::class)->findBy(array("status"=>3),array('started'=>'DESC'),24);

        return $this->render('NumaCCCAdminBundle:billingperiod:batches.html.twig', array(
            'entities' => $entities,
            'formSubmit' => $formSubmit,
            'pagerfanta' => $pagerfanta,
            'searchForm' => $searchForm->createView(),
        ));
    }

    public function probillsImagesAction(Request $request) {
        $formSubmit = false;
        $custcode = $request->get('custcode');
        //$batchid = intval($request->get('batchid'));
        $billingPeriods = 0;
        $searchText = "";
        $em = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository(Probills::class);///??
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $cc="";
        if($user instanceof Customers) {
            $cc = $user->getCustcode();
        }

        $searchForm = $this->createSearchForm();
        $searchForm->handleRequest($request);

        if ($searchForm->isValid()) {
            $data = $searchForm->getData();
            $searchText = $data['text'];
            $billingPeriods = $data['batch'];
            $formSubmit = true;
        }

        if (!empty($custcode)) {
            $cc = $custcode;
        }
        $securityContext = $this->container->get('security.authorization_checker');
        //dump($securityContext->isGranted("ROLE_SUPER_ADMIN"));die();
        if ($securityContext->isGranted("ROLE_SUPER_ADMIN") || $securityContext->isGranted("ROLE_CSR") ) {
            $query = $repository->findAllByBillingPeriod(0, $searchText);
        } else {
            $query = $repository->findAllByCustomer($user, $billingPeriods, $searchText);
        }

        $billingPeriods = $em->getRepository(BillingPeriod::class)->findBy(array("active"=>1));
        $pagerfanta = new Pagerfanta(new DoctrineORMAdapter($query));
        $pagerfanta->setMaxPerPage(15);

        $page = $request->get('page');
        if (!$page) {
            $page = 1;
        }

        try {
            $pagerfanta->setCurrentPage($page);
        } catch (NotValidCurrentPageException $e) {
            throw new NotFoundHttpException();
        }

        $user = $this->get('security.token_storage')->getToken()->getUser();
        return $this->render('NumaCCCAdminBundle:billingperiod:probills_images.html.twig', array(
            'billingPeriods' => $billingPeriods,
            'formSubmit' => $formSubmit,
            'pagerfanta' => $pagerfanta,
            'custcode' => $cc,
            'searchForm' => $searchForm->createView(),
        ));

        //$entities = $query->getResult();
        //$entities = $em->getRepository('NumaCCCAdminBundle:Probills')->findBy(array('customersId' => '1536'));
    }

    /**
     * Creates a form to search, filter Probills
     *
     * @param Probills $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createSearchForm() {
        $data = array();
        $em = $this->getDoctrine()->getManager();

        $billingPeriods = $em->getRepository(BillingPeriod::class)->findBy(array("active"=>1));
        foreach ($billingPeriods as $key => $batch) {
            $from = "";
            if($batch->getStarted() instanceof \DateTime){
                $from = $batch->getStarted()->format('d/m/Y');
            }

            $to = "";
            if($batch->getClosed() instanceof \DateTime){
                $to = $batch->getClosed()->format('d/m/Y');
            }
            $data[$from." - ".$to] =  $batch->getId();
        }

        $form = $this->createFormBuilder($data)
            ->setAction($this->generateUrl('billingperiod_probills_images'))
            ->setMethod("GET")
            ->setAttribute('class', 'form-inline')
            ->add('batch', ChoiceType::class,array('choices'=>$data, 'label'=>'Batch','required'=>false,'empty_data'=>'All'))
            ->add('text', TextType::class ,array('label'=>'Waybill','required'=>false))
            ->add('submit', SubmitType::class,array('label'=>'Search','attr'=>array('class'=>"btn btn-primary")))
            ->getForm();

        return $form;
    }


    /**
     * Creates a form to search, filter Probills
     *
     * @param Probills $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createNonBilledSearchForm()
    {
        $data = array();

        $form = $this->createFormBuilder($data)
            ->setAction($this->generateUrl("billingperiod_batches"))
            ->setMethod("GET")
            ->setAttribute('class', 'form-inline')
            ->add('text', TextType::class, array('label' => 'Waybill', 'required' => false))
            ->add('submit', SubmitType::class, array('label' => 'Search', 'attr' => array('class' => "btn btn-primary")))
            ->add('all', SubmitType::class, array('label' => 'Show All', 'attr' => array('class' => "btn btn-success")))
            ->getForm();

        return $form;
    }

    public function billingAction() {
        $em = $this->getDoctrine()->getManager();
        $status=2;
        if($this->get('security.authorization_checker')->isGranted("ROLE_SUPER_ADMIN")){
            $status=null;
        }
        //dump($status);die();
        $entities = $em->getRepository(BillingPeriod::class)->getBillingPeriodByStatus($status);

        return $this->render('NumaCCCAdminBundle:billingperiod:billings.html.twig', array(
            'entities' => $entities,
        ));
    }

    public function closeAction(BillingPeriod $billingPeriod,Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $action = $request->attributes->get('action');
        $billingPeriod->setStatus($action);
        $billingPeriod->setDateFinished(new \DateTime());
        if($action==2) {
            $this->addFlash("success", "Billing Period: " . $billingPeriod->getId() . " is closed. Ready for Billing.");
        }elseif($action==3) {
            $this->addFlash("success", "Billing Period: " . $billingPeriod->getId() . " is ready for the customers.");
        }
        $em->flush($billingPeriod);
        return $this->redirectToRoute('billingperiod_index');
    }

    public function nonBilledDetailsAction(Request $request, $id)
    {
        if ($this->container->has('profiler')) {
            $this->container->get('profiler')->disable();
        }
        $em = $this->getDoctrine()->getManager();
        $cc = 0;


        $probill = $em->getRepository('NumaCCCAdminBundle:Probills')->find($id);
        //dump($data);die();
        $html = $this->renderView(
            'NumaCCCAdminBundle:reports:pendingDetails.html.twig', array(
                'probill' => $probill
            )
        );;

        return new Response(
            $html
        );
    }

    /**
     * Finds and displays a Probills entity.
     *
     */
    public function nonBilledscanAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository(Probills::class)->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Pending Probill entity.');
        }

        $upload_url = $this->container->getParameter('scans_url');
        $image_src = $upload_url. "/".$entity->getScanImage();

        return $this->render('NumaCCCAdminBundle:PendingProbills:pscan.html.twig', array(
            'image_src' => $image_src));
    }

    /**
     * Finds and displays a Probills entity.
     *
     */
    public function nonBilledPdfAction($id){

        $report = new CustomerDeliveryReport($this->container,new BillingPeriod());
        return $report->singleProbillReport($id);
        //return  $report->rendermPDF();
    }

    public function generateAction(Request $request,BillingPeriod $billingPeriod, $sending=0) {

        //$command = 'php ' . $this->get('kernel')->getRootDir() . '/console billing_period generateReports ' . $id;
        $msg = '';

        $command = 'php ' . $this->get('kernel')->getRootDir().'/../bin/console billing_period generateReports ' . $billingPeriod->getId();
        //$command = 'ls -lsa';

        //$process = new \Symfony\Component\Process\Process($command);

        //$process->start();

        $process = new BackgroundProcess($command);
        $process->run();
        //sleep(3);

        $request->getSession()->getFlashBag()->add('success', 'Emails and reports are created for batch# ' . $billingPeriod->getId());

        return $this->redirectToRoute('email_progress',array('billingPeriod'=>$billingPeriod->getId()));
    }

    public function zipScansAction(Request $request, BillingPeriod $billingPeriod){
        $em = $this->getDoctrine()->getManager();

        $user = $this->get('security.token_storage')->getToken()->getUser();
        $custcode = $request->query->get('cust');
        $custid = $request->query->get('cust');
        if($user instanceof Customers){
            if(!empty($user->getCustcode()))
            {
                $custcode = $user->getCustcode();
                $custid   = $user->getId();
            }
        }

        $scanPath   = $this->container->getParameter('scans_path');

        $foldername = $billingPeriod->getName();

        $filename   = $custcode."-".$foldername.".zip";
        $zipfolderPath = $scanPath.$foldername;
        $zipPath   = $zipfolderPath."/".$filename;

        $zip = new \ZipArchive();

        if(file_exists($zipPath)){

            $response = new Response(file_get_contents($zipPath));

            $d = $response->headers->makeDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $filename);
            $response->headers->set('Content-Disposition', $d);

            return $response;


            header('Content-Type', 'application/zip');
            header("Content-Description: File Transfer");
            header("Content-type: application/octet-stream");
            header('Content-disposition: attachment; filename="' . $filename . '"');
            header('Content-Length: ' . filesize($zipPath));



            return new Response();
            $response = new Response();
            $response->headers->set('Content-Description', 'File Transfer');
            $response->headers->set('Content-type', ' application/octet-stream');
            $response->headers->set('Content-disposition',  'attachment; filename="' . $filename . '"');
            $response->headers->set('Content-Length',  filesize($zipPath));
            $response->send();
        }
        $test = $zip->open($zipPath, \ZipArchive::CREATE);

        if ($test !== TRUE) {
            exit("cannot open <$filename>\n");
        }

//        foreach (new \DirectoryIterator($zipfolderPath) as $fileInfo) {
//            if($fileInfo->isDot()) continue;
//            echo $fileInfo->getFilename() . "<br>\n";
//        }
//        $zip->addFile($invoice, 'PdfInvoice.pdf');

        $probills = $em->getRepository(Probills::class)->findAllByBillingPeriod($billingPeriod->getId(),null,$custid)->getResult();
        foreach($probills as $probill)
        {
            $scanImgFilename = $probill->getWaybill().".jpg";
            $scanImgPath     = $zipfolderPath."/".$scanImgFilename;

            if(file_exists($scanImgPath)){

                $zip->addFile($scanImgPath, $scanImgFilename);
            }
        }

        //$zip->addFile("/var/www/ccc/web/scans/DEC31-15/0456305.jpg","test.jpg");
        $zip->close();
        if(file_exists($zipPath)){
            //header('Content-Type', 'application/zip');
            header("Content-Description: File Transfer");
            header("Content-type: application/octet-stream");
            header('Content-disposition: attachment; filename="' . $filename . '"');
            header('Content-Length: ' . filesize($zipPath));
            readfile($zipPath);
            return;
        }
        die();
    }
}
