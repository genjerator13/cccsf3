<?php

namespace Numa\CCCAdminBundle\Controller;

use Numa\CCCAdminBundle\Entity\Media;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;


/**
 * MediaThek controller.
 *
 */
class MediaController extends Controller
{

    public function showAction(Media $media = NULL)
    {

        if ($this->container->has('profiler'))
        {
            $this->container->get('profiler')->disable();
        }

//            $media = $this->getDoctrine()->getManager()->getRepository(Media::class)->find($id);

        if (strtoupper($media->getMimetype()) == "PDF" || strtolower($media->getMimetype()) == "application/pdf") {

            return $this->render('NumaCCCAdminBundle:Media:media.pdf.twig', array(
                'media' => $media
            ));
        }elseif (strtoupper($media->getMimetype()) == "XLS") {
            $tmpfile = tempnam(sys_get_temp_dir(), 'xls');
            file_put_contents($tmpfile, base64_decode($media->getContent()));



            $excel2 = \PHPExcel_IOFactory::createReader('Excel5');
            $excel2 = $excel2->load($tmpfile); // Empty Sheet

            $objWriter = \PHPExcel_IOFactory::createWriter($excel2, 'Excel5');

            header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            header("Content-Disposition: attachment;filename=".$media->getName());

            $objWriter->save('php://output');


            die();
        }
            return $this->render('NumaCCCAdminBundle:Media:media.xls.twig', array(
                'media' => $media
            ));

    }


}
