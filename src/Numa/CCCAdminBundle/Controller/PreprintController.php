<?php

namespace Numa\CCCAdminBundle\Controller;

use Numa\CCCAdminBundle\Entity\Preprint;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Preprint controller.
 *
 */
class PreprintController extends Controller
{
    /**
     * Lists all preprint entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        //$preprints = $em->getRepository('NumaCCCAdminBundle:Preprint')->findAll();

        return $this->render('NumaCCCAdminBundle:Preprint:index.html.twig', array(
            //'preprints' => $preprints,
        ));
    }

    /**
     * Creates a new preprint entity.
     *
     */
    public function newAction(Request $request)
    {
        $preprint = new Preprint();
        $form = $this->createForm('Numa\CCCAdminBundle\Form\PreprintType', $preprint);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($preprint);
            $em->flush();

            return $this->redirectToRoute('preprint_show', array('id' => $preprint->getId()));
        }

        return $this->render('preprint/new.html.twig', array(
            'preprint' => $preprint,
            'form' => $form->createView(),
        ));
    }

    public function printAction(Request $request, Preprint $preprint)
    {
        $mpdf = new \Mpdf\Mpdf(array('format' => 'letter-L', "margin_left" => 5, "margin_right" => 5, "margin_top" => 5, "margin_bottom" => 5));


        $mpdf->useOnlyCoreFonts = true;    // false is default
        $mpdf->SetTitle("PrePrint");
        $mpdf->SetAuthor("Custom Courier o.");
        $mpdf->SetDisplayMode('fullpage');

        $templating = $this->get('templating');

        $html = $this->render( 'NumaCCCAdminBundle:Preprint:preprint.pdf.twig',array("preprint"=>$preprint));

        $mpdf->WriteHTML($html);

        header('Content-Type: application/pdf');
        header('Content-Disposition: attachment;filename=preprint.pdf"');
        header('Cache-Control: max-age=0');
        $mpdf->Output();
        exit;
    }

    /**
     * Finds and displays a preprint entity.
     *
     */
    public function showAction(Preprint $preprint)
    {
        $deleteForm = $this->createDeleteForm($preprint);

        return $this->render('preprint/show.html.twig', array(
            'preprint' => $preprint,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing preprint entity.
     *
     */
    public function editAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $preprint = $em->getRepository(Preprint::class)->find(1);
        $editForm = $this->createForm('Numa\CCCAdminBundle\Form\PreprintType', $preprint);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('preprint_edit');
        }

        return $this->render('NumaCCCAdminBundle:Preprint:edit.html.twig', array(
            'preprint' => $preprint,
            'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Deletes a preprint entity.
     *
     */
    public function deleteAction(Request $request, Preprint $preprint)
    {
        $form = $this->createDeleteForm($preprint);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($preprint);
            $em->flush();
        }

        return $this->redirectToRoute('preprint_index');
    }

    /**
     * Creates a form to delete a preprint entity.
     *
     * @param Preprint $preprint The preprint entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Preprint $preprint)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('preprint_delete', array('id' => $preprint->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
