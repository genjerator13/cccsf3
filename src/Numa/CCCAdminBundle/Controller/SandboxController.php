<?php

namespace Numa\CCCAdminBundle\Controller;

use Numa\CCCAdminBundle\Entity\BillingPeriod;
use Numa\CCCAdminBundle\Entity\BillingReport;
use Numa\CCCAdminBundle\Entity\CustomerReport;
use Numa\CCCAdminBundle\Entity\Customers;
use Numa\CCCAdminBundle\Entity\Drivers;
use Numa\CCCAdminBundle\Entity\Media;
use Numa\CCCAdminBundle\Entity\Probills;
use Numa\CCCAdminBundle\Lib\Report\InvoiceReport;
use Numa\CCCAdminBundle\Lib\Report\ReportFactory;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class SandboxController extends Controller
{
    public function testAction()
    {
        $emoldccc = $this->get('doctrine')->getManager('cccold');
        $em = $this->get('doctrine')->getManager();
        $sql = "SELECT * FROM batchX ";


        $stmt = $emoldccc->getConnection()->prepare($sql);
        $stmt->execute();
        $oldBatches = $stmt->fetchAll();

        foreach ($oldBatches as $batch) {
            $closed = \DateTime::createFromFormat("Y-m-d", $batch['closed'])->setTime(0, 0, 0);
            $name = $this->get("numa.batch")->generateNameFromDate($closed);
            $bp = $em->getRepository(BillingPeriod::class)->findOneBy(array('name' => $name));
            if (!$bp instanceof BillingPeriod) {
                $bp = new BillingPeriod();
                $em->persist($bp);
            }
            $bp->setStarted(\DateTime::createFromFormat("Y-m-d", $batch['started'])->setTime(0, 0, 0));
            $bp->setClosed($closed);

            $bp->setName($name);
            $bp->setWorkingDays($batch['working_days']);

            $bp->setActive(false);


            dump($bp);
        }

        $em->flush();
        die();
    }

    public function test2Action()
    {
        $em=$this->getDoctrine()->getManager();
        $probill = $em->getRepository(Probills::class)->findOneBy(array("waybill"=>"0108418"));
        //dump($probill);
        $ppp = $this->get("numa.probill")->calculateDriver($probill);
        dump($ppp);
        die();
    }

}
