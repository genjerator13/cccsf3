<?php

namespace Numa\CCCAdminBundle\Controller;

use Numa\CCCAdminBundle\Entity\Drivers;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Driver controller.
 *
 */
class DriversController extends Controller
{
    /**
     * Lists all driver entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $drivers = $em->getRepository('NumaCCCAdminBundle:Drivers')->findAll();

        return $this->render('NumaCCCAdminBundle:drivers:indexDataGrid.html.twig', array(
            'drivers' => $drivers,
        ));
    }

    /**
     * Creates a new driver entity.
     *
     */
    public function newAction(Request $request)
    {
        $driver = new Drivers();
        $form = $this->createForm('Numa\CCCAdminBundle\Form\DriversType', $driver);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($driver);
            $em->flush();

            return $this->redirectToRoute('drivers_show', array('id' => $driver->getId()));
        }

        return $this->render('NumaCCCAdminBundle:drivers:new.html.twig', array(
            'driver' => $driver,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a driver entity.
     *
     */
    public function showAction(Drivers $driver)
    {
        $deleteForm = $this->createDeleteForm($driver);

        return $this->render('NumaCCCAdminBundle:drivers:show.html.twig', array(
            'driver' => $driver,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing driver entity.
     *
     */
    public function editAction(Request $request, Drivers $driver)
    {
        $deleteForm = $this->createDeleteForm($driver);
        $editForm = $this->createForm('Numa\CCCAdminBundle\Form\DriversType', $driver);
        $editForm->handleRequest($request);
        $oldpass=$driver->getPassword();
        if ($editForm->isSubmitted() && $editForm->isValid()) {

            $pass=$request->request->get('numa_cccadminbundle_drivers')['password'];

            $factory = $this->container->get('security.encoder_factory');
            $encoder = $factory->getEncoder($driver);
            if (!empty($pass)) {

                $encodedPassword = $encoder->encodePassword($pass, $driver->getSalt());
                $driver->setPassword($encodedPassword);
            }else{
                //$encodedPassword = $encoder->encodePassword($oldpass, $user->getSalt());
                $driver->setPassword($oldpass);
            }
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('drivers_edit', array('id' => $driver->getId()));
        }

        return $this->render('NumaCCCAdminBundle:drivers:edit.html.twig', array(
            'driver' => $driver,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a driver entity.
     *
     */
    public function deleteAction(Request $request, Drivers $driver)
    {
        $form = $this->createDeleteForm($driver);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($driver);
            $em->flush();
        }

        return $this->redirectToRoute('drivers_index');
    }

    /**
     * Creates a form to delete a driver entity.
     *
     * @param Drivers $driver The driver entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Drivers $driver)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('drivers_delete', array('id' => $driver->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    public function exportAction(){
        $em = $this->getDoctrine()->getManager();
        $drivers = $em->getRepository(Drivers::class)->findAll();

        $html= $this->render('NumaCCCAdminBundle:drivers:index_excel.html.twig', array(
            'drivers' => $drivers,
        ));

        // save $table inside temporary file that will be deleted later
        return $this->get("numa.report")->htmlToXls($html->getContent(),"Drivers",'Drivers.xls');
    }

    public function activateAction(Request $request)
    {
        $activate = $request->attributes->get('activate');
        $ids = $this->get("numa.uigrid")->getSelectedIds($request);
        $em = $this->getDoctrine()->getManager();
        $em->getRepository(Drivers::class)->activate($ids, $activate);
        die();
    }
}
