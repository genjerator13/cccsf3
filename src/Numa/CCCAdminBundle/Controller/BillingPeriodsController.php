<?php

namespace Numa\CCCAdminBundle\Controller;


use Numa\CCCAdminBundle\Entity\BillingPeriod;
use Numa\CCCAdminBundle\Entity\Customers;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Numa\CCCAdminBundle\Entity\batchX;
use Doctrine\DBAL\Driver;
use Numa\CCCAdminBundle\Entity\Probills;
use Symfony\Component\HttpFoundation\Response;

/**
 * batchX controller.
 *
 */
class BillingPeriodsController extends Controller {

    /**
     * Lists all batchX entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('NumaCCCAdminBundle:batchX')->findBy(array(),array('id'=>'DESC'),24);

        return $this->render('NumaCCCAdminBundle:BillingPeriods:index.html.twig', array(
                    'entities' => $entities,
        ));
    }

    public function missingImagesAction(Request $request, $batchid)
    {
        $em = $this->getDoctrine()->getManager();
        $batch = $em->getRepository("NumaCCCAdminBundle:batchX")->find($batchid);
        $probills = $em->getRepository('NumaCCCAdminBundle:Probills')->findBy(array('batchId'=>$batchid));
        $scanFolder = $batch->getScansFolder();
        $scanPath   = $this->container->getParameter('scans_path');


        $batchPath = $scanPath."/".$scanFolder;
        $missing = array();
        foreach($probills as $probill)
        {

            $scanImgFilename = $probill->getWaybill().".jpg";
            $scanImgPath     = $batchPath."/".$scanImgFilename;
            if(!file_exists($scanImgPath)){
                $missing[$probill->getId()]=$probill;
            }
        }


        return $this->render('NumaCCCAdminBundle:BillingPeriods:missing.html.twig', array(
            'probills' => $probills,
            'missing' => $missing,
            'batchid'=>$batchid,
        ));
    }




    public function createInvoicesAction(BillingPeriod $billingPeriod,Request $request)
    {
        $form = $this->createFinishForm();
        $form->handleRequest($request);
        if($form->isSubmitted()){
            $data=$form->getData();
            $invoice = $data['invoice'];
            $gst = $data['gst'];
            $this->addFlash("success","Invoices are successfully generated starting from # ".$invoice. " with GST=".$gst);
            $this->get("numa.batch")->generateInvoiceNumbers($billingPeriod,$invoice,$gst);
            return $this->redirectToRoute("billingperiod_billings");
        }
        return $this->render('NumaCCCAdminBundle:billingperiod:finish.html.twig', array(
            'billing' => $billingPeriod,
            'form' =>$form->createView()
        ));
    }

    private function createFinishForm():Form
    {
        $em = $this->getDoctrine()->getManager();

        $form = $this->createFormBuilder();
        $form->add('invoice', TextType::class, array('label' => 'Invoice #'));
        $form->add('gst', TextType::class, array('label' => "GST Amount"));
        $form->add('finish', SubmitType::class);

        return $form->getForm();
    }


}
