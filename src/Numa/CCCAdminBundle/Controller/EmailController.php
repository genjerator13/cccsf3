<?php

namespace Numa\CCCAdminBundle\Controller;

use Numa\CCCAdminBundle\Entity\Attachment;
use Numa\CCCAdminBundle\Entity\BillingPeriod;
use Numa\CCCAdminBundle\Entity\CustomerReport;
use Numa\CCCAdminBundle\Entity\Media;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Numa\CCCAdminBundle\Entity\Email;
use Numa\CCCAdminBundle\Form\EmailType;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\ArrayAdapter;
use Symfony\Component\HttpFoundation\Response;
use Cocur\BackgroundProcess\BackgroundProcess;

/**
 * Email controller.
 *
 */
class EmailController extends Controller {

    /**
     * Lists all Email entities.
     *
     */
    public function indexAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('NumaCCCAdminBundle:Email')->findAll();
        $adapter = new ArrayAdapter($entities);
        $pagerfanta = new Pagerfanta($adapter);
        $pagerfanta->setMaxPerPage(15);
        $page = $request->get('page');
        if (!$page) {
            $page = 1;
        }
        return $this->render('NumaCCCAdminBundle:Email:index.html.twig', array(
                    'entities' => $entities,
                    'pagerfanta' => $pagerfanta,
        ));
    }

    public function generateAction(Request $request, $batch_id,$sending=0) {

        //$command = 'php ' . $this->get('kernel')->getRootDir() . '/console numa:dbutil fetchFeed ' . $id;   
        $msg = '';

        $command = 'php /var/www/cccsf3/bin/console billing_period generateReports ' . $batch_id." ".$sending;
        //$command = 'ls -lsa';

        //$process = new \Symfony\Component\Process\Process($command);

        //$process->start();
        $process = new BackgroundProcess($command);
        $process->run();
        //sleep(3);
        $request->getSession()->getFlashBag()->add('success', 'Emails are created for batch# ' . $batch_id);

        return $this->redirectToRoute('email_progress',array('batch_id'=>$batch_id));
    }

    public function sendAction(Request $request, BillingPeriod $billingPeriod){
//        //$command = 'php ' . $this->get('kernel')->getRootDir() . '/console numa:dbutil fetchFeed ' . $id;
//        $msg = '';
//
//        $command = 'php ' . $this->get('kernel')->getRootDir() . '/console billing_period sendAllEmails '.$billingPeriod->getId();
//
//        $process = new \Symfony\Component\Process\Process($command);
//
//        ////$process->start();
//
//        //sleep(3);
//        $request->getSession()->getFlashBag()->add('success', 'Emails are queued for sending. Sending in progress# ' . $billingPeriod->getId());
//
//        return $this->redirectToRoute('email_progress',array('billingPeriod'=>$billingPeriod->getId()));

        //$command = 'php ' . $this->get('kernel')->getRootDir() . '/console numa:dbutil fetchFeed ' . $id;
//        $msg = '';
//
//        $command = 'php bin/console billing_period sendAllEmails '.$billingPeriod->getId();
//        //$command = 'ls -lsa';
//
//        //$process = new \Symfony\Component\Process\Process($command);
//
//        //$process->start();
//        $process = new BackgroundProcess($command);
//        $process->run();
//        //sleep(3);
//
//
//

        ///php bin/console billing_period generateCustomerSnailReport 1
        //$command = 'php ' . $this->container->get('kernel')->getRootDir()."/../bin/console billing_period generateCustomer".$subtype."Report".$format." " . $billingPeriod->getId();
        $command = 'php ' . $this->container->get('kernel')->getRootDir()."/../bin/console billing_period sendAllEmails ".$billingPeriod->getId();

        $request->getSession()->getFlashBag()->add('success', 'Emails are sending for billing period# ' . $billingPeriod->getId());
        $process = new BackgroundProcess($command);
        $process->run();
        return $this->redirectToRoute('email_progress',array('billingPeriod'=>$billingPeriod->getId()));

    }

    public function sendTestAction(Request $request, BillingPeriod $billingPeriod){
        $em = $this->getDoctrine()->getManager();

        $customerReports = $em->getRepository(CustomerReport::class)->findBy(array('billing_period_id'=>$billingPeriod->getId()));

        return $this->render('NumaCCCAdminBundle:Email:viewEmails.html.twig', array(
            'customerReports' => $customerReports  ,
            'billingPeriod'=> $billingPeriod,
        ));
    }

    public function deleteAttAction(Request $request,BillingPeriod $billingPeriod,Attachment $attachment){
        $em = $this->getDoctrine()->getManager();
        $em->remove($attachment);
        $em->flush();
        $this->addFlash("Success","Attachment ".$attachment->getMedia()->getName()." has been removed");
        return $this->redirectToRoute("batchx_preview_emails",array("billingPeriod"=>$billingPeriod->getId()));
    }

    public function proccessEmailSendingAction(Request $request, $action = "") {
        //$command = 'php ' . $this->get('kernel')->getRootDir() . '/console numa:dbutil fetchFeed ' . $id;   
        $msg = '';
        if ($action == "sendEmailsNoAttachment") {
            $msg = 'only emails with no Attachment';
        } elseif ($action == "sendEmailsWithAttachment") {
            $msg = 'only emails with Attachment';
        } elseif ($action == "sendAllEmails") {
            $msg = 'Send All Emails';
        } else {
            throw $this->createNotFoundException('Unable to find requested action.');
        }
        $command = 'php ' . $this->get('kernel')->getRootDir() . '/console numa:CCC:emails ' . $action;
        //$command = 'php ' . $this->get('kernel')->getRootDir() ."/console numa:CCC:users genjerator13 xxx";
        $process = new \Symfony\Component\Process\Process($command);

        $process->start();

        sleep(3);
        $request->getSession()->getFlashBag()
                ->add('success', 'Emails sending (' . $msg . ") started");
        return $this->redirectToRoute('command_log_home');
    }

    public function progressAction(Request $request,BillingPeriod $billingPeriod) {
        $em = $this->getDoctrine()->getManager();
        

        $generate = $em->getRepository('NumaCCCAdminBundle:CommandLog')->findEmailLog($billingPeriod->getId(),0);
        $email = $em->getRepository('NumaCCCAdminBundle:CommandLog')->findEmailLog($billingPeriod->getId(),1);

        $noattach = $em->getRepository('NumaCCCAdminBundle:CommandLog')->findEmailLog($billingPeriod->getId(),2);
        $attach   = $em->getRepository('NumaCCCAdminBundle:CommandLog')->findEmailLog($billingPeriod->getId(),3);

        if(empty($generate)){
            $generate['total']=0;
            $generate['current']=0;
        }
        if(empty($email)){
            $email['total']=0;
            $email['current']=0;
        }
        if(empty($noattach)){
            $noattach['total']=0;
            $noattach['current']=0;
        }
        if(empty($attach)){
            $attach['total']=0;
            $attach['current']=0;
        }
        
        return $this->render('NumaCCCAdminBundle:Email:progress.html.twig', array(
                    'billingPeriod' => $billingPeriod->getId()  ,
                    'generate' => $generate  ,                  
                    'email' => $email  ,
                    'noattach' => $noattach ,
                    'attach' => $attach                    
        ));
    }
    
    public function progressAjaxAction(Request $request,BillingPeriod $billingPeriod) {
        $em = $this->getDoctrine()->getManager();
        //$entities = $em->getRepository('NumaCCCAdminBundle:CommandLog')->findEmailLog(3,"email",true);
        //$generate = $em->getRepository('NumaCCCAdminBundle:CommandLog')->findEmailLog(3,"email",true);;
        $generate = $em->getRepository('NumaCCCAdminBundle:CommandLog')->findEmailLog($billingPeriod->getId(),0);
        $email = $em->getRepository('NumaCCCAdminBundle:CommandLog')->findEmailLog($billingPeriod->getId(),1);

        $noAttachment = $em->getRepository('NumaCCCAdminBundle:CommandLog')->findEmailLog($billingPeriod->getId(),2);
        $attachment   = $em->getRepository('NumaCCCAdminBundle:CommandLog')->findEmailLog($billingPeriod->getId(),3);

        $res = array();
        
        $res['generate_total'] = $generate['total'];
        $res['generate_current'] = $generate['current'];

        $res['email_total'] = $email['total'];
        $res['email_current'] = $email['current'];

        $res['noatt_total'] = $noAttachment['total'];
        $res['noatt_current'] = $noAttachment['current'];
        $res['withatt_total'] = $attachment['total'];
        $res['withatt_current'] = $attachment['current'];
        $res['generate'] = 0;
        $res['generate'] = $generate['total']>0?($generate['current']/$generate['total'])*100:0;
        $res['email'] = 0;
        $res['email'] = $email['total']>0?($email['current']/$email['total'])*100:0;

        $res['noatt'] = 0;
        $res['noatt'] = $noAttachment['total']>0?($noAttachment['current']/$noAttachment['total'])*100:0;
        $res['withatt'] = $attachment['total']>0?($attachment['current']/$attachment['total'])*100:0;
        //dump($res);
        
        return new \Symfony\Component\HttpFoundation\JsonResponse(array('res' => $res));
    }
    /**
     * Displays a form to edit an existing email entity.
     *
     */
    public function editAction(Request $request, Email $email)
    {
        $em = $this->getDoctrine()->getManager();
        $editForm = $this->createForm(EmailType::class,$email);
        $editForm->handleRequest($request);
        $billingPeriod = $request->query->get("billingPeriod");
        if ($editForm->isSubmitted() && $editForm->isValid()) {

            $data = $request->request->all();
            $file = $request->files->all("email")['email']['Attachment'];
            if($file instanceof UploadedFile) {
                $attachment = new Attachment();
                $media = new Media();
                $em->persist($media);
                $em->persist($attachment);
                $media->setName($file->getClientOriginalName());
                $media->setMimetype($file->getMimeType());
                $content = file_get_contents($file->getPathname());
                $media->setContent(base64_encode($content));
                $attachment->setMedia($media);
                $attachment->setEmail($email);
                $em->flush();
                $this->addFlash("success","Attachemnt ".$media->getName()." added to the email #".$email->getId());

            }
            return $this->redirectToRoute('batchx_preview_emails', array('billingPeriod' => $billingPeriod));
        }

        return $this->render('NumaCCCAdminBundle:Email:edit.html.twig', array(
            'email' => $email,
            'edit_form' => $editForm->createView(),
        ));
    }
}
