<?php

namespace Numa\CCCAdminBundle\Controller;

use Numa\CCCAdminBundle\Entity\Customers;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{

    public function indexAction()
    {
        return $this->render('NumaCCCAdminBundle:Default:index.html.twig', array('name' => "test"));
    }

    public function loginAction()
    {
        $helper = $this->get('security.authentication_utils');

        return $this->render('NumaCCCAdminBundle:Default:login.html.twig', array(
            'last_username' => $helper->getLastUsername(),
            'error' => $helper->getLastAuthenticationError(),
        ));
    }

    public function pageNotFoundAction()
    {
        return $this->render('NumaCCCAdminBundle:Errors:error404.html.twig', array(
        ));
    }

    public function downloadAction($filename, $folder)
    {

        $path = $this->get('kernel')->getRootDir() . "/../web/upload/";
        $content = file_get_contents($path . $folder . "/" . $filename);

        $response = new \Symfony\Component\HttpFoundation\Response();

        //set headers
        $response->headers->set('Content-Type', 'mime/type');
        $response->headers->set('Content-Disposition', 'attachment;filename="' . $filename);

        $response->setContent($content);
        return $response;
    }

    public function downloadDelAction($filename, $folder)
    {
        $path = $this->get('kernel')->getRootDir() . "/../web/upload/";
        $fullpath = $path . $folder . "/" . $filename;
        unlink($fullpath);
        $this->addFlash(
            'success', 'Newsletter file has been deleted!'
        );
        return $this->redirect($this->generateUrl('batchx'));
    }

    public function customRateAction()
    {
        $customer =
        $customer = $this->get('security.token_storage')->getToken()->getUser();
        $customRate = "";

        if ($customer instanceof Customers) {
            $customRate = $this->get("numa.customer")->getCustomPdf($customer);
        }

        return $this->render('NumaCCCAdminBundle:Default:customRate.html.twig', array('customrate' => $customRate
        ));
    }


    public function HomepageAction()
    {
        return $this->redirectToRoute("numa_ccc_admin_homepage");
    }

    public function alertAction()
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        return new JsonResponse(!empty($user));
    }




}
