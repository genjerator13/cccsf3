<?php

namespace Numa\CCCAdminBundle\Controller;

use Numa\CCCAdminBundle\Entity\Drops;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Drop controller.
 *
 */
class DropsController extends Controller
{
    /**
     * Lists all drop entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $drops = $em->getRepository('NumaCCCAdminBundle:Drops')->findAll();

        return $this->render('drops/index.html.twig', array(
            'drops' => $drops,
        ));
    }

    /**
     * Creates a new drop entity.
     *
     */
    public function newAction(Request $request)
    {
        $drop = new Drop();
        $form = $this->createForm('Numa\CCCAdminBundle\Form\DropsType', $drop);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($drop);
            $em->flush();

            return $this->redirectToRoute('drops_show', array('id' => $drop->getId()));
        }

        return $this->render('drops/new.html.twig', array(
            'drop' => $drop,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a drop entity.
     *
     */
    public function showAction(Drops $drop)
    {
        $deleteForm = $this->createDeleteForm($drop);

        return $this->render('drops/show.html.twig', array(
            'drop' => $drop,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing drop entity.
     *
     */
    public function editAction(Request $request, Drops $drop)
    {
        $deleteForm = $this->createDeleteForm($drop);
        $editForm = $this->createForm('Numa\CCCAdminBundle\Form\DropsType', $drop);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('drops_edit', array('id' => $drop->getId()));
        }

        return $this->render('drops/edit.html.twig', array(
            'drop' => $drop,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a drop entity.
     *
     */
    public function deleteAction(Request $request, Drops $drop)
    {
        $form = $this->createDeleteForm($drop);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($drop);
            $em->flush();
        }

        return $this->redirectToRoute('drops_index');
    }

    /**
     * Creates a form to delete a drop entity.
     *
     * @param Drops $drop The drop entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Drops $drop)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('drops_delete', array('id' => $drop->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
