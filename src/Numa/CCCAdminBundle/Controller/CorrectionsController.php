<?php

namespace Numa\CCCAdminBundle\Controller;

use Numa\CCCAdminBundle\Entity\Batch;
use Numa\CCCAdminBundle\Entity\Probills;
use Numa\CCCAdminBundle\Form\CorrectionType;
use Numa\CCCAdminBundle\Form\ProbillsType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\HttpFoundation\Request;

/**
 * Batch controller.
 *
 */
class CorrectionsController extends Controller
{

    public function indexAction(Request $request)
    {
        $form = $this->createForm(ProbillsType::class, new Probills(), array(
            'attr' => array("ng-submit" => "finalize()")
        ));
        $form->add("id", HiddenType::class);
        return $this->render('NumaCCCAdminBundle:corrections:form.html.twig', array(
            'form' => $form->createView(),

        ));
    }

    public function batchIndexAction(Request $request, $id)
    {

        $form = $this->createForm(CorrectionType::class, new Probills());
        $finalized = $request->attributes->get('finalized');
        $em=$this->getDoctrine()->getManager();
        $batch = $em->getRepository(Batch::class)->find($id);
        $id=0;
        $locked=2;
        $billingPeriod=null;
        if(!$finalized){
            $id = $batch->getId();
            $locked = $batch->isLocked();
            $billingPeriod=$batch->getBillingPeriod();
        }
        return $this->render('NumaCCCAdminBundle:corrections:form.html.twig', array(
                'billingPeriod'=>$billingPeriod,
                'entity'=>'batch',
                'id'=>$id,
                'billType'=>"ALL",
                'locked'=>$locked,
                'form'=>$form->createView(),
                'action'=>"test",
                'correction'=>true,
                'finalized'=>$finalized
            )
        );
    }
}
