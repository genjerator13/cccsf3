<?php

namespace Numa\CCCAdminBundle\Controller;

use Numa\CCCAdminBundle\Entity\Pickup;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Pickup controller.
 *
 */
class PickupController extends Controller
{
    /**
     * Lists all pickup entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $pickups = $em->getRepository('NumaCCCAdminBundle:Pickup')->findAll();

        return $this->render('pickup/index.html.twig', array(
            'pickups' => $pickups,
        ));
    }

    /**
     * Creates a new pickup entity.
     *
     */
    public function newAction(Request $request)
    {
        $pickup = new Pickup();
        $form = $this->createForm('Numa\CCCAdminBundle\Form\PickupType', $pickup);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($pickup);
            $em->flush();

            return $this->redirectToRoute('pickup_show', array('id' => $pickup->getId()));
        }

        return $this->render('pickup/new.html.twig', array(
            'pickup' => $pickup,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a pickup entity.
     *
     */
    public function showAction(Pickup $pickup)
    {
        $deleteForm = $this->createDeleteForm($pickup);

        return $this->render('pickup/show.html.twig', array(
            'pickup' => $pickup,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing pickup entity.
     *
     */
    public function editAction(Request $request, Pickup $pickup)
    {
        $deleteForm = $this->createDeleteForm($pickup);
        $editForm = $this->createForm('Numa\CCCAdminBundle\Form\PickupType', $pickup);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('pickup_edit', array('id' => $pickup->getId()));
        }

        return $this->render('pickup/edit.html.twig', array(
            'pickup' => $pickup,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a pickup entity.
     *
     */
    public function deleteAction(Request $request, Pickup $pickup)
    {
        $form = $this->createDeleteForm($pickup);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($pickup);
            $em->flush();
        }

        return $this->redirectToRoute('pickup_index');
    }

    /**
     * Creates a form to delete a pickup entity.
     *
     * @param Pickup $pickup The pickup entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Pickup $pickup)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('pickup_delete', array('id' => $pickup->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
