<?php

namespace Numa\CCCAdminBundle\Controller;


use Doctrine\ORM\EntityRepository;
use Numa\CCCAdminBundle\Entity\Batch;
use Numa\CCCAdminBundle\Entity\BillingPeriod;
use Numa\CCCAdminBundle\Entity\BillingReport;
use Numa\CCCAdminBundle\Entity\Cheque;
use Numa\CCCAdminBundle\Entity\CommandLog;
use Numa\CCCAdminBundle\Entity\Customers;
use Numa\CCCAdminBundle\Lib\Report\CustomerDeliveryReport;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Numa\CCCAdminBundle\Lib\Report\ReportFactory;


/**
 * batchX controller.
 *
 */
class ReportController extends Controller
{
    public function dispatcherAction(Request $request)
    {
        $report = $request->attributes->get('report');
        $message = "";
        $form = $this->createDataEntryForm($report);
        if($report=="details_snail" ||$report=="details_all" ||$report=="driver_details"){
            $form = $this->createDataEntryForm($report,true);
        }
        $form->handleRequest($request);
        if ($report == 'driver_details') {
            $em = $this->getDoctrine()->getManager();
            $billingPeriods = $em->getRepository(BillingPeriod::class)->findBy(array("active"=>1),array("id"=>"desc"));
            $commandLogs=$this->driverDetailsProgress($billingPeriods);

            return $this->render('NumaCCCAdminBundle:report/driverDetails:index.html.twig', array(
                'billingPeriods' => $billingPeriods,
                'report'   => $report,
                'commandLogs'   => $commandLogs,
            ));
        }elseif($report == 'details_snail' || $report == "details_all"){
            $em = $this->getDoctrine()->getManager();
            $billingPeriods = $em->getRepository(BillingPeriod::class)->findBy(array("active"=>1),array("id"=>"desc"));
            $subtype="snail";
            if($report == "details_all"){
                $subtype="all";
            }
            $commandLogs=$this->customerProgress($billingPeriods,$subtype);

            return $this->render('NumaCCCAdminBundle:report/customerDelivery:indexSnail.html.twig', array(
                'billingPeriods' => $billingPeriods,
                'report'   => $report,
                'commandLogs'   => $commandLogs,
            ));
        }
        if ($form->isSubmitted()) {
            $data = $form->getData();

            $xls = $form->get('xls')->isClicked();
            $pdf = $form->get('pdf')->isClicked();

            if(array_key_exists("generate",$form->all())) {
                $generate = $form->get('generate')->isClicked();
            }
            //$format=$xls?"XLS":"PDF";
            if ($xls) {
                $format = "XLS";
            } elseif ($pdf) {
                $format = "PDF";
            } elseif ($generate) {
                $format = "GENERATE";
            }


            $billing_period = $data['billingPeriod'];
            $customer = "";
            if (!empty($data['customer'])) {
                $customer = $data['customer'];
            }
            $customer_id = 0;
            if (!empty($customer)) {
                $customer_id = $customer->getId();
            }

            if ($report == 'details_snail') {
                return $this->redirectToRoute("report_customer_details_snail_billing_period", array("billing_period" => $billing_period->getId(), "format" => $format, 'customer' => $customer_id));
            } elseif ($report == 'details_all') {
                return $this->redirectToRoute("report_customer_details_all_billing_period", array("billing_period" => $billing_period->getId(), "format" => $format, 'customer' => $customer_id));
            } elseif ($report == 'customer_summary') {
                return $this->redirectToRoute("report_customer_summary_billing_period", array("billing_period" => $billing_period->getId(), "format" => $format));
            } elseif ($report == 'driver_summary') {
                return $this->redirectToRoute("report_driver_summary_billing_period", array("billing_period" => $billing_period->getId(), "format" => $format));
            } elseif ($report == 'driver_details') {
                $em = $this->getDoctrine()->getManager();
                $billingPeriods = $em->getRepository(BillingPeriod::class)->findAllBy(array("active"=>1));
                return $this->render('NumaCCCAdminBundle:report/driverDetails:index.html.twig', array(
                    'billings' => $billingPeriods
                ));
            } elseif ($report == 'trailer') {
                return $this->trailerBillingPeriodAction($request, $billing_period);
                //$this->redirectToRoute("report_trailer_billing_period", array("billing_period" => $billing_period, "format" => $format));
            } elseif ($report == 'misc') {
                return $this->miscBillingPeriodAction($request,$billing_period);
                //return $this->redirectToRoute("report_misc_billing_period", array("billing_period" => $billing_period, "format" => $format));
            } elseif ($report == 'vehicles') {
                return $this->redirectToRoute("report_vehicles_billing_period", array("billing_period" => $billing_period->getId(), "format" => $format));
            } elseif ($report == 'cheque') {
                return $this->redirectToRoute("report_cheque_billing_period", array("billing_period" => $billing_period->getId(), "format" => $format));
            } elseif ($report == 'invoices') {
                return $this->redirectToRoute("report_invoices_billing_period", array("billing_period" => $billing_period->getId(), "format" => $format));
            } elseif ($report == 'remittance') {
                return $this->redirectToRoute("report_remittance_billing_period", array("billing_period" => $billing_period->getId(), "format" => $format));
            }


        }

        return $this->render('NumaCCCAdminBundle:report:prereport.html.twig', array(
            'form' => $form->createView(),
            'report' => $report,
            'message' => $message
        ));
    }

    public function driverDetailsProgressAction(){
        $em = $this->getDoctrine()->getManager();
        $billingPeriods = $em->getRepository(BillingPeriod::class)->findBy(array("active"=>1),array("id"=>"desc"));
        $cls = $this->driverDetailsProgress($billingPeriods);
        $serializer = $this->container->get('jms_serializer');

        return new JsonResponse($serializer->toArray($cls));
    }
    public function driverDetailsProgress($billingPeriods){
        $commandLogs=[];
        $em = $this->getDoctrine()->getManager();
        foreach($billingPeriods as $billingPeriod){
            $namePdf = "generate_driver_detail_report_".$billingPeriod->getId()."_pdf";
            $clPdf = $em->getRepository(CommandLog::class)->findOneBy(array("command"=>$namePdf),array("id"=>"desc"));
            $commandLogs[$billingPeriod->getId()]['pdf'] =  $clPdf;

            $nameXls = "generate_driver_detail_report_".$billingPeriod->getId()."_xls";
            $clXls = $em->getRepository(CommandLog::class)->findOneBy(array("command"=>$nameXls),array("id"=>"desc"));
            $commandLogs[$billingPeriod->getId()]['xls'] =  $clXls;
        }
        return $commandLogs;
    }

    public function customerProgressAction(Request $request){
        $subtype = $request->attributes->get('subtype');
        $em = $this->getDoctrine()->getManager();
        $billingPeriods = $em->getRepository(BillingPeriod::class)->findBy(array("active"=>1),array("id"=>"desc"));
        $cls = $this->customerProgress($billingPeriods,$subtype);
        $serializer = $this->container->get('jms_serializer');

        return new JsonResponse($serializer->toArray($cls));
    }
    public function customerProgress($billingPeriods,$subtype='snail'){
        $commandLogs=[];
        $em = $this->getDoctrine()->getManager();
        foreach($billingPeriods as $billingPeriod){
            $namePdf = "generate_customer_".$subtype."_report_".$billingPeriod->getId()."_pdf";
            $clPdf = $em->getRepository(CommandLog::class)->findOneBy(array("command"=>$namePdf),array("id"=>"desc"));
            $commandLogs[$billingPeriod->getId()]['pdf'] =  $clPdf;

            $nameXls = "generate_customer_".$subtype."_report_".$billingPeriod->getId()."_xls";
            $clXls = $em->getRepository(CommandLog::class)->findOneBy(array("command"=>$nameXls),array("id"=>"desc"));
            $commandLogs[$billingPeriod->getId()]['xls'] =  $clXls;
        }
        return $commandLogs;
    }

    public function generateDriversDetailsReportAction(BillingPeriod $billing_period){
        $em = $this->getDoctrine()->getManager();
        $commandPdf = "generate_driver_detail_report_".$billing_period->getId()."_pdf";

        $pdfRunning = $em->getRepository(CommandLog::class)->anyCommandRunning($commandPdf);

        if(!$pdfRunning instanceof CommandLog) {
            $this->get("numa.report")->generateDriverDetails($billing_period, "pdf");
            $this->addFlash("success","Driver Details PDF Report started...");
        }else{
            $this->addFlash("success","Driver Details PDF Report is still running...");

        }
        $commandXls = "generate_driver_detail_report_".$billing_period->getId()."_xls";
        $xlsRunning = $em->getRepository(CommandLog::class)->anyCommandRunning($commandXls);
        if(!$xlsRunning instanceof CommandLog) {
            $this->get("numa.report")->generateDriverDetails($billing_period, "xls");
            $this->addFlash("success","Driver Details Excel Report started...");
        }else{
            $this->addFlash("success","Driver Details Excel Report is still running...");
        }
        return $this->redirectToRoute("report_driver_detail_reports");
    }

    public function generateCustomerAction(Request $request, BillingPeriod $billing_period){
        $em = $this->getDoctrine()->getManager();
        $subtype = $request->attributes->get('subtype');
        $commandPdf = "generate_customer_".$subtype."_report_".$billing_period->getId()."_pdf";

        $pdfRunning = $em->getRepository(CommandLog::class)->anyCommandRunning($commandPdf);

        if(!$pdfRunning instanceof CommandLog) {
            $this->get("numa.report")->generateCustomerDetails($billing_period,$subtype, 'Pdf');

            $this->addFlash("success","Customer ".$subtype." PDF Report started...");
        }else{
            $this->addFlash("success","Customer ".$subtype." PDF Report is still running...");

        }
        $commandXls =  "generate_customer_".$subtype."_report_".$billing_period->getId()."_xls";
        $xlsRunning = $em->getRepository(CommandLog::class)->anyCommandRunning($commandXls);
        if(!$xlsRunning instanceof CommandLog) {
            $this->get("numa.report")->generateCustomerDetails($billing_period,$subtype, 'Xls');
            $this->addFlash("success","Customer ".$subtype."  Excel Report started...");
        }else{
            $this->addFlash("success","Customer ".$subtype."  Excel Report is still running...");
        }
        return $this->redirectToRoute("report_customer_details_".$subtype);
    }

    public function renderDriversDetailsReportAction(Request $request,BillingPeriod $billing_period){
        $em = $this->getDoctrine()->getManager();
        $format = $request->attributes->get('format');

        $driverDetailsReport = ReportFactory::create($this->container, "driver_details", $billing_period);
        $billingPeriodCustomerDetails = $em->getRepository(BillingReport::class)->findOneBy(array('billing_period_id' => $billing_period->getId(), "name" => "driver_details_".$format));
        $this->get("numa.media")->downloadFromTmp($billingPeriodCustomerDetails->getMedia());
        //}
        return $driverDetailsReport->rendermPDF();
    }

    public function renderCustomerReportAction(Request $request,BillingPeriod $billing_period){
        $em = $this->getDoctrine()->getManager();
        $format = $request->attributes->get('format');
        $report = $request->attributes->get('report');

        $driverDetailsReport = ReportFactory::create($this->container, $report, $billing_period);
        $billingPeriodCustomerDetails = $em->getRepository(BillingReport::class)->findOneBy(array('billing_period_id' => $billing_period->getId(), "name" => $report."_".$format));
        $this->get("numa.media")->downloadFromTmp($billingPeriodCustomerDetails->getMedia());
        //}
        return $driverDetailsReport->rendermPDF();
    }

    public function driverDetailsBillingPeriodAction(Request $request, BillingPeriod $billing_period)
    {
        $driverDetailsReport = ReportFactory::create($this->container, "driver_details", $billing_period);
        $format = $request->query->get("format");
        $em = $this->getDoctrine()->getManager();

        if ($format == "GENERATE") {
            $commandPdf = "generate_driver_detail_report_".$billing_period->getId()."_pdf";

            $pdfRunning = $em->getRepository(CommandLog::class)->anyCommandRunning($commandPdf);
            dump($commandPdf);
            dump($pdfRunning);
            if(!$pdfRunning instanceof CommandLog) {
                $this->get("numa.report")->generateDriverDetails($billing_period, "pdf");
                dump("not running");
            }else{
                dump($commandPdf."  runnning");
                die();

            }
            $commandXls = "generate_driver_detail_report_".$billing_period->getId()."_xls";
            $commandXls = $em->getRepository(CommandLog::class)->anyCommandRunning($commandPdf);
            if($commandXls instanceof CommandLog) {
                $this->get("numa.report")->generateDriverDetails($billing_period, "xls");
                dump($commandXls."  runnning");
                die();
            }
            $this->addFlash("success","Driver reports are currently generating...");
            die();
            return $this->redirectToRoute("report_driver_detail_reports");
        }

        $billingPeriodCustomerDetails = $em->getRepository(BillingReport::class)->findOneBy(array('billing_period_id' => $billing_period->getId(), "name" => "driver_details_".$format));
        $this->get("numa.media")->downloadFromTmp($billingPeriodCustomerDetails->getMedia());
        //}
        return $driverDetailsReport->rendermPDF();
    }

    public function driverDetailsBatchAction(Request $request, Batch $batch)
    {

        $driverDetailsReport = ReportFactory::create($this->container, "driver_details", new BillingPeriod());
        $format = $request->query->get("format");
        $driverDetailsReport->setBatch($batch);
        if ($format == "XLS") {
            return $driverDetailsReport->renderXLS();
        }
        return $driverDetailsReport->rendermPDF();
    }

    public function customerDetailsAllBillingPeriodAction(Request $request, BillingPeriod $billing_period)
    {
        $format = $request->attributes->get('format');
        $deliveryReport = ReportFactory::create($this->container, "delivery_all", $billing_period);
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if ($user instanceof Customers) {
            $deliveryReport->setCustomers(array($user));

            if (strtolower($format) == 'pdf') {
                return $deliveryReport->rendermPDF(array(), false);
            } elseif (strtolower($format) == 'xls') {
                return $deliveryReport->renderXLS();
            }
        } else {
            $em = $this->getDoctrine()->getManager();
            $customerId = $request->query->get('customer');
            $customer = $em->getRepository(Customers::class)->find($customerId);
            if ($customer instanceof Customers) {
                $deliveryReport->setCustomers(array($customer));

                if (strtolower($format) == 'pdf') {
                    return $deliveryReport->rendermPDF(array(), false);
                } elseif (strtolower($format) == 'xls') {
                    return $deliveryReport->renderXLS();
                }
            } else {
                $deliveryReport->setAllCustomers();
            }

            $billingPeriodCustomerDetails = $em->getRepository(BillingReport::class)->findOneBy(array('billing_period_id' => $billing_period->getId(), "name" => "customer_details"));

            $this->get("numa.media")->downloadFromTmp($billingPeriodCustomerDetails->getMedia());

        }

        $deliveryReport->prepareDataForBillingPeriod();

        if (strtolower($format) == 'pdf') {
            return $deliveryReport->rendermPDF(array(), false);
        } elseif (strtolower($format) == 'xls') {
            return $deliveryReport->renderXLS();
        }
    }


    private function createCustomerDeliveryReportObject(BillingPeriod $billingPeriod, $customers, $type = 'all')
    {
        $deliveryReport = ReportFactory::create($this->container, "delivery_" . $type, $billingPeriod);
        $deliveryReport->setCustomers($customers);
        return $deliveryReport;
    }

    private function generateCustomerDeliveryReports( BillingPeriod $billingPeriod,$subType="all")
    {
        if($subType=='snail') {
            $this->get("numa.report")->generateCustomerDetails($billingPeriod,'Snail', 'Pdf');
            $this->get("numa.report")->generateCustomerDetails($billingPeriod,'Snail', 'Xls');
        }elseif($subType=='all'){
            $this->get("numa.report")->generateCustomerDetails($billingPeriod,'All', 'Pdf');
            $this->get("numa.report")->generateCustomerDetails($billingPeriod,'All', 'Xls');
        }
        $this->addFlash("success", "All Customer Detailed report ".$subType." report is currently generating.");
        return $this->redirectToRoute("report_customer_details_snail");
    }

    public function customerDetailsBillingPeriodAction(Request $request, BillingPeriod $billing_period)
    {

        $format = $request->attributes->get('format');
        $subtype = $request->attributes->get('subtype');
        $formatQ = $request->query->get('format');
        if (strtoupper($formatQ) == "GENERATE") {
            return $this->generateCustomerDeliveryReports($billing_period, $subtype);
        }


        $user = $this->get('security.token_storage')->getToken()->getUser();

        $em = $this->getDoctrine()->getManager();
        $customerId = $request->query->get('customer');
        $customer = "";
        if (!empty($customerId)){
            $customer = $em->getRepository(Customers::class)->find($customerId);
    }
        if ($customer instanceof Customers) {
            $deliveryReport = $this->createCustomerDeliveryReportObject($billing_period, array($customer), $subtype);
            $deliveryReport->setCustomers(array($customer));
        }


        if ($user instanceof Customers) {
            $deliveryReport = $this->createCustomerDeliveryReportObject($billing_period, array($customer), $subtype);
            $deliveryReport->setCustomers(array($user));
        }

        if ($customer instanceof Customers || $user instanceof Customers){
            if (strtolower($format) == 'pdf') {
                return $deliveryReport->rendermPDF(array(), false);
            } elseif (strtolower($format) == 'xls') {
                return $deliveryReport->renderXLS();
            }
        }else{
            if (strtoupper($formatQ) == 'PDF') {
                $billingPeriodCustomerDetails = $em->getRepository(BillingReport::class)->findOneBy(array('billing_period_id' => $billing_period->getId(), "name" => "customer_details_Snail_pdf"));
                $this->get("numa.media")->downloadFromTmp($billingPeriodCustomerDetails->getMedia());
            } elseif (strtoupper($formatQ) == 'XLS') {
                $name = "customer_details_snail_xls";
                $billingPeriodCustomerDetails = $em->getRepository(BillingReport::class)->findOneBy(array('billing_period_id' => $billing_period->getId(), "name" => "customer_details_Snail_xls"));
                $this->get("numa.media")->downloadFromTmp($billingPeriodCustomerDetails->getMedia());
            }
        }

    }

    public
    function customerSummaryBillingPeriodAction(Request $request, BillingPeriod $billing_period)
    {

        $customerSummaryReport = ReportFactory::create($this->container, "customer_summary", $billing_period);
        $format = $request->query->get("format");

        if ($format == "XLS") {
            return $customerSummaryReport->renderXLS();
        }
        return $customerSummaryReport->renderPDF();
    }

    public
    function driverSummaryBillingPeriodAction(Request $request, BillingPeriod $billing_period)
    {
        $driverSummaryReport = ReportFactory::create($this->container, "driver_summary", $billing_period);
        $format = $request->query->get("format");

        if ($format == "XLS") {
            return $driverSummaryReport->renderXLS();
        } elseif ($format == "PDF") {
            return $driverSummaryReport->renderPDF();
        } elseif (($format == "GENERATE")) {

        }
    }

    public
    function miscBillingPeriodAction(Request $request, $billing_period)
    {
        $miscReport = ReportFactory::create($this->container, "misc", new BillingPeriod(), $billing_period);
        $format = $request->query->get("format");

        if ($format == "XLS") {
            return $miscReport->renderXLS();
        }
        return $miscReport->renderPDF();

    }

    public
    function trailerBillingPeriodAction(Request $request, $billing_period)
    {
        $trailerReport = ReportFactory::create($this->container, "trailer", new BillingPeriod(),$billing_period);

        $format = $request->query->get("format");
        if ($format == "XLS") {
            return $trailerReport->renderXLS();
        }
        return $trailerReport->renderPDF();
    }


    public
    function vehiclesBillingPeriodAction(Request $request, BillingPeriod $billing_period)
    {
        $vehiclesrReport = ReportFactory::create($this->container, "vehicles", $billing_period);
        $format = $request->query->get("format");
        if ($format == "XLS") {
            return $vehiclesrReport->renderXLS();
        }
        return $vehiclesrReport->renderPDF();
    }

    public
    function invoicesBillingPeriodAction(BillingPeriod $billing_period)
    {
        $invoicesReport = ReportFactory::create($this->container, "invoices", $billing_period);
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if ($user instanceof Customers) {
            $invoicesReport->setCustomers(array($user));
        }else{
            $invoicesReport->setSnailCustomers();
        }

        return $invoicesReport->renderPDF2();
    }

    public
    function remittanceBillingPeriodAction(Request $request, BillingPeriod $billing_period)
    {
        $remittanceReport = ReportFactory::create($this->container, "remittance", $billing_period);
        $format = $request->query->get("format");
        if ($format == "XLS") {
            return $remittanceReport->renderXLS();
        }
        return $remittanceReport->renderPDF();
    }

    public
    function ChequeAction(Cheque $cheque)
    {
        $trailerReport = ReportFactory::createChequeReport($this->container, $cheque);
        return $trailerReport->renderPDF();
    }

    private
    function createDataEntryForm($report, $generate=false):Form
    {
        $multiple = false;
        $expaned = false;
        if($report=='misc' || $report=='trailer' ){
            $multiple = true;
            $expaned = true;
        }
        $em = $this->getDoctrine()->getManager();
        $batches = $em->getRepository(BillingPeriod::class)->getLastBillingPeriods();
        $form = $this->createFormBuilder();
        $form->add('billingPeriod', ChoiceType::class, array('multiple'=>$multiple,'expanded'=>$expaned,'choices' => $batches, 'choice_label' => 'displayName'));
        if($generate) {
            $form->add('generate', SubmitType::class, array('attr' => array('class' => 'btn btn-purple')));
        }
        $form->add('pdf',SubmitType::class,array('attr'=>array('class'=>'btn btn-success')));
        $form->add('xls',SubmitType::class,array('attr'=>array('class'=>'btn btn-primarry')));

        return $form->getForm();
    }

}
