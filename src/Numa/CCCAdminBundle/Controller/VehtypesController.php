<?php

namespace Numa\CCCAdminBundle\Controller;

use Numa\CCCAdminBundle\Entity\Vehtypes;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Vehtype controller.
 *
 */
class VehtypesController extends Controller
{
    /**
     * Lists all vehtype entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $vehtypes = $em->getRepository('NumaCCCAdminBundle:Vehtypes')->findAll();

        return $this->render('NumaCCCAdminBundle:vehtypes:indexDataGrid.html.twig', array(
            'vehtypes' => $vehtypes,
        ));
    }

    /**
     * Creates a new vehtype entity.
     *
     */
    public function newAction(Request $request)
    {
        $vehtype = new Vehtypes();
        $form = $this->createForm('Numa\CCCAdminBundle\Form\VehtypesType', $vehtype);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($vehtype);
            $em->flush();

            return $this->redirectToRoute('vehtypes_show', array('id' => $vehtype->getId()));
        }

        return $this->render('NumaCCCAdminBundle:vehtypes:new.html.twig', array(
            'vehtype' => $vehtype,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a vehtype entity.
     *
     */
    public function showAction(Vehtypes $vehtype)
    {
        $deleteForm = $this->createDeleteForm($vehtype);

        return $this->render('NumaCCCAdminBundle:vehtypes:show.html.twig', array(
            'vehtype' => $vehtype,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing vehtype entity.
     *
     */
    public function editAction(Request $request, Vehtypes $vehtype)
    {
        $deleteForm = $this->createDeleteForm($vehtype);
        $editForm = $this->createForm('Numa\CCCAdminBundle\Form\VehtypesType', $vehtype);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash("success","Vehtype updated.");
            return $this->redirectToRoute('vehtypes_index');
        }

        return $this->render('NumaCCCAdminBundle:vehtypes:edit.html.twig', array(
            'vehtype' => $vehtype,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a vehtype entity.
     *
     */
    public function deleteAction(Request $request, Vehtypes $vehtype)
    {
        $form = $this->createDeleteForm($vehtype);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($vehtype);
            $em->flush();
        }

        return $this->redirectToRoute('vehtypes_index');
    }

    /**
     * Creates a form to delete a vehtype entity.
     *
     * @param Vehtypes $vehtype The vehtype entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Vehtypes $vehtype)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('vehtypes_delete', array('id' => $vehtype->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    public function activateAction(Request $request)
    {
        $activate = $request->attributes->get('activate');
        $ids = $this->get("numa.uigrid")->getSelectedIds($request);
        $em = $this->getDoctrine()->getManager();
        $em->getRepository(Vehtypes::class)->activate($ids, $activate);
        die();
    }

    public function exportAction(){
        $em = $this->getDoctrine()->getManager();
        $vehtypes = $em->getRepository(Vehtypes::class)->findAll();

        $html= $this->render('NumaCCCAdminBundle:vehtypes:index_excel.html.twig', array(
            'vehtypes' => $vehtypes,
        ));

        // save $table inside temporary file that will be deleted later
        return $this->get("numa.report")->htmlToXls($html->getContent(),"VehicleTypes",'VehicleTypes.xls');

    }
}
