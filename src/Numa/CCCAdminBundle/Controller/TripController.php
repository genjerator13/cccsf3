<?php

namespace Numa\CCCAdminBundle\Controller;

use Numa\CCCAdminBundle\Entity\Drivers;
use Numa\CCCAdminBundle\Entity\Trip;

use Numa\CCCAdminBundle\Form\TripDriverType;
use Numa\CCCAdminBundle\Form\TripType;
use Pagerfanta\Adapter\ArrayAdapter;
use Pagerfanta\Pagerfanta;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
/**
 * Dispatchcard controller.
 *
 */
class TripController extends Controller
{
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $trips = $em->getRepository(Trip::class)->findAll();
        $forms = $this->createTripForms();
        return $this->render('NumaCCCAdminBundle:Trip:index.html.twig', array(
            'trips' => $trips,
            'forms' => $forms,

        ));
    }

    private function createTripForms()
    {
        $em = $this->getDoctrine()->getManager();
        $trips = $em->getRepository(Trip::class)->findAll();
        $forms=[];
        foreach($trips as $trip){
            $form = $this->createTripForm($trip);

            $forms[]=$form->createView();
        }
        return $forms;
    }

    public function driverAction()
    {
        $em = $this->getDoctrine()->getManager();
        $driver = $this->get('security.token_storage')->getToken()->getUser();
        if(!$driver instanceof Driverss){

        }
        $trips = $em->getRepository(Trip::class)->findBy(array("Driver"=>$driver));

        return $this->render('NumaCCCAdminBundle:Trip:indexDriver.html.twig', array(
            'trips' => $trips,

        ));
    }

    public function editAction(Request $request,Trip $trip)
    {
        $form = $this->createTripDriverForm($trip);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $entity = $form->getData();
            dump($entity);
            $em = $this->getDoctrine()->getManager();
            $em->flush();
            $this->addFlash("success","Trip successfully updated");
            return $this->redirectToRoute("trip_driver");
        }

        return $this->render('NumaCCCAdminBundle:Trip:edit.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    private function createTripForm(Trip $trip){
        $form = $this->createForm(TripType::class, $trip, array(
            'action' => $this->generateUrl('trip_update', array('trip' => $trip->getId())),
            'method' => 'POST'
        ));
        return $form;
    }

    private function createTripDriverForm(Trip $trip){
        $form = $this->createForm(TripDriverType::class, $trip, array(
            'action' => $this->generateUrl('trip_edit', array('trip' => $trip->getId())),
            'method' => 'POST'
        ));
        return $form;
    }

    public function updateAction(Request $request, Trip $trip)
    {

        $em = $this->getDoctrine()->getManager();

        $form = $this->createTripForm($trip);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();
        }
        die("AAA");
    }

}
