<?php

namespace Numa\CCCAdminBundle\Controller;

use Numa\CCCAdminBundle\Entity\PayCode;
use Numa\CCCAdminBundle\Form\PayCodeType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Paycode controller.
 *
 */
class PayCodeController extends Controller
{
    /**
     * Lists all payCode entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $payCodes = $em->getRepository('NumaCCCAdminBundle:PayCode')->findAll();

        return $this->render('NumaCCCAdminBundle:paycode:index.html.twig', array(
            'payCodes' => $payCodes,
        ));
    }

    /**
     * Creates a new payCode entity.
     *
     */
    public function newAction(Request $request)
    {
        $payCode = new Paycode();
        $form = $this->createForm(PayCodeType::class, $payCode);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($payCode);
            $em->flush();

            return $this->redirectToRoute('paycode_index');
        }

        return $this->render('NumaCCCAdminBundle:paycode:new.html.twig', array(
            'payCode' => $payCode,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a payCode entity.
     *
     */
    public function showAction(PayCode $payCode)
    {
        $deleteForm = $this->createDeleteForm($payCode);

        return $this->render('NumaCCCAdminBundle:paycode:show.html.twig', array(
            'payCode' => $payCode,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing payCode entity.
     *
     */
    public function editAction(Request $request, PayCode $payCode)
    {
        $deleteForm = $this->createDeleteForm($payCode);
        $editForm = $this->createForm(PayCodeType::class, $payCode);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('paycode_index');
        }

        return $this->render('NumaCCCAdminBundle:paycode:edit.html.twig', array(
            'payCode' => $payCode,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a payCode entity.
     *
     */
    public function deleteAction(Request $request, PayCode $payCode)
    {
        $form = $this->createDeleteForm($payCode);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($payCode);
            $em->flush();
        }

        return $this->redirectToRoute('paycode_index');
    }

    /**
     * Creates a form to delete a payCode entity.
     *
     * @param PayCode $payCode The payCode entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(PayCode $payCode)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('paycode_delete', array('id' => $payCode->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
