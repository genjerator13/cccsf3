<?php

namespace Numa\CCCAdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Debug\Exception\FlattenException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Log\DebugLoggerInterface;


class ExceptionController  extends Controller
{

    public function showExceptionAction(Request $request, FlattenException $exception, DebugLoggerInterface $logger = null)
    {
        dump($exception);die();
        return $this->render("NumaCCCAdminBundle:Errors:error403.html.twig",array("message"=>$exception->getMessage()));

    }



}
