<?php

namespace Numa\CCCAdminBundle\Controller;

use Cocur\BackgroundProcess\BackgroundProcess;
use Numa\CCCAdminBundle\Entity\BillingPeriod;
use Numa\CCCAdminBundle\Entity\Probills;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


/**
 * MediaThek controller.
 *
 */
class SystemController extends Controller
{

    public function ExportAction()
    {
        $em = $this->getDoctrine()->getManager();
        $billingPeriod = $em->getRepository(BillingPeriod::class)->findBy(array("active"=>1));
        $file = $this->getParameter('upload')."/export.xml";
        $fileinfo = "";
        if(file_exists($file)){
            $fileinfo = date ("F d Y H:i:s.", filemtime($file));//filemtime($file);
        }
        return $this->render('NumaCCCAdminBundle:System:export.html.twig', array(
            'billingPeriods'=>$billingPeriod,
            'fileinfo'=>$fileinfo,
            'url'=>$this->getParameter('upload')."export.xml"
        ));

    }

    public function detachedAction()
    {
        $em=$this->getDoctrine()->getManager();
        $detached = $em->getRepository(Probills::class)->findBy(array("batch_id"=>null));
        $vehtypes = $em->getRepository(Probills::class)->findBy(array("vehtypes_id"=>null));
        $customer = $em->getRepository(Probills::class)->findBy(array("customersId"=>null));
        $driver = $em->getRepository(Probills::class)->findBy(array("drivers_id"=>null));
        $rates = $em->getRepository(Probills::class)->findBy(array("rates_id"=>null));
        return $this->render('NumaCCCAdminBundle:System:detached.html.twig', array(
            'detached'=>$detached,
            'vehtype'=>$vehtypes,
            'customer'=>$customer,
            'driver'=>$driver,
            'rates'=>$rates,
        ));

    }

    public function doExportAction(BillingPeriod $billingPeriod)
    {
        $command = 'php ' . $this->container->get('kernel')->getRootDir()."/../bin/console billing_period exportXML ".$billingPeriod->getId();

        $process = new BackgroundProcess($command);
        $process->run();
        $this->addFlash("success","Billing period ".$billingPeriod->getId()." start exporting the probills. Will be done in few minutes.");
        return $this->redirectToRoute("system_export_numa");
    }

}
