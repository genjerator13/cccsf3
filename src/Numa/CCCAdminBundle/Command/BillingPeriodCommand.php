<?php

namespace Numa\CCCAdminBundle\Command;

use Numa\CCCAdminBundle\Entity\Attachment;
use Numa\CCCAdminBundle\Entity\BillingPeriod;
use Numa\CCCAdminBundle\Entity\BillingPeriodReport;
use Numa\CCCAdminBundle\Entity\BillingReport;
use Numa\CCCAdminBundle\Entity\CommandLog;
use Numa\CCCAdminBundle\Entity\CustomerEmails;
use Numa\CCCAdminBundle\Entity\CustomerReport;
use Numa\CCCAdminBundle\Entity\Customers;
use Numa\CCCAdminBundle\Entity\Drivers;
use Numa\CCCAdminBundle\Entity\Email;
use Numa\CCCAdminBundle\Entity\EmailLog;
use Numa\CCCAdminBundle\Entity\Media;
use Numa\CCCAdminBundle\Entity\Probills;
use Numa\CCCAdminBundle\Entity\Settings;
use Numa\CCCAdminBundle\Lib\Report\ReportFactory;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Numa\DOAAdminBundle\Entity\User;

class BillingPeriodCommand extends ContainerAwareCommand
{

    protected $commandLog;

    //const subject = "Custom Courier Electronic Billing Notification";
    const subject = "CORRECT BILLING FOR NOV 16 to 30, 2016";

    protected function configure()
    {
        //php bin/console billing_period generateReports
        //php bin/console billing_period
        $this
            ->setName('billing_period')
            ->setDescription('Send Emails')
            ->addArgument('action', InputArgument::REQUIRED, 'Action')
            ->addArgument('batchid', InputArgument::OPTIONAL, 'Batch ID')
            ->addArgument('sending', InputArgument::OPTIONAL, 'sending')
            ->addArgument('format', InputArgument::OPTIONAL, 'format');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $logger = $this->getContainer()->get('logger');
        $action = $input->getArgument('action');
        $batchid = $input->getArgument('batchid');
        $sending = $input->getArgument('sending');
        $format = $input->getArgument('format');
        dump($action);
        dump($batchid);
        dump($sending);
        dump($format);

        if ($action == 'generateReports') {
            $logger->warning("STARTING generate reports");
            $this->generateReports($batchid);
            //$this->generateEmails($input, $batchid);
            if ($sending == 1) {
                //$this->sendAllEmails($input, $batchid);
            }
            die();
        } elseif ($action == 'sendEmailsNoAttachment') {
            $logger->warning("STARTING sendEmailsNoAttachment" . $batchid);
            $this->sendEmailsNoAttachment($input, $batchid);
            die();
        } elseif ($action == 'sendEmailsWithAttachment') {
            $logger->warning("STARTING sendEmailsWithAttachment");
            $this->sendEmailsWithAttachment($input, true, $batchid);
            die();
        } elseif ($action == 'sendAllEmails') {
            $logger->warning("STARTING sendAllEmails");
            $this->sendAllEmails($input, $batchid);
            die();
        } elseif ($action == 'generateEmails') {
            $logger->warning("STARTING generateEmails");
            $this->generateEmails($input, $batchid);
            die();
        } elseif ($action == 'generateInvoiceReports') {
            $logger->warning("STARTING generateInvoiceReports");
            //$this->generateInvoicere($input, $batchid);
            die();
        } elseif ($action == 'import') {
            $logger->warning("STARTING generateInvoiceReports");
            $this->importFromXml($input, $batchid);
            die();
        } elseif ($action == 'import_customer_emails') {
            $logger->warning("STARTING importCustomerEmails");
            $this->importCustomerEmails($input, $batchid);
            die();
        } elseif ($action == 'generateCustomerReport') {
            $logger->warning("STARTING generateCustomerReport");
            $this->generateCustomerReport($input, $batchid, $sending, $format);
            die();
        } elseif ($action == 'generateDriverReport') {
            $logger->warning("STARTING generateDriverReport");
            $this->generateDriverReport($input, $batchid, $sending);
            die();
        } elseif ($action == 'exportXML') {
            $logger->warning("STARTING exportXML");
            $this->exportXML($input, $batchid);
            die();
        } elseif ($action == 'calculateDriver') {
            $logger->warning("STARTING calculateDriver");
            $this->calculateDriver($input, $output, $batchid);
            die();
        }
    }

    public function sendAllEmails($input, $billing_period_id)
    {

        $logger = $this->getContainer()->get('logger');
        $limit = 50;

        $em = $this->getContainer()->get('doctrine')->getManager();
        $logger->warning("inside sendAllEmails");
        $emails = $em->getRepository(Email::class)->findEmailsByBillingPeriod($billing_period_id);

        $countAllEmails = count($emails);
        $logger->warning("COUNT sendAllEmails " . $countAllEmails);
        $ok = true;

        if ($countAllEmails > 0) {

            $action = $input->getArgument('action');
            $mailer = $this->getContainer()->get('mailer');

            $logger->warning("sendAllEmails createing command log" . $countAllEmails);
            $commandName = "billing_period sendAllEmails " . $billing_period_id;
            $this->commandLog = new \Numa\CCCAdminBundle\Entity\CommandLog();
            $this->commandLog->setCommand($commandName);
            $this->commandLog->setStartedAt(new \DateTime());
            $this->commandLog->setStatus('started');
            $this->commandLog->setCategory('email');
            $this->commandLog->setTotalProbills($billing_period_id);
            $this->commandLog->setCurrentProbill(2);
            $this->commandLog->setTotal($countAllEmails);
            $em->persist($this->commandLog);
            $em->flush();
            $logger->warning("sendAllEmails flush command log" . $countAllEmails);

            $current = 0;
            foreach ($emails as $email) {
                if ($email instanceof Email) {

                }

                if($email->getStatus()==0) {

                    $current++;
                    $logger->warning("sendAllEmails current $current");


                    $message = \Swift_Message::newInstance()
                        ->setFrom($email->getEmailFrom())
                        //->setTo('e.medjesi@gmail.com')
                        ->setTo($email->getEmailTo())
                        ->setBody($email->getBody());
                    //$message->addBcc($email->getEmailTo());
                    $message->setSubject($email->getSubject());

                    $attachments = $email->getAttachment();

                    if (!empty($attachments)) {

                        foreach ($attachments as $attachment) {

                            if ($attachment instanceof Attachment) {
                                $media = $attachment->getMedia();

                                if ($media instanceof Media) {
                                    //$message->attach(\Swift_Attachment::fromPath($attachment->getMedia()));
                                    $mime = 'application/pdf';
                                    if (stripos($media->getMimetype(), 'pdf') !== false) {
                                        $mime = 'application/pdf';
                                    } elseif (stripos($media->getMimetype(), 'xls') !== false) {
                                        $mime = 'application/vnd.ms-excel';
                                    }


                                    $message->attach(\Swift_Attachment::newInstance(base64_decode($media->getContent()), $media->getName(), $mime));

                                }
                            }
                        }

                    }
                }
                $email->setStatus(1);
                $email->setDateCreated(new \DateTime());
                $logger->warning("sendAllEmails EMAIL add BCC  " . $email->getEmailTo());


                $logger->warning("sendAllEmails EMAIL SENDING " . $email->getEmailTo());

                $ok = $this->getContainer()->get('mailer')->send($message);

                if ($ok) {
                    $this->commandLog->setCurrent($current);
                    $this->commandLog->setEndedAt(new \DateTime());
                    $this->commandLog->setStatus('ended');
                    $em->flush();
                    $logger->warning("sendEmailsNoAttachment END successfully  ");
                } else {
                    $logger->warning("sendEmailsNoAttachment END NOT successfully  ");
                }
            }
        }
    }

    public function sendEmailsWithAttachment($input, $attachment, $billing_period_id)
    {
        $logger = $this->getContainer()->get('logger');
        $limit = 10;
        $logger->warning("inside sendEmailsWithAttachment" . $billing_period_id);
        if ($attachment) {
            $commandName = "Numa:emails sendEmailsWithAttachment" . $billing_period_id;
        } else {
            $commandName = "Numa:emails sendEmailsWithNOAttachment" . $billing_period_id;
        }
        $em = $this->getContainer()->get('doctrine')->getManager();


        $countEmailsWithAttachment = $em->getRepository('NumaCCCAdminBundle:Email')->countNotSendEmails($attachment, $billing_period_id);
        $logger->warning("COUNT countEmailsWithAttachment " . $countEmailsWithAttachment);


        $logger->warning("COUNT sendEmailsNoAttachment " . $countEmailsWithAttachment);
        $current = 0;
        if ($countEmailsWithAttachment > 0) {
            $this->commandLog = new \Numa\CCCAdminBundle\Entity\CommandLog();

//            $action = $input->getArgument('action');
//            $mailer = $this->getContainer()->get('mailer');

            $this->commandLog->setCommand($commandName);
            $this->commandLog->setStartedAt(new \DateTime());
            $this->commandLog->setStatus('started');
            $this->commandLog->setCategory('email');
            $this->commandLog->setTotal($countEmailsWithAttachment);
            $this->commandLog->setTotalProbills($billing_period_id);
            $this->commandLog->setCurrentProbill(3);//code for attacment need for progress
            $em->persist($this->commandLog);
            $em->flush();
            $logger->warning("countEmailsWithAttachment COMMAND LOG STARTED");

            while ($emails = $em->getRepository('NumaCCCAdminBundle:Email')->getNotSendEmails($limit, $attachment, $billing_period_id)) {
                $logger->warning("countEmailsWithAttachment inside WHILE LOOP getNotSendEmails");

                foreach ($emails as $email) {
                    $current++;
                    $logger->warning("sendEmailsNoAttachment inside EMAILS LOOP " . $current);
                    $message = \Swift_Message::newInstance()
                        ->setSubject($email->getSubject())
                        ->setFrom($emailFrom)
                        //->setTo('e.medjesi@gmail.com') //
                        ->setTo($email->getEmailTo())
                        ->setBody($emailBody->getValue());
                    $attachment = $email->getAttachment();
                    if (!empty($attachment)) {
                        $attachments = explode(";", $attachment);
                        foreach ($attachments as $attachment) {
                            $message->attach(\Swift_Attachment::fromPath($attachment));
                        }
                    }

                    $logger->warning("sendEmailsNoAttachment before email sent " . $email->getEmailTo());
                    $ok = $this->getContainer()->get('mailer')->send($message);
                    $logger->warning("sendEmailsNoAttachment after email sent " . $email->getEmailTo());
                    ///make a email log
                    $logger->warning("sendEmailsNoAttachment making email log " . $email->getEmailTo());
                    $emailLog = new EmailLog();
                    $emailLog->setEmail($message);
                    $emailLog->setBatchId($billing_period_id);
                    $emailLog->getStatus("ERROR");
                    if ($ok) {
                        $emailLog->setStatus("SUCCESS");
                    }
                    $em->persist($emailLog);

                    $logger->warning("sendEmailsNoAttachment SEND EMAIL ATTACHMENT " . $email->getEmailTo());
                    //$testHtml = $this->getContainer()->get('Numa.Controiller.Reports')->renderSwiftMessage($message, $attachment);
                    //dump($testHtml);
                    if ($ok) {
                        $email->setStatus('sent');
                        $email->setEndedAt(new \DateTime());
                        $this->commandLog->setCurrent($current);
                        $em->flush();

                        $logger->warning("sendEmailsNoAttachment SEND EMAIL ATTACHMENT SUCCESS before sleep 30 after email status sent flushed " . $email->getEmailTo());
                        //sleep(30);
                        $logger->warning("sendEmailsNoAttachment SEND EMAIL ATTACHMENT SUCCESS " . $email->getEmailTo());
                    } else {
                        $logger->error("sendEmailsNoAttachment SEND EMAIL ATTACHMENT ERROR " . $email->getEmailTo());
                    }
                }
            }
            $this->commandLog->setEndedAt(new \DateTime());
            $this->commandLog->setStatus('ended');
            $em->flush();
            $logger->warning("sendEmailsNoAttachment END ");
        }
    }

//    public function sendAllEmails($input, $billing_period_id)
//    {
//        $this->sendEmailsNoAttachment($input, $billing_period_id);
//        $this->sendEmailsWithAttachment($input, true, $billing_period_id);
//    }

    public function generateReports($billingPeriodId)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $billingPeriod = $em->getRepository(BillingPeriod::class)->find($billingPeriodId);
        $em->getRepository(CustomerReport::class)->clearAllReportEmails($billingPeriod);
        //get all the customers from the batch which has send email yes
        $logger = $this->getContainer()->get('logger');
        $logger->warning("generateReportsaaaaaaaaaaaaa");

        $em->getRepository(CommandLog::class)->deleteProgressLog($billingPeriodId);
        $customers = $em->getRepository(Customers::class)->findOnlyActive($billingPeriodId);

        $emails = array();
        //dump("customers fetched" . count($customerSendmailYes));
        $commandName = "billing_period generateReports " . $billingPeriodId . ' ';
        $current = 0;
        $this->commandLog = new \Numa\CCCAdminBundle\Entity\CommandLog();
        $this->commandLog->setCurrent($current);
        $this->commandLog->setTotal(count($customers));
        $this->commandLog->setStartedAt(new \DateTime());
        $this->commandLog->setStatus('started');
        $this->commandLog->setCategory('email');
        $this->commandLog->setTotalProbills($billingPeriodId);
        $this->commandLog->setCurrentProbill(0);
        $this->commandLog->setCommand($commandName);

        $em->persist($this->commandLog);
        $em->flush();

        $commandName = "numa:emails generateEmails " . $billingPeriodId . ' ';
        $current = 0;
        $emailCommand = new \Numa\CCCAdminBundle\Entity\CommandLog();
        $emailCommand->setCurrent($current);

        $emailCommand->setTotal(count($customers));
        $emailCommand->setStartedAt(new \DateTime());
        $emailCommand->setStatus('started');
        $emailCommand->setCategory('email');
        $emailCommand->setTotalProbills($billingPeriodId);
        $emailCommand->setCurrentProbill(1); //code for emails generate need for progress bar
        $emailCommand->setCommand($commandName);
        $em->persist($emailCommand);
        $em->flush();


        $text = "";
        $logger->warning("generateReportsXXXXXXX");

        foreach ($customers as $customer) {

            $current++;
            $progressText = "proccessing: " . $customer->getCustcode();
            $logger->warning($progressText);
            $text = $text . $progressText . "<br>/n";
            $anyProbills = $em->getRepository(Probills::class)->anyProbillsForCustomerBillingPeriod($customer,$billingPeriod);
            //die();
            if ($customer instanceof Customers && $anyProbills && !$customer->getCustomerEmails()->isEmpty()) {

                $logger->warning("create PDF for " . $customer->getName());
                $customerDeliveryReport = ReportFactory::create($this->getContainer(), "delivery_all", $billingPeriod);
                //$customerDeliveryReport->prepareDataForBillingPeriod();
                $customerDeliveryReport->setCustomers(array($customer));
                //$pdf = $customerDeliveryReport->renderPDF();

                $logger->warning("create PDF for " . $customer->getCustcode());
                $pdf = $customerDeliveryReport->rendermPDF();

                $logger->warning("create XLS for " . $customer->getCustcode());
                $xls = $customerDeliveryReport->renderXLS(array(), true);

                //generate invoice PDF
                $invoiceReport = ReportFactory::create($this->getContainer(), "invoices", $billingPeriod);
                $invoiceReport->setCustomers(array($customer));
                $invoicePdf = $invoiceReport->renderPDF2("content");
                //$customerReport = $em->getRepository(CustomerReport::class)->findOneBy(array("billing_period_id" => $billingPeriodId, "customer_id" => $customer->getId()));
                $customerReport = new CustomerReport();
                $customerReport->setBillingPeriod($billingPeriod);
                $customerReport->setCustomers($customer);
                $attachments = array();
                $logger->warning("send emails::::::::::::::::: " . $customer->getSendmail());
                if (strtoupper($customer->getSendmail() == "Y")) {


                    $deliveryPdfMedia = new Media();
                    $deliveryXlsMedia = new Media();
                    $invoiceMedia = new Media();
                    $email = new Email();


                    $deliveryPdfMedia->setContent(base64_encode($pdf));
                    $deliveryPdfMedia->setName("CustomerDelivery" . $billingPeriod . ".pdf");
                    $deliveryPdfMedia->setMimetype("pdf");
                    $em->persist($deliveryPdfMedia);
                    dump("deliveryXlsMedia");
                    $deliveryXlsMedia->setContent(base64_encode($xls));
                    $deliveryXlsMedia->setName("CustomerDelivery" . $billingPeriod . ".xls");
                    $deliveryXlsMedia->setMimetype("xls");
                    $em->persist($deliveryXlsMedia);
                    dump("invoiceMedia");
                    $invoiceMedia->setContent(base64_encode($invoicePdf));
                    $invoiceMedia->setName("CustomerInvoice" . $billingPeriod . ".pdf");
                    $invoiceMedia->setMimetype("pdf");
                    $em->persist($invoiceMedia);

                    $attachments['deliveryPdfMedia'] = $deliveryPdfMedia;
                    $attachments['deliveryXlsMedia'] = $deliveryXlsMedia;
                    $attachments['invoiceMedia'] = $invoiceMedia;

                }
                $this->getContainer()->get("numa.report.mailing")->attachEmailToReport($em, $customerReport, $attachments, $billingPeriodId);
                $em->persist($customerReport);
                //dump($customerReport);
                $em->flush();
                dump("flush");
            }


//            if ($current % 50 == 0) {
            $query = "update command_log set current=$current where id=" . $this->commandLog->getId();
            dump($query);
            $stmt = $em->getConnection()->prepare($query);
            $stmt->execute();
            $query = "update command_log set current=$current where id=" . $emailCommand->getId();
            $stmt = $em->getConnection()->prepare($query);
            $stmt->execute();
            dump($query);

//            }
        }
        $em->flush();

        $this->commandLog->setEndedAt(new \DateTime());
        $this->commandLog->setStatus('ended');
        $em->flush();
    }


    private function importFromXml($input, $batchid)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();;
        $billingPeriod = $em->getRepository(BillingPeriod::class)->find($batchid);
        //dump("billing period: "+$batchid);
        //dump($billingPeriod);
        $this->getContainer()->get("numa.billing_period")->importFromXml($billingPeriod);
    }

    private function importCustomerEmails($input, $batchid)
    {
        $emmysql = $this->getContainer()->get('doctrine')->getManager('default');
        $emoldccc = $this->getContainer()->get('doctrine')->getManager('cccold');
//        dump($emoldccc->);
//        die();
        $emails = $emoldccc->getRepository(Customers::class)->getCustomersEmails();
        $emailsnew = $emmysql->getRepository(Customers::class)->getCustomersEmails();

        $customerEmails = $this->getContainer()->get('doctrine')
            ->getRepository(Customers::class, 'cccold')
            ->getCustomersEmails();
        // dump($customers);

        foreach ($customerEmails as $email) {
            dump($email);
            $cemail = new CustomerEmails();
            $cemail->setEmail($email['email']);
            $cemail->setSelected($email['selected']);
            if (!empty($email['custcode'])) {
                dump($email['custcode']);
                $customer = $emmysql->getRepository(Customers::class)->findOneBy(array('custcode' => $email['custcode']));
                if ($customer instanceof Customers) {
                    $cemail->setCustomers($customer);
                    $cemail->setCustomerId($customer->getId());
                    $emmysql->persist($cemail);
                    $emmysql->flush();
                }
            }
        }
        die();
        //return $emsqlite;
    }

    private function generateCustomerReport($input, $billingPeriodId, $subtype, $format)
    {
        dump("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
        dump($billingPeriodId);
        dump($subtype);
        dump($format);
        //die();
        $em = $this->getContainer()->get('doctrine')->getManager();
        $logger = $this->getContainer()->get('logger');
        dump("generateCustomerReport");
        dump($billingPeriodId);
        dump($format);
        $command = "generate_customer_".$subtype."_report_".$billingPeriodId."_".$format;
        $logger->warning($command);
        $cl = new CommandLog();
        $cl->setCategory("report");
        $cl->setCommand($command);
        $cl->setStatus("started");
        $cl->setStartedAt(new \DateTime());

        $logger->warning("COMMAND LOG CREATED1");
        $em = $this->getContainer()->get('doctrine')->getManager();
        $logger->warning("COMMAND LOG CREATED2");
        $em->persist($cl);
        $logger->warning("COMMAND LOG CREATED3");
        $em->flush();
        $logger->warning("COMMAND LOG CREATED4");
        $billingPeriod = $em->getRepository(BillingPeriod::class)->find($billingPeriodId);
        $deliveryReport = ReportFactory::create($this->getContainer(), "delivery_" . strtolower($subtype), $billingPeriod);
        if (strtolower($subtype) == "all") {
            $deliveryReport->setAllCustomers();
        } elseif (strtolower($subtype) == "snail") {
            $deliveryReport->setSnailCustomers();

        }
        $deliveryReport->prepareDataForBillingPeriod();
        $content = "";
        if (strtoupper($format) == "PDF") {
            $content = $deliveryReport->rendermPDF(array(), true);
        } elseif (strtoupper($format) == "XLS") {
            $content = $deliveryReport->renderXLS(array(), true);
        }

        $billingPeriodCustomerDetails = $em->getRepository(BillingReport::class)->findOneBy(array('billing_period_id' => $billingPeriodId, "name" => "customer_details"));

        if (!$billingPeriodCustomerDetails instanceof BillingReport) {
            $billingPeriodCustomerDetails = new BillingReport();
            $em->persist($billingPeriodCustomerDetails);
        }

        $media = $this->getMedia($content, $subtype, $billingPeriodId, $format);
        $em->persist($media);
        $billingPeriodCustomerDetails->setBillingPeriod($billingPeriod);
        $billingPeriodCustomerDetails->setName("customer_details_" . $subtype . "_" . $format);
        $billingPeriodCustomerDetails->setMedia($media);
        $cl->setStatus("finished");
        $cl->setEndedAt(new \DateTime());

        $em->flush();
        //dump($pdf);
        die();
    }

    private function getMedia($content, $subtype, $billingPeriodId, $format)
    {
        $name = "customer_details_" . $subtype . "_" . strtolower($format) . "_" . $billingPeriodId . "." . strtolower($format);
        $em = $this->getContainer()->get('doctrine')->getManager();
        $media = $em->getRepository(Media::class)->findOneBy(array('name' => $name));
        if (!$media instanceof Media) {
            $media = new Media();
        }
        $media->setDateCreated(new \DateTime());
        $media->setName($name);
        $media->setContent(base64_encode($content));
        $media->setMimetype(strtolower($format));
        return $media;

    }


    private function exportXML($input, $batchid)
    {
        $file = $upload = $this->getContainer()->getParameter('upload') . "export.xml";

        $em = $this->getContainer()->get('doctrine.orm.default_entity_manager');
        $billingPeriod = $em->getRepository(BillingPeriod::class)->find($batchid);

        $xml = $this->getContainer()->get("numa.billing_period")->exportToXml($billingPeriod);
        if (!file_exists($file)) {
            //$myfile = fopen($file, "w");
        }
        $xml->save($file);

        die();
    }

    private function calculateDriver($input, $ouput, $batchid)
    {
        $em = $this->getContainer()->get('doctrine.orm.default_entity_manager');
        $billingPeriod = $em->getRepository(BillingPeriod::class)->find($batchid);
        $probills = $em->getRepository(Probills::class)->findAllByBillingPeriod($billingPeriod->getId())->getResult();
        $i = 0;
        foreach ($probills as $probill) {
            $drivername = "";

            if ($probill instanceof Probills) {

            }
            if ($probill->getDrivers() instanceof Drivers) {
                $drivername = $probill->getDrivers()->getDrivernam();
                dump($drivername);
            }
            $i++;
            //dump($probill->getWaybill());
            $ouput->writeLn("Driver calculation for " . $probill->getWaybill() . ": drivername=" . $drivername);
            $probill = $this->getContainer()->get('numa.probill')->calculateDriver($probill);
            if ($i % 50 == 0) {
                dump("flush" . $i);

                $em->flush();
            }
        }
        $em->flush();
        die();
    }

    private function generateDriverReport($input, $billingPeriodId, $format)
    {
        $logger = $this->getContainer()->get('logger');
        dump("generateDriverReport");
        dump($billingPeriodId);
        dump($format);
        $command = "generate_driver_detail_report_".$billingPeriodId."_".$format;
        $logger->warning($command);
        $cl = new CommandLog();
        $cl->setCategory("report");
        $cl->setCommand($command);
        $cl->setStatus("started");
        $cl->setStartedAt(new \DateTime());

        $logger->warning("COMMAND LOG CREATED1");
        $em = $this->getContainer()->get('doctrine')->getManager();
        $logger->warning("COMMAND LOG CREATED2");
        $em->persist($cl);
        $logger->warning("COMMAND LOG CREATED3");
        $em->flush();
        $logger->warning("COMMAND LOG CREATED4");
        $billingPeriod = $em->getRepository(BillingPeriod::class)->find($billingPeriodId);
        $driverDetailReport = ReportFactory::create($this->getContainer(), "driver_details", $billingPeriod);
        //$deliveryReport->setAllCustomers();

        //$deliveryReport->prepareDataForBillingPeriod();
        $content = "";
        if (strtoupper($format) == "PDF") {
            $content = $driverDetailReport->rendermPDF(array(), true);
        } elseif (strtoupper($format) == "XLS") {
            $content = $driverDetailReport->renderXLS(array(), true);
        }

        $billingPeriodDriverDetails = $em->getRepository(BillingReport::class)->findOneBy(array('billing_period_id' => $billingPeriodId, "name" => "driver_details"));

        if (!$billingPeriodDriverDetails instanceof BillingReport) {
            $billingPeriodDriverDetails = new BillingReport();
            $em->persist($billingPeriodDriverDetails);
        }

        $media = $this->getMedia($content, "", $billingPeriodId, $format);
        $em->persist($media);
        $billingPeriodDriverDetails->setBillingPeriod($billingPeriod);
        $billingPeriodDriverDetails->setName("driver_details_" . $format);
        $billingPeriodDriverDetails->setMedia($media);
        $em->flush();
        dump($media->getId());
        $cl->setStatus("finished");
        $cl->setEndedAt(new \DateTime());
        $em->flush();
        die();
    }


}
