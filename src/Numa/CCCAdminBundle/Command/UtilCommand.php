<?php
namespace Numa\CCCAdminBundle\Command;

use Numa\CCCAdminBundle\Entity\Batch;
use Numa\CCCAdminBundle\Entity\BillingPeriod;
use Numa\CCCAdminBundle\Entity\Drivers;
use Numa\CCCAdminBundle\Entity\PendingProbill;
use Numa\CCCAdminBundle\Entity\Probills;
use Numa\CCCAdminBundle\Entity\Rates;
use Numa\CCCAdminBundle\Entity\Vehtypes;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class UtilCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        //php app/console numa:DOA:users admin admin
        $this
            ->setName('numa:CCC:util')
            ->setDescription('Various utils')
            ->addArgument('action', InputArgument::REQUIRED, 'action')
            ->addArgument('argument1', InputArgument::OPTIONAL, 'argument1');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $action = $input->getArgument('action');
        $arg1 = $input->getArgument('argument1');

        if ($action == "batch_names") {
            $this->generateNamesForAllBatches();
        }
        if ($action == "drivers") {
            $this->connectDriversToProbill($arg1);
        }
        if ($action == "rates") {
            $this->connectRatesToProbill($arg1);
        }
        if ($action == "tobatches") {
            dump($action);
            $this->toBatches($arg1);
        }
        if ($action == "convertBillingPeriodFromOld") {
            dump($action);
            $this->convertBillingPeriodFromOld();
        }
        if ($action == "convertOldProbills") {
            dump($action);
            $this->convertOldProbills($arg1);
        }if ($action == "setDriverCredentials") {
            dump($action);
            $this->setDriverCredentials($arg1);
        }

    }

    public function generateNamesForAllBatches()
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $batches = $em->getRepository(Batch::class)->findAll();
        foreach ($batches as $batch) {
            $name = $this->getContainer()->get("numa.batch")->generateNameFromDate($batch->getBatchDate());
            $batch->setName($name);
        }
        $em->flush();
    }

    public function connectDriversToProbill($batchid)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $batch = $em->getRepository(Batch::class)->find($batchid);

        $probills = $em->getRepository(Probills::class)->findBy(array("batchPB" => $batch));
        foreach ($probills as $probill) {
            $driver = $em->getRepository(Drivers::class)->findOneBy(array("drivernum" => $probill->getDriverCode()));
            if ($driver instanceof Drivers) {
                dump($probill->getDriverCode());
                $probill->setDrivers($driver);
            }
        }
        $em->flush();
    }

    public function connectRatesToProbill($batchid)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $batch = $em->getRepository(Batch::class)->find($batchid);

        $probills = $em->getRepository(Probills::class)->findBy(array("batchPB" => $batch));

        foreach ($probills as $probill) {
            $rate = $em->getRepository(Rates::class)->findOneBy(array("rateDescription" => $probill->getRateDescription()));
            $driver = $em->getRepository(Drivers::class)->findOneBy(array("drivernum" => $probill->getDriverCode()));
            $vehcode = $em->getRepository(Vehtypes::class)->findOneBy(array("vehdesc" => $probill->getVe()));
            if ($rate instanceof Rates) {
                dump($probill->getRateDescription());
                $probill->setRates($rate);
            }
            if ($driver instanceof Drivers) {
                dump($probill->getDriverCode());
                $probill->setDrivers($driver);
            }
        }
        $em->flush();
    }

    public function toBatches($billingPeriodId)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $probills = $em->getRepository(Probills::class)->findNullBatch();
        $billingPeriod = $em->getRepository(BillingPeriod::class)->find($billingPeriodId);

        foreach ($probills as $probill) {
            $probill = $this->assignProbillToBatch($probill, $billingPeriod);
        }
        $em->flush();
        die();
    }

    public function assignProbillToBatch($probill,$billingPeriod){
        $em = $this->getContainer()->get('doctrine')->getManager();
        if ($probill instanceof Probills) {
        }
        $createdAt = $probill->getCreatedAt();
        $createdSplit = explode("-", $createdAt);

        $timestamp = $createdSplit[0];

        $date = date('Y-m-d', $timestamp);

        $datetime = new \DateTime($date);
        $currentWeekDay = date("w", $timestamp);

        switch ($currentWeekDay) {
            case "1": {  // monday
                $datetime->modify("-3 day");
                break;
            }
            case "0": {  // sunday
                $datetime->modify("-2 day");
                break;
            }
            default: {  //all other days
                $datetime->modify("-1 day");
                break;
            }
        }

        $batch = $this->getContainer()->get("numa.batch")->createIfNotExists($em, $datetime);
        $batch->setBillingPeriod($billingPeriod);
        $probill->setBatchPB($batch);

        return $probill;
    }

    public function convertBillingPeriodFromOld()
    {

        $emoldccc = $this->getContainer()->get('doctrine')->getManager('cccold');
        $em = $this->getContainer()->get('doctrine')->getManager();
        $sql = "SELECT * FROM batchX ";


        $stmt = $emoldccc->getConnection()->prepare($sql);
        $stmt->execute();
        $oldBatches = $stmt->fetchAll();

        foreach ($oldBatches as $batch) {
            $closed = \DateTime::createFromFormat("Y-m-d", $batch['closed'])->setTime(0, 0, 0);
            $name = $this->getContainer()->get("numa.batch")->generateNameFromDate($closed);
            $bp = $em->getRepository(BillingPeriod::class)->findOneBy(array('name' => $name));
            if (!$bp instanceof BillingPeriod) {
                $bp = new BillingPeriod();
                $em->persist($bp);
            }
            $bp->setStarted(\DateTime::createFromFormat("Y-m-d", $batch['started'])->setTime(0, 0, 0));
            $bp->setClosed($closed);

            $bp->setName($name);
            $bp->setWorkingDays($batch['working_days']);
            $bp->setOldBatchId($batch['id']);

            $bp->setActive(false);

        }

        $em->flush();
        die();
    }

    public function convertOldProbills($oldBatchId){
        $emoldccc = $this->getContainer()->get('doctrine')->getManager('cccold');
        $em = $this->getContainer()->get('doctrine')->getManager();
        $sql = "SELECT * FROM Probills WHERE batch_x_id= ".intval($oldBatchId);
        $stmt = $emoldccc->getConnection()->prepare($sql);
        $stmt->execute();
        $oldProbills = $stmt->fetchAll();

        $billingPeriod = $em->getRepository(BillingPeriod::class)->findOneBy(array('old_batch_id'=>$oldBatchId));
        $i=0;
        foreach ($oldProbills as $oldProbill) {
            $i++;
            $probill = $em->getRepository(Probills::class)->findOneBy(array('createdAt'=>$oldProbill['created_at']));

            if(!$probill instanceof Probills){
                $probill = new Probills();
                $em->persist($probill);
            }
            $probill->setCreatedAt($oldProbill['created_at']);

            if(!empty($probill->getCreatedAt())) {

                $probill = $this->assignProbillToBatch($probill, $billingPeriod);

                $probill = $this->getContainer()->get('numa.probill')->bindProbillFromOldArray($oldProbill, $probill,$emoldccc);
                $probill->setCreatedAt($oldProbill['created_at']);

            }
            dump($i.":::".$probill->getWaybill());
            if($i%100==0) {
                $em->flush();
            }
        }
        $em->flush();
        die();
    }

    public function setDriverCredentials(){
        $em = $this->getContainer()->get('doctrine')->getManager();
        $drivers = $em->getRepository(Drivers::class)->findAll();
        foreach($drivers as $driver){
            $words = explode(" ",$driver->getDrivernam());
            dump($words[0]);

            $username= $words[0];
            if(!empty($words[1])){

                $username= $words[0].$words[1][0];
            }
            $password = $username.$driver->getDrivernum();
            $driver->setUsername($username);

            $factory = $this->getContainer()->get('security.encoder_factory');
            $encoder = $factory->getEncoder($driver);
            if (!empty($password)) {

                $encodedPassword = $encoder->encodePassword($password, $driver->getSalt());
                $driver->setPassword($encodedPassword);
            }

        }
        $em->flush();
    }




}
