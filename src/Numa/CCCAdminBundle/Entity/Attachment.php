<?php

namespace Numa\CCCAdminBundle\Entity;

/**
 * Attachment
 */
class Attachment
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $email_id;

    /**
     * @var integer
     */
    private $media_id;

    /**
     * @var \Numa\CCCAdminBundle\Entity\Email
     */
    private $Email;

    /**
     * @var \Numa\CCCAdminBundle\Entity\Media
     */
    private $Media;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set emailId
     *
     * @param integer $emailId
     *
     * @return Attachment
     */
    public function setEmailId($emailId)
    {
        $this->email_id = $emailId;

        return $this;
    }

    /**
     * Get emailId
     *
     * @return integer
     */
    public function getEmailId()
    {
        return $this->email_id;
    }

    /**
     * Set mediaId
     *
     * @param integer $mediaId
     *
     * @return Attachment
     */
    public function setMediaId($mediaId)
    {
        $this->media_id = $mediaId;

        return $this;
    }

    /**
     * Get mediaId
     *
     * @return integer
     */
    public function getMediaId()
    {
        return $this->media_id;
    }

    /**
     * Set email
     *
     * @param \Numa\CCCAdminBundle\Entity\Email $email
     *
     * @return Attachment
     */
    public function setEmail(\Numa\CCCAdminBundle\Entity\Email $email = null)
    {
        $this->Email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return \Numa\CCCAdminBundle\Entity\Email
     */
    public function getEmail()
    {
        return $this->Email;
    }

    /**
     * Set media
     *
     * @param \Numa\CCCAdminBundle\Entity\Media $media
     *
     * @return Attachment
     */
    public function setMedia(\Numa\CCCAdminBundle\Entity\Media $media = null)
    {
        $this->Media = $media;

        return $this;
    }

    /**
     * Get media
     *
     * @return \Numa\CCCAdminBundle\Entity\Media
     */
    public function getMedia()
    {
        return $this->Media;
    }

    public function __toString()
    {

        return $this->getMedia() instanceof Media?$this->getMedia()->getName():" ";
    }
}
