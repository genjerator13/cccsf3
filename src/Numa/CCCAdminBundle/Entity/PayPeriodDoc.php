<?php

namespace Numa\CCCAdminBundle\Entity;

/**
 * PayPeriodDoc
 */
class PayPeriodDoc
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $pay_period_id;

    /**
     * @var integer
     */
    private $wcb_report;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $PayPeriod;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $WcbReport;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->PayPeriod = new \Doctrine\Common\Collections\ArrayCollection();
        $this->WcbReport = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set payPeriodId
     *
     * @param integer $payPeriodId
     *
     * @return PayPeriodDoc
     */
    public function setPayPeriodId($payPeriodId)
    {
        $this->pay_period_id = $payPeriodId;

        return $this;
    }

    /**
     * Get payPeriodId
     *
     * @return integer
     */
    public function getPayPeriodId()
    {
        return $this->pay_period_id;
    }

    /**
     * Set wcbReport
     *
     * @param integer $wcbReport
     *
     * @return PayPeriodDoc
     */
    public function setWcbReport($wcbReport)
    {
        $this->wcb_report = $wcbReport;

        return $this;
    }

    /**
     * Get wcbReport
     *
     * @return integer
     */
    public function getWcbReport()
    {
        return $this->wcb_report;
    }

    /**
     * Add payPeriod
     *
     * @param \Numa\CCCAdminBundle\Entity\PayPeriod $payPeriod
     *
     * @return PayPeriodDoc
     */
    public function addPayPeriod(\Numa\CCCAdminBundle\Entity\PayPeriod $payPeriod)
    {
        $this->PayPeriod[] = $payPeriod;

        return $this;
    }

    /**
     * Remove payPeriod
     *
     * @param \Numa\CCCAdminBundle\Entity\PayPeriod $payPeriod
     */
    public function removePayPeriod(\Numa\CCCAdminBundle\Entity\PayPeriod $payPeriod)
    {
        $this->PayPeriod->removeElement($payPeriod);
    }

    /**
     * Get payPeriod
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPayPeriod()
    {
        return $this->PayPeriod;
    }

    /**
     * Add wcbReport
     *
     * @param \Numa\CCCAdminBundle\Entity\Media $wcbReport
     *
     * @return PayPeriodDoc
     */
    public function addWcbReport(\Numa\CCCAdminBundle\Entity\Media $wcbReport)
    {
        $this->WcbReport[] = $wcbReport;

        return $this;
    }

    /**
     * Remove wcbReport
     *
     * @param \Numa\CCCAdminBundle\Entity\Media $wcbReport
     */
    public function removeWcbReport(\Numa\CCCAdminBundle\Entity\Media $wcbReport)
    {
        $this->WcbReport->removeElement($wcbReport);
    }
    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        // Add your code here
    }

    /**
     * @ORM\PreUpdate
     */
    public function setUpdatedAtValue()
    {
        // Add your code here
    }
}
