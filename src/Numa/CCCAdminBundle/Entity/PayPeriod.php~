<?php

namespace Numa\CCCAdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation as JMS;
/**
 * PayPeriod
 * @JMS\ExclusionPolicy("ALL")
 */
class PayPeriod
{
    /**
     * @var integer
     * @JMS\Expose
     */
    private $id;

    /**
     * @var string
     * @JMS\Expose
     */
    private $name;

    /**
     * @var \DateTime
     * @JMS\Expose
     */
    private $started;

    /**
     * @var \DateTime
     * @JMS\Expose
     */
    private $closed;

    /**
     * @var \DateTime
     * @JMS\Expose
     */
    private $date_created;

    /**
     * @var \DateTime
     * @JMS\Expose
     */
    private $date_updated;

    /**
     * @var \DateTime
     * @JMS\Expose
     */
    private $date_finished;

    /**
     * @var integer
     * @JMS\Expose
     */
    private $user_id;

    /**
     * @var string
     */
    private $from_billing_period;

    /**
     * @var integer
     * @JMS\Expose
     */
    private $type;

    /**
     * @var integer
     * @JMS\Expose
     */
    private $status;

    /**
     * @var \Numa\CCCAdminBundle\Entity\User
     */
    private $User;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return PayPeriod
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set started
     *
     * @param \DateTime $started
     *
     * @return PayPeriod
     */
    public function setStarted($started)
    {
        $this->started = $started;

        return $this;
    }

    /**
     * Get started
     *
     * @return \DateTime
     */
    public function getStarted()
    {
        return $this->started;
    }

    /**
     * Set closed
     *
     * @param \DateTime $closed
     *
     * @return PayPeriod
     */
    public function setClosed($closed)
    {
        $this->closed = $closed;

        return $this;
    }

    /**
     * Get closed
     *
     * @return \DateTime
     */
    public function getClosed()
    {
        return $this->closed;
    }

    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     *
     * @return PayPeriod
     */
    public function setDateCreated($dateCreated)
    {
        $this->date_created = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * Set dateUpdated
     *
     * @param \DateTime $dateUpdated
     *
     * @return PayPeriod
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->date_updated = $dateUpdated;

        return $this;
    }

    /**
     * Get dateUpdated
     *
     * @return \DateTime
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * Set dateFinished
     *
     * @param \DateTime $dateFinished
     *
     * @return PayPeriod
     */
    public function setDateFinished($dateFinished)
    {
        $this->date_finished = $dateFinished;

        return $this;
    }

    /**
     * Get dateFinished
     *
     * @return \DateTime
     */
    public function getDateFinished()
    {
        return $this->date_finished;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return PayPeriod
     */
    public function setUserId($userId)
    {
        $this->user_id = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set fromBillingPeriod
     *
     * @param string $fromBillingPeriod
     *
     * @return PayPeriod
     */
    public function setFromBillingPeriod($fromBillingPeriod)
    {
        $this->from_billing_period = $fromBillingPeriod;

        return $this;
    }

    /**
     * Get fromBillingPeriod
     *
     * @return string
     */
    public function getFromBillingPeriod()
    {
        return $this->from_billing_period;
    }

    /**
     * Set type
     *
     * @param integer $type
     *
     * @return PayPeriod
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }

    public function getTypeString()
    {
        if($this->getType()==2){
            return "2 Weeks";
        }elseif($this->getType()==4){
            return "4 Weeks";
        }
        return "No Commission";
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return PayPeriod
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set user
     *
     * @param \Numa\CCCAdminBundle\Entity\User $user
     *
     * @return PayPeriod
     */
    public function setUser(\Numa\CCCAdminBundle\Entity\User $user = null)
    {
        $this->User = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Numa\CCCAdminBundle\Entity\User
     */
    public function getUser()
    {
        return $this->User;
    }
    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        $this->date_created = new \DateTime();
        $this->date_updated = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function setUpdatedAtValue()
    {
        $this->date_updated = new \DateTime();
    }
    /**
     * @var string
     * @JMS\Expose
     */
    private $wcb_rate = '0.00';

    /**
     * @var string
     * @JMS\Expose
     */
    private $ebc_deduction = '0.00';

    /**
     * @var \Numa\CCCAdminBundle\Entity\BillingPeriod
     */
    private $BillingPeriod;


    /**
     * Set wcbRate
     *
     * @param string $wcbRate
     *
     * @return PayPeriod
     */
    public function setWcbRate($wcbRate)
    {
        $this->wcb_rate = $wcbRate;

        return $this;
    }

    /**
     * Get wcbRate
     *
     * @return string
     */
    public function getWcbRate()
    {
        return $this->wcb_rate;
    }

    /**
     * Set ebcDeduction
     *
     * @param string $ebcDeduction
     *
     * @return PayPeriod
     */
    public function setEbcDeduction($ebcDeduction)
    {
        $this->ebc_deduction = $ebcDeduction;

        return $this;
    }

    /**
     * Get ebcDeduction
     *
     * @return string
     */
    public function getEbcDeduction()
    {
        return $this->ebc_deduction;
    }

    /**
     * Set billingPeriod
     *
     * @param \Numa\CCCAdminBundle\Entity\BillingPeriod $billingPeriod
     *
     * @return PayPeriod
     */
    public function setBillingPeriod(\Numa\CCCAdminBundle\Entity\BillingPeriod $billingPeriod = null)
    {
        $this->BillingPeriod = $billingPeriod;

        return $this;
    }

    /**
     * Get billingPeriod
     *
     * @return \Numa\CCCAdminBundle\Entity\BillingPeriod
     */
    public function getBillingPeriod()
    {
        return $this->BillingPeriod;
    }
    /**
     * @var integer
     */
    private $billing_period_id;


    /**
     * Set billingPeriodId
     *
     * @param integer $billingPeriodId
     *
     * @return PayPeriod
     */
    public function setBillingPeriodId($billingPeriodId)
    {
        $this->billing_period_id = $billingPeriodId;

        return $this;
    }

    /**
     * Get billingPeriodId
     *
     * @return integer
     */
    public function getBillingPeriodId()
    {
        return $this->billing_period_id;
    }
    /**
     * @var string
     */
    private $wcb_deduction = '0.00';


    /**
     * Set wcbDeduction
     *
     * @param string $wcbDeduction
     *
     * @return PayPeriod
     */
    public function setWcbDeduction($wcbDeduction)
    {
        $this->wcb_deduction = $wcbDeduction;

        return $this;
    }

    /**
     * Get wcbDeduction
     *
     * @return string
     */
    public function getWcbDeduction()
    {
        return $this->wcb_deduction;
    }
    /**
     * @var \DateTime
     */
    private $cheque_date;


    /**
     * Set chequeDate
     *
     * @param \DateTime $chequeDate
     *
     * @return PayPeriod
     */
    public function setChequeDate($chequeDate)
    {
        $this->cheque_date = $chequeDate;

        return $this;
    }

    /**
     * Get chequeDate
     *
     * @return \DateTime
     */
    public function getChequeDate()
    {
        return $this->cheque_date;
    }
}
