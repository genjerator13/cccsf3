<?php

namespace Numa\CCCAdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Numa\CCCAdminBundle\Lib\Report\CustomersAwareTrait;

/**
 * Email
 */
class Email
{
    use CustomersAwareTrait;
    /**
     * @var integer
     */
    private $id;


    /**
     * @var string
     */
    private $status;

    /**
     * @var string
     */
    private $body;

    /**
     * @var string
     */
    private $attachment;

    /**
     * @var string
     */
    private $subject;

    /**
     * @var string
     */
    private $email_from;

    /**
     * @var string
     */
    private $email_to;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }



    /**
     * Set status
     *
     * @param string $status
     * @return Email
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set body
     *
     * @param string $body
     * @return Email
     */
    public function setBody($body)
    {
        $this->body = $body;
    
        return $this;
    }

    /**
     * Get body
     *
     * @return string 
     */
    public function getBody()
    {
        return $this->body;
    }


    /**
     * Set subject
     *
     * @param string $subject
     * @return Email
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    
        return $this;
    }

    /**
     * Get subject
     *
     * @return string 
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set email_from
     *
     * @param string $emailFrom
     * @return Email
     */
    public function setEmailFrom($emailFrom)
    {
        $this->email_from = $emailFrom;
    
        return $this;
    }

    /**
     * Get email_from
     *
     * @return string 
     */
    public function getEmailFrom()
    {
        return $this->email_from;
    }

    /**
     * Set email_to
     *
     * @param string $emailTo
     * @return Email
     */
    public function setEmailTo($emailTo)
    {
        $this->email_to = $emailTo;
    
        return $this;
    }

    /**
     * Get email_to
     *
     * @return string 
     */
    public function getEmailTo()
    {
        return $this->email_to;
    }


    public function getAttachmentArray($base){
        $att = $this->attachment;
        $attArray = explode(";",$att);
        $res=array();
        if(!empty($att)) {
            foreach ($attArray as $attach) {
                $temp=array();
                $attachArray = explode("..", $attach);
                $attachment = $attachArray[1];
                //$attachment = str_replace("/web", "", $attachArray[1]);
                $temp['href'] = $base . $attachment;
                $temp['filename'] = "";
                $temp['extension'] = "";
                $fileArray = explode("/", $attachArray[1]);

                if(is_array($fileArray)){
                    $filename = $fileArray[count($fileArray)-1];
                    $temp['filename'] = $filename;

                    $filenameArray = explode(".", $filename);
                    if(is_array($filenameArray)){
                        $extension = $filenameArray[count($filenameArray)-1];
                        $temp['extension'] = $extension;
                    }
                }

                $res[] = $temp;
            }
        }

        return $res;

    }




    /**
     * @var \DateTime
     */
    private $date_created;

    /**
     * @var \DateTime
     */
    private $date_updated;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $Attachment;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->Attachment = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     *
     * @return Email
     */
    public function setDateCreated($dateCreated)
    {
        $this->date_created = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * Set dateUpdated
     *
     * @param \DateTime $dateUpdated
     *
     * @return Email
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->date_updated = $dateUpdated;

        return $this;
    }

    /**
     * Get dateUpdated
     *
     * @return \DateTime
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * Add attachment
     *
     * @param \Numa\CCCAdminBundle\Entity\Attachment $attachment
     *
     * @return Email
     */
    public function addAttachment(\Numa\CCCAdminBundle\Entity\Attachment $attachment)
    {
        $this->Attachment[] = $attachment;

        return $this;
    }

    /**
     * Remove attachment
     *
     * @param \Numa\CCCAdminBundle\Entity\Attachment $attachment
     */
    public function removeAttachment(\Numa\CCCAdminBundle\Entity\Attachment $attachment)
    {
        $this->Attachment->removeElement($attachment);
    }
    /**
     * @var integer
     */
    private $customer_id;

    /**
     * @var integer
     */
    private $billing_period_id;


    /**
     * Set customerId
     *
     * @param integer $customerId
     *
     * @return Email
     */
    public function setCustomerId($customerId)
    {
        $this->customer_id = $customerId;

        return $this;
    }

    /**
     * Get customerId
     *
     * @return integer
     */
    public function getCustomerId()
    {
        return $this->customer_id;
    }

    /**
     * Set billingPeriodId
     *
     * @param integer $billingPeriodId
     *
     * @return Email
     */
    public function setBillingPeriodId($billingPeriodId)
    {
        $this->billing_period_id = $billingPeriodId;

        return $this;
    }

    /**
     * Get billingPeriodId
     *
     * @return integer
     */
    public function getBillingPeriodId()
    {
        return $this->billing_period_id;
    }

    /**
     * Get attachment
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAttachment()
    {
        return $this->Attachment;
    }
    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        // Add your code here
    }

    /**
     * @ORM\PreUpdate
     */
    public function setUpdatedAtValue()
    {
        // Add your code here
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $CustomerReportEmail;


    /**
     * Add customerReportEmail
     *
     * @param \Numa\CCCAdminBundle\Entity\CustomerReportEmail $customerReportEmail
     *
     * @return Email
     */
    public function addCustomerReportEmail(\Numa\CCCAdminBundle\Entity\CustomerReportEmail $customerReportEmail)
    {
        $this->CustomerReportEmail[] = $customerReportEmail;

        return $this;
    }

    /**
     * Remove customerReportEmail
     *
     * @param \Numa\CCCAdminBundle\Entity\CustomerReportEmail $customerReportEmail
     */
    public function removeCustomerReportEmail(\Numa\CCCAdminBundle\Entity\CustomerReportEmail $customerReportEmail)
    {
        $this->CustomerReportEmail->removeElement($customerReportEmail);
    }

    /**
     * Get customerReportEmail
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCustomerReportEmail()
    {
        return $this->CustomerReportEmail;
    }

    public function __toString()
    {

        return $this->getSubject();
    }
}
