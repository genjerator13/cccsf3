<?php

namespace Numa\CCCAdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation as JMS;
/**
 * Cheque
 * @JMS\ExclusionPolicy("ALL")
 */
class Cheque
{
    /**
     * @var integer
     * @JMS\Expose
     */
    public  $id;

    /**
     * @var \DateTime
     * @JMS\Expose
     */
    private $cheque_date;

    /**
     * @var string
     * @JMS\Expose
     */
    private $ref_num;

    /**
     * @var string
     * @JMS\Expose
     */
    private $lump_sum_pay_amt = '0.00';

    /**
     * @var integer
     * @JMS\Expose
     */
    private $hours;

    /**
     * @var string
     * @JMS\Expose
     */
    private $hrate = '0.00';

    /**
     * @var integer
     * @JMS\Expose
     */
    private $htotal;

    /**
     * @var string
     * @JMS\Expose
     */
    private $mile_rate = '0.00';

    /**
     * @var integer
     * @JMS\Expose
     */
    private $miles;

    /**
     * @var integer
     * @JMS\Expose
     */
    private $mile_total;

    /**
     * @var string
     * @JMS\Expose
     */
    private $misc_pay_desc;

    /**
     * @var string
     * @JMS\Expose
     */
    private $misc_pay_amt = '0.00';

    /**
     * @var string
     * @JMS\Expose
     */
    private $gross_pay = '0.00';

    /**
     * @var string
     * @JMS\Expose
     */
    private $cpp = '0.00';

    /**
     * @var string
     * @JMS\Expose
     */
    private $social_fund = '0.00';

    /**
     * @var string
     * @JMS\Expose
     */
    private $advance = '0.00';

    /**
     * @var string
     * @JMS\Expose
     */
    private $advance_fee = '0.00';


    /**
     * @var string
     * @JMS\Expose
     */
    private $fuel_expences = '0.00';

    /**
     * @var string
     * @JMS\Expose
     */
    private $fuel_card;

    /**
     * @var string
     * @JMS\Expose
     */
    private $fuel_card_fee = '0.00';

    /**
     * @var string
     * @JMS\Expose
     */
    private $purchases = '0.00';

    /**
     * @var string
     * @JMS\Expose
     */
    private $purchases_ref;

    /**
     * @var string
     * @JMS\Expose
     */
    private $purchases_fee = '0.00';

    /**
     * @var string
     * @JMS\Expose
     */
    private $amount_in_word;

    /**
     * @var string
     * @JMS\Expose
     */
    private $check_num = '0.00';

    /**
     * @var string
     * @JMS\Expose
     */
    private $net_amount = '0.00';

    /**
     * @var string
     * @JMS\Expose
     */
    private $net_pay = '0.00';

    /**
     * @var string
     * @JMS\Expose
     */
    private $reason;

    /**
     * @var string
     * @JMS\Expose
     */
    private $deduc_total = '0.00';

    /**
     * @var string
     * @JMS\Expose
     */
    private $wcb = '0.00';

    /**
     * @var string
     * @JMS\Expose
     */
    private $advance_ref;

    /**
     * @var boolean
     * @JMS\Expose
     */
    private $pay_wbc = false;

    /**
     * @var string
     * @JMS\Expose
     */
    private $wcb_code;

    /**
     * @var string
     * @JMS\Expose
     */
    private $wcb_rate = '0.00';

    /**
     * @var string
     * @JMS\Expose
     */
    private $wcb_base = '0.00';

    /**
     * @var string
     * @JMS\Expose
     */
    private $cargo_insurance = '0.00';

    /**
     * @var string
     * @JMS\Expose
     */
    private $fuel_credits = '0.00';

    /**
     * @var string
     * @JMS\Expose
     */
    private $city_prov_postal;

    /**
     * @var string
     * @JMS\Expose
     */
    private $print_status;

    /**
     * @var string
     * @JMS\Expose
     */
    private $misc1 = '0.00';

    /**
     * @var string
     * @JMS\Expose
     */
    private $misc2 = '0.00';

    /**
     * @var string
     * @JMS\Expose
     */
    private $misc3 = '0.00';

    /**
     * @var string
     * @JMS\Expose
     */
    private $misc4 = '0.00';

    /**
     * @var string
     * @JMS\Expose
     */
    private $misc1_description;

    /**
     * @var string
     * @JMS\Expose
     */
    private $misc2_description;

    /**
     * @var string
     * @JMS\Expose
     */
    private $misc3_description;

    /**
     * @var string
     * @JMS\Expose
     */
    private $misc4_description;

    /**
     * @var string
     * @JMS\Expose
     */
    private $misc1_GST = '0.00';

    /**
     * @var string
     * @JMS\Expose
     */
    private $misc2_GST = '0.00';

    /**
     * @var string
     * @JMS\Expose
     */
    private $misc3_GST = '0.00';

    /**
     * @var string
     * @JMS\Expose
     */
    private $misc4_GST = '0.00';

    /**
     * @var string
     * @JMS\Expose
     */
    private $misc1_total = '0.00';

    /**
     * @var string
     * @JMS\Expose
     */
    private $misc2_total = '0.00';

    /**
     * @var string
     * @JMS\Expose
     */
    private $misc3_total = '0.00';

    /**
     * @var string
     * @JMS\Expose
     */
    private $misc4_total = '0.00';

    /**
     * @var string
     * @JMS\Expose
     */
    private $fuel_gst = '0.00';

    /**
     * @var string
     * @JMS\Expose
     */
    private $fuel_total = '0.00';

    /**
     * @var \Numa\CCCAdminBundle\Entity\Drivers
     */
    private $Driver;

    /**
     * @var \Numa\CCCAdminBundle\Entity\PayCode

     */
    private $PayCode;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set chequeDate
     *
     * @param \DateTime $chequeDate
     *
     * @return Cheque
     */
    public function setChequeDate($chequeDate)
    {
        $this->cheque_date = $chequeDate;

        return $this;
    }

    /**
     * Get chequeDate
     *
     * @return \DateTime
     */
    public function getChequeDate()
    {
        return $this->cheque_date;
    }

    /**
     * Set refNum
     *
     * @param string $refNum
     *
     * @return Cheque
     */
    public function setRefNum($refNum)
    {
        $this->ref_num = $refNum;

        return $this;
    }

    /**
     * Get refNum
     *
     * @return string
     */
    public function getRefNum()
    {
        return $this->ref_num;
    }

    /**
     * Set lumpSumPayAmt
     *
     * @param string $lumpSumPayAmt
     *
     * @return Cheque
     */
    public function setLumpSumPayAmt($lumpSumPayAmt)
    {
        $this->lump_sum_pay_amt = $lumpSumPayAmt;

        return $this;
    }

    /**
     * Get lumpSumPayAmt
     *
     * @return string
     */
    public function getLumpSumPayAmt()
    {
        return $this->lump_sum_pay_amt;
    }

    /**
     * Set hours
     *
     * @param integer $hours
     *
     * @return Cheque
     */
    public function setHours($hours)
    {
        $this->hours = $hours;

        return $this;
    }

    /**
     * Get hours
     *
     * @return integer
     */
    public function getHours()
    {
        return $this->hours;
    }

    /**
     * Set hrate
     *
     * @param string $hrate
     *
     * @return Cheque
     */
    public function setHrate($hrate)
    {
        $this->hrate = $hrate;

        return $this;
    }

    /**
     * Get hrate
     *
     * @return string
     */
    public function getHrate()
    {
        return $this->hrate;
    }

    /**
     * Set htotal
     *
     * @param integer $htotal
     *
     * @return Cheque
     */
    public function setHtotal($htotal)
    {
        $this->htotal = $htotal;

        return $this;
    }

    /**
     * Get htotal
     *
     * @return integer
     */
    public function getHtotal()
    {
        return $this->htotal;
    }

    /**
     * Set mileRate
     *
     * @param string $mileRate
     *
     * @return Cheque
     */
    public function setMileRate($mileRate)
    {
        $this->mile_rate = $mileRate;

        return $this;
    }

    /**
     * Get mileRate
     *
     * @return string
     */
    public function getMileRate()
    {
        return $this->mile_rate;
    }

    /**
     * Set miles
     *
     * @param integer $miles
     *
     * @return Cheque
     */
    public function setMiles($miles)
    {
        $this->miles = $miles;

        return $this;
    }

    /**
     * Get miles
     *
     * @return integer
     */
    public function getMiles()
    {
        return $this->miles;
    }

    /**
     * Set mileTotal
     *
     * @param integer $mileTotal
     *
     * @return Cheque
     */
    public function setMileTotal($mileTotal)
    {
        $this->mile_total = $mileTotal;

        return $this;
    }

    /**
     * Get mileTotal
     *
     * @return integer
     */
    public function getMileTotal()
    {
        return $this->mile_total;
    }

    /**
     * Set miscPayDesc
     *
     * @param string $miscPayDesc
     *
     * @return Cheque
     */
    public function setMiscPayDesc($miscPayDesc)
    {
        $this->misc_pay_desc = $miscPayDesc;

        return $this;
    }

    /**
     * Get miscPayDesc
     *
     * @return string
     */
    public function getMiscPayDesc()
    {
        return $this->misc_pay_desc;
    }

    /**
     * Set miscPayAmt
     *
     * @param string $miscPayAmt
     *
     * @return Cheque
     */
    public function setMiscPayAmt($miscPayAmt)
    {
        $this->misc_pay_amt = $miscPayAmt;

        return $this;
    }

    /**
     * Get miscPayAmt
     *
     * @return string
     */
    public function getMiscPayAmt()
    {
        return $this->misc_pay_amt;
    }

    /**
     * Set grossPay
     *
     * @param string $grossPay
     *
     * @return Cheque
     */
    public function setGrossPay($grossPay)
    {
        $this->gross_pay = $grossPay;

        return $this;
    }

    /**
     * Get grossPay
     *
     * @return string
     */
    public function getGrossPay()
    {
        return $this->gross_pay;
    }

    /**
     * Set cpp
     *
     * @param string $cpp
     *
     * @return Cheque
     */
    public function setCpp($cpp)
    {
        $this->cpp = $cpp;

        return $this;
    }

    /**
     * Get cpp
     *
     * @return string
     */
    public function getCpp()
    {
        return $this->cpp;
    }

    /**
     * Set socialFund
     *
     * @param string $socialFund
     *
     * @return Cheque
     */
    public function setSocialFund($socialFund)
    {
        $this->social_fund = $socialFund;

        return $this;
    }

    /**
     * Get socialFund
     *
     * @return string
     */
    public function getSocialFund()
    {
        return $this->social_fund;
    }

    /**
     * Set advance
     *
     * @param string $advance
     *
     * @return Cheque
     */
    public function setAdvance($advance)
    {
        $this->advance = $advance;

        return $this;
    }

    /**
     * Get advance
     *
     * @return string
     */
    public function getAdvance()
    {
        return $this->advance;
    }

    /**
     * Set advanceFee
     *
     * @param string $advanceFee
     *
     * @return Cheque
     */
    public function setAdvanceFee($advanceFee)
    {
        $this->advance_fee = $advanceFee;

        return $this;
    }

    /**
     * Get advanceFee
     *
     * @return string
     */
    public function getAdvanceFee()
    {
        return $this->advance_fee;
    }

    /**
     * Set fuelExpences
     *
     * @param string $fuelExpences
     *
     * @return Cheque
     */
    public function setFuelExpences($fuelExpences)
    {
        $this->fuel_expences = $fuelExpences;

        return $this;
    }

    /**
     * Get fuelExpences
     *
     * @return string
     */
    public function getFuelExpences()
    {
        return $this->fuel_expences;
    }

    /**
     * Set fuelCard
     *
     * @param string $fuelCard
     *
     * @return Cheque
     */
    public function setFuelCard($fuelCard)
    {
        $this->fuel_card = $fuelCard;

        return $this;
    }

    /**
     * Get fuelCard
     *
     * @return string
     */
    public function getFuelCard()
    {
        return $this->fuel_card;
    }

    /**
     * Set fuelCardFee
     *
     * @param string $fuelCardFee
     *
     * @return Cheque
     */
    public function setFuelCardFee($fuelCardFee)
    {
        $this->fuel_card_fee = $fuelCardFee;

        return $this;
    }

    /**
     * Get fuelCardFee
     *
     * @return string
     */
    public function getFuelCardFee()
    {
        return $this->fuel_card_fee;
    }

    /**
     * Set purchases
     *
     * @param string $purchases
     *
     * @return Cheque
     */
    public function setPurchases($purchases)
    {
        $this->purchases = $purchases;

        return $this;
    }

    /**
     * Get purchases
     *
     * @return string
     */
    public function getPurchases()
    {
        return $this->purchases;
    }

    /**
     * Set purchasesRef
     *
     * @param string $purchasesRef
     *
     * @return Cheque
     */
    public function setPurchasesRef($purchasesRef)
    {
        $this->purchases_ref = $purchasesRef;

        return $this;
    }

    /**
     * Get purchasesRef
     *
     * @return string
     */
    public function getPurchasesRef()
    {
        return $this->purchases_ref;
    }

    /**
     * Set purchasesFee
     *
     * @param string $purchasesFee
     *
     * @return Cheque
     */
    public function setPurchasesFee($purchasesFee)
    {
        $this->purchases_fee = $purchasesFee;

        return $this;
    }

    /**
     * Get purchasesFee
     *
     * @return string
     */
    public function getPurchasesFee()
    {
        return $this->purchases_fee;
    }

    /**
     * Set amountInWord
     *
     * @param string $amountInWord
     *
     * @return Cheque
     */
    public function setAmountInWord($amountInWord)
    {
        $this->amount_in_word = $amountInWord;

        return $this;
    }

    /**
     * Get amountInWord
     *
     * @return string
     */
    public function getAmountInWord()
    {
        return $this->amount_in_word;
    }

    /**
     * Set checkNum
     *
     * @param string $checkNum
     *
     * @return Cheque
     */
    public function setCheckNum($checkNum)
    {
        $this->check_num = $checkNum;

        return $this;
    }

    /**
     * Get checkNum
     *
     * @return string
     */
    public function getCheckNum()
    {
        return $this->check_num;
    }

    /**
     * Set netAmount
     *
     * @param string $netAmount
     *
     * @return Cheque
     */
    public function setNetAmount($netAmount)
    {
        $this->net_amount = $netAmount;

        return $this;
    }

    /**
     * Get netAmount
     *
     * @return string
     */
    public function getNetAmount()
    {
        return $this->net_amount;
    }

    /**
     * Set netPay
     *
     * @param string $netPay
     *
     * @return Cheque
     */
    public function setNetPay($netPay)
    {
        $this->net_pay = $netPay;

        return $this;
    }

    /**
     * Get netPay
     *
     * @return string
     */
    public function getNetPay()
    {
        return $this->net_pay;
    }

    /**
     * Set reason
     *
     * @param string $reason
     *
     * @return Cheque
     */
    public function setReason($reason)
    {
        $this->reason = $reason;

        return $this;
    }

    /**
     * Get reason
     *
     * @return string
     */
    public function getReason()
    {
        return $this->reason;
    }

    /**
     * Set deducTotal
     *
     * @param string $deducTotal
     *
     * @return Cheque
     */
    public function setDeducTotal($deducTotal)
    {
        $this->deduc_total = $deducTotal;

        return $this;
    }

    /**
     * Get deducTotal
     *
     * @return string
     */
    public function getDeducTotal()
    {
        return $this->deduc_total;
    }

    /**
     * Set wcb
     *
     * @param string $wcb
     *
     * @return Cheque
     */
    public function setWcb($wcb)
    {
        $this->wcb = $wcb;

        return $this;
    }

    /**
     * Get wcb
     *
     * @return string
     */
    public function getWcb()
    {
        return $this->wcb;
    }

    /**
     * Set advanceRef
     *
     * @param string $advanceRef
     *
     * @return Cheque
     */
    public function setAdvanceRef($advanceRef)
    {
        $this->advance_ref = $advanceRef;

        return $this;
    }

    /**
     * Get advanceRef
     *
     * @return string
     */
    public function getAdvanceRef()
    {
        return $this->advance_ref;
    }

    /**
     * Set payWbc
     *
     * @param boolean $payWbc
     *
     * @return Cheque
     */
    public function setPayWbc($payWbc)
    {
        $this->pay_wbc = $payWbc;

        return $this;
    }

    /**
     * Get payWbc
     *
     * @return boolean
     */
    public function getPayWbc()
    {
        return $this->pay_wbc;
    }

    /**
     * Set wcbCode
     *
     * @param string $wcbCode
     *
     * @return Cheque
     */
    public function setWcbCode($wcbCode)
    {
        $this->wcb_code = $wcbCode;

        return $this;
    }

    /**
     * Get wcbCode
     *
     * @return string
     */
    public function getWcbCode()
    {
        return $this->wcb_code;
    }

    /**
     * Set wcbRate
     *
     * @param string $wcbRate
     *
     * @return Cheque
     */
    public function setWcbRate($wcbRate)
    {
        $this->wcb_rate = $wcbRate;

        return $this;
    }

    /**
     * Get wcbRate
     *
     * @return string
     */
    public function getWcbRate()
    {
        return $this->wcb_rate;
    }

    /**
     * Set wcbBase
     *
     * @param string $wcbBase
     *
     * @return Cheque
     */
    public function setWcbBase($wcbBase)
    {
        $this->wcb_base = $wcbBase;

        return $this;
    }

    /**
     * Get wcbBase
     *
     * @return string
     */
    public function getWcbBase()
    {
        return $this->wcb_base;
    }

    /**
     * Set cargoInsurance
     *
     * @param string $cargoInsurance
     *
     * @return Cheque
     */
    public function setCargoInsurance($cargoInsurance)
    {
        $this->cargo_insurance = $cargoInsurance;

        return $this;
    }

    /**
     * Get cargoInsurance
     *
     * @return string
     */
    public function getCargoInsurance()
    {
        return $this->cargo_insurance;
    }

    /**
     * Set fuelCredits
     *
     * @param string $fuelCredits
     *
     * @return Cheque
     */
    public function setFuelCredits($fuelCredits)
    {
        $this->fuel_credits = $fuelCredits;

        return $this;
    }

    /**
     * Get fuelCredits
     *
     * @return string
     */
    public function getFuelCredits()
    {
        return $this->fuel_credits;
    }

    /**
     * Set cityProvPostal
     *
     * @param string $cityProvPostal
     *
     * @return Cheque
     */
    public function setCityProvPostal($cityProvPostal)
    {
        $this->city_prov_postal = $cityProvPostal;

        return $this;
    }

    /**
     * Get cityProvPostal
     *
     * @return string
     */
    public function getCityProvPostal()
    {
        return $this->city_prov_postal;
    }

    /**
     * Set printStatus
     *
     * @param string $printStatus
     *
     * @return Cheque
     */
    public function setPrintStatus($printStatus)
    {
        $this->print_status = $printStatus;

        return $this;
    }

    /**
     * Get printStatus
     *
     * @return string
     */
    public function getPrintStatus()
    {
        return $this->print_status;
    }

    /**
     * Set misc1
     *
     * @param string $misc1
     *
     * @return Cheque
     */
    public function setMisc1($misc1)
    {
        $this->misc1 = $misc1;

        return $this;
    }

    /**
     * Get misc1
     *
     * @return string
     */
    public function getMisc1()
    {
        return $this->misc1;
    }

    /**
     * Set misc2
     *
     * @param string $misc2
     *
     * @return Cheque
     */
    public function setMisc2($misc2)
    {
        $this->misc2 = $misc2;

        return $this;
    }

    /**
     * Get misc2
     *
     * @return string
     */
    public function getMisc2()
    {
        return $this->misc2;
    }

    /**
     * Set misc3
     *
     * @param string $misc3
     *
     * @return Cheque
     */
    public function setMisc3($misc3)
    {
        $this->misc3 = $misc3;

        return $this;
    }

    /**
     * Get misc3
     *
     * @return string
     */
    public function getMisc3()
    {
        return $this->misc3;
    }

    /**
     * Set misc4
     *
     * @param string $misc4
     *
     * @return Cheque
     */
    public function setMisc4($misc4)
    {
        $this->misc4 = $misc4;

        return $this;
    }

    /**
     * Get misc4
     *
     * @return string
     */
    public function getMisc4()
    {
        return $this->misc4;
    }

    /**
     * Set misc1Description
     *
     * @param string $misc1Description
     *
     * @return Cheque
     */
    public function setMisc1Description($misc1Description)
    {
        $this->misc1_description = $misc1Description;

        return $this;
    }

    /**
     * Get misc1Description
     *
     * @return string
     */
    public function getMisc1Description()
    {
        return $this->misc1_description;
    }

    /**
     * Set misc2Description
     *
     * @param string $misc2Description
     *
     * @return Cheque
     */
    public function setMisc2Description($misc2Description)
    {
        $this->misc2_description = $misc2Description;

        return $this;
    }

    /**
     * Get misc2Description
     *
     * @return string
     */
    public function getMisc2Description()
    {
        return $this->misc2_description;
    }

    /**
     * Set misc3Description
     *
     * @param string $misc3Description
     *
     * @return Cheque
     */
    public function setMisc3Description($misc3Description)
    {
        $this->misc3_description = $misc3Description;

        return $this;
    }

    /**
     * Get misc3Description
     *
     * @return string
     */
    public function getMisc3Description()
    {
        return $this->misc3_description;
    }

    /**
     * Set misc4Description
     *
     * @param string $misc4Description
     *
     * @return Cheque
     */
    public function setMisc4Description($misc4Description)
    {
        $this->misc4_description = $misc4Description;

        return $this;
    }

    /**
     * Get misc4Description
     *
     * @return string
     */
    public function getMisc4Description()
    {
        return $this->misc4_description;
    }

    /**
     * Set misc1GST
     *
     * @param string $misc1GST
     *
     * @return Cheque
     */
    public function setMisc1GST($misc1GST)
    {
        $this->misc1_GST = $misc1GST;

        return $this;
    }

    /**
     * Get misc1GST
     *
     * @return string
     */
    public function getMisc1GST()
    {
        return $this->misc1_GST;
    }

    /**
     * Set misc2GST
     *
     * @param string $misc2GST
     *
     * @return Cheque
     */
    public function setMisc2GST($misc2GST)
    {
        $this->misc2_GST = $misc2GST;

        return $this;
    }

    /**
     * Get misc2GST
     *
     * @return string
     */
    public function getMisc2GST()
    {
        return $this->misc2_GST;
    }

    /**
     * Set misc3GST
     *
     * @param string $misc3GST
     *
     * @return Cheque
     */
    public function setMisc3GST($misc3GST)
    {
        $this->misc3_GST = $misc3GST;

        return $this;
    }

    /**
     * Get misc3GST
     *
     * @return string
     */
    public function getMisc3GST()
    {
        return $this->misc3_GST;
    }

    /**
     * Set misc4GST
     *
     * @param string $misc4GST
     *
     * @return Cheque
     */
    public function setMisc4GST($misc4GST)
    {
        $this->misc4_GST = $misc4GST;

        return $this;
    }

    /**
     * Get misc4GST
     *
     * @return string
     */
    public function getMisc4GST()
    {
        return $this->misc4_GST;
    }

    /**
     * Set misc1Total
     *
     * @param string $misc1Total
     *
     * @return Cheque
     */
    public function setMisc1Total($misc1Total)
    {
        $this->misc1_total = $misc1Total;

        return $this;
    }

    /**
     * Get misc1Total
     *
     * @return string
     */
    public function getMisc1Total()
    {
        return $this->misc1_total;
    }

    /**
     * Set misc2Total
     *
     * @param string $misc2Total
     *
     * @return Cheque
     */
    public function setMisc2Total($misc2Total)
    {
        $this->misc2_total = $misc2Total;

        return $this;
    }

    /**
     * Get misc2Total
     *
     * @return string
     */
    public function getMisc2Total()
    {
        return $this->misc2_total;
    }

    /**
     * Set misc3Total
     *
     * @param string $misc3Total
     *
     * @return Cheque
     */
    public function setMisc3Total($misc3Total)
    {
        $this->misc3_total = $misc3Total;

        return $this;
    }

    /**
     * Get misc3Total
     *
     * @return string
     */
    public function getMisc3Total()
    {
        return $this->misc3_total;
    }

    /**
     * Set misc4Total
     *
     * @param string $misc4Total
     *
     * @return Cheque
     */
    public function setMisc4Total($misc4Total)
    {
        $this->misc4_total = $misc4Total;

        return $this;
    }

    /**
     * Get misc4Total
     *
     * @return string
     */
    public function getMisc4Total()
    {
        return $this->misc4_total;
    }

    /**
     * Set fuelGst
     *
     * @param string $fuelGst
     *
     * @return Cheque
     */
    public function setFuelGst($fuelGst)
    {
        $this->fuel_gst = $fuelGst;

        return $this;
    }

    /**
     * Get fuelGst
     *
     * @return string
     */
    public function getFuelGst()
    {
        return $this->fuel_gst;
    }

    /**
     * Set fuelTotal
     *
     * @param string $fuelTotal
     *
     * @return Cheque
     */
    public function setFuelTotal($fuelTotal)
    {
        $this->fuel_total = $fuelTotal;

        return $this;
    }

    /**
     * Get fuelTotal
     *
     * @return string
     */
    public function getFuelTotal()
    {
        return $this->fuel_total;
    }

    /**
     * Set driver
     *
     * @param \Numa\CCCAdminBundle\Entity\Drivers $driver
     *
     * @return Cheque
     */
    public function setDriver(\Numa\CCCAdminBundle\Entity\Drivers $driver = null)
    {
        $this->Driver = $driver;

        return $this;
    }

    /**
     * Get driver
     *
     * @return \Numa\CCCAdminBundle\Entity\Drivers
     */
    public function getDriver()
    {
        return $this->Driver;
    }

    /**
     * Set payCode
     *
     * @param \Numa\CCCAdminBundle\Entity\PayCode $payCode
     *
     * @return Cheque
     */
    public function setPayCode(\Numa\CCCAdminBundle\Entity\PayCode $payCode = null)
    {
        $this->PayCode = $payCode;

        return $this;
    }

    /**
     * Get payCode
     *
     * @return \Numa\CCCAdminBundle\Entity\PayCode
     */
    public function getPayCode()
    {
        return $this->PayCode;
    }
    /**
     * @var \Numa\CCCAdminBundle\Entity\BillingPeriod
     */
    private $BillingPeriod;


    /**
     * Set billingPeriod
     *
     * @param \Numa\CCCAdminBundle\Entity\BillingPeriod $billingPeriod
     *
     * @return Cheque
     */
    public function setBillingPeriod(\Numa\CCCAdminBundle\Entity\BillingPeriod $billingPeriod = null)
    {
        $this->BillingPeriod = $billingPeriod;

        return $this;
    }

    /**
     * Get billingPeriod
     *
     * @return \Numa\CCCAdminBundle\Entity\BillingPeriod
     */
    public function getBillingPeriod()
    {
        return $this->BillingPeriod;
    }
    /**
     * @var string
     * @JMS\Expose
     */
    private $contract = '0.00';


    /**
     * Set contract
     *
     * @param string $contract
     *
     * @return Cheque
     */
    public function setContract($contract)
    {
        $this->contract = $contract;

        return $this;
    }

    /**
     * Get contract
     *
     * @return string
     */
    public function getContract()
    {
        return $this->contract;
    }
    /**
     * @var \DateTime
     */
    private $date_create;

    /**
     * @var \DateTime
     */
    private $date_update;


    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return Cheque
     */
    public function setDateCreate($dateCreate)
    {
        $this->date_create = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->date_create;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return Cheque
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->date_update = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->date_update;
    }
    /**
     * @ORM\PrePersist
     */
    public function setDateCreated()
    {

        if (!$this->getDateUpdate()) {
            $this->date_update = new \DateTime();
        }
        if (!$this->getDateCreate()) {
            $this->date_create = new \DateTime();
        }

    }

    /**
     * @ORM\PreUpdate
     */
    public function setDateUpdated()
    {
        if (!$this->getDateUpdate()) {
            $this->date_update = new \DateTime();
        }
    }


    /**
     * @var integer
     */
    private $dealer_id;

    /**
     * @var integer
     */
    private $paycode_id;

    /**
     * @var integer
     */
    private $billing_period_id;


    /**
     * Set dealerId
     *
     * @param integer $dealerId
     *
     * @return Cheque
     */
    public function setDealerId($dealerId)
    {
        $this->dealer_id = $dealerId;

        return $this;
    }

    /**
     * Get dealerId
     *
     * @return integer
     */
    public function getDealerId()
    {
        return $this->dealer_id;
    }

    /**
     * Set paycodeId
     *
     * @param integer $paycodeId
     *
     * @return Cheque
     */
    public function setPaycodeId($paycodeId)
    {
        $this->paycode_id = $paycodeId;

        return $this;
    }

    /**
     * Get paycodeId
     *
     * @return integer
     */
    public function getPaycodeId()
    {
        return $this->paycode_id;
    }

    /**
     * Set billingPeriodId
     *
     * @param integer $billingPeriodId
     *
     * @return Cheque
     */
    public function setBillingPeriodId($billingPeriodId)
    {
        $this->billing_period_id = $billingPeriodId;

        return $this;
    }

    /**
     * Get billingPeriodId
     *
     * @return integer
     */
    public function getBillingPeriodId()
    {
        return $this->billing_period_id;
    }
    /**
     * @var integer
     * @JMS\Expose
     */
    private $pay_period_id;

    /**
     * @var \Numa\CCCAdminBundle\Entity\PayPeriod

     */
    private $PayPeriod;


    /**
     * Set payPeriodId
     *
     * @param integer $payPeriodId
     *
     * @return Cheque
     */
    public function setPayPeriodId($payPeriodId)
    {
        $this->pay_period_id = $payPeriodId;

        return $this;
    }

    /**
     * Get payPeriodId
     *
     * @return integer
     */
    public function getPayPeriodId()
    {
        return $this->pay_period_id;
    }

    /**
     * Set payPeriod
     *
     * @param \Numa\CCCAdminBundle\Entity\PayPeriod $payPeriod
     *
     * @return Cheque
     */
    public function setPayPeriod(\Numa\CCCAdminBundle\Entity\PayPeriod $payPeriod = null)
    {
        $this->PayPeriod = $payPeriod;

        return $this;
    }

    /**
     * Get payPeriod
     *
     * @return \Numa\CCCAdminBundle\Entity\PayPeriod
     */
    public function getPayPeriod()
    {
        return $this->PayPeriod;
    }
    /**
     * @var integer
     */
    private $user_id;

    /**
     * @var integer
     */
    private $driver_id;

    /**
     * @var \Numa\CCCAdminBundle\Entity\User
     */
    private $User;


    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return Cheque
     */
    public function setUserId($userId)
    {
        $this->user_id = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set driverId
     *
     * @param integer $driverId
     *
     * @return Cheque
     */
    public function setDriverId($driverId)
    {
        $this->driver_id = $driverId;

        return $this;
    }

    /**
     * Get driverId
     *
     * @return integer
     */
    public function getDriverId()
    {
        return $this->driver_id;
    }

    /**
     * Set user
     *
     * @param \Numa\CCCAdminBundle\Entity\User $user
     *
     * @return Cheque
     */
    public function setUser(\Numa\CCCAdminBundle\Entity\User $user = null)
    {
        $this->User = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Numa\CCCAdminBundle\Entity\User
     */
    public function getUser()
    {
        return $this->User;
    }

    /**
     * @return string
     * @JMS\VirtualProperty
     * @JMS\Expose
     */
    public function driverPid(){
        return $this->driver_id;
    }

    /**
     * @return string
     * @JMS\VirtualProperty
     * @JMS\Expose
     */
    public function paycodePid(){
        return $this->paycode_id;
    }
    /**
     * @var string
     */
    private $wcb_deduction = '0.00';


    /**
     * Set wcbDeduction
     *
     * @param string $wcbDeduction
     *
     * @return Cheque
     */
    public function setWcbDeduction($wcbDeduction)
    {
        $this->wcb_deduction = $wcbDeduction;

        return $this;
    }

    /**
     * Get wcbDeduction
     *
     * @return string
     */
    public function getWcbDeduction()
    {
        return $this->wcb_deduction;
    }

    /**
     * @return string
     * @JMS\VirtualProperty
     */
    public function driverName(){
        $driver =$this->getDriver();

        if($driver instanceof Drivers){
            return $driver->getDrivernum()." - ".$driver->getDrivernam();
        }
        return $this->getDriverCode();
    }

    /**
     * @return string
     * @JMS\VirtualProperty
     */
    public function cdate(){

        return $this->cheque_date->format("Y-n-d");
    }
}
