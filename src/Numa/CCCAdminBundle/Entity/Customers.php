<?php

namespace Numa\CCCAdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\XmlRoot;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation as JMS;
/**
 * Customers
 * @JMS\ExclusionPolicy("ALL")
 */

class Customers implements UserInterface{

    /**
     * @var integer
     * @JMS\Expose
     */
    private $id;

    /**
     * @var string
     * @JMS\Expose
     */
    private $custcode;

    /**
     * @var string
     * @JMS\Expose
     */
    private $name;

    /**
     * @var string
     * @JMS\Expose
     */
    private $address1;

    /**
     * @var string
     * @JMS\Expose
     */
    private $address2;

    /**
     * @var string
     * @JMS\Expose
     */
    private $city;

    /**
     * @var string
     * @JMS\Expose
     */
    private $prov;

    /**
     * @var string
     * @JMS\Expose
     */
    private $postal;

    /**
     * @var string
     * @JMS\Expose
     */
    private $phone;

    /**
     * @var string
     * @JMS\Expose
     */
    private $fax;

    /**
     * @var integer
     * @JMS\Expose
     */
    private $duedays;

    /**
     * @var \DateTime
     * @JMS\Expose
     */
    private $lastpay;

    /**
     * @var \DateTime
     * @JMS\Expose
     */
    private $lastpur;

    /**
     * @var float
     * @JMS\Expose
     */
    private $totbalan;

    /**
     * @var string
     * @JMS\Expose
     */
    private $comments;

    /**
     * @var string
     * @JMS\Expose
     */
    private $taxcode;

    /**
     * @var float
     * @JMS\Expose
     */
    private $custsurchargerate;

    /**
     * @var string
     * @JMS\Expose
     */
    private $note;

    /**
     * @var float
     * @JMS\Expose
     */
    private $zerodays;

    /**
     * @var float
     * @JMS\Expose
     */
    private $thirtydays;

    /**
     * @var float
     * @JMS\Expose
     */
    private $sixtydays;

    /**
     * @var float
     * @JMS\Expose
     */
    private $ninetydays;

    /**
     * @var float
     * @JMS\Expose
     */
    private $discount;

    /**
     * @var string
     * @JMS\Expose
     */
    private $ratelevel;

    /**
     * @var string
     * @JMS\Expose
     */
    private $email;

    /**
     * @var string
     * @JMS\Expose
     */
    private $website;

    /**
     * @var string
     * @JMS\Expose
     */
    private $cell;

    /**
     * @var string
     * @JMS\Expose
     */
    private $cityprovpostal;

    /**
     * @var string
     * @JMS\Expose
     */
    private $addressblock;

    /**
     * @var \DateTime
     * @JMS\Expose
     */
    private $revised;

    /**
     * @var string
     * @JMS\Expose
     */
    private $status;

    /**
     * @var string
     * @JMS\Expose
     */
    private $sendmail;

    /**
     * @var string
     * @JMS\Expose
     */
    private $contact;

    /**
     * @var string
     * @JMS\Expose
     */
    private $username;

    /**
     * @var string
     * @JMS\Expose
     */
    private $password;

    /**
     * @var integer
     */
    private $terminalid;

    /**
     * @var boolean
     * @JMS\Expose
     */
    private $israteoverride;

    /**
     * @var boolean
     * @JMS\Expose
     */
    private $ishwyrateoverride;

    /**
     * @var float
     * @JMS\Expose
     */
    private $custhwysurchargerate;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $probills;

    /**
     * Constructor
     */
    public function __construct() {
        $this->probills = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function set($fieldname, $value) {
        $fieldname = strtolower($fieldname);
        $this->$fieldname = $value;

        //check if date

        if (preg_match("/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/", $value)) {
            $this->$fieldname = new \DateTime($value);
        }
        if (preg_match("/^[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}$/", $value)) {

            $this->$fieldname = new \DateTime($value);
        }
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set custcode
     *
     * @param string $custcode
     * @return Customers
     */
    public function setCustcode($custcode) {
        $this->custcode = $custcode;

        return $this;
    }

    /**
     * Get custcode
     *
     * @return string 
     */
    public function getCustcode() {
        return $this->custcode;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Customers
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set address1
     *
     * @param string $address1
     * @return Customers
     */
    public function setAddress1($address1) {
        $this->address1 = $address1;

        return $this;
    }

    /**
     * Get address1
     *
     * @return string 
     */
    public function getAddress1() {
        return $this->address1;
    }

    /**
     * Set address2
     *
     * @param string $address2
     * @return Customers
     */
    public function setAddress2($address2) {
        $this->address2 = $address2;

        return $this;
    }

    /**
     * Get address2
     *
     * @return string 
     */
    public function getAddress2() {
        return $this->address2;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return Customers
     */
    public function setCity($city) {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string 
     */
    public function getCity() {
        return $this->city;
    }

    /**
     * Set prov
     *
     * @param string $prov
     * @return Customers
     */
    public function setProv($prov) {
        $this->prov = $prov;

        return $this;
    }

    /**
     * Get prov
     *
     * @return string 
     */
    public function getProv() {
        return $this->prov;
    }

    /**
     * Set postal
     *
     * @param string $postal
     * @return Customers
     */
    public function setPostal($postal) {
        $this->postal = $postal;

        return $this;
    }

    /**
     * Get postal
     *
     * @return string 
     */
    public function getPostal() {
        return $this->postal;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return Customers
     */
    public function setPhone($phone) {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone() {
        return $this->phone;
    }

    /**
     * Set fax
     *
     * @param string $fax
     * @return Customers
     */
    public function setFax($fax) {
        $this->fax = $fax;

        return $this;
    }

    /**
     * Get fax
     *
     * @return string 
     */
    public function getFax() {
        return $this->fax;
    }

    /**
     * Set duedays
     *
     * @param integer $duedays
     * @return Customers
     */
    public function setDuedays($duedays) {
        $this->duedays = $duedays;

        return $this;
    }

    /**
     * Get duedays
     *
     * @return integer 
     */
    public function getDuedays() {
        return $this->duedays;
    }

    /**
     * Set lastpay
     *
     * @param \DateTime $lastpay
     * @return Customers
     */
    public function setLastpay($lastpay) {
        $this->lastpay = $lastpay;

        return $this;
    }

    /**
     * Get lastpay
     *
     * @return \DateTime 
     */
    public function getLastpay() {
        return $this->lastpay;
    }

    /**
     * Set lastpur
     *
     * @param \DateTime $lastpur
     * @return Customers
     */
    public function setLastpur($lastpur) {
        $this->lastpur = $lastpur;

        return $this;
    }

    /**
     * Get lastpur
     *
     * @return \DateTime 
     */
    public function getLastpur() {
        return $this->lastpur;
    }

    /**
     * Set totbalan
     *
     * @param float $totbalan
     * @return Customers
     */
    public function setTotbalan($totbalan) {
        $this->totbalan = $totbalan;

        return $this;
    }

    /**
     * Get totbalan
     *
     * @return float 
     */
    public function getTotbalan() {
        return $this->totbalan;
    }

    /**
     * Set comments
     *
     * @param string $comments
     * @return Customers
     */
    public function setComments($comments) {
        $this->comments = $comments;

        return $this;
    }

    /**
     * Get comments
     *
     * @return string 
     */
    public function getComments() {
        return $this->comments;
    }

    /**
     * Set taxcode
     *
     * @param string $taxcode
     * @return Customers
     */
    public function setTaxcode($taxcode) {
        $this->taxcode = $taxcode;

        return $this;
    }

    /**
     * Get taxcode
     *
     * @return string 
     */
    public function getTaxcode() {
        return $this->taxcode;
    }

    /**
     * Set custsurchargerate
     *
     * @param float $custsurchargerate
     * @return Customers
     */
    public function setCustsurchargerate($custsurchargerate) {
        $this->custsurchargerate = $custsurchargerate;

        return $this;
    }

    /**
     * Get custsurchargerate
     *
     * @return float 
     */
    public function getCustsurchargerate() {
        return $this->custsurchargerate;
    }

    /**
     * Set note
     *
     * @param string $note
     * @return Customers
     */
    public function setNote($note) {
        $this->note = $note;

        return $this;
    }

    /**
     * Get note
     *
     * @return string 
     */
    public function getNote() {
        return $this->note;
    }

    /**
     * Set zerodays
     *
     * @param float $zerodays
     * @return Customers
     */
    public function setZerodays($zerodays) {
        $this->zerodays = $zerodays;

        return $this;
    }

    /**
     * Get zerodays
     *
     * @return float 
     */
    public function getZerodays() {
        return $this->zerodays;
    }

    /**
     * Set thirtydays
     *
     * @param float $thirtydays
     * @return Customers
     */
    public function setThirtydays($thirtydays) {
        $this->thirtydays = $thirtydays;

        return $this;
    }

    /**
     * Get thirtydays
     *
     * @return float 
     */
    public function getThirtydays() {
        return $this->thirtydays;
    }

    /**
     * Set sixtydays
     *
     * @param float $sixtydays
     * @return Customers
     */
    public function setSixtydays($sixtydays) {
        $this->sixtydays = $sixtydays;

        return $this;
    }

    /**
     * Get sixtydays
     *
     * @return float 
     */
    public function getSixtydays() {
        return $this->sixtydays;
    }

    /**
     * Set ninetydays
     *
     * @param float $ninetydays
     * @return Customers
     */
    public function setNinetydays($ninetydays) {
        $this->ninetydays = $ninetydays;

        return $this;
    }

    /**
     * Get ninetydays
     *
     * @return float 
     */
    public function getNinetydays() {
        return $this->ninetydays;
    }

    /**
     * Set discount
     *
     * @param float $discount
     * @return Customers
     */
    public function setDiscount($discount) {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Get discount
     *
     * @return float 
     */
    public function getDiscount() {
        return $this->discount;
    }

    /**
     * Set ratelevel
     *
     * @param string $ratelevel
     * @return Customers
     */
    public function setRatelevel($ratelevel) {
        $this->ratelevel = $ratelevel;

        return $this;
    }

    /**
     * Get ratelevel
     *
     * @return string 
     */
    public function getRatelevel() {
        return $this->ratelevel;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Customers
     */
    public function setEmail($email) {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * Set website
     *
     * @param string $website
     * @return Customers
     */
    public function setWebsite($website) {
        $this->website = $website;

        return $this;
    }

    /**
     * Get website
     *
     * @return string 
     */
    public function getWebsite() {
        return $this->website;
    }

    /**
     * Set cell
     *
     * @param string $cell
     * @return Customers
     */
    public function setCell($cell) {
        $this->cell = $cell;

        return $this;
    }

    /**
     * Get cell
     *
     * @return string 
     */
    public function getCell() {
        return $this->cell;
    }

    /**
     * Set cityprovpostal
     *
     * @param string $cityprovpostal
     * @return Customers
     */
    public function setCityprovpostal($cityprovpostal) {
        $this->cityprovpostal = $cityprovpostal;

        return $this;
    }

    /**
     * Get cityprovpostal
     *
     * @return string 
     */
    public function getCityprovpostal() {
        return $this->cityprovpostal;
    }

    /**
     * Set addressblock
     *
     * @param string $addressblock
     * @return Customers
     */
    public function setAddressblock($addressblock) {
        $this->addressblock = $addressblock;

        return $this;
    }

    /**
     * Get addressblock
     *
     * @return string 
     */
    public function getAddressblock() {
        return $this->addressblock;
    }

    /**
     * Set revised
     *
     * @param \DateTime $revised
     * @return Customers
     */
    public function setRevised($revised) {
        $this->revised = $revised;

        return $this;
    }

    /**
     * Get revised
     *
     * @return \DateTime 
     */
    public function getRevised() {
        return $this->revised;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return Customers
     */
    public function setStatus($status) {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus() {
        return $this->status;
    }

    /**
     * Set sendmail
     *
     * @param string $sendmail
     * @return Customers
     */
    public function setSendmail($sendmail) {
        $this->sendmail = $sendmail;

        return $this;
    }

    /**
     * Get sendmail
     *
     * @return string 
     */
    public function getSendmail() {
        return $this->sendmail;
    }

    /**
     * Set contact
     *
     * @param string $contact
     * @return Customers
     */
    public function setContact($contact) {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get contact
     *
     * @return string 
     */
    public function getContact() {
        return $this->contact;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return Customers
     */
    public function setUsername($username) {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string 
     */
    public function getUsername() {
        return $this->username;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return Customers
     */
    public function setPassword($password) {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword() {
        return $this->password;
    }

    /**
     * Set terminalid
     *
     * @param integer $terminalid
     * @return Customers
     */
    public function setTerminalid($terminalid) {
        $this->terminalid = $terminalid;

        return $this;
    }

    /**
     * Get terminalid
     *
     * @return integer 
     */
    public function getTerminalid() {
        return $this->terminalid;
    }

    /**
     * Set israteoverride
     *
     * @param boolean $israteoverride
     * @return Customers
     */
    public function setIsrateoverride($israteoverride) {
        $this->israteoverride = $israteoverride;

        return $this;
    }

    /**
     * Get israteoverride
     *
     * @return boolean 
     */
    public function getIsrateoverride() {
        return $this->israteoverride;
    }

    /**
     * Set ishwyrateoverride
     *
     * @param boolean $ishwyrateoverride
     * @return Customers
     */
    public function setIshwyrateoverride($ishwyrateoverride) {
        $this->ishwyrateoverride = $ishwyrateoverride;

        return $this;
    }

    /**
     * Get ishwyrateoverride
     *
     * @return boolean 
     */
    public function getIshwyrateoverride() {
        return $this->ishwyrateoverride;
    }

    /**
     * Set custhwysurchargerate
     *
     * @param float $custhwysurchargerate
     * @return Customers
     */
    public function setCusthwysurchargerate($custhwysurchargerate) {
        $this->custhwysurchargerate = $custhwysurchargerate;

        return $this;
    }

    /**
     * Get custhwysurchargerate
     *
     * @return float 
     */
    public function getCusthwysurchargerate() {
        return $this->custhwysurchargerate;
    }

    /**
     * Add probills
     *
     * @param \Numa\CCCAdminBundle\Entity\Probills $probills
     * @return Customers
     */
    public function addProbill(\Numa\CCCAdminBundle\Entity\Probills $probills) {
        $this->probills[] = $probills;

        return $this;
    }

    /**
     * Remove probills
     *
     * @param \Numa\CCCAdminBundle\Entity\Probills $probills
     */
    public function removeProbill(\Numa\CCCAdminBundle\Entity\Probills $probills) {
        $this->probills->removeElement($probills);
    }

    /**
     * Get probills
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProbills() {
        return $this->probills;
    }

    /**
     * @var boolean
     */
    private $is_admin;

    /**
     * Set is_admin
     *
     * @param boolean $isAdmin
     * @return Customers
     */
    public function setIsAdmin($isAdmin) {
        $this->isAdmin = $isAdmin;

        return $this;
    }

    /**
     * Get is_admin
     *
     * @return boolean 
     */
    public function getIsAdmin() {
        return $this->isAdmin;
    }
    
    public function getRoles()
    {
        
        if($this->getIsAdmin()){
            //return array('ROLE_SUPER_ADMIN');
        }
        if($this->getUserGroup()=="CSR"){
            //return array('ROLE_CSR');
        }
        return array('ROLE_CUSTOMER');
        
    }
 
    public function getSalt()
    {
        return null;
    }
 
    public function eraseCredentials()
    {
 
    }
 
    public function equals(Customers $user)
    {
        return $user->getUsername() == $this->getUsername();
    }    
    
    public function __toString() {
        
        return $this->getName()."";
    }

    /**
     * @var boolean
     */
    private $isAdmin;


    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $Dispatchcard;


    /**
     * Add Dispatchcard
     *
     * @param \Numa\CCCAdminBundle\Entity\Dispatchcard $dispatchcard
     * @return Customers
     */
    public function addDispatchcard(\Numa\CCCAdminBundle\Entity\Dispatchcard $dispatchcard)
    {
        $this->Dispatchcard[] = $dispatchcard;
    
        return $this;
    }

    /**
     * Remove Dispatchcard
     *
     * @param \Numa\CCCAdminBundle\Entity\Dispatchcard $dispatchcard
     */
    public function removeDispatchcard(\Numa\CCCAdminBundle\Entity\Dispatchcard $dispatchcard)
    {
        $this->Dispatchcard->removeElement($dispatchcard);
    }

    /**
     * Get Dispatchcard
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDispatchcard()
    {
        return $this->Dispatchcard;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $CustomerEmails;


    /**
     * Add CustomerEmails
     *
     * @param \Numa\CCCAdminBundle\Entity\CustomerEmails $customerEmails
     * @return Customers
     */
    public function addCustomerEmail(\Numa\CCCAdminBundle\Entity\CustomerEmails $customerEmails)
    {
        $this->CustomerEmails[] = $customerEmails;
    
        return $this;
    }

    /**
     * Remove CustomerEmails
     *
     * @param \Numa\CCCAdminBundle\Entity\CustomerEmails $customerEmails
     */
    public function removeCustomerEmail(\Numa\CCCAdminBundle\Entity\CustomerEmails $customerEmails)
    {
        $this->CustomerEmails->removeElement($customerEmails);
    }

    /**
     * Get CustomerEmails
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCustomerEmails()
    {
        return $this->CustomerEmails;
    }
    /**
     * @var string
     */
    private $user_group;


    /**
     * Set userGroup
     *
     * @param string $userGroup
     *
     * @return Customers
     */
    public function setUserGroup($userGroup)
    {
        $this->user_group = $userGroup;

        return $this;
    }

    /**
     * Get userGroup
     *
     * @return string
     */
    public function getUserGroup()
    {
        return $this->user_group;
    }


    /**
     * @var string
     * @JMS\Expose
     */
    private $rate_pdf;


    /**
     * Set ratePdf
     *
     * @param string $ratePdf
     *
     * @return Customers
     */
    public function setRatePdf($ratePdf)
    {
        $this->rate_pdf = $ratePdf;

        return $this;
    }

    /**
     * Get ratePdf
     *
     * @return string
     */
    public function getRatePdf()
    {
        return $this->rate_pdf;
    }
    /**
     * @var string
     */
    private $rate_pdf_file;


    /**
     * Set ratePdfFile
     *
     * @param string $ratePdfFile
     *
     * @return Customers
     */
    public function setRatePdfFile($ratePdfFile)
    {
        $this->rate_pdf_file = $ratePdfFile;

        return $this;
    }

    /**
     * Get ratePdfFile
     *
     * @return string
     */
    public function getRatePdfFile()
    {
        return $this->rate_pdf_file;
    }

    public function getAbsolutePath()
    {
        return null === $this->rate_pdf ? null : $this->getUploadRootDir() . '/' . $this->rate_pdf;
    }

    public function getWebPath()
    {
        //dump($this->rate_pdf);die();
        return null === $this->rate_pdf ? null : $this->getUploadDir() . '/' . $this->rate_pdf;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__ . '/../../../../web/' . $this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'upload/customer';
    }

    public function upload()
    {
        // the file property can be empty if the field is not required
        if (null === $this->getRatePdfFile()) {
            return;
        }

        // use the original file name here but you should
        // sanitize it at least to avoid any security issues
        // move takes the target directory and then the
        // target filename to move to
        if(!is_dir($this->getUploadRootDir())){
            mkdir($this->getUploadRootDir(),0777,true);
        }
        $this->getRatePdfFile()->move(
            $this->getUploadRootDir(), $this->getRatePdfFile()->getClientOriginalName()
        );

        // set the path property to the filename where you've saved the file
        $this->rate_pdf = $this->getRatePdfFile()->getClientOriginalName();

        // clean up the file property as you won't need it anymore
        $this->rate_pdf_file = null;
    }
    /**
     * @var boolean
     * @JMS\Expose
     */
    private $activate;


    /**
     * Set activate
     *
     * @param boolean $activate
     *
     * @return Customers
     */
    public function setActivate($activate)
    {
        $this->activate = $activate;

        return $this;
    }

    /**
     * Get activate
     *
     * @return boolean
     */
    public function getActivate()
    {
        return $this->activate;
    }

    public function isDeactivated()
    {
        return !($this->activate===null || $this->activate);
    }
    /**
     * @return string
     * @JMS\VirtualProperty
     */
    public function displayName(){
        return $this->custcode." | ".$this->name." | ".$this->getAddress1();
    }
    /**
     * @var string
     */
    private $loc1Address1;

    /**
     * @var string
     */
    private $loc1Address2;

    /**
     * @var string
     */
    private $loc1City;

    /**
     * @var string
     */
    private $loc1Prov;

    /**
     * @var string
     */
    private $loc1Postal;

    /**
     * @var string
     */
    private $loc1Contact;

    /**
     * @var string
     */
    private $loc2Address1;

    /**
     * @var string
     */
    private $loc2Address2;

    /**
     * @var string
     */
    private $loc2City;

    /**
     * @var string
     */
    private $loc2Prov;

    /**
     * @var string
     */
    private $loc2Postal;

    /**
     * @var string
     */
    private $loc2Contact;

    /**
     * @var string
     */
    private $loc3Address1;

    /**
     * @var string
     */
    private $loc3Address2;

    /**
     * @var string
     */
    private $loc3City;

    /**
     * @var string
     */
    private $loc3Prov;

    /**
     * @var string
     */
    private $loc3Postal;

    /**
     * @var string
     */
    private $loc3Contact;

    /**
     * @var string
     */
    private $loc4Address1;

    /**
     * @var string
     */
    private $loc4Address2;

    /**
     * @var string
     */
    private $loc4City;

    /**
     * @var string
     */
    private $loc4Prov;

    /**
     * @var string
     */
    private $loc4Postal;

    /**
     * @var string
     */
    private $loc4Contact;

    /**
     * @var string
     */
    private $loc5Address1;

    /**
     * @var string
     */
    private $loc5Address2;

    /**
     * @var string
     */
    private $loc5City;

    /**
     * @var string
     */
    private $loc5Prov;

    /**
     * @var string
     */
    private $loc5Postal;

    /**
     * @var string
     */
    private $loc5Contact;


    /**
     * Set loc1Address1
     *
     * @param string $loc1Address1
     *
     * @return Customers
     */
    public function setLoc1Address1($loc1Address1)
    {
        $this->loc1Address1 = $loc1Address1;

        return $this;
    }

    /**
     * Get loc1Address1
     *
     * @return string
     */
    public function getLoc1Address1()
    {
        return $this->loc1Address1;
    }

    /**
     * Set loc1Address2
     *
     * @param string $loc1Address2
     *
     * @return Customers
     */
    public function setLoc1Address2($loc1Address2)
    {
        $this->loc1Address2 = $loc1Address2;

        return $this;
    }

    /**
     * Get loc1Address2
     *
     * @return string
     */
    public function getLoc1Address2()
    {
        return $this->loc1Address2;
    }

    /**
     * Set loc1City
     *
     * @param string $loc1City
     *
     * @return Customers
     */
    public function setLoc1City($loc1City)
    {
        $this->loc1City = $loc1City;

        return $this;
    }

    /**
     * Get loc1City
     *
     * @return string
     */
    public function getLoc1City()
    {
        return $this->loc1City;
    }

    /**
     * Set loc1Prov
     *
     * @param string $loc1Prov
     *
     * @return Customers
     */
    public function setLoc1Prov($loc1Prov)
    {
        $this->loc1Prov = $loc1Prov;

        return $this;
    }

    /**
     * Get loc1Prov
     *
     * @return string
     */
    public function getLoc1Prov()
    {
        return $this->loc1Prov;
    }

    /**
     * Set loc1Postal
     *
     * @param string $loc1Postal
     *
     * @return Customers
     */
    public function setLoc1Postal($loc1Postal)
    {
        $this->loc1Postal = $loc1Postal;

        return $this;
    }

    /**
     * Get loc1Postal
     *
     * @return string
     */
    public function getLoc1Postal()
    {
        return $this->loc1Postal;
    }

    /**
     * Set loc1Contact
     *
     * @param string $loc1Contact
     *
     * @return Customers
     */
    public function setLoc1Contact($loc1Contact)
    {
        $this->loc1Contact = $loc1Contact;

        return $this;
    }

    /**
     * Get loc1Contact
     *
     * @return string
     */
    public function getLoc1Contact()
    {
        return $this->loc1Contact;
    }

    /**
     * Set loc2Address1
     *
     * @param string $loc2Address1
     *
     * @return Customers
     */
    public function setLoc2Address1($loc2Address1)
    {
        $this->loc2Address1 = $loc2Address1;

        return $this;
    }

    /**
     * Get loc2Address1
     *
     * @return string
     */
    public function getLoc2Address1()
    {
        return $this->loc2Address1;
    }

    /**
     * Set loc2Address2
     *
     * @param string $loc2Address2
     *
     * @return Customers
     */
    public function setLoc2Address2($loc2Address2)
    {
        $this->loc2Address2 = $loc2Address2;

        return $this;
    }

    /**
     * Get loc2Address2
     *
     * @return string
     */
    public function getLoc2Address2()
    {
        return $this->loc2Address2;
    }

    /**
     * Set loc2City
     *
     * @param string $loc2City
     *
     * @return Customers
     */
    public function setLoc2City($loc2City)
    {
        $this->loc2City = $loc2City;

        return $this;
    }

    /**
     * Get loc2City
     *
     * @return string
     */
    public function getLoc2City()
    {
        return $this->loc2City;
    }

    /**
     * Set loc2Prov
     *
     * @param string $loc2Prov
     *
     * @return Customers
     */
    public function setLoc2Prov($loc2Prov)
    {
        $this->loc2Prov = $loc2Prov;

        return $this;
    }

    /**
     * Get loc2Prov
     *
     * @return string
     */
    public function getLoc2Prov()
    {
        return $this->loc2Prov;
    }

    /**
     * Set loc2Postal
     *
     * @param string $loc2Postal
     *
     * @return Customers
     */
    public function setLoc2Postal($loc2Postal)
    {
        $this->loc2Postal = $loc2Postal;

        return $this;
    }

    /**
     * Get loc2Postal
     *
     * @return string
     */
    public function getLoc2Postal()
    {
        return $this->loc2Postal;
    }

    /**
     * Set loc2Contact
     *
     * @param string $loc2Contact
     *
     * @return Customers
     */
    public function setLoc2Contact($loc2Contact)
    {
        $this->loc2Contact = $loc2Contact;

        return $this;
    }

    /**
     * Get loc2Contact
     *
     * @return string
     */
    public function getLoc2Contact()
    {
        return $this->loc2Contact;
    }

    /**
     * Set loc3Address1
     *
     * @param string $loc3Address1
     *
     * @return Customers
     */
    public function setLoc3Address1($loc3Address1)
    {
        $this->loc3Address1 = $loc3Address1;

        return $this;
    }

    /**
     * Get loc3Address1
     *
     * @return string
     */
    public function getLoc3Address1()
    {
        return $this->loc3Address1;
    }

    /**
     * Set loc3Address2
     *
     * @param string $loc3Address2
     *
     * @return Customers
     */
    public function setLoc3Address2($loc3Address2)
    {
        $this->loc3Address2 = $loc3Address2;

        return $this;
    }

    /**
     * Get loc3Address2
     *
     * @return string
     */
    public function getLoc3Address2()
    {
        return $this->loc3Address2;
    }

    /**
     * Set loc3City
     *
     * @param string $loc3City
     *
     * @return Customers
     */
    public function setLoc3City($loc3City)
    {
        $this->loc3City = $loc3City;

        return $this;
    }

    /**
     * Get loc3City
     *
     * @return string
     */
    public function getLoc3City()
    {
        return $this->loc3City;
    }

    /**
     * Set loc3Prov
     *
     * @param string $loc3Prov
     *
     * @return Customers
     */
    public function setLoc3Prov($loc3Prov)
    {
        $this->loc3Prov = $loc3Prov;

        return $this;
    }

    /**
     * Get loc3Prov
     *
     * @return string
     */
    public function getLoc3Prov()
    {
        return $this->loc3Prov;
    }

    /**
     * Set loc3Postal
     *
     * @param string $loc3Postal
     *
     * @return Customers
     */
    public function setLoc3Postal($loc3Postal)
    {
        $this->loc3Postal = $loc3Postal;

        return $this;
    }

    /**
     * Get loc3Postal
     *
     * @return string
     */
    public function getLoc3Postal()
    {
        return $this->loc3Postal;
    }

    /**
     * Set loc3Contact
     *
     * @param string $loc3Contact
     *
     * @return Customers
     */
    public function setLoc3Contact($loc3Contact)
    {
        $this->loc3Contact = $loc3Contact;

        return $this;
    }

    /**
     * Get loc3Contact
     *
     * @return string
     */
    public function getLoc3Contact()
    {
        return $this->loc3Contact;
    }

    /**
     * Set loc4Address1
     *
     * @param string $loc4Address1
     *
     * @return Customers
     */
    public function setLoc4Address1($loc4Address1)
    {
        $this->loc4Address1 = $loc4Address1;

        return $this;
    }

    /**
     * Get loc4Address1
     *
     * @return string
     */
    public function getLoc4Address1()
    {
        return $this->loc4Address1;
    }

    /**
     * Set loc4Address2
     *
     * @param string $loc4Address2
     *
     * @return Customers
     */
    public function setLoc4Address2($loc4Address2)
    {
        $this->loc4Address2 = $loc4Address2;

        return $this;
    }

    /**
     * Get loc4Address2
     *
     * @return string
     */
    public function getLoc4Address2()
    {
        return $this->loc4Address2;
    }

    /**
     * Set loc4City
     *
     * @param string $loc4City
     *
     * @return Customers
     */
    public function setLoc4City($loc4City)
    {
        $this->loc4City = $loc4City;

        return $this;
    }

    /**
     * Get loc4City
     *
     * @return string
     */
    public function getLoc4City()
    {
        return $this->loc4City;
    }

    /**
     * Set loc4Prov
     *
     * @param string $loc4Prov
     *
     * @return Customers
     */
    public function setLoc4Prov($loc4Prov)
    {
        $this->loc4Prov = $loc4Prov;

        return $this;
    }

    /**
     * Get loc4Prov
     *
     * @return string
     */
    public function getLoc4Prov()
    {
        return $this->loc4Prov;
    }

    /**
     * Set loc4Postal
     *
     * @param string $loc4Postal
     *
     * @return Customers
     */
    public function setLoc4Postal($loc4Postal)
    {
        $this->loc4Postal = $loc4Postal;

        return $this;
    }

    /**
     * Get loc4Postal
     *
     * @return string
     */
    public function getLoc4Postal()
    {
        return $this->loc4Postal;
    }

    /**
     * Set loc4Contact
     *
     * @param string $loc4Contact
     *
     * @return Customers
     */
    public function setLoc4Contact($loc4Contact)
    {
        $this->loc4Contact = $loc4Contact;

        return $this;
    }

    /**
     * Get loc4Contact
     *
     * @return string
     */
    public function getLoc4Contact()
    {
        return $this->loc4Contact;
    }

    /**
     * Set loc5Address1
     *
     * @param string $loc5Address1
     *
     * @return Customers
     */
    public function setLoc5Address1($loc5Address1)
    {
        $this->loc5Address1 = $loc5Address1;

        return $this;
    }

    /**
     * Get loc5Address1
     *
     * @return string
     */
    public function getLoc5Address1()
    {
        return $this->loc5Address1;
    }

    /**
     * Set loc5Address2
     *
     * @param string $loc5Address2
     *
     * @return Customers
     */
    public function setLoc5Address2($loc5Address2)
    {
        $this->loc5Address2 = $loc5Address2;

        return $this;
    }

    /**
     * Get loc5Address2
     *
     * @return string
     */
    public function getLoc5Address2()
    {
        return $this->loc5Address2;
    }

    /**
     * Set loc5City
     *
     * @param string $loc5City
     *
     * @return Customers
     */
    public function setLoc5City($loc5City)
    {
        $this->loc5City = $loc5City;

        return $this;
    }

    /**
     * Get loc5City
     *
     * @return string
     */
    public function getLoc5City()
    {
        return $this->loc5City;
    }

    /**
     * Set loc5Prov
     *
     * @param string $loc5Prov
     *
     * @return Customers
     */
    public function setLoc5Prov($loc5Prov)
    {
        $this->loc5Prov = $loc5Prov;

        return $this;
    }

    /**
     * Get loc5Prov
     *
     * @return string
     */
    public function getLoc5Prov()
    {
        return $this->loc5Prov;
    }

    /**
     * Set loc5Postal
     *
     * @param string $loc5Postal
     *
     * @return Customers
     */
    public function setLoc5Postal($loc5Postal)
    {
        $this->loc5Postal = $loc5Postal;

        return $this;
    }

    /**
     * Get loc5Postal
     *
     * @return string
     */
    public function getLoc5Postal()
    {
        return $this->loc5Postal;
    }

    /**
     * Set loc5Contact
     *
     * @param string $loc5Contact
     *
     * @return Customers
     */
    public function setLoc5Contact($loc5Contact)
    {
        $this->loc5Contact = $loc5Contact;

        return $this;
    }

    /**
     * Get loc5Contact
     *
     * @return string
     */
    public function getLoc5Contact()
    {
        return $this->loc5Contact;
    }
    /**
     * @var integer
     */
    private $location;


    /**
     * Set location
     *
     * @param integer $location
     *
     * @return Customers
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return integer
     */
    public function getLocation()
    {
        return $this->location;
    }
    /**
     * @var string
     */
    private $description;


    /**
     * Set description
     *
     * @param string $description
     *
     * @return Customers
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
    /**
     * @var string
     * @JMS\Expose
     */
    private $data_entry_notes;


    /**
     * Set dataEntryNotes
     *
     * @param string $dataEntryNotes
     *
     * @return Customers
     */
    public function setDataEntryNotes($dataEntryNotes)
    {
        $this->data_entry_notes = $dataEntryNotes;

        return $this;
    }

    /**
     * Get dataEntryNotes
     *
     * @return string
     */
    public function getDataEntryNotes()
    {
        return $this->data_entry_notes;
    }
}
