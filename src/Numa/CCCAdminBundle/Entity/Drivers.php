<?php

namespace Numa\CCCAdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Drivers
 * @JMS\ExclusionPolicy("ALL")
 */
class Drivers implements UserInterface {

    /**
     * @var integer
     * @JMS\Expose
     */
    private $id;

    /**
     * @var string
     * @JMS\Expose
     */
    private $drivernum;

    /**
     * @var string
     * @JMS\Expose
     */
    private $drivernam;

    /**
     * @var string
     * @JMS\Expose
     */
    private $address;

    /**
     * @var string
     * @JMS\Expose
     */
    private $city;

    /**
     * @var string
     * @JMS\Expose
     */
    private $postal;

    /**
     * @var string
     * @JMS\Expose
     */
    private $tel;

    /**
     * @var string
     * @JMS\Expose
     */
    private $cell;

    /**
     * @var \DateTime
     * @JMS\Expose
     */
    private $hired;

    /**
     * @var \DateTime
     * @JMS\Expose
     */
    private $dob;

    /**
     * @var string
     * @JMS\Expose
     */
    private $sin;

    /**
     * @var string
     * @JMS\Expose
     */
    private $married;

    /**
     * @var string
     * @JMS\Expose
     */
    private $pic;

    /**
     * @var \DateTime
     * @JMS\Expose
     */
    private $picExpiry;

    /**
     * @var \DateTime
     * @JMS\Expose
     */
    private $vehExpiry;

    /**
     * @var string
     * @JMS\Expose
     */
    private $vehPlat;

    /**
     * @var string
     * @JMS\Expose
     */
    private $vehYear;

    /**
     * @var string
     * @JMS\Expose
     */
    private $vehType;

    /**
     * @var string
     * @JMS\Expose
     */
    private $vehColor;

    /**
     * @var string
     * @JMS\Expose
     */
    private $vehSerial;

    /**
     * @var string
     * @JMS\Expose
     */
    private $vehClass;

    /**
     * @var float
     * @JMS\Expose
     */
    private $drivRate;

    /**
     * @var string
     * @JMS\Expose
     */
    private $insAgent;

    /**
     * @var string
     * @JMS\Expose
     */
    private $insPolicy;

    /**
     * @var float
     * @JMS\Expose
     */
    private $insAmt;

    /**
     * @var \DateTime
     * @JMS\Expose
     */
    private $insExpiry;

    /**
     * @var string
     * @JMS\Expose
     */
    private $comments;

    /**
     * @var string
     * @JMS\Expose
     */
    private $dgc;

    /**
     * @var \DateTime
     * @JMS\Expose
     */
    private $dgcExpiry;

    /**
     * @var float
     * @JMS\Expose
     */
    private $drvsurrate;

    /**
     * @var \Doctrine\Common\Collections\Collection

     */
    private $probills;

    /**
     * Constructor
     */
    public function __construct() {
        $this->probills = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set drivernum
     *
     * @param string $drivernum
     * @return Drivers
     */
    public function setDrivernum($drivernum) {
        $this->drivernum = $drivernum;

        return $this;
    }

    /**
     * Get drivernum
     *
     * @return string 
     */
    public function getDrivernum() {
        return $this->drivernum;
    }

    public function set($fieldname, $value) {
        $fieldname = strtolower($fieldname);

        $fieldnameParts = explode("_", $fieldname);
        if (count($fieldnameParts) == 2) {
            $fieldname = $fieldnameParts[0] . ucfirst($fieldnameParts[1]);
        }

        $this->$fieldname = $value;


        //check if date
        if (preg_match("/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/", $value)) {
            $this->$fieldname = new \DateTime($value);
        }
        if (preg_match("/^[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}$/", $value)) {
            $this->$fieldname = new \DateTime($value);
        }
    }

    /**
     * Set drivernam
     *
     * @param string $drivernam
     * @return Drivers
     */
    public function setDrivernam($drivernam) {
        $this->drivernam = $drivernam;

        return $this;
    }

    /**
     * Get drivernam
     *
     * @return string 
     */
    public function getDrivernam() {
        return $this->drivernam;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Drivers
     */
    public function setAddress($address) {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress() {
        return $this->address;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return Drivers
     */
    public function setCity($city) {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string 
     */
    public function getCity() {
        return $this->city;
    }

    /**
     * Set postal
     *
     * @param string $postal
     * @return Drivers
     */
    public function setPostal($postal) {
        $this->postal = $postal;

        return $this;
    }

    /**
     * Get postal
     *
     * @return string 
     */
    public function getPostal() {
        return $this->postal;
    }

    /**
     * Set tel
     *
     * @param string $tel
     * @return Drivers
     */
    public function setTel($tel) {
        $this->tel = $tel;

        return $this;
    }

    /**
     * Get tel
     *
     * @return string 
     */
    public function getTel() {
        return $this->tel;
    }

    /**
     * Set cell
     *
     * @param string $cell
     * @return Drivers
     */
    public function setCell($cell) {
        $this->cell = $cell;

        return $this;
    }

    /**
     * Get cell
     *
     * @return string 
     */
    public function getCell() {
        return $this->cell;
    }

    /**
     * Set hired
     *
     * @param \DateTime $hired
     * @return Drivers
     */
    public function setHired($hired) {
        $this->hired = $hired;

        return $this;
    }

    /**
     * Get hired
     *
     * @return \DateTime 
     */
    public function getHired() {
        return $this->hired;
    }

    /**
     * Set dob
     *
     * @param \DateTime $dob
     * @return Drivers
     */
    public function setDob($dob) {
        $this->dob = $dob;

        return $this;
    }

    /**
     * Get dob
     *
     * @return \DateTime 
     */
    public function getDob() {
        return $this->dob;
    }

    /**
     * Set sin
     *
     * @param string $sin
     * @return Drivers
     */
    public function setSin($sin) {
        $this->sin = $sin;

        return $this;
    }

    /**
     * Get sin
     *
     * @return string 
     */
    public function getSin() {
        return $this->sin;
    }

    /**
     * Set married
     *
     * @param string $married
     * @return Drivers
     */
    public function setMarried($married) {
        $this->married = $married;

        return $this;
    }

    /**
     * Get married
     *
     * @return string 
     */
    public function getMarried() {
        return $this->married;
    }

    /**
     * Set pic
     *
     * @param string $pic
     * @return Drivers
     */
    public function setPic($pic) {
        $this->pic = $pic;

        return $this;
    }

    /**
     * Get pic
     *
     * @return string 
     */
    public function getPic() {
        return $this->pic;
    }

    /**
     * Set picExpiry
     *
     * @param \DateTime $picExpiry
     * @return Drivers
     */
    public function setPicExpiry($picExpiry) {
        $this->picExpiry = $picExpiry;

        return $this;
    }

    /**
     * Get picExpiry
     *
     * @return \DateTime 
     */
    public function getPicExpiry() {
        return $this->picExpiry;
    }

    /**
     * Set vehExpiry
     *
     * @param \DateTime $vehExpiry
     * @return Drivers
     */
    public function setVehExpiry($vehExpiry) {
        $this->vehExpiry = $vehExpiry;

        return $this;
    }

    /**
     * Get vehExpiry
     *
     * @return \DateTime 
     */
    public function getVehExpiry() {
        return $this->vehExpiry;
    }

    /**
     * Set vehPlat
     *
     * @param string $vehPlat
     * @return Drivers
     */
    public function setVehPlat($vehPlat) {
        $this->vehPlat = $vehPlat;

        return $this;
    }

    /**
     * Get vehPlat
     *
     * @return string 
     */
    public function getVehPlat() {
        return $this->vehPlat;
    }

    /**
     * Set vehYear
     *
     * @param string $vehYear
     * @return Drivers
     */
    public function setVehYear($vehYear) {
        $this->vehYear = $vehYear;

        return $this;
    }

    /**
     * Get vehYear
     *
     * @return string 
     */
    public function getVehYear() {
        return $this->vehYear;
    }

    /**
     * Set vehType
     *
     * @param string $vehType
     * @return Drivers
     */
    public function setVehType($vehType) {
        $this->vehType = $vehType;

        return $this;
    }

    /**
     * Get vehType
     *
     * @return string 
     */
    public function getVehType() {
        return $this->vehType;
    }

    /**
     * Set vehColor
     *
     * @param string $vehColor
     * @return Drivers
     */
    public function setVehColor($vehColor) {
        $this->vehColor = $vehColor;

        return $this;
    }

    /**
     * Get vehColor
     *
     * @return string 
     */
    public function getVehColor() {
        return $this->vehColor;
    }

    /**
     * Set vehSerial
     *
     * @param string $vehSerial
     * @return Drivers
     */
    public function setVehSerial($vehSerial) {
        $this->vehSerial = $vehSerial;

        return $this;
    }

    /**
     * Get vehSerial
     *
     * @return string 
     */
    public function getVehSerial() {
        return $this->vehSerial;
    }

    /**
     * Set vehClass
     *
     * @param string $vehClass
     * @return Drivers
     */
    public function setVehClass($vehClass) {
        $this->vehClass = $vehClass;

        return $this;
    }

    /**
     * Get vehClass
     *
     * @return string 
     */
    public function getVehClass() {
        return $this->vehClass;
    }

    /**
     * Set drivRate
     *
     * @param float $drivRate
     * @return Drivers
     */
    public function setDrivRate($drivRate) {
        $this->drivRate = $drivRate;

        return $this;
    }

    /**
     * Get drivRate
     *
     * @return float 
     */
    public function getDrivRate() {
        return $this->drivRate;
    }

    /**
     * Set insAgent
     *
     * @param string $insAgent
     * @return Drivers
     */
    public function setInsAgent($insAgent) {
        $this->insAgent = $insAgent;

        return $this;
    }

    /**
     * Get insAgent
     *
     * @return string 
     */
    public function getInsAgent() {
        return $this->insAgent;
    }

    /**
     * Set insPolicy
     *
     * @param string $insPolicy
     * @return Drivers
     */
    public function setInsPolicy($insPolicy) {
        $this->insPolicy = $insPolicy;

        return $this;
    }

    /**
     * Get insPolicy
     *
     * @return string 
     */
    public function getInsPolicy() {
        return $this->insPolicy;
    }

    /**
     * Set insAmt
     *
     * @param float $insAmt
     * @return Drivers
     */
    public function setInsAmt($insAmt) {
        $this->insAmt = $insAmt;

        return $this;
    }

    /**
     * Get insAmt
     *
     * @return float 
     */
    public function getInsAmt() {
        return $this->insAmt;
    }

    /**
     * Set insExpiry
     *
     * @param \DateTime $insExpiry
     * @return Drivers
     */
    public function setInsExpiry($insExpiry) {
        $this->insExpiry = $insExpiry;

        return $this;
    }

    /**
     * Get insExpiry
     *
     * @return \DateTime 
     */
    public function getInsExpiry() {
        return $this->insExpiry;
    }

    /**
     * Set comments
     *
     * @param string $comments
     * @return Drivers
     */
    public function setComments($comments) {
        $this->comments = $comments;

        return $this;
    }

    /**
     * Get comments
     *
     * @return string 
     */
    public function getComments() {
        return $this->comments;
    }

    /**
     * Set dgc
     *
     * @param string $dgc
     * @return Drivers
     */
    public function setDgc($dgc) {
        $this->dgc = $dgc;

        return $this;
    }

    /**
     * Get dgc
     *
     * @return string 
     */
    public function getDgc() {
        return $this->dgc;
    }

    /**
     * Set dgcExpiry
     *
     * @param \DateTime $dgcExpiry
     * @return Drivers
     */
    public function setDgcExpiry($dgcExpiry) {
        $this->dgcExpiry = $dgcExpiry;

        return $this;
    }

    /**
     * Get dgcExpiry
     *
     * @return \DateTime 
     */
    public function getDgcExpiry() {
        return $this->dgcExpiry;
    }

    /**
     * Set drvsurrate
     *
     * @param float $drvsurrate
     * @return Drivers
     */
    public function setDrvsurrate($drvsurrate) {
        $this->drvsurrate = $drvsurrate;

        return $this;
    }

    /**
     * Get drvsurrate
     *
     * @return float 
     */
    public function getDrvsurrate() {
        return $this->drvsurrate;
    }

    /**
     * Add probills
     *
     * @param \Numa\CCCAdminBundle\Entity\Probills $probills
     * @return Drivers
     */
    public function addProbill(\Numa\CCCAdminBundle\Entity\Probills $probills) {
        $this->probills[] = $probills;

        return $this;
    }

    /**
     * Remove probills
     *
     * @param \Numa\CCCAdminBundle\Entity\Probills $probills
     */
    public function removeProbill(\Numa\CCCAdminBundle\Entity\Probills $probills) {
        $this->probills->removeElement($probills);
    }

    /**
     * Get probills
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProbills() {
        return $this->probills;
    }
    public function getRoles()
    {
        return array('ROLE_DRIVER');
    }

    public function getSalt()
    {
        return null;
    }

    public function eraseCredentials()
    {

    }

    public function equals(Customers $user)
    {
        return $user->getUsername() == $this->getUsername();
    }

    public function __toString() {
        return $this->getDrivernam();
    }

    public function displayName(){
        return $this->getDrivernum()." | ".$this->getDrivernam()." (".$this->getDrivRate().") ";
    }

    /**
     * @var integer
     * @JMS\Expose
     */
    private $frame;


    /**
     * Set frame
     *
     * @param integer $frame
     *
     * @return Drivers
     */
    public function setFrame($frame)
    {
        $this->frame = $frame;

        return $this;
    }

    /**
     * Get frame
     *
     * @return integer
     */
    public function getFrame()
    {
        return $this->frame;
    }
    /**
     * @var boolean
     * @JMS\Expose
     */
    private $active;


    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return Drivers
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @return string
     * @JMS\VirtualProperty
     */
    public function isActive(){
        return $this->active?"Active":"Inactive";
    }

    /**
     * @return string
     * @JMS\VirtualProperty
     */
    public function getFrameString()
    {
        if($this->getFrame()==2){
            return "2 Weeks";
        }elseif($this->getFrame()==4){
            return "4 Weeks";
        }
        return "No Commission";
    }


    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $password;


    /**
     * Set username
     *
     * @param string $username
     *
     * @return Drivers
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return Drivers
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }
    /**
     * @var integer
     */
    private $chanel;


    /**
     * Set chanel
     *
     * @param integer $chanel
     *
     * @return Drivers
     */
    public function setChanel($chanel)
    {
        $this->chanel = $chanel;

        return $this;
    }

    /**
     * Get chanel
     *
     * @return integer
     */
    public function getChanel()
    {
        return $this->chanel;
    }
}
