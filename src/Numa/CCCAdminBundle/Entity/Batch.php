<?php

namespace Numa\CCCAdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
 * Batch
 */
class Batch
{
    const LOCK = 'lock';
    const UNLOCK = 'unlock';
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var \DateTime
     */
    private $batch_date;

    /**
     * @var \DateTime
     */
    private $started;

    /**
     * @var \DateTime
     */
    private $closed;

    /**
     * @var integer
     */
    private $working_days;

    /**
     * @var \DateTime
     */
    private $date_created;

    /**
     * @var \DateTime
     */
    private $date_updated;

    /**
     * @var integer
     */
    private $billing_period_id;

    /**
     * @var integer
     */
    private $user_id;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $Probills;

    /**
     * @var \Numa\CCCAdminBundle\Entity\User
     */
    private $User;

    /**
     * @var \Numa\CCCAdminBundle\Entity\BillingPeriod
     */
    private $BillingPeriod;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->Probills = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Batch
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set batchDate
     *
     * @param \DateTime $batchDate
     *
     * @return Batch
     */
    public function setBatchDate($batchDate)
    {
        $this->batch_date = $batchDate;

        return $this;
    }

    /**
     * Get batchDate
     *
     * @return \DateTime
     */
    public function getBatchDate()
    {
        return $this->batch_date;
    }

    /**
     * Set started
     *
     * @param \DateTime $started
     *
     * @return Batch
     */
    public function setStarted($started)
    {
        $this->started = $started;

        return $this;
    }

    /**
     * Get started
     *
     * @return \DateTime
     */
    public function getStarted()
    {
        return $this->started;
    }

    /**
     * Set closed
     *
     * @param \DateTime $closed
     *
     * @return Batch
     */
    public function setClosed($closed)
    {
        $this->closed = $closed;

        return $this;
    }

    /**
     * Get closed
     *
     * @return \DateTime
     */
    public function getClosed()
    {
        return $this->closed;
    }

    /**
     * Set workingDays
     *
     * @param integer $workingDays
     *
     * @return Batch
     */
    public function setWorkingDays($workingDays)
    {
        $this->working_days = $workingDays;

        return $this;
    }

    /**
     * Get workingDays
     *
     * @return integer
     */
    public function getWorkingDays()
    {
        return $this->working_days;
    }

    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     *
     * @return Batch
     */
    public function setDateCreated($dateCreated)
    {
        $this->date_created = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * Set dateUpdated
     *
     * @param \DateTime $dateUpdated
     *
     * @return Batch
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->date_updated = $dateUpdated;

        return $this;
    }

    /**
     * Get dateUpdated
     *
     * @return \DateTime
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * Set billingPeriodId
     *
     * @param integer $billingPeriodId
     *
     * @return Batch
     */
    public function setBillingPeriodId($billingPeriodId)
    {
        $this->billing_period_id = $billingPeriodId;

        return $this;
    }

    /**
     * Get billingPeriodId
     *
     * @return integer
     */
    public function getBillingPeriodId()
    {
        return $this->billing_period_id;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return Batch
     */
    public function setUserId($userId)
    {
        $this->user_id = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Add probill
     *
     * @param \Numa\CCCAdminBundle\Entity\Probills $probill
     *
     * @return Batch
     */
    public function addProbill(\Numa\CCCAdminBundle\Entity\Probills $probill)
    {
        $this->Probills[] = $probill;

        return $this;
    }

    /**
     * Remove probill
     *
     * @param \Numa\CCCAdminBundle\Entity\Probills $probill
     */
    public function removeProbill(\Numa\CCCAdminBundle\Entity\Probills $probill)
    {
        $this->Probills->removeElement($probill);
    }

    /**
     * Get probills
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProbills()
    {
        return $this->Probills;
    }

    /**
     * Set user
     *
     * @param \Numa\CCCAdminBundle\Entity\User $user
     *
     * @return Batch
     */
    public function setUser(\Numa\CCCAdminBundle\Entity\User $user = null)
    {
        $this->User = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Numa\CCCAdminBundle\Entity\User
     */
    public function getUser()
    {
        return $this->User;
    }

    /**
     * Set billingPeriod
     *
     * @param \Numa\CCCAdminBundle\Entity\BillingPeriod $billingPeriod
     *
     * @return Batch
     */
    public function setBillingPeriod(\Numa\CCCAdminBundle\Entity\BillingPeriod $billingPeriod = null)
    {
        $this->BillingPeriod = $billingPeriod;

        return $this;
    }

    /**
     * Get billingPeriod
     *
     * @return \Numa\CCCAdminBundle\Entity\BillingPeriod
     */
    public function getBillingPeriod()
    {
        return $this->BillingPeriod;
    }
    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        $this->date_created = new \DateTime();
        $this->date_updated = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function setUpdatedAtValue()
    {
        $this->date_updated = new \DateTime();
    }
    /**
     * @var string
     */
    private $status;


    /**
     * Set status
     *
     * @param string $status
     *
     * @return Batch
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    public function isLocked(){
        return $this->status==1;
    }

    public function isFinalized(){
        return $this->status==2;
    }

    public function lock(){
        $this->setStatus(1);
    }

    public function unlock(){
        $this->setStatus(0);
    }

    public function displayName(){
        return $this->getName();//."  (".$this->getId().")";
    }


    /**
     * @var boolean
     */
    private $corrected = false;


    /**
     * Set corrected
     *
     * @param boolean $corrected
     *
     * @return Batch
     */
    public function setCorrected($corrected)
    {
        $this->corrected = $corrected;

        return $this;
    }

    /**
     * Get corrected
     *
     * @return boolean
     */
    public function getCorrected()
    {
        return $this->corrected;
    }
}
