<?php

namespace Numa\CCCAdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;


/**
 * Pickup
 */
class Pickup
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $address;

    /**
     * @var integer
     */
    private $pieces;

    /**
     * @var integer
     */
    private $weight;

    /**
     * @var \DateTime
     */
    private $date_created;

    /**
     * @var \DateTime
     */
    private $date_updated;

    /**
     * @var integer
     */
    private $probill_id;

    /**
     * @var \Numa\CCCAdminBundle\Entity\Probill
     */
    private $Probill;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Pickup
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set pieces
     *
     * @param integer $pieces
     *
     * @return Pickup
     */
    public function setPieces($pieces)
    {
        $this->pieces = $pieces;

        return $this;
    }

    /**
     * Get pieces
     *
     * @return integer
     */
    public function getPieces()
    {
        return $this->pieces;
    }

    /**
     * Set weight
     *
     * @param integer $weight
     *
     * @return Pickup
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Get weight
     *
     * @return integer
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     *
     * @return Pickup
     */
    public function setDateCreated($dateCreated)
    {
        $this->date_created = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * Set dateUpdated
     *
     * @param \DateTime $dateUpdated
     *
     * @return Pickup
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->date_updated = $dateUpdated;

        return $this;
    }

    /**
     * Get dateUpdated
     *
     * @return \DateTime
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * Set probillId
     *
     * @param integer $probillId
     *
     * @return Pickup
     */
    public function setProbillId($probillId)
    {
        $this->probill_id = $probillId;

        return $this;
    }

    /**
     * Get probillId
     *
     * @return integer
     */
    public function getProbillId()
    {
        return $this->probill_id;
    }

    /**
     * Set probill
     *
     * @param \Numa\CCCAdminBundle\Entity\Probills $probill
     *
     * @return Pickup
     */
    public function setProbill(\Numa\CCCAdminBundle\Entity\Probills $probill = null)
    {
        $this->Probill = $probill;

        return $this;
    }

    /**
     * Get probill
     *
     * @return \Numa\CCCAdminBundle\Entity\Probills
     */
    public function getProbill()
    {
        return $this->Probill;
    }
    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        $this->date_updated(new \DateTime());
        $this->date_created(new \DateTime());
    }

    /**
     * @ORM\PreUpdate
     */
    public function setUpdatedAtValue()
    {
        $this->date_updated(new \DateTime());
    }
    /**
     * @var \Numa\CCCAdminBundle\Entity\Probills
     */
    private $Probills;


    /**
     * Set probills
     *
     * @param \Numa\CCCAdminBundle\Entity\Probills $probills
     *
     * @return Pickup
     */
    public function setProbills(\Numa\CCCAdminBundle\Entity\Probills $probills = null)
    {
        $this->Probills = $probills;

        return $this;
    }

    /**
     * Get probills
     *
     * @return \Numa\CCCAdminBundle\Entity\Probills
     */
    public function getProbills()
    {
        return $this->Probills;
    }
}
