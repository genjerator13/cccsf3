<?php

namespace Numa\CCCAdminBundle\Entity;

/**
 * Mediathek
 */
class Mediathek
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $created = 'CURRENT_TIMESTAMP';


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return Mediathek
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }
}
