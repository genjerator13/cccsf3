<?php

namespace Numa\CCCAdminBundle\Entity;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;

/**
 * BillingPeriod
 */
class BillingPeriod
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var \DateTime
     */
    private $started;

    /**
     * @var \DateTime
     */
    private $closed;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return BillingPeriod
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set started
     *
     * @param \DateTime $started
     *
     * @return BillingPeriod
     */
    public function setStarted($started)
    {
        $this->started = $started;

        return $this;
    }

    /**
     * Get started
     *
     * @return \DateTime
     */
    public function getStarted()
    {
        return $this->started;
    }

    /**
     * Set closed
     *
     * @param \DateTime $closed
     *
     * @return BillingPeriod
     */
    public function setClosed($closed)
    {
        $this->closed = $closed;

        return $this;
    }

    /**
     * Get closed
     *
     * @return \DateTime
     */
    public function getClosed()
    {
        return $this->closed;
    }
    /**
     * @var integer
     */
    private $working_days;

    /**
     * @var \DateTime
     */
    private $date_created;

    /**
     * @var \DateTime
     */
    private $date_updated;




    /**
     * Set workingDays
     *
     * @param integer $workingDays
     *
     * @return BillingPeriod
     */
    public function setWorkingDays($workingDays)
    {
        $this->working_days = $workingDays;

        return $this;
    }

    /**
     * Get workingDays
     *
     * @return integer
     */
    public function getWorkingDays()
    {
        return $this->working_days;
    }

    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     *
     * @return BillingPeriod
     */
    public function setDateCreated($dateCreated)
    {
        $this->date_created = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * Set dateUpdated
     *
     * @param \DateTime $dateUpdated
     *
     * @return BillingPeriod
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->date_updated = $dateUpdated;

        return $this;
    }

    /**
     * Get dateUpdated
     *
     * @return \DateTime
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * @var integer
     */
    private $user_id;

    /**
     * @var \Numa\CCCAdminBundle\Entity\User
     */
    private $User;


    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return BillingPeriod
     */
    public function setUserId($userId)
    {
        $this->user_id = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set user
     *
     * @param \Numa\CCCAdminBundle\Entity\User $user
     *
     * @return BillingPeriod
     */
    public function setUser(\Numa\CCCAdminBundle\Entity\User $user = null)
    {
        $this->User = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Numa\CCCAdminBundle\Entity\User
     */
    public function getUser()
    {
        return $this->User;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        $this->date_created = new \DateTime();
        $this->date_updated = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function setUpdatedAtValue()
    {
        $this->date_updated = new \DateTime();
    }

    public function __toString()
    {
       return $this->getName();
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $Batch;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->Batch = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add batch
     *
     * @param \Numa\CCCAdminBundle\Entity\Batch $batch
     *
     * @return BillingPeriod
     */
    public function addBatch(\Numa\CCCAdminBundle\Entity\Batch $batch)
    {
        $this->Batch[] = $batch;

        return $this;
    }

    /**
     * Remove batch
     *
     * @param \Numa\CCCAdminBundle\Entity\Batch $batch
     */
    public function removeBatch(\Numa\CCCAdminBundle\Entity\Batch $batch)
    {
        $this->Batch->removeElement($batch);
    }

    /**
     * Get batch
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBatch()
    {
        return $this->Batch;
    }

    public function getNotLockedBatch(){
        $criteria = Criteria::create()
            ->where(Criteria::expr()->neq("status", 1));
        $batch = $this->getBatch()->matching($criteria);
        return $batch;
    }

    public function getLockedBatches(){
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq("status", 1));
        $batch = $this->getBatch()->matching($criteria);
        dump($this->getBatch());
        dump($batch);
        return $batch;
    }

    public function displayName() {
        return $this->getName();
    }
    /**
     * @var \DateTime
     */
    private $date_finished;


    /**
     * Set dateFinished
     *
     * @param \DateTime $dateFinished
     *
     * @return BillingPeriod
     */
    public function setDateFinished($dateFinished)
    {
        $this->date_finished = $dateFinished;

        return $this;
    }

    /**
     * Get dateFinished
     *
     * @return \DateTime
     */
    public function getDateFinished()
    {
        return $this->date_finished;
    }
    /**
     * @var integer
     */
    private $newsletter_id;

    /**
     * @var \Numa\CCCAdminBundle\Entity\Media
     */
    private $newsletter;


    /**
     * Set newsletterId
     *
     * @param integer $newsletterId
     *
     * @return BillingPeriod
     */
    public function setNewsletterId($newsletterId)
    {
        $this->newsletter_id = $newsletterId;

        return $this;
    }

    /**
     * Get newsletterId
     *
     * @return integer
     */
    public function getNewsletterId()
    {
        return $this->newsletter_id;
    }

    /**
     * Set newsletter
     *
     * @param \Numa\CCCAdminBundle\Entity\Media $newsletter
     *
     * @return BillingPeriod
     */
    public function setNewsletter(\Numa\CCCAdminBundle\Entity\Media $newsletter = null)
    {
        $this->newsletter = $newsletter;

        return $this;
    }

    /**
     * Get newsletter
     *
     * @return \Numa\CCCAdminBundle\Entity\Media
     */
    public function getNewsletter()
    {
        return $this->newsletter;
    }
    /**
     * @var integer
     */
    private $status = 1;


    /**
     * Set status
     *
     * @param integer $status
     *
     * @return BillingPeriod
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }
    /**
     * @var string
     */
    private $email_subject;

    /**
     * @var string
     */
    private $email_template;


    /**
     * Set emailSubject
     *
     * @param string $emailSubject
     *
     * @return BillingPeriod
     */
    public function setEmailSubject($emailSubject)
    {
        $this->email_subject = $emailSubject;

        return $this;
    }

    /**
     * Get emailSubject
     *
     * @return string
     */
    public function getEmailSubject()
    {
        return $this->email_subject;
    }

    /**
     * Set emailTemplate
     *
     * @param string $emailTemplate
     *
     * @return BillingPeriod
     */
    public function setEmailTemplate($emailTemplate)
    {
        $this->email_template = $emailTemplate;

        return $this;
    }

    /**
     * Get emailTemplate
     *
     * @return string
     */
    public function getEmailTemplate()
    {
        return $this->email_template;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $BillingReport;


    /**
     * Add billingReport
     *
     * @param \Numa\CCCAdminBundle\Entity\BillingReport $billingReport
     *
     * @return BillingPeriod
     */
    public function addBillingReport(\Numa\CCCAdminBundle\Entity\BillingReport $billingReport)
    {
        $this->BillingReport[] = $billingReport;

        return $this;
    }

    /**
     * Remove billingReport
     *
     * @param \Numa\CCCAdminBundle\Entity\BillingReport $billingReport
     */
    public function removeBillingReport(\Numa\CCCAdminBundle\Entity\BillingReport $billingReport)
    {
        $this->BillingReport->removeElement($billingReport);
    }

    /**
     * Get billingReport
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBillingReport()
    {
        return $this->BillingReport;
    }
    /**
     * @var boolean
     */
    private $active = true;


    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return BillingPeriod
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }
    /**
     * @var integer
     */
    private $old_batch_id;


    /**
     * Set oldBatchId
     *
     * @param integer $oldBatchId
     *
     * @return BillingPeriod
     */
    public function setOldBatchId($oldBatchId)
    {
        $this->old_batch_id = $oldBatchId;

        return $this;
    }

    /**
     * Get oldBatchId
     *
     * @return integer
     */
    public function getOldBatchId()
    {
        return $this->old_batch_id;
    }
}
