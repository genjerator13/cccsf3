<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Numa\CCCAdminBundle\Entity;


/**
 * DateStampableTrait trait.
 *
 * @author ja
 */
trait DateStampableTrait
{
    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {

        if (!$this->getDateUpdated()) {
            $this->date_updated = new \DateTime();
        }
        if (!$this->getDateCreated()) {
            $this->date_created = new \DateTime();
        }

    }

    /**
     * @ORM\PreUpdate
     */
    public function setUpdatedAtValue()
    {
        if (!$this->getDateUpdated()) {
            $this->date_updated = new \DateTime();
        }
    }
}