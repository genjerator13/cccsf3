<?php

namespace Numa\CCCAdminBundle\Entity;

/**
 * Dispatch
 */
class Dispatch
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $vehicletype_id;

    /**
     * @var string
     */
    private $service_expectation;

    /**
     * @var string
     */
    private $estimate_pickup;

    /**
     * @var \Numa\CCCAdminBundle\Entity\Vehtypes
     */
    private $VehicleType;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set vehicletypeId
     *
     * @param integer $vehicletypeId
     *
     * @return Dispatch
     */
    public function setVehicletypeId($vehicletypeId)
    {
        $this->vehicletype_id = $vehicletypeId;

        return $this;
    }

    /**
     * Get vehicletypeId
     *
     * @return integer
     */
    public function getVehicletypeId()
    {
        return $this->vehicletype_id;
    }

    /**
     * Set serviceExpectation
     *
     * @param string $serviceExpectation
     *
     * @return Dispatch
     */
    public function setServiceExpectation($serviceExpectation)
    {
        $this->service_expectation = $serviceExpectation;

        return $this;
    }

    /**
     * Get serviceExpectation
     *
     * @return string
     */
    public function getServiceExpectation()
    {
        return $this->service_expectation;
    }

    /**
     * Set estimatePickup
     *
     * @param string $estimatePickup
     *
     * @return Dispatch
     */
    public function setEstimatePickup($estimatePickup)
    {
        $this->estimate_pickup = $estimatePickup;

        return $this;
    }

    /**
     * Get estimatePickup
     *
     * @return string
     */
    public function getEstimatePickup()
    {
        return $this->estimate_pickup;
    }

    /**
     * Set vehicleType
     *
     * @param \Numa\CCCAdminBundle\Entity\Vehtypes $vehicleType
     *
     * @return Dispatch
     */
    public function setVehicleType(\Numa\CCCAdminBundle\Entity\Vehtypes $vehicleType = null)
    {
        $this->VehicleType = $vehicleType;

        return $this;
    }

    /**
     * Get vehicleType
     *
     * @return \Numa\CCCAdminBundle\Entity\Vehtypes
     */
    public function getVehicleType()
    {
        return $this->VehicleType;
    }
}
