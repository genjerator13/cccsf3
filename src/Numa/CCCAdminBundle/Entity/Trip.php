<?php

namespace Numa\CCCAdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
 * Trip
 */
class Trip
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $driver_id;

    /**
     * @var string
     */
    private $pickup;

    /**
     * @var string
     */
    private $delivery;

    /**
     * @var string
     */
    private $comments;

    /**
     * @var \Numa\CCCAdminBundle\Entity\Dispatchcard
     */
    private $Dispatchcard;

    /**
     * @var \Numa\CCCAdminBundle\Entity\Probills
     */
    private $Probill;

    /**
     * @var \Numa\CCCAdminBundle\Entity\Drivers
     */
    private $Driver;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set driverId
     *
     * @param integer $driverId
     *
     * @return Trip
     */
    public function setDriverId($driverId)
    {
        $this->driver_id = $driverId;

        return $this;
    }

    /**
     * Get driverId
     *
     * @return integer
     */
    public function getDriverId()
    {
        return $this->driver_id;
    }

    /**
     * Set pickup
     *
     * @param string $pickup
     *
     * @return Trip
     */
    public function setPickup($pickup)
    {
        $this->pickup = $pickup;

        return $this;
    }

    /**
     * Get pickup
     *
     * @return string
     */
    public function getPickup()
    {
        return $this->pickup;
    }

    /**
     * Set delivery
     *
     * @param string $delivery
     *
     * @return Trip
     */
    public function setDelivery($delivery)
    {
        $this->delivery = $delivery;

        return $this;
    }

    /**
     * Get delivery
     *
     * @return string
     */
    public function getDelivery()
    {
        return $this->delivery;
    }

    /**
     * Set comments
     *
     * @param string $comments
     *
     * @return Trip
     */
    public function setComments($comments)
    {
        $this->comments = $comments;

        return $this;
    }

    /**
     * Get comments
     *
     * @return string
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Set dispatchcard
     *
     * @param \Numa\CCCAdminBundle\Entity\Dispatchcard $dispatchcard
     *
     * @return Trip
     */
    public function setDispatchcard(\Numa\CCCAdminBundle\Entity\Dispatchcard $dispatchcard = null)
    {
        $this->Dispatchcard = $dispatchcard;

        return $this;
    }

    /**
     * Get dispatchcard
     *
     * @return \Numa\CCCAdminBundle\Entity\Dispatchcard
     */
    public function getDispatchcard()
    {
        return $this->Dispatchcard;
    }

    /**
     * Set probill
     *
     * @param \Numa\CCCAdminBundle\Entity\Probills $probill
     *
     * @return Trip
     */
    public function setProbill(\Numa\CCCAdminBundle\Entity\Probills $probill = null)
    {
        $this->Probill = $probill;

        return $this;
    }

    /**
     * Get probill
     *
     * @return \Numa\CCCAdminBundle\Entity\Probills
     */
    public function getProbill()
    {
        return $this->Probill;
    }

    /**
     * Set driver
     *
     * @param \Numa\CCCAdminBundle\Entity\Drivers $driver
     *
     * @return Trip
     */
    public function setDriver(\Numa\CCCAdminBundle\Entity\Drivers $driver = null)
    {
        $this->Driver = $driver;

        return $this;
    }

    /**
     * Get driver
     *
     * @return \Numa\CCCAdminBundle\Entity\Drivers
     */
    public function getDriver()
    {
        return $this->Driver;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        $this->date_created = new \DateTime();
        $this->date_updated = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function setUpdatedAtValue()
    {
        $this->date_updated = new \DateTime();
    }
    /**
     * @var \DateTime
     */
    private $date_created;

    /**
     * @var \DateTime
     */
    private $date_updated;


    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     *
     * @return Trip
     */
    public function setDateCreated($dateCreated)
    {
        $this->date_created = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * Set dateUpdated
     *
     * @param \DateTime $dateUpdated
     *
     * @return Trip
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->date_updated = $dateUpdated;

        return $this;
    }

    /**
     * Get dateUpdated
     *
     * @return \DateTime
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }
    /**
     * @var integer
     */
    private $user_id;

    /**
     * @var \Numa\CCCAdminBundle\Entity\User
     */
    private $User;


    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return Trip
     */
    public function setUserId($userId)
    {
        $this->user_id = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set user
     *
     * @param \Numa\CCCAdminBundle\Entity\User $user
     *
     * @return Trip
     */
    public function setUser(\Numa\CCCAdminBundle\Entity\User $user = null)
    {
        $this->User = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Numa\CCCAdminBundle\Entity\User
     */
    public function getUser()
    {
        return $this->User;
    }
    /**
     * @var string
     */
    private $waybill;

    /**
     * @var string
     */
    private $PCE;

    /**
     * @var string
     */
    private $WGT;

    /**
     * @var integer
     */
    private $vehtype_id;

    /**
     * @var string
     */
    private $signature;


    /**
     * Set waybill
     *
     * @param string $waybill
     *
     * @return Trip
     */
    public function setWaybill($waybill)
    {
        $this->waybill = $waybill;

        return $this;
    }

    /**
     * Get waybill
     *
     * @return string
     */
    public function getWaybill()
    {
        return $this->waybill;
    }

    /**
     * Set pCE
     *
     * @param string $pCE
     *
     * @return Trip
     */
    public function setPCE($pCE)
    {
        $this->PCE = $pCE;

        return $this;
    }

    /**
     * Get pCE
     *
     * @return string
     */
    public function getPCE()
    {
        return $this->PCE;
    }

    /**
     * Set wGT
     *
     * @param string $wGT
     *
     * @return Trip
     */
    public function setWGT($wGT)
    {
        $this->WGT = $wGT;

        return $this;
    }

    /**
     * Get wGT
     *
     * @return string
     */
    public function getWGT()
    {
        return $this->WGT;
    }

    /**
     * Set vehtypeId
     *
     * @param integer $vehtypeId
     *
     * @return Trip
     */
    public function setVehtypeId($vehtypeId)
    {
        $this->vehtype_id = $vehtypeId;

        return $this;
    }

    /**
     * Get vehtypeId
     *
     * @return integer
     */
    public function getVehtypeId()
    {
        return $this->vehtype_id;
    }

    /**
     * Set signature
     *
     * @param string $signature
     *
     * @return Trip
     */
    public function setSignature($signature)
    {
        $this->signature = $signature;

        return $this;
    }

    /**
     * Get signature
     *
     * @return string
     */
    public function getSignature()
    {
        return $this->signature;
    }
    /**
     * @var integer
     */
    private $pickupHr;

    /**
     * @var integer
     */
    private $pickup_mn;

    /**
     * @var integer
     */
    private $delivery_hr;

    /**
     * @var integer
     */
    private $delivery_mn;


    /**
     * Set pickupHr
     *
     * @param integer $pickupHr
     *
     * @return Trip
     */
    public function setPickupHr($pickupHr)
    {
        $this->pickupHr = $pickupHr;

        return $this;
    }

    /**
     * Get pickupHr
     *
     * @return integer
     */
    public function getPickupHr()
    {
        return $this->pickupHr;
    }

    /**
     * Set pickupMn
     *
     * @param integer $pickupMn
     *
     * @return Trip
     */
    public function setPickupMn($pickupMn)
    {
        $this->pickup_mn = $pickupMn;

        return $this;
    }

    /**
     * Get pickupMn
     *
     * @return integer
     */
    public function getPickupMn()
    {
        return $this->pickup_mn;
    }

    /**
     * Set deliveryHr
     *
     * @param integer $deliveryHr
     *
     * @return Trip
     */
    public function setDeliveryHr($deliveryHr)
    {
        $this->delivery_hr = $deliveryHr;

        return $this;
    }

    /**
     * Get deliveryHr
     *
     * @return integer
     */
    public function getDeliveryHr()
    {
        return $this->delivery_hr;
    }

    /**
     * Set deliveryMn
     *
     * @param integer $deliveryMn
     *
     * @return Trip
     */
    public function setDeliveryMn($deliveryMn)
    {
        $this->delivery_mn = $deliveryMn;

        return $this;
    }

    /**
     * Get deliveryMn
     *
     * @return integer
     */
    public function getDeliveryMn()
    {
        return $this->delivery_mn;
    }
    /**
     * @var integer
     */
    private $ttime_hr;

    /**
     * @var integer
     */
    private $ttime_mn;


    /**
     * Set ttimeHr
     *
     * @param integer $ttimeHr
     *
     * @return Trip
     */
    public function setTtimeHr($ttimeHr)
    {
        $this->ttime_hr = $ttimeHr;

        return $this;
    }

    /**
     * Get ttimeHr
     *
     * @return integer
     */
    public function getTtimeHr()
    {
        return $this->ttime_hr;
    }

    /**
     * Set ttimeMn
     *
     * @param integer $ttimeMn
     *
     * @return Trip
     */
    public function setTtimeMn($ttimeMn)
    {
        $this->ttime_mn = $ttimeMn;

        return $this;
    }

    /**
     * Get ttimeMn
     *
     * @return integer
     */
    public function getTtimeMn()
    {
        return $this->ttime_mn;
    }
    /**
     * @var integer
     */
    private $pickup_hr;


}
