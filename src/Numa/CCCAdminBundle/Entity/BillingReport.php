<?php

namespace Numa\CCCAdminBundle\Entity;

/**
 * BillingReport
 */
class BillingReport
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $billing_period_id;

    /**
     * @var integer
     */
    private $media_id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var integer
     */
    private $status = 1;

    /**
     * @var \Numa\CCCAdminBundle\Entity\Media
     */
    private $Media;

    /**
     * @var \Numa\CCCAdminBundle\Entity\BillingPeriod
     */
    private $BillingPeriod;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set billingPeriodId
     *
     * @param string $billingPeriodId
     *
     * @return BillingReport
     */
    public function setBillingPeriodId($billingPeriodId)
    {
        $this->billing_period_id = $billingPeriodId;

        return $this;
    }

    /**
     * Get billingPeriodId
     *
     * @return string
     */
    public function getBillingPeriodId()
    {
        return $this->billing_period_id;
    }

    /**
     * Set mediaId
     *
     * @param integer $mediaId
     *
     * @return BillingReport
     */
    public function setMediaId($mediaId)
    {
        $this->media_id = $mediaId;

        return $this;
    }

    /**
     * Get mediaId
     *
     * @return integer
     */
    public function getMediaId()
    {
        return $this->media_id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return BillingReport
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return BillingReport
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set media
     *
     * @param \Numa\CCCAdminBundle\Entity\Media $media
     *
     * @return BillingReport
     */
    public function setMedia(\Numa\CCCAdminBundle\Entity\Media $media = null)
    {
        $this->Media = $media;

        return $this;
    }

    /**
     * Get media
     *
     * @return \Numa\CCCAdminBundle\Entity\Media
     */
    public function getMedia()
    {
        return $this->Media;
    }

    /**
     * Set billingPeriod
     *
     * @param \Numa\CCCAdminBundle\Entity\BillingPeriod $billingPeriod
     *
     * @return BillingReport
     */
    public function setBillingPeriod(\Numa\CCCAdminBundle\Entity\BillingPeriod $billingPeriod = null)
    {
        $this->BillingPeriod = $billingPeriod;

        return $this;
    }

    /**
     * Get billingPeriod
     *
     * @return \Numa\CCCAdminBundle\Entity\BillingPeriod
     */
    public function getBillingPeriod()
    {
        return $this->BillingPeriod;
    }
}
