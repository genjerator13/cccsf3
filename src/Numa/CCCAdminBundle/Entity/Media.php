<?php

namespace Numa\CCCAdminBundle\Entity;

/**
 * Media
 */
class Media
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $content;

    /**
     * @var string
     */
    private $mimetype;

    /**
     * @var \Numa\CCCAdminBundle\Entity\Mediathek
     */
    private $mediathek;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Media
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Media
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set mimetype
     *
     * @param string $mimetype
     *
     * @return Media
     */
    public function setMimetype($mimetype)
    {
        $this->mimetype = $mimetype;

        return $this;
    }

    /**
     * Get mimetype
     *
     * @return string
     */
    public function getMimetype()
    {
        return $this->mimetype;
    }

    /**
     * Set mediathek
     *
     * @param \Numa\CCCAdminBundle\Entity\Mediathek $mediathek
     *
     * @return Media
     */
    public function setMediathek(\Numa\CCCAdminBundle\Entity\Mediathek $mediathek = null)
    {
        $this->mediathek = $mediathek;

        return $this;
    }

    /**
     * Get mediathek
     *
     * @return \Numa\CCCAdminBundle\Entity\Mediathek
     */
    public function getMediathek()
    {
        return $this->mediathek;
    }
    /**
     * @var \DateTime
     */
    private $date_created;


    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     *
     * @return Media
     */
    public function setDateCreated($dateCreated)
    {
        $this->date_created = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $Attachment;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->Attachment = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add attachment
     *
     * @param \Numa\CCCAdminBundle\Entity\Attachment $attachment
     *
     * @return Media
     */
    public function addAttachment(\Numa\CCCAdminBundle\Entity\Attachment $attachment)
    {
        $this->Attachment[] = $attachment;

        return $this;
    }

    /**
     * Remove attachment
     *
     * @param \Numa\CCCAdminBundle\Entity\Attachment $attachment
     */
    public function removeAttachment(\Numa\CCCAdminBundle\Entity\Attachment $attachment)
    {
        $this->Attachment->removeElement($attachment);
    }

    /**
     * Get attachment
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAttachment()
    {
        return $this->Attachment;
    }
    /**
     * @var string
     */
    private $origin;


    /**
     * Set origin
     *
     * @param string $origin
     *
     * @return Media
     */
    public function setOrigin($origin)
    {
        $this->origin = $origin;

        return $this;
    }

    /**
     * Get origin
     *
     * @return string
     */
    public function getOrigin()
    {
        return $this->origin;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        $this->date_created = new \DateTime();

    }
}
