<?php

namespace Numa\CCCAdminBundle\Entity;

/**
 * Preprint
 */
class Preprint
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $dept;

    /**
     * @var string
     */
    private $pickup;

    /**
     * @var float
     */
    private $delivery;

    /**
     * @var string
     */
    private $po;

    /**
     * @var \DateTime
     */
    private $date_created;

    /**
     * @var \DateTime
     */
    private $date_updated;

    /**
     * @var boolean
     */
    private $active = true;

    /**
     * @var integer
     */
    private $ccustomer_id;

    /**
     * @var \Numa\CCCAdminBundle\Entity\Customers
     */
    private $Customer;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dept
     *
     * @param string $dept
     *
     * @return Preprint
     */
    public function setDept($dept)
    {
        $this->dept = $dept;

        return $this;
    }

    /**
     * Get dept
     *
     * @return string
     */
    public function getDept()
    {
        return $this->dept;
    }

    /**
     * Set pickup
     *
     * @param string $pickup
     *
     * @return Preprint
     */
    public function setPickup($pickup)
    {
        $this->pickup = $pickup;

        return $this;
    }

    /**
     * Get pickup
     *
     * @return string
     */
    public function getPickup()
    {
        return $this->pickup;
    }

    /**
     * Set delivery
     *
     * @param float $delivery
     *
     * @return Preprint
     */
    public function setDelivery($delivery)
    {
        $this->delivery = $delivery;

        return $this;
    }

    /**
     * Get delivery
     *
     * @return float
     */
    public function getDelivery()
    {
        return $this->delivery;
    }

    /**
     * Set po
     *
     * @param string $po
     *
     * @return Preprint
     */
    public function setPo($po)
    {
        $this->po = $po;

        return $this;
    }

    /**
     * Get po
     *
     * @return string
     */
    public function getPo()
    {
        return $this->po;
    }

    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     *
     * @return Preprint
     */
    public function setDateCreated($dateCreated)
    {
        $this->date_created = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * Set dateUpdated
     *
     * @param \DateTime $dateUpdated
     *
     * @return Preprint
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->date_updated = $dateUpdated;

        return $this;
    }

    /**
     * Get dateUpdated
     *
     * @return \DateTime
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return Preprint
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set ccustomerId
     *
     * @param integer $ccustomerId
     *
     * @return Preprint
     */
    public function setCcustomerId($ccustomerId)
    {
        $this->ccustomer_id = $ccustomerId;

        return $this;
    }

    /**
     * Get ccustomerId
     *
     * @return integer
     */
    public function getCcustomerId()
    {
        return $this->ccustomer_id;
    }

    /**
     * Set customer
     *
     * @param \Numa\CCCAdminBundle\Entity\Customers $customer
     *
     * @return Preprint
     */
    public function setCustomer(\Numa\CCCAdminBundle\Entity\Customers $customer = null)
    {
        $this->Customer = $customer;

        return $this;
    }

    /**
     * Get customer
     *
     * @return \Numa\CCCAdminBundle\Entity\Customers
     */
    public function getCustomer()
    {
        return $this->Customer;
    }
    /**
     * @var integer
     */
    private $customer_id;


    /**
     * Set customerId
     *
     * @param integer $customerId
     *
     * @return Preprint
     */
    public function setCustomerId($customerId)
    {
        $this->customer_id = $customerId;

        return $this;
    }

    /**
     * Get customerId
     *
     * @return integer
     */
    public function getCustomerId()
    {
        return $this->customer_id;
    }
}
