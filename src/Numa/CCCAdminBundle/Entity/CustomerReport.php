<?php

namespace Numa\CCCAdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation as JMS;
/**
 * CustomerReport
 */
class CustomerReport
{
    use DateStampableTrait;
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $customer_id;

    /**
     * @var \Numa\CCCAdminBundle\Entity\Customers
     */
    private $Customers;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set customerId
     *
     * @param integer $customerId
     *
     * @return CustomerReport
     */
    public function setCustomerId($customerId)
    {
        $this->customer_id = $customerId;

        return $this;
    }

    /**
     * Get customerId
     *
     * @return integer
     */
    public function getCustomerId()
    {
        return $this->customer_id;
    }

    /**
     * Set customers
     *
     * @param \Numa\CCCAdminBundle\Entity\Customers $customers
     *
     * @return CustomerReport
     */
    public function setCustomers(\Numa\CCCAdminBundle\Entity\Customers $customers = null)
    {
        $this->Customers = $customers;

        return $this;
    }

    /**
     * Get customers
     *
     * @return \Numa\CCCAdminBundle\Entity\Customers
     */
    public function getCustomers()
    {
        return $this->Customers;
    }
    /**
     * @var integer
     */
    private $billing_period_id;
//
//    /**
//     * @var integer
//     */
//    private $delivery_report_id;
//
//    /**
//     * @var integer
//     */
//    private $invoice_report_id;

    /**
     * @var \Numa\CCCAdminBundle\Entity\BillingPeriod
     */
    private $BillingPeriod;

    /**
     * @var \Numa\CCCAdminBundle\Entity\Media
     */
    private $DeliveryReport;

    /**
     * @var \Numa\CCCAdminBundle\Entity\Media
     */
    private $InvoiceReport;


    /**
     * Set billingPeriodId
     *
     * @param integer $billingPeriodId
     *
     * @return CustomerReport
     */
    public function setBillingPeriodId($billingPeriodId)
    {
        $this->billing_period_id = $billingPeriodId;

        return $this;
    }

    /**
     * Get billingPeriodId
     *
     * @return integer
     */
    public function getBillingPeriodId()
    {
        return $this->billing_period_id;
    }

    /**
     * Set deliveryReportId
     *
     * @param integer $deliveryReportId
     *
     * @return CustomerReport
     */
    public function setDeliveryReportId($deliveryReportId)
    {
        $this->delivery_report_id = $deliveryReportId;

        return $this;
    }

    /**
     * Get deliveryReportId
     *
     * @return integer
     */
    public function getDeliveryReportId()
    {
        return $this->delivery_report_id;
    }

    /**
     * Set invoiceReportId
     *
     * @param integer $invoiceReportId
     *
     * @return CustomerReport
     */
    public function setInvoiceReportId($invoiceReportId)
    {
        $this->invoice_report_id = $invoiceReportId;

        return $this;
    }

    /**
     * Get invoiceReportId
     *
     * @return integer
     */
    public function getInvoiceReportId()
    {
        return $this->invoice_report_id;
    }

    /**
     * Set billingPeriod
     *
     * @param \Numa\CCCAdminBundle\Entity\BillingPeriod $billingPeriod
     *
     * @return CustomerReport
     */
    public function setBillingPeriod(\Numa\CCCAdminBundle\Entity\BillingPeriod $billingPeriod = null)
    {
        $this->BillingPeriod = $billingPeriod;

        return $this;
    }

    /**
     * Get billingPeriod
     *
     * @return \Numa\CCCAdminBundle\Entity\BillingPeriod
     */
    public function getBillingPeriod()
    {
        return $this->BillingPeriod;
    }

    /**
     * Set deliveryReport
     *
     * @param \Numa\CCCAdminBundle\Entity\Media $deliveryReport
     *
     * @return CustomerReport
     */
    public function setDeliveryReport(\Numa\CCCAdminBundle\Entity\Media $deliveryReport = null)
    {
        $this->DeliveryReport = $deliveryReport;

        return $this;
    }

    /**
     * Get deliveryReport
     *
     * @return \Numa\CCCAdminBundle\Entity\Media
     */
    public function getDeliveryReport()
    {
        return $this->DeliveryReport;
    }

    /**
     * Set invoiceReport
     *
     * @param \Numa\CCCAdminBundle\Entity\Media $invoiceReport
     *
     * @return CustomerReport
     */
    public function setInvoiceReport(\Numa\CCCAdminBundle\Entity\Media $invoiceReport = null)
    {
        $this->InvoiceReport = $invoiceReport;

        return $this;
    }

    /**
     * Get invoiceReport
     *
     * @return \Numa\CCCAdminBundle\Entity\Media
     */
    public function getInvoiceReport()
    {
        return $this->InvoiceReport;
    }
//    /**
//     * @var integer
//     */
//    private $delivery_report_pdf_id;
//
//    /**
//     * @var integer
//     */
//    private $delivery_report_xls_id;
//
//    /**
//     * @var \Numa\CCCAdminBundle\Entity\Media
//     */
//    private $DeliveryReportPdf;
//
//    /**
//     * @var \Numa\CCCAdminBundle\Entity\Media
//     */
//    private $DeliveryReportXls;


//    /**
//     * Set deliveryReportPdfId
//     *
//     * @param integer $deliveryReportPdfId
//     *
//     * @return CustomerReport
//     */
//    public function setDeliveryReportPdfId($deliveryReportPdfId)
//    {
//        $this->delivery_report_pdf_id = $deliveryReportPdfId;
//
//        return $this;
//    }
//
//    /**
//     * Get deliveryReportPdfId
//     *
//     * @return integer
//     */
//    public function getDeliveryReportPdfId()
//    {
//        return $this->delivery_report_pdf_id;
//    }
//
//    /**
//     * Set deliveryReportXlsId
//     *
//     * @param integer $deliveryReportXlsId
//     *
//     * @return CustomerReport
//     */
//    public function setDeliveryReportXlsId($deliveryReportXlsId)
//    {
//        $this->delivery_report_xls_id = $deliveryReportXlsId;
//
//        return $this;
//    }
//
//    /**
//     * Get deliveryReportXlsId
//     *
//     * @return integer
//     */
//    public function getDeliveryReportXlsId()
//    {
//        return $this->delivery_report_xls_id;
//    }
//
//    /**
//     * Set deliveryReportPdf
//     *
//     * @param \Numa\CCCAdminBundle\Entity\Media $deliveryReportPdf
//     *
//     * @return CustomerReport
//     */
//    public function setDeliveryReportPdf(\Numa\CCCAdminBundle\Entity\Media $deliveryReportPdf = null)
//    {
//        $this->DeliveryReportPdf = $deliveryReportPdf;
//
//        return $this;
//    }
//
//    /**
//     * Get deliveryReportPdf
//     *
//     * @return \Numa\CCCAdminBundle\Entity\Media
//     */
//    public function getDeliveryReportPdf()
//    {
//        return $this->DeliveryReportPdf;
//    }
//
//    /**
//     * Set deliveryReportXls
//     *
//     * @param \Numa\CCCAdminBundle\Entity\Media $deliveryReportXls
//     *
//     * @return CustomerReport
//     */
//    public function setDeliveryReportXls(\Numa\CCCAdminBundle\Entity\Media $deliveryReportXls = null)
//    {
//        $this->DeliveryReportXls = $deliveryReportXls;
//
//        return $this;
//    }
//
//    /**
//     * Get deliveryReportXls
//     *
//     * @return \Numa\CCCAdminBundle\Entity\Media
//     */
//    public function getDeliveryReportXls()
//    {
//        return $this->DeliveryReportXls;
//    }
    /**
     * @var \DateTime
     */
    private $date_created;

    /**
     * @var \DateTime
     */
    private $date_updated;


    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     *
     * @return CustomerReport
     */
    public function setDateCreated($dateCreated)
    {
        $this->date_created = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * Set dateUpdated
     *
     * @param \DateTime $dateUpdated
     *
     * @return CustomerReport
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->date_updated = $dateUpdated;

        return $this;
    }

    /**
     * Get dateUpdated
     *
     * @return \DateTime
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * @var integer
     */
    private $email_id;

    /**
     * @var \Numa\CCCAdminBundle\Entity\email
     */
    private $Email;


    /**
     * Set emailId
     *
     * @param integer $emailId
     *
     * @return CustomerReport
     */
    public function setEmailId($emailId)
    {
        $this->email_id = $emailId;

        return $this;
    }

    /**
     * Get emailId
     *
     * @return integer
     */
    public function getEmailId()
    {
        return $this->email_id;
    }

    /**
     * Set email
     *
     * @param \Numa\CCCAdminBundle\Entity\email $email
     *
     * @return CustomerReport
     */
    public function setEmail(\Numa\CCCAdminBundle\Entity\email $email = null)
    {
        $this->Email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return \Numa\CCCAdminBundle\Entity\email
     */
    public function getEmail()
    {
        return $this->Email;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $Emails;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->Emails = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add email
     *
     * @param \Numa\CCCAdminBundle\Entity\Email $email
     *
     * @return CustomerReport
     */
    public function addEmail(\Numa\CCCAdminBundle\Entity\Email $email)
    {
        $this->Emails[] = $email;

        return $this;
    }

    /**
     * Remove email
     *
     * @param \Numa\CCCAdminBundle\Entity\Email $email
     */
    public function removeEmail(\Numa\CCCAdminBundle\Entity\Email $email)
    {
        $this->Emails->removeElement($email);
    }

    /**
     * Get emails
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEmails()
    {
        return $this->Emails;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $CustomerReportEmail;


    /**
     * Add customerReportEmail
     *
     * @param \Numa\CCCAdminBundle\Entity\CustomerReportEmail $customerReportEmail
     *
     * @return CustomerReport
     */
    public function addCustomerReportEmail(\Numa\CCCAdminBundle\Entity\CustomerReportEmail $customerReportEmail)
    {
        $this->CustomerReportEmail[] = $customerReportEmail;

        return $this;
    }

    /**
     * Remove customerReportEmail
     *
     * @param \Numa\CCCAdminBundle\Entity\CustomerReportEmail $customerReportEmail
     */
    public function removeCustomerReportEmail(\Numa\CCCAdminBundle\Entity\CustomerReportEmail $customerReportEmail)
    {
        $this->CustomerReportEmail->removeElement($customerReportEmail);
    }

    /**
     * Get customerReportEmail
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCustomerReportEmail()
    {
        return $this->CustomerReportEmail;
    }
}
