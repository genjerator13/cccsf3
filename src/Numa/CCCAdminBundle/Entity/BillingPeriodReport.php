<?php

namespace Numa\CCCAdminBundle\Entity;

/**
 * BillingPeriodReport
 */
class BillingPeriodReport
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $billiing_id;

    /**
     * @var \DateTime
     */
    private $media_id;

    /**
     * @var integer
     */
    private $status = 1;

    /**
     * @var \Numa\CCCAdminBundle\Entity\Media
     */
    private $Media;

    /**
     * @var \Numa\CCCAdminBundle\Entity\BillingPeriod
     */
    private $BillingPeriod;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set billiingId
     *
     * @param string $billiingId
     *
     * @return BillingPeriodReport
     */
    public function setBilliingId($billiingId)
    {
        $this->billiing_id = $billiingId;

        return $this;
    }

    /**
     * Get billiingId
     *
     * @return string
     */
    public function getBilliingId()
    {
        return $this->billiing_id;
    }

    /**
     * Set mediaId
     *
     * @param \DateTime $mediaId
     *
     * @return BillingPeriodReport
     */
    public function setMediaId($mediaId)
    {
        $this->media_id = $mediaId;

        return $this;
    }

    /**
     * Get mediaId
     *
     * @return \DateTime
     */
    public function getMediaId()
    {
        return $this->media_id;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return BillingPeriodReport
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set media
     *
     * @param \Numa\CCCAdminBundle\Entity\Media $media
     *
     * @return BillingPeriodReport
     */
    public function setMedia(\Numa\CCCAdminBundle\Entity\Media $media = null)
    {
        $this->Media = $media;

        return $this;
    }

    /**
     * Get media
     *
     * @return \Numa\CCCAdminBundle\Entity\Media
     */
    public function getMedia()
    {
        return $this->Media;
    }

    /**
     * Set billingPeriod
     *
     * @param \Numa\CCCAdminBundle\Entity\BillingPeriod $billingPeriod
     *
     * @return BillingPeriodReport
     */
    public function setBillingPeriod(\Numa\CCCAdminBundle\Entity\BillingPeriod $billingPeriod = null)
    {
        $this->BillingPeriod = $billingPeriod;

        return $this;
    }

    /**
     * Get billingPeriod
     *
     * @return \Numa\CCCAdminBundle\Entity\BillingPeriod
     */
    public function getBillingPeriod()
    {
        return $this->BillingPeriod;
    }
    /**
     * @var string
     */
    private $billing_period_id;


    /**
     * Set billingPeriodId
     *
     * @param string $billingPeriodId
     *
     * @return BillingPeriodReport
     */
    public function setBillingPeriodId($billingPeriodId)
    {
        $this->billing_period_id = $billingPeriodId;

        return $this;
    }

    /**
     * Get billingPeriodId
     *
     * @return string
     */
    public function getBillingPeriodId()
    {
        return $this->billing_period_id;
    }
    /**
     * @var string
     */
    private $name;


    /**
     * Set name
     *
     * @param string $name
     *
     * @return BillingPeriodReport
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
