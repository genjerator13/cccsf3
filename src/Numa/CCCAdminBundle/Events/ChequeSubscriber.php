<?php

namespace Numa\CCCAdminBundle\Events;

use Numa\CCCAdminBundle\Entity\Drivers;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Validator\Tests\Fixtures\Entity;

class ChequeSubscriber implements EventSubscriberInterface
{


    protected $container;
    protected $frame;

    public function __construct($container, $frame)
    {

        $this->container = $container;
        $this->frame = intval($frame);


    }

    public static function getSubscribedEvents()
    {
        // Tells the dispatcher that you want to listen on the form.pre_set_data
        // event and that the preSetData method should be called.
        return array(FormEvents::PRE_SET_DATA => 'preSetData');
    }


    /**
     * @param FormEvent $event
     * Based by the type of the each field in the form (item)
     * Fills the select  or other fields
     */
    public function preSetData(FormEvent $event)
    {
        $cheque = $event->getData();
        $form = $event->getForm();
        $form->add('Driver', EntityType::class,
            array('choice_name' => 'displayName',
                'class' => Drivers::class,
                'choice_label' => "displayName",
                //'placeholder' => 'Choose a Billing Period',
                'query_builder' => function ($lr) {
                    // echo get_class($lr);
                    return $lr->createQueryBuilder('d')
                        ->addOrderBy("d.drivernum","ASC")
                        ->andWhere("d.frame=".$this->frame);
                },
            )
        );


    }

}
