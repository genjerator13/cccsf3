<?php

namespace Numa\CCCAdminBundle\Events;

use Doctrine\DBAL\Types\StringType;
use Numa\CCCAdminBundle\Entity\Drivers;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Validator\Tests\Fixtures\Entity;

class SettingSubscriber implements EventSubscriberInterface
{


    protected $container;
    protected $frame;

    public function __construct()
    {

    }

    public static function getSubscribedEvents()
    {
        // Tells the dispatcher that you want to listen on the form.pre_set_data
        // event and that the preSetData method should be called.
        return array(FormEvents::PRE_SET_DATA => 'preSetData');
    }


    /**
     * @param FormEvent $event
     * Based by the type of the each field in the form (item)
     * Fills the select  or other fields
     */
    public function preSetData(FormEvent $event)
    {
        $entity = $event->getData();
        $form = $event->getForm();
        $form->add('value',null,array('label' => $entity->getName()));
        if($entity->getName()=="email_subject"){
            $form->add('value',TextType::class,array('label' => $entity->getName()));
        }

    }

}
