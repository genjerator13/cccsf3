<?php

namespace Numa\CCCAdminBundle\Events;

use Doctrine\DBAL\Types\StringType;
use Numa\CCCAdminBundle\Entity\Drivers;
use Numa\CCCAdminBundle\Entity\UserGroup;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Validator\Tests\Fixtures\Entity;

class UserSubscriber implements EventSubscriberInterface
{


    protected $container;
    protected $frame;

    public function __construct()
    {

    }

    public static function getSubscribedEvents()
    {
        // Tells the dispatcher that you want to listen on the form.pre_set_data
        // event and that the preSetData method should be called.
        return array(FormEvents::PRE_SET_DATA => 'preSetData');
    }


    /**
     * @param FormEvent $event
     * Based by the type of the each field in the form (item)
     * Fills the select  or other fields
     */
    public function preSetData(FormEvent $event)
    {
        $entity = $event->getData();
        $form = $event->getForm();

        if($entity->getUserGroup() instanceof UserGroup && $entity->getUserGroup()->getName()=='DISPATCH'){
            $form->add('chanel',ChoiceType::class,array('label'=>'Chanel','choices'=>array("No Chanel"=>0,"Chanel 1"=>1,"Chanel 2"=>2,"Chanel 3"=>3,"Chanel 4"=>4)))
            ;
        }

    }

}
